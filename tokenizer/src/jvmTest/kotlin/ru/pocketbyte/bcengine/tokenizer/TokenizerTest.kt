package ru.pocketbyte.bcengine.tokenizer

import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class TokenizerTest {

    @Test
    fun testComment() {
        val text = "\n# Comment\n"
        val tokens = Tokenizer.tokenize(text, 0, text.length - 1)

        assertContentEquals(
            listOf(
                CommentToken(text, 1, 10)
            ),
            tokens
        )
    }
    @Test
    fun testGroups() {
        val text =
            "SQUARE[project]\n" +
            "ROUND (empty, 1, )\n" +
            "CURLY{ 123 }\n" +
            "\$PROP1(param1, param2)"
        val tokens = Tokenizer.tokenize(text, 0, text.length - 1)

        assertEquals(
            listOf(
                NamedGroupToken(
                    "SQUARE",
                    listOf(
                        word("project", 7, text)
                    ),
                    NamedGroupToken.Bracers.SQUARE,
                    text, 0, 14
                ),
                NamedGroupToken(
                    "ROUND",
                    listOf(
                        word("empty", 23, text), coma(28, text),
                        word("1", 30, text), coma(31, text),
                        empty(32, 1, text)
                    ),
                    NamedGroupToken.Bracers.ROUND,
                    text, 16, 33
                ),
                NamedGroupToken(
                    "CURLY",
                    listOf(
                        WordToken("123", text, 42, 44)
                    ),
                    NamedGroupToken.Bracers.CURLY,
                    text, 35, 46
                ),
                NamedGroupToken(
                    "\$PROP1",
                    listOf(
                        word("param1", 55, text), coma(61, text),
                        word("param2", 63, text)
                    ),
                    NamedGroupToken.Bracers.ROUND,
                    text, 48, 69
                ),
            ),
            tokens
        )
    }

    @Test
    fun testProperty() {
        val text =
            "property1= \"value1\"\n" +
            "property2 = value2\n"

        val tokens = Tokenizer.tokenize(text, 0, text.length - 1)
        assertEquals(
            listOf(
                NamedPropertyToken("property1", text, 0, 9),
                StringToken("value1", text, 11, 18),
                NamedPropertyToken("property2", text, 20, 30),
                WordToken("value2", text, 32, 37),
            ),
            tokens
        )
    }

    @Test
    fun testCurlyGroupContent() {
        val text =
            "LAYOUT(0%, 0%) {\n" +
            "  RECT(0%, 0%, 10%, 10%)\n" +
            "}"
        val tokens = Tokenizer.tokenize(text, 0, text.length - 1)
        assertEquals(
            listOf(
                NamedGroupToken(
                    "LAYOUT",
                    listOf(
                        word("0%", 7, text), coma(9, text),
                        word("0%", 11, text)
                    ),
                    NamedGroupToken.Bracers.ROUND,
                    text, 0, 13
                ),
                NamedGroupToken(
                    "",
                    listOf(
                        NamedGroupToken(
                            "RECT",
                            listOf(
                                word("0%", 24, text), coma(26, text),
                                word("0%", 28, text), coma(30, text),
                                word("10%", 32, text), coma(35, text),
                                word("10%", 37, text)
                            ),
                            NamedGroupToken.Bracers.ROUND,
                            text, 19, 40
                        ),
                    ),
                    NamedGroupToken.Bracers.CURLY,
                    text, 15, 42
                ),
            ),
            tokens
        )
    }

    @Test
    fun testIndent() {
        val text =
            "if(\n" +
            "  isEmpty(\$TEXT);\n" +
            "  \"value1\";\n" +
            "  \"value2\" + \$TEXT\n" +
            ")"

        val tokens = Tokenizer.tokenize(text)
        assertEquals(
            listOf(
                NamedGroupToken("if", listOf(
                    NamedGroupToken("isEmpty", listOf(
                        WordToken("\$TEXT", text, 14, 18)
                    ), NamedGroupToken.Bracers.ROUND, text, 6, 19),
                    SeparatorToken(SeparatorToken.Type.SEMICOLON, text, 20, 20),
                    StringToken("value1", text, 24, 31),
                    SeparatorToken(SeparatorToken.Type.SEMICOLON, text, 32, 32),
                    StringToken("value2", text, 36, 43),
                    OperatorToken(OperatorToken.Type.PLUS, text, 45, 45),
                    WordToken("\$TEXT", text, 47, 51)
                ), NamedGroupToken.Bracers.ROUND, text, 0, 53)
            ),
            tokens
        )
    }

    private fun word(value: String, startIndex: Int, parentString: String): WordToken {
        return WordToken(value, parentString, startIndex, startIndex + value.length - 1)
    }
    private fun string(value: String, startIndex: Int, endIndex: Int, parentString: String): StringToken {
        return StringToken(value, parentString, startIndex, endIndex)
    }

    private fun empty(index: Int, length: Int, parentString: String): EmptyStringToken {
        return EmptyStringToken(parentString, index, index + length - 1)
    }

    private fun coma(index: Int, parentString: String): SeparatorToken {
        return SeparatorToken(SeparatorToken.Type.COMMA, parentString, index, index)
    }

    private fun semicolon(index: Int, parentString: String): SeparatorToken {
        return SeparatorToken(SeparatorToken.Type.SEMICOLON, parentString, index, index)
    }


}