package ru.pocketbyte.bcengine.tokenizer

import ru.pocketbyte.bcengine.tokenizer.OperatorToken.Type
import kotlin.test.Test
import kotlin.test.assertEquals

class FormulaTokenizerTest {

    @Test
    fun test1() {
        val tokenizer = Tokenizer

        val string = "STR(2 - 3.5 /\$a) + STR(false & true) + \"a\\\"aa\""

        assertEquals(
            listOf(
                NamedGroupToken(
                    "STR",
                    listOf(
                        WordToken("2", string, 4, 4),
                        OperatorToken(Type.MINUS, string, 6, 6),
                        WordToken("3.5", string, 8, 10),
                        OperatorToken(Type.DIVIDE, string, 12, 12),
                        WordToken("\$a", string, 13, 14)
                    ), NamedGroupToken.Bracers.ROUND,
                    string, 0, 15
                ),
                OperatorToken(Type.PLUS, string, 17, 17),
                NamedGroupToken(
                    "STR",
                    listOf(
                        WordToken("false", string, 23, 27),
                        OperatorToken(Type.AND, string, 29, 29),
                        WordToken("true", string, 31, 34)
                    ), NamedGroupToken.Bracers.ROUND,
                    string, 19, 35
                ),
                OperatorToken(Type.PLUS, string, 37, 37),
                StringToken("a\\\"aa", string, 39, 45)
            ),
            tokenizer.tokenize(string)
        )
    }

    @Test
    fun test2() {
        val tokenizer = Tokenizer

        val string = "MAX(3;4)<=2"
        assertEquals(
            listOf(
                NamedGroupToken("MAX",
                    listOf(
                        WordToken("3", string, 4, 4),
                        SeparatorToken(SeparatorToken.Type.SEMICOLON, string, 5, 5),
                        WordToken("4", string, 6, 6)
                    ), NamedGroupToken.Bracers.ROUND,
                    string, 0, 7
                ),
                OperatorToken(Type.LESS_OR_EQUAL, string, 8, 9),
                WordToken("2", string, 10, 10)
            ),
            tokenizer.tokenize(string)
        )
    }

    @Test
    fun test3() {
        val tokenizer = Tokenizer

        val string = "\"a\" + \$B"
        assertEquals(
            listOf(
                StringToken("a", string, 0, 2),
                OperatorToken(Type.PLUS, string, 4, 4),
                WordToken("\$B", string, 6, 7)
            ),
            tokenizer.tokenize(string, 0, string.length - 1)
        )
    }
}