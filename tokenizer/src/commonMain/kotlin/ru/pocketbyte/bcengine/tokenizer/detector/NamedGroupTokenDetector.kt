package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.*
import ru.pocketbyte.bcengine.tokenizer.exception.TokenizeException

class NamedGroupTokenDetector(
    private val tokenizer: Tokenizer
) : TokenDetector<NamedGroupToken> {
    override fun detect(string: String, startIndex: Int, endIndex: Int): NamedGroupToken? {
        var groupBracers = NamedGroupToken.Bracers.ROUND
        val subItems = mutableListOf<ProjectToken>()
        var index = startIndex

        // Skip Whitespace
        while (index <= endIndex && (string[index].isWhitespace() || string[index] == '\n')) {
            index += 1
        }
        val nameStart = index
        var nameEnd = -1

        // Look up for a group name
        while (index <= endIndex && nameEnd == -1) {
            var char = string[index]
            if (char.isNameChar()) {
                index += 1
                continue
            } else {
                while (index < endIndex && char.isWhitespace()) {
                    index += 1
                    char = string.getOrNull(index) ?: return null
                }
                if(char.isOpenBrace()) {
                    nameEnd = index
                    groupBracers = NamedGroupToken.Bracers.findByOpenChar(char)
                        ?: groupBracers
                } else if(char.isCloseBrace()) {
                    throw TokenizeException(
                        string, nameStart,
                        "Missing opening brace at $index: $string"
                    )
                } else {
                    return null
                }
            }
        }

        val name = if (nameEnd == -1) {
            return null
        } else {
            string.substring(nameStart, nameEnd).trim()
        }

        // Looking fro the group content
        val openingBraceIndex = index
        val bracersStack = mutableListOf(groupBracers)
        var isQuote = false
        var parameterStartIndex = index + 1
        while (bracersStack.isNotEmpty()) {
            index += 1
            if (index > endIndex) {
                throw TokenizeException(
                    string, openingBraceIndex,
                    "Missing closing brace"
                )
            }
            val char = string[index]
            when {
                char.isOpenBrace() -> {
                    if (isQuote.not()) {
                        NamedGroupToken.Bracers.findByOpenChar(char)?.let {
                            bracersStack.add(it)
                        }
                    }
                }
                char.isCloseBrace() -> {
                    if (isQuote.not()) {
                        NamedGroupToken.Bracers.findByCloseChar(char)?.let {
                            if (it != bracersStack.lastOrNull()) {
                                throw TokenizeException(
                                    string, index,
                                    "Missing opening brace at $index"
                                )
                            }
                            bracersStack.removeLast()
                        }
                    }
                }
                char == '"' -> {
                    if (string[index - 1] != '\\') {
                        isQuote = !isQuote
                    }
                }
                char.isSeparator() -> {
                    if (isQuote.not() && bracersStack.size == 1) {
                        subItems.addAll(
                            tokenizeSubItem(string, parameterStartIndex, index - 1)
                        )
                        SeparatorToken.Type.values().firstOrNull { it.char == char }?.let {
                            subItems.add(SeparatorToken(it, string, index, index))
                        }
                        parameterStartIndex = index + 1
                    }
                }
            }
        }

        subItems.addAll(
            tokenizeSubItem(string, parameterStartIndex, index - 1)
        )

        return NamedGroupToken(
            name,
            subItems,
            groupBracers, string, nameStart, index
        )
    }

    private fun tokenizeSubItem(
        string: String, start: Int, end: Int
    ): List<ProjectToken> {
        return try {
            tokenizer.tokenize(
                string, start, end
            )
        } catch (e: TokenizeException) {
            return listOf(StringToken(string.substring(start, end + 1), string, start, end))
        }
    }

    private fun Char.isOpenBrace(): Boolean {
        NamedGroupToken.Bracers.values().forEach {
            if (it.openChar == this) {
                return true
            }
        }
        return false
    }

    private fun Char.isCloseBrace(): Boolean {
        NamedGroupToken.Bracers.values().forEach {
            if (it.closeChar == this) {
                return true
            }
        }
        return false
    }

    private fun Char.isSeparator(): Boolean {
        SeparatorToken.Type.values().forEach {
            if (it.char == this) {
                return true
            }
        }
        return false
    }

    private fun Char.isNameChar(): Boolean {
        return isLetterOrDigit() ||  listOf('_', '$').contains(this)
    }
}