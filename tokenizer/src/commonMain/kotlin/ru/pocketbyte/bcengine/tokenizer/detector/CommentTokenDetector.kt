package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.CommentToken
import kotlin.math.min

object CommentTokenDetector: TokenDetector<CommentToken> {

    private const val COMMENT_CHAR = '#'

    override fun detect(string: String, startIndex: Int, endIndex: Int): CommentToken? {
        if (!isNewLine(string, startIndex)) {
            return null
        }
        var index = startIndex
        while (index < endIndex && string[index].isWhitespace() && string[index] != '\n') {
            index += 1
        }
        if (string[index] == COMMENT_CHAR) {
            val rowEnd = string.indexOf('\n', startIndex).let {
                if (it == -1 ) { endIndex } else { it }
            }
            return CommentToken(string, index, min(rowEnd, endIndex))
        }
        return null
    }

    private fun isNewLine(string: String, startIndex: Int): Boolean {
        var index = startIndex - 1
        while (true) {
            if (index <= 0) {
                return true
            }
            if (string[index] == '\n') {
                return true
            }
            if (!string[index].isWhitespace()) {
                return false
            }
            index--
        }
    }
}