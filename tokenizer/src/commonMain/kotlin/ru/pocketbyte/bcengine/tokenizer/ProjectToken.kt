package ru.pocketbyte.bcengine.tokenizer

import ru.pocketbyte.bcengine.token.AbsToken
import ru.pocketbyte.bcengine.token.Token

sealed interface ProjectToken: Token

data class CommentToken(
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {
    override fun toString(): String {
        return "${this.javaClass.simpleName}($startIndex;$endIndex)"
    }
}

data class EmptyStringToken(
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {
    override fun toString(): String {
        return "${this.javaClass.simpleName}($startIndex;$endIndex)"
    }
}

data class NamedGroupToken(
    val name: String,
    val subTokens: List<ProjectToken>,
    val bracers: Bracers,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {
    enum class Bracers(
        val openChar: Char,
        val closeChar: Char
    ) {
        ROUND('(', ')'),
        SQUARE('[', ']'),
        CURLY('{', '}');

        companion object {
            fun findByOpenChar(char: Char): Bracers? {
                values().forEach {
                    if (it.openChar == char) {
                        return it
                    }
                }
                return null
            }

            fun findByCloseChar(char: Char): Bracers? {
                values().forEach {
                    if (it.closeChar == char) {
                        return it
                    }
                }
                return null
            }
        }
    }

    override fun toString(): String {
        return "${this.javaClass.simpleName}($name;$bracers;$startIndex;$endIndex;[${subTokens.joinToString()}])"
    }
}

data class NamedPropertyToken(
    val name: String,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {

    override fun toString(): String {
        return "${this.javaClass.simpleName}($name;$startIndex;$endIndex)"
    }
}

data class SeparatorToken(
    val type: Type,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {

    enum class Type(
        val char: Char
    ) {
        COMMA(','),
        SEMICOLON(';')
    }

    override fun toString(): String {
        return "${this.javaClass.simpleName}($type;$startIndex;$endIndex)"
    }
}

data class StringToken(
    val value: String,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {
    override fun toString(): String {
        return "${this.javaClass.simpleName}($value;$startIndex;$endIndex)"
    }
}

data class WordToken(
    val value: String,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {
    override fun toString(): String {
        return "${this.javaClass.simpleName}($value;$startIndex;$endIndex)"
    }
}

data class OperatorToken(
    val type: Type,
    override val parentString: String,
    override val startIndex: Int,
    override val endIndex: Int
) : AbsToken(), ProjectToken {

    enum class Type(
        val string: String,
        val priority: Int = 1
    ) {
        PLUS("+", priority = 1),
        MINUS("-", priority = 1),
        MULTIPLY("*", priority = 2),
        DIVIDE("/", priority = 2),

        AND("&", priority = 0),
        OR("|", priority = 0),

        EQUAL("==", priority = 0),
        GREATER_OR_EQUAL(">=", priority = 0),
        GREATER(">", priority = 0),
        LESS_OR_EQUAL("<=", priority = 0),
        LESS("<", priority = 0)
        ;

        val isLetterType = string.firstOrNull { it.isLetter() } != null
    }

    override fun toString(): String {
        return "${this.javaClass.simpleName}($type;$startIndex;$endIndex)"
    }
}