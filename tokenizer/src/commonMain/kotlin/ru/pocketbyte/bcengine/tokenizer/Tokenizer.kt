package ru.pocketbyte.bcengine.tokenizer

import ru.pocketbyte.bcengine.tokenizer.detector.*
import ru.pocketbyte.bcengine.tokenizer.exception.UnknownFormulaException

object Tokenizer {

    private val detector: TokenDetector<ProjectToken> = object : TokenDetector<ProjectToken> {

        private val detectors = listOf<TokenDetector<ProjectToken>>(
            CommentTokenDetector,
            EmptyStringTokenDetector,
            OperatorTokenDetector,
            StringTokenDetector,
            NamedPropertyTokenDetector,
            NamedGroupTokenDetector(this@Tokenizer),
            WordTokenDetector
        )

        override fun detect(string: String, startIndex: Int, endIndex: Int): ProjectToken? {
            detectors.forEach { detector ->
                detector.detect(string, startIndex, endIndex)?.let { return it }
            }
            return null
        }
    }

    fun tokenize(
        string: String
    ): List<ProjectToken> {
        return tokenize(string, 0, string.length - 1)
    }

    fun tokenize(
        string: String,
        startIndex: Int,
        endIndex: Int
    ): List<ProjectToken> {
        val result = mutableListOf<ProjectToken>()
        var index = startIndex
        while (index <= endIndex) {
            if (string[index].isWhitespace()) {
                index += 1
                continue
            }
            detector.detect(string, index, endIndex)?.let {
                result.add(it)
                index = it.endIndex + 1
            } ?: throw UnknownFormulaException(
                string,
                index
            )
        }
        return if (result.isEmpty()) {
            listOf(EmptyStringToken(string, startIndex, endIndex))
        } else {
            result
        }
    }
}