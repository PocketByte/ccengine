package ru.pocketbyte.bcengine.tokenizer.exception

open class TokenizeException(
    val string: String,
    val index: Int,
    message: String
): Throwable(message)