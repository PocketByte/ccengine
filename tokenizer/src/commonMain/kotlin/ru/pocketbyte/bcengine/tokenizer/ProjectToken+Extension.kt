package ru.pocketbyte.bcengine.tokenizer

fun NamedGroupToken.subTokensToString(): String {
    val first = subTokens.firstOrNull() ?: return ""
    val last = subTokens.lastOrNull() ?: return ""

    return parentString.substring(first.startIndex, last.endIndex + 1)
}