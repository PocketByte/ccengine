package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.EmptyStringToken

object EmptyStringTokenDetector: TokenDetector<EmptyStringToken> {

    override fun detect(string: String, startIndex: Int, endIndex: Int): EmptyStringToken? {
        var index = startIndex
        while (index < endIndex && string[index].isWhitespace()) {
            index += 1
        }
        if (string[index] == '\n') {
            return EmptyStringToken(string, startIndex, endIndex)
        }
        return null
    }
}