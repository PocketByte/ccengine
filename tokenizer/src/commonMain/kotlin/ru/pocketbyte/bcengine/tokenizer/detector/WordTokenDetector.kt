package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.lookUpSubstring
import ru.pocketbyte.bcengine.tokenizer.OperatorToken
import ru.pocketbyte.bcengine.tokenizer.WordToken

object WordTokenDetector : TokenDetector<WordToken> {

    private val disallowedChars = listOf('"', '\'', ',', '(', ')', '{', '}', '[', ']') +
            OperatorToken.Type.values().mapNotNull {
                if (it.isLetterType) {
                    null
                } else {
                    it.string[0]
                }
            }

    override fun detect(string: String, startIndex: Int, endIndex: Int) : WordToken? {
        if (!string[startIndex].isWhitespace()) {
            val subString = string.lookUpSubstring(startIndex, endIndex) { _, char ->
                !char.isWhitespace() && !disallowedChars.contains(char)
            }
            if (subString.isNotEmpty()) {
                return WordToken(
                    subString, string, startIndex, startIndex + subString.length - 1
                )
            }
        }
        return null
    }
}