package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.NamedPropertyToken

object NamedPropertyTokenDetector : TokenDetector<NamedPropertyToken> {

    override fun detect(string: String, startIndex: Int, endIndex: Int): NamedPropertyToken? {
        var index = startIndex

        // Skip Whitespace
        while (index <= endIndex && (string[index].isWhitespace() || string[index] == '\n')) {
            index += 1
        }
        val nameStart = index
        var nameEnd = -1

        // Look up for a group name
        while (index <= endIndex && nameEnd == -1) {
            var char = string[index]
            if (char.isNameChar()) {
                index += 1
                continue
            } else {
                while (char.isWhitespace()) {
                    index += 1
                    char = string.getOrNull(index) ?: return null
                }
                if(char == '=') {
                    nameEnd = index
                } else {
                    return null
                }
            }
        }
        val name = if (nameEnd == -1) {
            return null
        } else {
            string.substring(nameStart, nameEnd).trim()
        }

        return NamedPropertyToken(name, string, nameStart, nameEnd)
    }

    private fun Char.isNameChar(): Boolean {
        return isLetterOrDigit()
    }
}