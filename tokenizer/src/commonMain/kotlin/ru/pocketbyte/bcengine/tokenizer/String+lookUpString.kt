package ru.pocketbyte.bcengine.tokenizer

fun String.lookUpSubstring(
    startIndex: Int,
    endIndex: Int,
    condition: (previousChar: Char, char: Char) -> Boolean
): String {
    val builder = StringBuilder()
    var index = startIndex
    while (index <= endIndex && condition(if (index == 0) ' ' else this[index - 1], this[index])) {
        builder.append(this[index])
        index += 1
    }
    return builder.toString()
}