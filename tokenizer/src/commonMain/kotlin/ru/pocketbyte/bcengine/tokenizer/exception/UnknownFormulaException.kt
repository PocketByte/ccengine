package ru.pocketbyte.bcengine.tokenizer.exception

class UnknownFormulaException(
    string: String,
    index: Int,
): TokenizeException(string, index, "Unknown formula")