package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.lookUpSubstring
import ru.pocketbyte.bcengine.tokenizer.StringToken

object StringTokenDetector : TokenDetector<StringToken> {

    const val QUOTE_CHAR = '"'
    const val ESCAPE_CHAR = '\\'

    override fun detect(string: String, startIndex: Int, endIndex: Int) : StringToken? {
        if (string[startIndex] == QUOTE_CHAR) {
            val subString = string.lookUpSubstring(
                startIndex + 1,
                endIndex
            ) { previousChar, char ->
                char != QUOTE_CHAR || previousChar == ESCAPE_CHAR
            }
            return StringToken(
                subString, string, startIndex,
                startIndex + subString.length + 1
            )
        }
        return null
    }
}
