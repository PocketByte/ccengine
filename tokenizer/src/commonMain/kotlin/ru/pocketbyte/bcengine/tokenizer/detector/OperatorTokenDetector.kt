package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.tokenizer.OperatorToken

object OperatorTokenDetector : TokenDetector<OperatorToken> {
    override fun detect(string: String, startIndex: Int, endIndex: Int) : OperatorToken? {
        OperatorToken.Type.values().forEach {
            if (string.isMatch(it, startIndex, endIndex)) {
                return OperatorToken(
                    it, string, startIndex,
                    startIndex + it.string.length - 1
                )
            }
        }
        return null
    }

    private fun String.isMatch(
        expectedType: OperatorToken.Type,
        startIndex: Int, endIndex: Int
    ): Boolean {
        val operatorLength = expectedType.string.length
        if (regionMatches(
                startIndex, expectedType.string,
                0, operatorLength , false
            ).not()
        ) {
            return false
        }

        return if (expectedType.isLetterType) {
            val operatorEndIndex = startIndex + operatorLength
            (endIndex < operatorEndIndex || this[operatorEndIndex].isLetterOrDigit().not())
        } else {
            true
        }
    }
}