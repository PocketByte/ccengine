package ru.pocketbyte.bcengine.tokenizer.detector

import ru.pocketbyte.bcengine.token.Token

interface TokenDetector<out Type: Token> {
    fun detect(string: String, startIndex: Int, endIndex: Int) : Type?
}