plugins {
    kotlin("multiplatform")
}

kotlin {
    jvm()

    sourceSets {
        all {
            languageSettings.optIn("kotlin.RequiresOptIn")
        }

        val commonMain by getting {
            dependencies {
                implementation(project(":core"))
                implementation(project(":tokenizer"))
                implementation(project(":parser"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation(libsBce.locolaser.runtime)
                implementation(libsBce.kydra.log)
            }
        }

        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(libsBce.locolaser.runtimeJvm)
            }
        }
    }
}
