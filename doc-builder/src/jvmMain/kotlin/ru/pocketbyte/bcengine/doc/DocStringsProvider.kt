package ru.pocketbyte.bcengine.doc

import ru.pocketbyte.locolaser.provider.IndexFormattedStringProvider
import ru.pocketbyte.locolaser.provider.JvmBundleStringProvider
import java.util.Locale

actual fun buildDocStringsProvider() : IndexFormattedStringProvider {
    return JvmBundleStringProvider(
        BundleClassProvider::class.java.classLoader,
        bundleName = "doc_strings",
        locale = Locale.getDefault(),
        fallbackHandler = { _, _, key -> key }
    )
}

private class BundleClassProvider