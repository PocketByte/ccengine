package ru.pocketbyte.bcengine.doc.utils

internal fun StringBuilder.appendNewLine(): StringBuilder {
    return append("<br>")
}

internal fun StringBuilder.appendNewParagraph(): StringBuilder {
    return append("<font size=6><br> <br></font>")
}

internal fun StringBuilder.appendHeader(
    level: Int,
    bodyBuilder: StringBuilder.() -> Unit
): StringBuilder {
    append("<h$level>")
    bodyBuilder(this)
    append("</h$level>")
    return this
}

internal fun StringBuilder.appendBold(
    bodyBuilder: StringBuilder.() -> Unit
): StringBuilder {
    append("<b>")
    bodyBuilder(this)
    append("</b>")
    return this
}

internal fun StringBuilder.appendItalic(
    bodyBuilder: StringBuilder.() -> Unit
): StringBuilder {
    append("<i>")
    bodyBuilder(this)
    append("</i>")
    return this
}