package ru.pocketbyte.bcengine.doc.builder

import ru.pocketbyte.bcengine.doc.utils.appendHeader
import ru.pocketbyte.bcengine.doc.utils.appendNewParagraph
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.locolaser.provider.IndexFormattedStringProvider

class ParametrisedProcessorDocBuilder(
    private val stringProvider: IndexFormattedStringProvider,
    private val parameterDocGenerator: ParameterDocBuilder
) : DocBuilder<ParametrisedProcessor, Unit> {

    companion object {
        private const val KEY_PARAMETER_HEADER = "doc__parameter_header"
    }

    override fun build(entity: ParametrisedProcessor, parent: Unit, stringBuilder: StringBuilder) {
        stringBuilder
            .appendDescription(entity)

        if (entity.parameters.isNotEmpty()) {
            stringBuilder
                .appendNewParagraph()
                .append(stringProvider.getString(KEY_PARAMETER_HEADER))

            entity.parameters.forEach {
                stringBuilder
                    .appendNewParagraph()
                parameterDocGenerator.build(
                    it, entity, stringBuilder
                )
            }
        }
    }

    private fun StringBuilder.appendName(entity: ParametrisedProcessor): StringBuilder {
        return appendHeader(3) { append(entity.name) }
    }

    private fun StringBuilder.appendDescription(entity: ParametrisedProcessor): StringBuilder {
        val key = getDescriptionKey(entity)
        val description = stringProvider.getString(key)
        return append(description)
    }

    private fun getDescriptionKey(
        entity: ParametrisedProcessor
    ): String {
        return "doc__${entity.descriptionKey}"
    }
}
