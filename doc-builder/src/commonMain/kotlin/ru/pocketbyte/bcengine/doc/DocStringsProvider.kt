package ru.pocketbyte.bcengine.doc

import ru.pocketbyte.locolaser.provider.IndexFormattedStringProvider

expect fun buildDocStringsProvider() : IndexFormattedStringProvider
