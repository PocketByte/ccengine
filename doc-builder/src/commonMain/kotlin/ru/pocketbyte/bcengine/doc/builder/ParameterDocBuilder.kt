package ru.pocketbyte.bcengine.doc.builder

import ru.pocketbyte.bcengine.doc.utils.appendBold
import ru.pocketbyte.bcengine.doc.utils.appendItalic
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.locolaser.provider.IndexFormattedStringProvider

class ParameterDocBuilder(
    private val stringProvider: IndexFormattedStringProvider
) : DocBuilder<Parameter<*>, ParametrisedProcessor> {

    override fun build(
        entity: Parameter<*>,
        parent: ParametrisedProcessor,
        stringBuilder: StringBuilder
    ) {
        stringBuilder
            .appendName(entity)
            .append(" ")
            .appendFormulaType(entity)
            .append(" : ")
            .appendDescription(entity, parent)
            .toString()
    }

    private fun StringBuilder.appendName(entity: Parameter<*>): StringBuilder {
        return appendBold { append(entity.name) }
    }

    private fun StringBuilder.appendFormulaType(entity: Parameter<*>): StringBuilder {
        when (entity.formulaType) {
            ParameterFormulaType.STATIC_VALUE -> {
                appendItalic {
                    appendType("const", entity.type.shortName)
                }
            }
            ParameterFormulaType.STATIC_FORMULA -> {
                appendItalic {
                    appendType("static", entity.type.shortName)
                }
            }
            ParameterFormulaType.FORMULA -> {
                appendType("fun", entity.type.shortName)
            }
        }
        return this
    }

    private fun StringBuilder.appendType(name: String, typeName: String): StringBuilder {
        return append("(")
            .append(name)
            .append(";")
            .append(typeName)
            .append(")")
    }

    private fun StringBuilder.appendDescription(
        entity: Parameter<*>, parent: ParametrisedProcessor
    ): StringBuilder {
        val key = getDescriptionKey(entity, parent)
        val description = stringProvider.getString(key)
        return append(description)
    }

    private fun getDescriptionKey(
        entity: Parameter<*>, parent: ParametrisedProcessor
    ): String {
        val name = entity.name
            .lowercase()
            .replace(" ", "_")
        return "doc__${parent.descriptionKey}__$name"
    }
}