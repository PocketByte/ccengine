package ru.pocketbyte.bcengine.doc.builder

interface DocBuilder<Type, ParentType> {
    fun build(entity: Type, parent: ParentType, stringBuilder: StringBuilder)

    fun build(entity: Type, parent: ParentType): String {
        return StringBuilder().apply {
            build(entity, parent,  this)
        }.toString()
    }
}
