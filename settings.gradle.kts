pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

include(":core")
include(":tokenizer")
include(":parser")
include(":canvas-compose")
include(":doc-builder")

include(":app-editor")

include("bc-engine")
//include(":samples:cp2077-gon")
//include(":samples:poker-hunter")

dependencyResolutionManagement {
    versionCatalogs {
        create("libsBce") {
            from(files("./libs.versions.toml"))
        }
    }
}