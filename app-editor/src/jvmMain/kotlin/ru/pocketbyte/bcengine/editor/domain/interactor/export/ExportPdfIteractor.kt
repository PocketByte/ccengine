package ru.pocketbyte.bcengine.editor.domain.interactor.export

import kotlinx.coroutines.CoroutineDispatcher
import org.apache.pdfbox.pdmodel.common.PDRectangle
import ru.pocketbyte.bcengine.ProjectLayoutOld
import ru.pocketbyte.bcengine.canvas.CanvasFactory
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.AbsPdfModel
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.AbsPdfModel.PageSize
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.JvmPdfModel
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.JvmPdfModel.Companion.PDF_DPI
import ru.pocketbyte.bcengine.entity.Orientation
import java.io.File
import kotlin.random.Random


actual class ExportPdfIteractor actual constructor(
    workDispatcher: CoroutineDispatcher,
    canvasFactory: CanvasFactory
) : AbsExportPdfIteractor(workDispatcher, canvasFactory) {

    private val tempFolder = File("./temp/pdf_export/")
    private val randomizer = Random(System.currentTimeMillis())

    override fun buildModel(
        documentName: String,
        pageSize: PageSize,
        pageOrientation: Orientation,
        paddings: Paddings
    ): AbsPdfModel {
        return JvmPdfModel(
            pageSize.asPdfSize().withOrientation(pageOrientation),
            id = randomizer.nextBytes(5).joinToString(separator = "-"),
            documentName = documentName,
            paddingLeft = ProjectLayoutOld.projectFloat(paddings.left, PDF_DPI),
            paddingRight = ProjectLayoutOld.projectFloat(paddings.right, PDF_DPI),
            paddingTop = ProjectLayoutOld.projectFloat(paddings.top, PDF_DPI),
            paddingBottom = ProjectLayoutOld.projectFloat(paddings.bottom, PDF_DPI),
            horizontalSpace = ProjectLayoutOld.projectFloat(paddings.horizontalSpace, PDF_DPI),
            verticalSpace = ProjectLayoutOld.projectFloat(paddings.verticalSpace, PDF_DPI),
        )
    }

    override fun getTempFilePath(
        pdfModel: AbsPdfModel,
        extension: String
    ): String {
        val modelFolder = pdfModel.getTempFolder()

        modelFolder.mkdirs()

        val name = randomizer
            .nextBytes(10)
            .joinToString(separator = "-")

        return File(modelFolder, "$name.$extension")
            .absolutePath
    }

    override fun saveAndRecycle(
        pdfModel: AbsPdfModel,
        outputFilePath: String
    ): Result {
        return try {
            pdfModel.saveAndClose(outputFilePath)
            Result.Success
        } catch (e: Throwable) {
            Result.ExceptionError(e)
        } finally {
            pdfModel.getTempFolder().deleteRecursively()
        }
    }

    private fun PageSize.asPdfSize() = when(this) {
        PageSize.A0 -> PDRectangle.A0
        PageSize.A1 -> PDRectangle.A1
        PageSize.A2 -> PDRectangle.A2
        PageSize.A3 -> PDRectangle.A3
        PageSize.A4 -> PDRectangle.A4
        PageSize.A5 -> PDRectangle.A5
        PageSize.A6 -> PDRectangle.A6
    }

    private fun PDRectangle.withOrientation(orientation: Orientation): PDRectangle {
        if (orientation != Orientation.HORIZONTAL) {
            return this
        }
        return PDRectangle(height, width)
    }

    private fun AbsPdfModel.getTempFolder(): File {
        return File(tempFolder, id)
    }
}