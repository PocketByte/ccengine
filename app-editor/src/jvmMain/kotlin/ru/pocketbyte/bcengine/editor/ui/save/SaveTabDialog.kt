package ru.pocketbyte.bcengine.editor.ui.save

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import ru.pocketbyte.bcengine.editor.presentation.save.SaveTabDialogPresenter

@Composable
fun SaveTabDialog(presenter: SaveTabDialogPresenter) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        SaveProjectDialog(state.fileState, presenter)
    }
}
