package ru.pocketbyte.bcengine.editor.ui.doc

import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.doc.ParametrisedProcessorDocDialogPresenter
import ru.pocketbyte.bcengine.compose.adaper.ColorAdapterImpl
import ru.pocketbyte.bcengine.compose.adaper.RangeSpanStyleAdapterImpl
import ru.pocketbyte.bcengine.compose.adaper.adapt
import ru.pocketbyte.bcengine.compose.repository.FontFamilyRepository
import ru.pocketbyte.bcengine.html.HtmlStringDecoder

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ParametrisedProcessorDocDialog(presenter: ParametrisedProcessorDocDialogPresenter) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::dismissMessage,
            title = state.title?.let {
                { Text(text = it) }
            },
            text = {
                Text(text = state.htmlText.htmlToAnnotatedString())
            },
            confirmButton = {
                Button(
                    onClick = presenter::dismissMessage
                ) {
                    Text("Cancel")
                }
            }
        )
    }
}

private val rangeSpanStyleAdapter = RangeSpanStyleAdapterImpl(
    FontFamilyRepository(), ColorAdapterImpl()
)

private fun String.htmlToAnnotatedString(): AnnotatedString {
    val decodeResult = HtmlStringDecoder.decode(this)
    return AnnotatedString(
        decodeResult.string,
        rangeSpanStyleAdapter.adapt(decodeResult.attrs)
    )
}
