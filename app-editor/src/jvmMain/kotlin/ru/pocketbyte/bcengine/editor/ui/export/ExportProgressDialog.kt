package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.foundation.layout.width
import androidx.compose.material.AlertDialog
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.ui.Str


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ExportProgressDialog(progress: Int) {
    AlertDialog(
        modifier = Modifier
            .width(200.dp),
        onDismissRequest = {},
        text = {
            Text(Str.dialog_export_progress(progress.toLong()))
        },
        buttons = {}
    )
}