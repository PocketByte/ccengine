package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTabsPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTabsPresenter.TabViewState
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.editor.ui.UIDimensions

@Composable
fun EditorTabsView(
    modifier: Modifier,
    presenter: EditorTabsPresenter
) {
    val tabsState = presenter.stateFlow.collectAsState().value
    EditorTabsView(modifier, tabsState, presenter::onTabSelected, presenter::onTabClosed)
}

@Composable
fun EditorTabsView(
    modifier: Modifier,
    tabs: List<TabViewState>,
    onTabSelected: (index: Int) -> Unit,
    onTabClosed: (index: Int) -> Unit
) {
    if (tabs.isEmpty()) {
        Text(
            Str.screen_editor_tabs_empty_message,
            modifier = modifier
                .padding(top = UIDimensions.paddingSmall)
                .padding(horizontal = UIDimensions.paddingNormal),
            color = MaterialTheme.colors.onSurface
        )
    } else {
        val stateHorizontal = rememberScrollState()
        Box(
            modifier = modifier
                .padding(top = UIDimensions.paddingSmall)
                .padding(horizontal = UIDimensions.paddingNormal)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .horizontalScroll(stateHorizontal),
                horizontalArrangement = Arrangement.spacedBy(UIDimensions.paddingSmall)
            ) {
                tabs.forEachIndexed { index, tabState ->
                    TabItemView(tabState, index, onTabSelected, onTabClosed)
                }
            }

            HorizontalScrollbar(
                modifier = Modifier.align(Alignment.BottomStart)
                    .fillMaxWidth(),
                adapter = rememberScrollbarAdapter(stateHorizontal)
            )
        }
    }
}

@OptIn(ExperimentalUnitApi::class)
@Composable
fun TabItemView(
    tabState: TabViewState,
    index: Int,
    onTabSelected: (index: Int) -> Unit,
    onTabClosed: (index: Int) -> Unit
) {
    val textColor = if (tabState.isSelected) {
        MaterialTheme.colors.onPrimary
    } else {
        MaterialTheme.colors.onSurface
    }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .border(
                width = Style.tabBorderWidth,
                color = Color.Black.copy(alpha = 0.5f)
            )
            .background(
                if (tabState.isSelected) {
                    MaterialTheme.colors.primary
                } else {
                    Color.Transparent
                }
            )
            .clickable {
                onTabSelected(index)
            },
        contentAlignment = Alignment.Center
    ) {
        Text(
            tabState.title + if (tabState.isEdited) "*" else "",
            modifier = Modifier
                .padding(10.dp)
                .padding(end = 30.dp),
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            fontSize = TextUnit(15f, TextUnitType.Sp),
            fontWeight = FontWeight.Normal,
            color = textColor
        )
        Text(
            "X",
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .padding(end = 15.dp)
                .clickable {
                    onTabClosed(index)
                },
            color = textColor
        )
    }
}

@Preview
@Composable
fun TabItemView_Preview() {
    TabItemView(
        TabViewState("Title", isEdited = false, isSelected = false),
        0, {}, {}
    )
}

private object Style {
    val tabBorderWidth = 2.dp
}