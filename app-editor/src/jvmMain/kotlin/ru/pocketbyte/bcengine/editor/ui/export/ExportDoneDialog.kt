package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.export.ExportDoneDialogPresenter
import ru.pocketbyte.bcengine.editor.ui.Str

@Composable
fun ExportDoneDialog(presenter: ExportDoneDialogPresenter) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::dismiss,
            title = state.title?.let {
                { Text(text = it) }
            },
            text = {
                Text(text = state.message)
            },
            confirmButton = {
                Button(
                    onClick = presenter::dismiss
                ) {
                    Text(Str.dialog_btn_cancel)
                }
            }
        )
    }
}