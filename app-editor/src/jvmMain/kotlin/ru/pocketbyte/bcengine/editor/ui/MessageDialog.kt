package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter

@Composable
fun MessageDialog(presenter: MessageDialogPresenter) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::dismissMessage,
            title = state.title?.let {
                { Text(text = it) }
            },
            text = {
                Text(text = state.message)
            },
            confirmButton = {
                Button(
                    onClick = presenter::dismissMessage
                ) {
                    Text(Str.dialog_btn_cancel)
                }
            }
        )
    }
}