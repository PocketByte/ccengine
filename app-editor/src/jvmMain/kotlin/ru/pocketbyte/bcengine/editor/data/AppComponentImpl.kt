package ru.pocketbyte.bcengine.editor.data

import androidx.compose.ui.window.ApplicationScope
import ru.pocketbyte.bcengine.editor.domain.AppComponent

class AppComponentImpl(
    private val appScope: ApplicationScope
) : AppComponent {

    override fun exitApplication() {
        appScope.exitApplication()
    }
}
