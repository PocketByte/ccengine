package ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject


class JvmPdfModel(
    private val pageSize: PDRectangle,
    id: String,
    documentName: String,
    paddingLeft: Float,
    paddingRight: Float,
    paddingTop: Float,
    paddingBottom: Float,
    horizontalSpace: Float,
    verticalSpace: Float
) : AbsPdfModel(
    id, documentName,
    paddingLeft, paddingRight, paddingTop, paddingBottom,
    horizontalSpace, verticalSpace
) {

    companion object {
        const val PDF_DPI = 72f

        private const val HEADER_FONT_SIZE = 8f
    }

    override val pageHeight: Float = pageSize.height
    override val pageWidth: Float = pageSize.width
    override val dpi: Float = PDF_DPI

    private var pagesCount = 0
    private val document = PDDocument()
    private var contentStream: PDPageContentStream? = null

    override fun prepareNextPage() {
        contentStream?.close()

        pagesCount++
        val page = PDPage(pageSize)

        document.addPage(page)
        contentStream = PDPageContentStream(document, page).apply {
            beginText()
            setFont(PDType1Font.COURIER, HEADER_FONT_SIZE)
            newLineAtOffset(paddingLeft, paddingBottom - HEADER_FONT_SIZE * 1.5f)
            showText("$documentName (Page $pagesCount)")
            endText()
        }
    }

    override fun drawCard(
        imagePath: String,
        positionX: Float,
        positionY: Float,
        cardWidth: Float,
        cardHeight: Float
    ) {
        contentStream?.drawImage(
            PDImageXObject.createFromFile(imagePath, document),
            positionX, positionY,
            cardWidth, cardHeight
        )
    }

    override fun saveAndClose(filePath: String) {
        contentStream?.close()
        document.save(filePath)
        document.close()
    }
}