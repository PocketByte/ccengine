package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.export.AbsExportDialogPresenter
import ru.pocketbyte.bcengine.editor.ui.Str

@Composable
fun ExportDialog(presenter: AbsExportDialogPresenter<*>, title: String) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::onCancelClick,
            title = {
                Text(text = title)
            },
            text = {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(Str.dialog_export_dir)
                    TextField(
                        state.dir,
                        onValueChange = presenter::onDirValueChange,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(16.dp))
                    Text(Str.dialog_export_file)
                    TextField(
                        state.file,
                        onValueChange = presenter::onFileValueChange,
                        enabled = state.fileEnabled,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            },
            confirmButton = {
                Button(
                    onClick = presenter::onOkClick
                ) {
                    Text(Str.dialog_export_btn_export)
                }
            },
            dismissButton = {
                Button(
                    onClick = presenter::onCancelClick
                ) {
                    Text(Str.dialog_export_btn_cancel)
                }
            }
        )
        if (state.progress >= 0) {
            ExportProgressDialog(state.progress)
        }
    }
}
