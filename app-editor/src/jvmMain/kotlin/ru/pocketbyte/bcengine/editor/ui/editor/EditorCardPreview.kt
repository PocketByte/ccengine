package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorDataSetViewPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorProjectViewPresenter
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.editor.ui.UIColors
import ru.pocketbyte.bcengine.editor.ui.UIDimensions
import ru.pocketbyte.bcengine.editor.ui.card.CardPreview

@Composable
fun EditorCardPreview(
    modifier: Modifier,
    projectState: EditorProjectViewPresenter.State,
    dataSetState: EditorDataSetViewPresenter.State,
    overlayState: EditorOverlayPresenter.State,
    composeComponent: ComposeComponent
) {
    Box(modifier = modifier) {
        val project = projectState.project
        val dataSetItem = dataSetState.getCurrentDataSetItem()
        if (project != null && dataSetItem != null) {
            CardPreview(
                modifier = Modifier
                    .background(UIColors.previewBackground)
                    .padding(UIDimensions.paddingNormal)
                    .fillMaxSize(),
                projectState.tag, projectState.context,
                project, dataSetItem, overlayState,
                composeComponent,
                projectState.projectErrorMessage == null
            )
        } else {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(UIColors.previewBackground)
            ) {
                Text(
                    Str.screen_editor_project_not_ready,
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
        }
    }
}