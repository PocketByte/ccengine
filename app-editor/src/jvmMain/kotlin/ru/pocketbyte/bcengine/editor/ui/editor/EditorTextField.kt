package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.TextFieldValue
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTextViewPresenter
import ru.pocketbyte.bcengine.editor.ui.UIDimensions
import ru.pocketbyte.bcengine.editor.ui.style.EditorStyle

@Composable
fun EditorTextField(
    modifier: Modifier,
    textState: EditorTextViewPresenter.State,
    onTextChanged: (text: String) -> Unit,
    onTextSelected: (start: Int, end: Int) -> Unit
) {
    val stateVertical = rememberScrollState()
    val stateHorizontal = rememberScrollState()
    Box(
        modifier = modifier
            .border(
                UIDimensions.borderNormal,
                EditorStyle.DEFAULT.viewBorder,
                RoundedCornerShape(UIDimensions.cornerRadiusNormal)
            )
            .clip(shape = RoundedCornerShape(UIDimensions.cornerRadiusNormal))
            .background(EditorStyle.DEFAULT.viewBackground)
    ) {
        BasicTextField(
            value = EditorTextFieldValueBuilder.buildTextFieldValue(textState),
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(stateVertical)
                .horizontalScroll(stateHorizontal)
                .padding(UIDimensions.paddingSmall)
                .padding(
                    end = UIDimensions.scrollbarPadding,
                    bottom = UIDimensions.scrollbarPadding
                ),
            textStyle = TextStyle(
                color = EditorStyle.DEFAULT.textDefault,
                fontFamily = FontFamily.Monospace,
                fontSize = EditorStyle.DEFAULT.textSize,
            ),
            cursorBrush = SolidColor(EditorStyle.DEFAULT.textDefault),
            onValueChange = { textFieldValue: TextFieldValue ->
                onTextChanged(textFieldValue.text)
                onTextSelected(textFieldValue.selection.start, textFieldValue.selection.end)
            }
        )
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd)
                .fillMaxHeight()
                .padding(
                    end = UIDimensions.paddingSmall,
                    top = UIDimensions.paddingSmall,
                    bottom = UIDimensions.paddingSmall
                ),
            adapter = rememberScrollbarAdapter(stateVertical)
        )
        HorizontalScrollbar(
            modifier = Modifier.align(Alignment.BottomStart)
                .fillMaxWidth()
                .padding(
                    start = UIDimensions.paddingSmall,
                    end = UIDimensions.scrollbarPadding + UIDimensions.paddingSmall,
                    bottom = UIDimensions.paddingSmall
                ),
            adapter = rememberScrollbarAdapter(stateHorizontal)
        )
    }
}
