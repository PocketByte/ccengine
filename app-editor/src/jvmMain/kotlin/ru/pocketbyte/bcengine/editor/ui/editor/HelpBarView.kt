package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import ru.pocketbyte.bcengine.editor.presentation.editorview.HelpBarViewPresenter
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.editor.ui.UIDimensions

@Composable
fun HelpBarView(
    modifier: Modifier,
    helpBarState: HelpBarViewPresenter.State,
    onHelpClick: () -> Unit
) {
    Row (
        modifier = modifier
            .border(UIDimensions.borderNormal, Color.Gray)
            .padding(UIDimensions.paddingSmall),
        horizontalArrangement = Arrangement.spacedBy(UIDimensions.paddingNormal),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            helpBarState.annotatedText(),
            color = MaterialTheme.colors.onSurface,
            modifier = Modifier
                .weight(1f)
                .padding(UIDimensions.paddingSmall)
        )

        if (helpBarState.text.isNotBlank()) {
            Button(onHelpClick) {
                Text(Str.screen_editor_help_btn_help)
            }
        }
    }
}

private fun HelpBarViewPresenter.State.annotatedText(): AnnotatedString {
    return AnnotatedString(
        text, listOf(
            AnnotatedString.Range(
                SpanStyle(fontWeight = FontWeight.Bold),
                selectedParameterStartIndex,
                selectedParameterEndIndex
            )
        )
    )
}