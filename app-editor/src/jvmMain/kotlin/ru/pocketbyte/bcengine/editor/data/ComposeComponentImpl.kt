package ru.pocketbyte.bcengine.editor.data

import androidx.compose.runtime.Composable
import androidx.compose.ui.awt.ComposeWindow
import androidx.compose.ui.platform.LocalFontFamilyResolver
import androidx.compose.ui.text.font.FontFamily
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.compose.canvas.ComposeImageRepository
import ru.pocketbyte.bcengine.compose.canvas.MutableComposeCanvas
import ru.pocketbyte.compose.ComposeCanvasFactory
import ru.pocketbyte.compose.canvas.ComposeImageRepositoryImpl

class ComposeComponentImpl private constructor(
    override val window: ComposeWindow,
    private val imageRepository: ComposeImageRepository,
    private val fontFamilyResolver: FontFamily.Resolver,
) : ComposeComponent {

    private val canvasMap = mutableMapOf<String, MutableComposeCanvas>()

    override val canvasFactory = ComposeCanvasFactory(imageRepository, fontFamilyResolver)

    override fun getMutableCanvas(tag: String) : MutableComposeCanvas {
        return canvasMap[tag]
            ?: MutableComposeCanvas(imageRepository, fontFamilyResolver).apply {
                canvasMap[tag] = this
            }
    }

    override fun recycleCanvas(tag: String) {
        canvasMap.remove(tag)?.let {
            it.recycle()
        }
    }

    override fun invalidateCache() {
        imageRepository.clear()
    }

    companion object {

        @Composable
        fun newInstance(window: ComposeWindow) : ComposeComponent {
            return ComposeComponentImpl(
                window,
                ComposeImageRepositoryImpl(),
                LocalFontFamilyResolver.current
            )
        }
    }
}
