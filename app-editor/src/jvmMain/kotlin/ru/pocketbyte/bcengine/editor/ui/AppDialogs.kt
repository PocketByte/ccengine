package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.runtime.Composable
import ru.pocketbyte.bcengine.editor.presentation.AppPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportAllPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.ui.doc.ParametrisedProcessorDocDialog
import ru.pocketbyte.bcengine.editor.ui.export.ExportAllPdfDialog
import ru.pocketbyte.bcengine.editor.ui.export.ExportDialog
import ru.pocketbyte.bcengine.editor.ui.export.ExportDoneDialog
import ru.pocketbyte.bcengine.editor.ui.export.ExportPdfDialog
import ru.pocketbyte.bcengine.editor.ui.export.ExportSingleImageDialog
import ru.pocketbyte.bcengine.editor.ui.save.SaveAllDialog
import ru.pocketbyte.bcengine.editor.ui.save.SaveTabDialog

@Composable
fun AppDialogs(appPresenter: AppPresenter) {
    MessageDialog(appPresenter.messageDialogPresenter)
    SaveAllDialog(appPresenter.saveAllDialogPresenter)
    SaveTabDialog(appPresenter.saveTabDialogPresenter)
    ExportDialog(appPresenter.exportImageDialogPresenter, Str.dialog_export_png)
    ExportSingleImageDialog(appPresenter.exportSingleImageDialogPresenter, Str.dialog_export_single_png)
    ExportPdfDialog(appPresenter.exportPdfDialogPresenter, Str.dialog_export_pdf)
    ExportAllPdfDialog(appPresenter.exportAllPdfDialogPresenter)
    ExportDoneDialog(appPresenter.exportDoneDialogPresenter)
    ParametrisedProcessorDocDialog(appPresenter.parametrisedProcessorDocDialogPresenter)
}