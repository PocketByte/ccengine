package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.CurrentEditorScreenPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTabsPresenter
import ru.pocketbyte.bcengine.editor.ui.editor.EditorScreen
import ru.pocketbyte.bcengine.editor.ui.editor.EditorTabsView

@Composable
fun MainScreen(
    currentScreenPresenter: CurrentEditorScreenPresenter,
    overlayPresenter: EditorOverlayPresenter,
    tabsPresenter: EditorTabsPresenter
) {
    val editor = currentScreenPresenter.currentEditorFlow.collectAsState().value
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(UIColors.windowBackground)
    ) {
        Column {
            EditorTabsView(
                modifier = Modifier
                    .fillMaxWidth(),
                tabsPresenter
            )

            if (editor != null) {
                EditorScreen(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    editor, overlayPresenter
                )
            }
        }
    }
}