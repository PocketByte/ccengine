package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.export.ExportPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.entity.Orientation

@Composable
fun ExportPdfDialog(
    presenter: ExportPdfDialogPresenter,
    title: String
) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::onCancelClick,
            title = {
                Text(text = title)
            },
            text = {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(Str.dialog_export_dir)
                    TextField(
                        state.dir,
                        onValueChange = presenter::onDirValueChange,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(16.dp))
                    Text(Str.dialog_export_file)
                    TextField(
                        state.file,
                        onValueChange = presenter::onFileValueChange,
                        enabled = state.fileEnabled,
                        modifier = Modifier.fillMaxWidth()
                    )
                    Text(Str.dialog_export_page_orientation)
                    Row {
                        Checkbox(
                            checked = state.orientation != Orientation.HORIZONTAL,
                            onCheckedChange = {
                                presenter.onOrientationChange(Orientation.VERTICAL)
                            }
                        )
                        Text(Str.dialog_export_page_orientation_portrait)

                        Spacer(modifier = Modifier.width(16.dp))

                        Checkbox(
                            checked = state.orientation == Orientation.HORIZONTAL,
                            onCheckedChange = {
                                presenter.onOrientationChange(Orientation.HORIZONTAL)
                            }
                        )
                        Text(Str.dialog_export_page_orientation_landscape)
                    }
                }
            },
            confirmButton = {
                Button(
                    onClick = presenter::onOkClick
                ) {
                    Text(Str.dialog_export_btn_export)
                }
            },
            dismissButton = {
                Button(
                    onClick = presenter::onCancelClick
                ) {
                    Text(Str.dialog_export_btn_cancel)
                }
            }
        )
        if (state.progress >= 0) {
            ExportProgressDialog(state.progress)
        }
    }
}