package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import ru.pocketbyte.bcengine.editor.presentation.export.ExportAllPdfDialogPresenter

@Composable
fun ExportAllPdfDialog(presenter: ExportAllPdfDialogPresenter) {
    val state = presenter.stateFlow.collectAsState().value ?: return
    if (state.progress >= 0) {
        ExportProgressDialog(state.progress)
    }
}
