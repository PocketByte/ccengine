package ru.pocketbyte.bcengine.editor.ui.export

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.presentation.export.ExportSingleImageDialogPresenter
import ru.pocketbyte.bcengine.editor.ui.Str


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ExportSingleImageDialog(presenter: ExportSingleImageDialogPresenter, title: String) {
    val state = presenter.stateFlow.collectAsState().value
    if (state != null) {
        AlertDialog(
            modifier = Modifier
                .widthIn(600.dp, 800.dp),
            onDismissRequest = presenter::onCancelClick,
            title = {
                Text(text = title)
            },
            text = {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(Str.dialog_export_dir)
                    TextField(
                        state.dir,
                        onValueChange = presenter::onDirValueChange,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(16.dp))
                    Text(Str.dialog_export_file)
                    TextField(
                        state.file,
                        onValueChange = presenter::onFileValueChange,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(16.dp))
                    Text(Str.dialog_export_row_size)
                    TextField(
                        state.rowSize,
                        onValueChange = presenter::onRowSizeValueChange,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            },
            confirmButton = {
                Button(
                    onClick = presenter::onOkClick
                ) {
                    Text(Str.dialog_export_btn_export)
                }
            },
            dismissButton = {
                Button(
                    onClick = presenter::onCancelClick
                ) {
                    Text(Str.dialog_export_btn_cancel)
                }
            }
        )
        if (state.progress >= 0) {
            ExportProgressDialog(state.progress)
        }
    }
}
