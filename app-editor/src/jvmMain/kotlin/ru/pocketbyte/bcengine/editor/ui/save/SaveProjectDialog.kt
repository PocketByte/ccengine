package ru.pocketbyte.bcengine.editor.ui.save

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorFileModel
import ru.pocketbyte.bcengine.editor.presentation.save.SaveDialogActions
import ru.pocketbyte.bcengine.editor.ui.Str


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SaveProjectDialog(
    fileState: EditorFileModel.State?,
    actions: SaveDialogActions
) {
    AlertDialog(
        modifier = Modifier
            .widthIn(400.dp, 600.dp),
        onDismissRequest = actions::onCloseClick,
        title = {
            Text(text = Str.dialog_save_project_title)
        },
        text = {
            Text(
                text = if (fileState == null) {
                    Str.dialog_save_project_not_saved
                } else {
                    Str.dialog_save_project_not_saved_named(fileState.title)
                } + "\n" + Str.dialog_save_project_saved_question
            )
        },
        buttons = {
            Row(
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                Button(
                    onClick = actions::onForgetClick
                ) {
                    Text(Str.dialog_save_project_btn_forget)
                }
                Spacer(Modifier.weight(1f))
                Button(
                    onClick = actions::onSaveClick
                ) {
                    Text(Str.dialog_save_project_btn_save)
                }
                Button(
                    onClick = actions::onCloseClick
                ) {
                    Text(Str.dialog_save_project_btn_cancel)
                }
            }
        }
    )
}
