package ru.pocketbyte.bcengine.editor.ui.menu

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.window.MenuBarScope
import androidx.compose.ui.window.MenuScope
import ru.pocketbyte.bcengine.editor.presentation.menu.MenuPresenter
import ru.pocketbyte.bcengine.editor.presentation.menu.MenuPresenter.ItemState
import ru.pocketbyte.bcengine.editor.ui.Str

@Composable
fun MenuBarScope.MenuGroup(
    presenter: MenuPresenter
) {
    MenuGroup(
        presenter.stateFlow.collectAsState().value,
        presenter::onOptionClick
    )
}

@Composable
fun MenuBarScope.MenuGroup(
    state: ItemState.Group,
    onOptionClick: (ItemState.Option) -> Unit
) {
    Menu(state.title, enabled = state.isEnabled) {
        MenuGroupItems(state.items, onOptionClick)
    }
}

@Composable
private fun MenuScope.MenuGroupItems(
    items: List<ItemState>,
    onOptionClick: (ItemState.Option) -> Unit
) {
    items.forEach { item ->
        when (item) {
            is ItemState.Option -> {
                Item(item.title, enabled = item.isEnabled) {
                    onOptionClick(item)
                }
            }
            is ItemState.Group -> {
                Menu(item.title, enabled = item.isEnabled) {
                    MenuGroupItems(item.items, onOptionClick)
                }
            }
            ItemState.Separator -> {
                Separator()
            }
        }
    }
}
