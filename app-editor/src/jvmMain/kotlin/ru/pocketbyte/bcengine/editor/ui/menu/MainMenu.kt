package ru.pocketbyte.bcengine.editor.ui.menu

import androidx.compose.runtime.Composable
import androidx.compose.ui.window.FrameWindowScope
import androidx.compose.ui.window.MenuBar
import ru.pocketbyte.bcengine.editor.presentation.menu.MainMenuPresenter

@Composable
fun FrameWindowScope.MainMenu(
    presenter: MainMenuPresenter
) {
    MenuBar {
        presenter.menuGroups.forEach {
            MenuGroup(it)
        }
    }
}