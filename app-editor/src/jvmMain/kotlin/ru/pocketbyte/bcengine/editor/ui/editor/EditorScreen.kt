package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.*
import ru.pocketbyte.bcengine.editor.ui.UIDimensions
import ru.pocketbyte.bcengine.editor.ui.dataset.DataSetStatusView

@Composable
fun EditorScreen(
    modifier: Modifier,
    presenter: EditorViewPresenter,
    overlayPresenter: EditorOverlayPresenter
) {
    val textState = presenter.text.stateFlow.collectAsState().value
    val dataSetState = presenter.dataSet.stateFlow.collectAsState().value
    val projectState = presenter.project.stateFlow.collectAsState().value
    val helpBarState = presenter.helpBar.stateFlow.collectAsState().value
    val overlayState = overlayPresenter.stateFlow.collectAsState().value
    EditorScreen(
        modifier,
        textState, dataSetState,
        projectState,
        helpBarState,
        presenter.helpBar::onHelpClick,
        overlayState,
        presenter.composeComponent,
        presenter.text::onTextChanged,
        presenter.text::onTextSelect,
        presenter.dataSet
    )
}

@Composable
fun EditorScreen(
    modifier: Modifier,
    textState: EditorTextViewPresenter.State,
    dataSetState: EditorDataSetViewPresenter.State,
    projectState: EditorProjectViewPresenter.State,
    helpBarState: HelpBarViewPresenter.State,
    onHelpClick: () -> Unit,
    overlayState: EditorOverlayPresenter.State,
    composeComponent: ComposeComponent,
    onTextChanged: (text: String) -> Unit,
    onTextSelected: (start: Int, end: Int) -> Unit,
    dataSetActions: DataSetActions
) {
    Row(
        modifier = modifier
            .padding(UIDimensions.paddingSmall),
        horizontalArrangement = Arrangement.spacedBy(UIDimensions.paddingSmall)
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .weight(1f)
        ) {
            EditorTextField(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(1f),
                textState = textState,
                onTextChanged = onTextChanged,
                onTextSelected= onTextSelected
            )
            HelpBarView(
                modifier = Modifier
                    .fillMaxWidth(),
                helpBarState = helpBarState,
                onHelpClick = onHelpClick
            )
        }
        Column(modifier = Modifier
            .fillMaxHeight()
            .width(500.dp)
        ) {
            Box(modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
            ) {
                EditorCardPreview(
                    modifier = Modifier
                        .fillMaxWidth(),
                    projectState = projectState,
                    dataSetState = dataSetState,
                    overlayState = overlayState,
                    composeComponent = composeComponent
                )
                val error = projectState.projectErrorMessage
                if (error != null) {
                    EditorErrorView(
                        modifier = Modifier
                            .fillMaxWidth(),
                        error
                    )
                }
            }
            DataSetStatusView(
                modifier = Modifier
                    .fillMaxWidth(),
                state = dataSetState,
                dataSetActions = dataSetActions
            )
        }
    }
}