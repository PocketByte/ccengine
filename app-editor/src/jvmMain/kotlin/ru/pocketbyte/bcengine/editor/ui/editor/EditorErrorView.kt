package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import ru.pocketbyte.bcengine.editor.ui.UIColors
import ru.pocketbyte.bcengine.editor.ui.UIDimensions

@Composable
fun EditorErrorView(
    modifier: Modifier,
    error: String
) {
    val stateHorizontal = rememberScrollState()
    Box(modifier = modifier
        .background(UIColors.errorBackground)
        .border(UIDimensions.borderNormal, UIColors.errorBorder)
        .padding(UIDimensions.paddingNormal)
    ) {
        Text(
            error,
            modifier = modifier
                .fillMaxWidth()
                .horizontalScroll(stateHorizontal),
            fontFamily = FontFamily.Monospace
        )
        HorizontalScrollbar(
            modifier = Modifier.align(Alignment.BottomStart)
                .fillMaxWidth(),
            adapter = rememberScrollbarAdapter(stateHorizontal)
        )
    }
}