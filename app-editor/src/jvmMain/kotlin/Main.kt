import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import kotlinx.coroutines.Dispatchers
import ru.pocketbyte.bcengine.editor.DesktopStringRepository
import ru.pocketbyte.bcengine.editor.data.AppComponentImpl
import ru.pocketbyte.bcengine.editor.data.ComposeComponentImpl
import ru.pocketbyte.bcengine.editor.presentation.AppPresenter
import ru.pocketbyte.bcengine.editor.ui.AppDialogs
import ru.pocketbyte.bcengine.editor.ui.LocalStrings
import ru.pocketbyte.bcengine.editor.ui.MainScreen
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.editor.ui.menu.MainMenu
import ru.pocketbyte.locolaser.provider.JvmBundleStringProvider

private lateinit var appPresenter: AppPresenter

fun main() = application {
    CompositionLocalProvider(
        LocalStrings provides DesktopStringRepository(
            JvmBundleStringProvider(
                DesktopStringRepository::class.java.classLoader
            )
        )
    ) {
        Window(
            title = Str.app_name,
            state = rememberWindowState(
                size = DpSize(1300.dp, 900.dp)
            ),
            onCloseRequest = ::onCloseApp
        ) {
            appPresenter = AppPresenter(
                rememberCoroutineScope(),
                Dispatchers.IO,
                AppComponentImpl(this@application),
                ComposeComponentImpl.newInstance(window),
                Str
            )

            MainMenu(appPresenter.mainMenuPresenter)

            MaterialTheme(
                colors = darkColors()
            ) {
                MainScreen(
                    appPresenter.currentEditorScreenPresenter,
                    appPresenter.overlayPresenter,
                    appPresenter.tabsPresenter
                )
                AppDialogs(appPresenter)
            }
        }
    }
}

private fun onCloseApp() {
    appPresenter.saveAllDialogPresenter.showDialogOrCloseApp()
}