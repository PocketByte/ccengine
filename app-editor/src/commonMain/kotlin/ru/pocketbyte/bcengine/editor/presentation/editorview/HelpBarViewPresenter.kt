package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.presentation.doc.ParametrisedProcessorDocDialogPresenter
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

class HelpBarViewPresenter(
    uiScope: CoroutineScope,
    private val editorModel: EditorModel,
    private val docPresenter: ParametrisedProcessorDocDialogPresenter,
) {

    data class State(
        val text: String = "",
        val selectedParameterStartIndex: Int = 0,
        val selectedParameterEndIndex: Int = 0
    )

    val stateFlow: StateFlow<State> get() = _stateFlow
    private val _stateFlow = MutableStateFlow(State())

    private var activeProcessor: ParametrisedProcessor? = null

    init {
        uiScope.launch {
            editorModel.text.selectionStateFlow.collect {
                if (it.start == it.end) {
                    onCursorPositionChanged(it.start)
                }
            }
        }
    }

    fun onHelpClick() {
        activeProcessor?.let {
            docPresenter.show(it)
        }
    }

    private fun onCursorPositionChanged(position: Int) {
        showHelp(editorModel.token.stateFlow.value.tokens, position)
    }

    private fun showHelp(
        tokens: List<ProjectToken>,
        position: Int
    ) {
        val token = findLowLevelParametrisedToken(
            tokens, position
        ) as? NamedGroupToken ?: return noHelp()

        val processor = token.getMetaData(Token.METADATA_PROCESSOR) as? ParametrisedProcessor

        if (processor == null) {
            return noHelp()
        } else {
            activeProcessor = processor
        }

        val builder = StringBuilder(processor.name)
        builder.append(token.bracers.openChar)

        val parameterIndex = findParameterIndex(
            token.subTokens, position, processor.parameterSeparator
        )

        var selectedParameterStartIndex = 0
        var selectedParameterEndIndex = 0
        processor.parameters.forEachIndexed { index, parameter ->
            if (index > 0) {
                builder.append(processor.parameterSeparator.char).append(' ')
            }
            if (index == parameterIndex) {
                selectedParameterStartIndex = builder.length
                selectedParameterEndIndex = builder.length + parameter.name.length
            }
            builder.append(parameter.name)
        }
        builder.append(token.bracers.closeChar)

        _stateFlow.value = _stateFlow.value.copy(
            text = builder.toString(),
            selectedParameterStartIndex = selectedParameterStartIndex,
            selectedParameterEndIndex = selectedParameterEndIndex
        )
    }

    private fun findLowLevelParametrisedToken(
        tokens: List<ProjectToken>,
        position: Int
    ): ProjectToken? {
        val token = tokens.find {
            it.startIndex <= position && (it.endIndex + 1) >= position
        } ?: return null

        if (token is NamedGroupToken) {
            findLowLevelParametrisedToken(token.subTokens, position)?.let {
                return it
            }
        }

        if (token.getMetaData(Token.METADATA_PROCESSOR) is ParametrisedProcessor) {
            return token
        }
        return null
    }

    private fun findParameterIndex(
        tokens: List<ProjectToken>,
        position: Int,
        parameterSeparator: SeparatorToken.Type
    ) : Int {
        var index = 0
        tokens.forEach {
            if (it is SeparatorToken && it.type == parameterSeparator) {
                index += 1
            }
            if (it.startIndex <= position && (it.endIndex + 1) >= position) {
                return index
            }
        }
        return -1
    }

    private fun noHelp() {
        activeProcessor = null
        _stateFlow.value = State()
    }
}
