package ru.pocketbyte.bcengine.editor.domain.repository

import java.io.File
import java.io.PrintWriter
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class StringListFileRepository(
    private val file: File
) {

    fun loadFromFile(): List<String> {
        if (!file.exists()) {
            return emptyList()
        }
        return String(
            Files.readAllBytes(Paths.get(file.absolutePath)),
            StandardCharsets.UTF_8
        ).split("\n")
            .filter { it.isNotBlank() }
    }

    fun <T> saveToFile(list: List<T>, toString: (T) -> String) {
        PrintWriter(file).use { out ->
            list.forEach {
                out.println(toString(it))
            }
            out.flush()
            out.close()
        }
    }

    fun saveToFile(list: List<String>) {
        saveToFile(list) { it }
    }
}