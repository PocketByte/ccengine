package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.dataset.DataSet
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.dataset.getOrNull
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.errors.ErrorsContainer

class EditorDataSetViewPresenter(
    uiScope: CoroutineScope,
    private val composeComponent: ComposeComponent,
    private val workDispatcher: CoroutineDispatcher,
    private val editorModel: EditorModel
) : DataSetActions {

    data class State(
        val dataSet: DataSet? = null,
        val currentItemIndex: Int = 0,
        val dataSetState: DataSetStatus = DataSetStatus.NotReady,
        val dataSetErrorMessage: String? = null,

        val previousItemEnabled: Boolean = false,
        val nextItemEnabled: Boolean = false,
    ) {
        fun getCurrentDataSetItem(): DataSetItem? {
            return dataSet?.getOrNull(currentItemIndex)
        }
    }

    val stateFlow: StateFlow<State> get() = _stateFlow
    private val _stateFlow = MutableStateFlow(State())

    private val scope = uiScope + Job()

    init {
        scope.launch {
            editorModel.dataSet.stateFlow.collect {
                _stateFlow.value = _stateFlow.value.copy(
                    dataSet = it.dataSet,
                    dataSetState = it.dataSetStatus,
                    currentItemIndex = it.currentItemIndex,
                    nextItemEnabled = it.currentItemIndex < (it.dataSet?.size ?: 0) - 1,
                    previousItemEnabled = it.currentItemIndex > 0,
                    dataSetErrorMessage = it.errors?.let { errors ->
                        ErrorsContainer.buildErrorString(errors)
                    }
                )
            }
        }
    }

    override fun loadDataSet() {
        scope.launch(workDispatcher) {
            composeComponent.invalidateCache()

            val projectState = editorModel.project.state

            if (projectState.project != null && projectState.errors == null) {
                editorModel.dataSet.loadDataset(
                    projectState.project
                )
            }
        }
    }

    override fun firstDataSetItem() {
        setDataSetItem(0)
    }

    override fun nextDataSetItem() {
        setDataSetItem(editorModel.dataSet.state.currentItemIndex + 1)
    }

    override fun previousDataSetItem() {
        setDataSetItem(editorModel.dataSet.state.currentItemIndex - 1)
    }

    override fun lastDataSetItem() {
        setDataSetItem((editorModel.dataSet.state.dataSet?.size ?: return) - 1)
    }

    override fun setDataSetItem(newIndex: Int) {
        scope.launch {
            editorModel.dataSet.setCurrentItemIndex(newIndex)
        }
    }
}