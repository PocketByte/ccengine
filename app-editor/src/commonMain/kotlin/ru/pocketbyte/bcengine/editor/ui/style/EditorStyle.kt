package ru.pocketbyte.bcengine.editor.ui.style

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType

@OptIn(ExperimentalUnitApi::class)
class EditorStyle(
    val viewBackground: Color,
    val viewBorder: Color,
    val textDefault: Color,
    val textSize: TextUnit,

    val comment: SpanStyle,
    val group: SpanStyle,
    val string: SpanStyle,
    val function:  SpanStyle,
    val shapeName: SpanStyle,
    val shapeSeparator: SpanStyle
) {
    companion object {
        val DEFAULT = EditorStyle(
            viewBackground = Color(0xff2b2b2b),
            viewBorder = Color(0xff555555),
            textDefault = Color(0xffabb7c4),
            textSize = TextUnit(16f, TextUnitType.Sp),

            comment = SpanStyle(color = Color(0xff808080)),
            group= SpanStyle(
                color = Color(0xffc17b41),
                fontWeight = FontWeight.Bold
            ),
            string = SpanStyle(color = Color(0xff70855e)),
            function = SpanStyle(color = Color(0xff9278a7)),
            shapeName = SpanStyle(color = Color(0xfff7c77b)),
            shapeSeparator = SpanStyle(color = Color(0xffc17b41))
        )
    }
}