package ru.pocketbyte.bcengine.editor.domain

interface AppComponent {
    fun exitApplication()
}