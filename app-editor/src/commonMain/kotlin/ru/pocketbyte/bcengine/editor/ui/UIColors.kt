package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.ui.graphics.Color

object UIColors {
    val windowBackground = Color(0xff3c3f41)
    val previewBackground = Color.LightGray

    val errorBackground = Color.White
    val errorBorder = Color.LightGray
}