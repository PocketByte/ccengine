package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import ru.pocketbyte.bcengine.editor.StringRepository

val LocalStrings = compositionLocalOf<StringRepository> {
    throw IllegalStateException("LocalStrings must be initialised by CompositionLocalProvider")
}

val Str: StringRepository
    @Composable
    get() = LocalStrings.current
