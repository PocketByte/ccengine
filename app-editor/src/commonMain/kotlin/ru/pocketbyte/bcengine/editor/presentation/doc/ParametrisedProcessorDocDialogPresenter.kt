package ru.pocketbyte.bcengine.editor.presentation.doc

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.doc.builder.DocBuilder
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor

class ParametrisedProcessorDocDialogPresenter(
    private val docBuilder: DocBuilder<ParametrisedProcessor, Unit>
) {

    data class State(
        val title: String?,
        val htmlText: String
    )

    val stateFlow: StateFlow<State?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<State?>(null)

    fun show(parametrisedProcessor: ParametrisedProcessor) {
        _stateFlow.value = State(
            parametrisedProcessor.name,
            docBuilder.build(parametrisedProcessor, Unit)
        )
    }

    fun dismissMessage() {
        _stateFlow.value = null
    }
}
