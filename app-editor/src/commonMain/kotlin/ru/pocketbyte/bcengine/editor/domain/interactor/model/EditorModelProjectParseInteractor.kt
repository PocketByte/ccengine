package ru.pocketbyte.bcengine.editor.domain.interactor.model

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorProjectModel
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.parser.project.ProjectParser
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class EditorModelProjectParseInteractor(
    private val projectModel: EditorProjectModel,
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
) {

    private var runJob: Job? = null

    fun parseProjectAndPublish(context: Context, tokens: List<ProjectToken>) {
        runJob?.cancel()
        runJob = uiScope.async(workDispatcher + Job()) {
            val project = try {
                ProjectParser().parse(context, tokens)
            } catch (e: Exception) {
                e.printStackTrace()
                return@async
            }

            if (context.errors.hasErrors()) {
                projectModel.onProjectParseError(context.errors.list)
            } else {
                projectModel.onProjectParsed(project)
            }
        }
    }
}