package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.errors.ErrorsContainer

class EditorModel(
    uiScope: CoroutineScope,
    workDispatcher: CoroutineDispatcher,
) {
    val tag: String = hashCode().toString()
    val file by lazy { EditorFileModel() }
    val text by lazy { EditorTextModel(file) }
    val token by lazy { EditorTokenModel(uiScope, workDispatcher) }
    val project by lazy { EditorProjectModel(uiScope, workDispatcher, contextFlow) }
    val dataSet by lazy { EditorDataSetModel(uiScope, workDispatcher, contextFlow) }

    val context: Context get() = _contextFlow.value
    val contextFlow: StateFlow<Context> get() = _contextFlow

    private val scope = uiScope + Job()

    private val _contextFlow = MutableStateFlow(Context("", ""))
    private val errorsContainer: ErrorsContainer get() = context.errors

    init {
        scope.launch {
            file.stateFlow.collect {
                val workDir = getParentDir(it?.path ?: "./")
                if (context.workDir != workDir) {
                    _contextFlow.emit(Context(workDir, "./tmp/"))
                }
            }
        }
        scope.launch {
            text.textStateFlow.collect {
                token.tokenize(it)
            }
        }
        scope.launch {
            token.stateFlow.collect {
                if (it.errors.isNullOrEmpty()) {
                    errorsContainer.clear()
                    project.parseProject(it.tokens)
                }
            }
        }
        scope.launch {
            project.stateFlow.collect {
                if (it.errors.isNullOrEmpty() && it.project != null) {
                    val dataSetStatus = dataSet.state.dataSetStatus
                    val dataSetChanged = it.project.dataSet != dataSet.state.factory ||
                            it.project.dataSetMultiply != dataSet.state.multiply

                    val isDataSetInvalid = dataSetChanged &&
                                (dataSetStatus == DataSetStatus.Loading ||
                                        dataSetStatus == DataSetStatus.Loaded)

                    if (isDataSetInvalid) {
                        dataSet.onDataSetStatusChanged(DataSetStatus.Invalid)
                    }
                }
            }
        }
    }

    private fun getParentDir(filePath: String): String {
        val slashIndex = filePath.lastIndexOf('/')
        return if (slashIndex == -1) {
            "./"
        } else {
            filePath.substring(0, slashIndex)
        }
    }
}