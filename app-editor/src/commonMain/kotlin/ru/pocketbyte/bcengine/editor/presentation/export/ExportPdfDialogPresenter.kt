package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.ProjectOutput
import ru.pocketbyte.bcengine.editor.domain.interactor.export.AbsExportPdfIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportPdfIteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import java.io.File

class ExportPdfDialogPresenter(
    uiScope: CoroutineScope,
    editorsSetModel: EditorsSetModel,
    messageDialogPresenter: MessageDialogPresenter,
    private val exportPdfIteractor: ExportPdfIteractor
): AbsExportDialogPresenter<ExportPdfDialogPresenter.State>(
    uiScope, editorsSetModel, messageDialogPresenter
) {

    data class State(
        override val index: Int,
        override val dir: String,
        override val file: String,
        override val progress: Int = -1,
        val orientation: Orientation
    ) : AbsExportDialogPresenter.State {
        override val fileEnabled = true
    }

    override fun copyCurrentState(dir: String?, file: String?): State? {
        return currentState?.copy(
            dir = dir ?: currentState?.dir ?: "",
            file = file ?: currentState?.file ?: ""
        )
    }

    override fun buildInitialState(index: Int): State? {
        return getEditor(index)?.project?.state?.project?.let { project ->
            State(
                index = index,
                dir = project.output.dir.get(project, null),
                file = project.output.pdf.get(project, null),
                orientation = Orientation.VERTICAL
            )
        }
    }

    fun onOrientationChange(orientation: Orientation) {
        currentState = currentState?.copy(
            orientation = orientation
        )
    }

    override fun onOkClick() {
        val state = currentState ?: return onCancelClick()
        val editor = getEditor(state.index) ?: return onCancelClick()
        val context = Context(editor.context)
        val project = editor.project.state.project ?: return onCancelClick()
        val output = ProjectOutput().apply {
            dir.formula = formulaStaticValue(state.dir)
            pdf.formula = formulaStaticValue(state.file)
        }
        currentState = currentState?.copy(
            progress = 0
        )

        val outputFile = File(
            File(context.workDir, output.dir.get(project, null)),
            output.pdf.get(project, null)
        )

        uiScope.launch {
            val result = exportPdfIteractor.export(
                outputFile.name,
                listOf(editor),
                pageOrientation = state.orientation,
                outputFilePath = outputFile.absolutePath
            ) { count, total ->
                currentState = currentState?.copy(
                    progress = (count.toFloat() / total.toFloat() * 100).toInt()
                )
            }
            currentState = null
            when (result) {
                AbsExportPdfIteractor.Result.Success -> {
                    messageDialogPresenter.showMessage(
                        null, "Export PDF done"
                    )
                }
                is AbsExportPdfIteractor.Result.ParseError -> {
                    messageDialogPresenter.showMessage(
                        "Failed to export PDF",
                        "Error: ${ErrorsContainer.buildErrorString(result.errors)}"
                    )
                }
                is AbsExportPdfIteractor.Result.ExceptionError -> {
                    messageDialogPresenter.showMessage(
                        "Failed to export PDF",
                        "Error: ${result.errors.message}"
                    )
                }
            }
        }
    }
}