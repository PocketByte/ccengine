package ru.pocketbyte.bcengine.editor.presentation.save

import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorFileModel

data class SaveTabState(
    val index: Int,
    val fileState: EditorFileModel.State?
)