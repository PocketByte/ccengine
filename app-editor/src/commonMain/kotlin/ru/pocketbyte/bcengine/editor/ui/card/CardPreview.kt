package ru.pocketbyte.bcengine.editor.ui.card

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Paint
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem

private val defaultPaint = Paint()

@Composable
fun CardPreview(
    modifier: Modifier = Modifier,
    tag: String,
    context: Context,
    project: Project,
    dataSetItem: DataSetItem,
    overlay: EditorOverlayPresenter.State,
    composeComponent: ComposeComponent,
    reDraw: Boolean = true
) {
    var isReDrawn = false
    Canvas(modifier = modifier) {
        val bceCanvas = composeComponent.getMutableCanvas(tag)

        if (reDraw && !isReDrawn) {
            bceCanvas.setLayout(project.layout)
            bceCanvas.setContext(context)
            project.measureAndDraw(context, bceCanvas, dataSetItem)

            if (overlay.drawDebug) {
                project.drawDebugShapeBorders(context, bceCanvas, dataSetItem)
            }

            isReDrawn = true
        }

        bceCanvas.bitmap?.let { bitmap ->
            val composeCanvas = drawContext.canvas
            val scaleX = drawContext.size.width / bitmap.width
            val scaleY = drawContext.size.height / bitmap.height
            val scale = minOf(scaleX, scaleY)
            val offsetX = (drawContext.size.width - bitmap.width * scale) / 2f
            val offsetY = (drawContext.size.height - bitmap.height * scale) / 2f

            composeCanvas.translate(offsetX, offsetY)
            composeCanvas.scale(scale, scale)
            composeCanvas.drawImage(bitmap, Offset.Zero, defaultPaint)
        }
    }
}