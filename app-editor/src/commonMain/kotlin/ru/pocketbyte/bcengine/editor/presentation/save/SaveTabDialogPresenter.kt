package ru.pocketbyte.bcengine.editor.presentation.save

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.SaveEditorInteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel

class SaveTabDialogPresenter(
    private val scope: CoroutineScope,
    private val editorsSetModel: EditorsSetModel,
    private val saveEditorInteractor: SaveEditorInteractor
) : SaveDialogActions {

    val stateFlow: StateFlow<SaveTabState?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<SaveTabState?>(null)
    private val currentState: SaveTabState? get() = _stateFlow.value

    fun showDialog(index: Int) {
        _stateFlow.value = SaveTabState(
            index = index,
            fileState = getEditor(index)?.file?.state
        )
    }

    override fun onCloseClick() {
        _stateFlow.value = null
    }

    override fun onSaveClick() {
        getEditor(currentState?.index)?.let {
            scope.launch {
                saveEditorInteractor.save(it)
                closeTab(it)
            }
        }
        _stateFlow.value = null
    }

    override fun onForgetClick() {
        closeTab(getEditor(currentState?.index))
        _stateFlow.value = null
    }

    private fun closeTab(editor: EditorModel?) {
        if (editor == null) return
        editorsSetModel.removeEditorModel(editor)
    }

    private fun getEditor(index: Int?): EditorModel? {
        if (index == null) return null
        return editorsSetModel.stateFlow.value.editors.getOrNull(index)
    }
}