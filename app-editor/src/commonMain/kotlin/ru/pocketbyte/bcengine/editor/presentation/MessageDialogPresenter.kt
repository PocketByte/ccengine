package ru.pocketbyte.bcengine.editor.presentation

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MessageDialogPresenter {

    data class State(
        val title: String?,
        val message: String
    )

    val stateFlow: StateFlow<State?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<State?>(null)

    fun showMessage(title: String?, message: String) {
        _stateFlow.value = State(title, message)
    }

    fun dismissMessage() {
        _stateFlow.value = null
    }
}