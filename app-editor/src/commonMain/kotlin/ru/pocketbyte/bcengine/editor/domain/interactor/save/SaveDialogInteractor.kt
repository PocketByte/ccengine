package ru.pocketbyte.bcengine.editor.domain.interactor.save

import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import java.awt.Frame
import java.io.File

class SaveDialogInteractor(
    private val composeComponent: ComposeComponent
) {
    suspend fun saveAs(
        title: String,
        filePath: String?,
        fallbackName: String,
        completion: suspend (file: File) -> Unit
    ) {
        java.awt.FileDialog(
            composeComponent.window as Frame,
            title,
            java.awt.FileDialog.SAVE
        ).let {
            val oldFile = filePath?.let { path -> File(path) }
            it.file = oldFile?.name ?: fallbackName
            it.directory = oldFile?.parent
            it.isVisible = true

            val directory = it.directory
            val fileName = it.file
            if (directory != null && fileName != null) {
                completion(File(directory, fileName))
            }
        }
    }
}