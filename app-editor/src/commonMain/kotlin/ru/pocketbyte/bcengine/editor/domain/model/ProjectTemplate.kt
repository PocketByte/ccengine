package ru.pocketbyte.bcengine.editor.domain.model

private const val sampleTextProject = ""+
        "[project]\n" +
        "DATASET(empty, 1)\n" +
        "LAYOUT(2.5inch, 3.5inch, #FFFFFF, )\n" +
        "[view]\n" +
        "TEXT(0%, 0%, 100%, 100%, \"Hello BCEngine\", ;24sp;, , center, center, ,)"


private const val imgTag = "<img " +
        "src=\"./photo.png\" " +
        "align=\"bottom\"" +
        "width=\"70\"" +
        "height=\"70\"" +
        ">"

private const val sampleTextLine1 = "" +
        "<b>Bold<sup>sup</sup></b> <i>Italic<sub>sub</sub></i> <u>Underline</u> <s>Strike</s>"

private const val sampleTextLine2 = "" +
        "<font size=\"50\" color=\"#0000FF\">Line</font> with $imgTag icon."

private const val sampleTextWithImageProject = "" +
        "[project]\n" +
        "DATASET(empty, 1)\n" +
        "LAYOUT(5.5inch, 3.5inch, #FFFFFF, 96)\n" +
        "PARAM(LINE1, str, \"$sampleTextLine1\") \n" +
        "PARAM(LINE2, str, \"$sampleTextLine2\")\n" +
        "FUN(TEXT, str, \$LINE1 + \"<br><br>\" + \$LINE2)\n" +
        "ICON(hany, \"./photo.png\", 100sp, 100sp)\n" +
        "FUN(H_ICONS, str_list, split(\"hany,hany\";\",\"))\n" +
        "[view]\n" +
        "TEXT(10%, 10%, 80%, 80%, \$TEXT, ;24sp, , center, center, ,)\n" +
        "ICONS(,,50%,50%, \$H_ICONS)"

const val ProjectTemplate = sampleTextWithImageProject