package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.interactor.model.EditorModelTokenizeInteractor
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class EditorTokenModel(
    uiScope: CoroutineScope,
    workDispatcher: CoroutineDispatcher,
) {

    data class State(
        val tokens: List<ProjectToken>,
        val errors: List<ProjectError>?
    )

    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(
        State(emptyList(), null)
    )

    private val tokenizeInteractor = EditorModelTokenizeInteractor(
        this, uiScope, workDispatcher
    )

    fun tokenize(text: String) {
        tokenizeInteractor.tokenizeTextAndPublish(text)
    }

    fun onTokenParsed(tokens: List<ProjectToken>) {
        _stateFlow.value = State(tokens, null)
    }

    fun onTokenParseError(errors: List<ProjectError>) {
        _stateFlow.value = _stateFlow.value.copy(
            errors = errors
        )
    }
}