package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.ProjectOutput
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportImageIteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.formula.formulaStaticValue

class ExportImageDialogPresenter(
    uiScope: CoroutineScope,
    editorsSetModel: EditorsSetModel,
    messageDialogPresenter: MessageDialogPresenter,
    private val exportImageIteractor: ExportImageIteractor
): AbsExportDialogPresenter<ExportImageDialogPresenter.State>(
    uiScope, editorsSetModel, messageDialogPresenter
) {

    data class State(
        override val index: Int,
        override val dir: String,
        override val file: String,
        override val progress: Int = -1
    ) : AbsExportDialogPresenter.State {
        override val fileEnabled = false
    }

    override fun copyCurrentState(dir: String?, file: String?): State? {
        return currentState?.copy(
            dir = dir ?: currentState?.dir ?: "",
            file = file ?: currentState?.file ?: ""
        )
    }

    override fun buildInitialState(index: Int): State? {
        return getEditor(index)?.project?.state?.project?.let { project ->
            State(
                index = index,
                dir = project.output.dir.get(project, null),
                file = project.output.image.getFormulaString()
            )
        }
    }

    override fun onOkClick() {
        val state = currentState ?: return onCancelClick()
        val editor = getEditor(state.index) ?: return onCancelClick()
        val project = editor.project.state.project ?: return onCancelClick()
        val output = ProjectOutput().apply {
            dir.formula = formulaStaticValue(state.dir)
            image.formula = project.output.image.formula ?: return
        }
        currentState = currentState?.copy(
            progress = 0
        )

        uiScope.launch {
            val result = exportImageIteractor.export(editor, output) { count, total ->
                currentState = currentState?.copy(
                    progress = (count.toFloat() / total.toFloat() * 100).toInt()
                )
            }
            currentState = null
            when (result) {
                ExportImageIteractor.Result.Success -> {
                    messageDialogPresenter.showMessage(
                        null, "Export PNG done"
                    )
                }
                is ExportImageIteractor.Result.ParseError -> {
                    messageDialogPresenter.showMessage(
                        "Failed to export PNG",
                        "Error: ${ErrorsContainer.buildErrorString(result.errors)}"
                    )
                }
            }
        }
    }
}