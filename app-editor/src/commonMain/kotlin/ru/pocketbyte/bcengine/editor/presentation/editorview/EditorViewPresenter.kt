package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.*
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.presentation.doc.ParametrisedProcessorDocDialogPresenter

class EditorViewPresenter(
    uiScope: CoroutineScope,
    val workDispatcher: CoroutineDispatcher,
    val composeComponent: ComposeComponent,
    val editorModel: EditorModel,
    docPresenter: ParametrisedProcessorDocDialogPresenter,
) {

    private val scope = uiScope + Job()

    val text = EditorTextViewPresenter(scope, workDispatcher, editorModel)
    val dataSet = EditorDataSetViewPresenter(scope, composeComponent, workDispatcher, editorModel)
    val project = EditorProjectViewPresenter(scope, workDispatcher, editorModel)
    val helpBar = HelpBarViewPresenter(scope, editorModel, docPresenter)

    fun recycle() {
        scope.cancel()
        composeComponent.recycleCanvas(editorModel.tag)
    }
}