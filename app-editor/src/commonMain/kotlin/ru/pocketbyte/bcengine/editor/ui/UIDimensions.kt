package ru.pocketbyte.bcengine.editor.ui

import androidx.compose.ui.unit.dp

object UIDimensions {
    val paddingSmall = 8.dp
    val paddingNormal = 16.dp
    val paddingLarge = 24.dp

    val scrollbarPadding = 12.dp

    val cornerRadiusNormal = 8.dp

    val borderNormal = 1.dp
}