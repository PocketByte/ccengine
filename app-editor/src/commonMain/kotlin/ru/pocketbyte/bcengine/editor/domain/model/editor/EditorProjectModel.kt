package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.interactor.model.EditorModelProjectParseInteractor
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class EditorProjectModel(
    uiScope: CoroutineScope,
    workDispatcher: CoroutineDispatcher,
    val contextFlow: StateFlow<Context>
) {

    data class State(
        val project: Project?,
        val errors: List<ProjectError>?
    )

    val state: State get() = stateFlow.value
    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(
        State(null, null)
    )

    private val projectParseInteractor = EditorModelProjectParseInteractor(
        this, uiScope, workDispatcher
    )

    fun parseProject(tokens: List<ProjectToken>) {
        projectParseInteractor.parseProjectAndPublish(contextFlow.value, tokens)
    }

    fun onProjectParsed(project: Project) {
        _stateFlow.value = State(project, null)
    }

    fun onProjectParseError(errors: List<ProjectError>) {
        _stateFlow.value = _stateFlow.value.copy(
            errors = errors
        )
    }
}