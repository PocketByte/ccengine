package ru.pocketbyte.bcengine.editor.ui.editor

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTextViewPresenter
import ru.pocketbyte.bcengine.editor.ui.style.EditorStyle
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.*
import kotlin.math.min

object EditorTextFieldValueBuilder {

    fun buildTextFieldValue(
        textState: EditorTextViewPresenter.State
    ): TextFieldValue {
        return TextFieldValue(
            AnnotatedString(
                textState.text,
                buildStylesList(textState.tokens, textState.text.length)
            ),
            TextRange(textState.selection.start, textState.selection.end)
        )
    }

    private fun buildStylesList(
        tokens: List<ProjectToken>, maxIndex: Int
    ): List<AnnotatedString.Range<SpanStyle>> {
        return mutableListOf<AnnotatedString.Range<SpanStyle>>().apply {
            fillStylesList(null, tokens, maxIndex, this)
        }
    }

    private fun fillStylesList(
        parentToken: NamedGroupToken?,
        tokens: List<ProjectToken>,
        maxIndex: Int,
        result: MutableList<AnnotatedString.Range<SpanStyle>>
    ) {
        tokens.forEach { token ->
            when (token) {
                is CommentToken -> {
                    result.add(buildSafeRange(
                        EditorStyle.DEFAULT.comment,
                        token.startIndex, token.endIndex + 1,
                        maxIndex
                    ))
                }
                is NamedGroupToken -> {
                    if (parentToken == null && token.bracers == NamedGroupToken.Bracers.SQUARE) {
                        result.add(buildSafeRange(
                            EditorStyle.DEFAULT.group,
                            token.startIndex, token.endIndex + 1,
                            maxIndex
                        ))
                    } else {
                        if (token.getMetaData(Token.METADATA_PROCESSOR) != null) {
                            result.add(
                                buildSafeRange(
                                    EditorStyle.DEFAULT.shapeName,
                                    token.startIndex, token.startIndex + token.name.length,
                                    maxIndex
                                )
                            )
                        }
                        fillStylesList(token, token.subTokens, maxIndex, result)
                    }
                }
                is StringToken -> {
                    result.add(buildSafeRange(
                        EditorStyle.DEFAULT.string,
                        token.startIndex, token.endIndex + 1,
                        maxIndex
                    ))
                }
                is WordToken -> {
                    // TODO refactor with metadata
                    if (token.value.getOrNull(0) == '$') {
                        result.add(buildSafeRange(
                            EditorStyle.DEFAULT.function,
                            token.startIndex, token.endIndex + 1,
                            maxIndex
                        ))
                    }
                }
                is SeparatorToken -> {
                    if (parentToken != null) {
                        result.add(buildSafeRange(
                            EditorStyle.DEFAULT.shapeSeparator,
                            token.startIndex, token.endIndex + 1,
                            maxIndex
                        ))
                    }
                }
                is EmptyStringToken,
                is OperatorToken,
                is NamedPropertyToken -> {
                    // Do nothing
                }
            }
        }
    }

    private fun buildSafeRange(
        item: SpanStyle, start: Int, end: Int, maxIndex: Int
    ): AnnotatedString.Range<SpanStyle> {
        return AnnotatedString.Range(item,
            min(start, maxIndex), min(end, maxIndex)
        )
    }
}
