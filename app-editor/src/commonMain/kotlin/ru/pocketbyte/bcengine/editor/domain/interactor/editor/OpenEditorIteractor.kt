package ru.pocketbyte.bcengine.editor.domain.interactor.editor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import ru.pocketbyte.bcengine.editor.domain.interactor.textfile.OpenTextFileInteractor
import ru.pocketbyte.bcengine.editor.domain.model.RecentModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.domain.model.asRecentModelItem
import java.io.File

class OpenEditorIteractor(
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
    private val editorsSetModel: EditorsSetModel,
    private val recentModel: RecentModel,
    private val openTextFileInteractor: OpenTextFileInteractor
) {

    suspend fun open(
        setActive: Boolean = false
    ) {
        openTextFileInteractor.open { file, text ->
            addEditorModel(file, text, setActive)
        }
    }

    suspend fun open(
        filePath: String,
        setActive: Boolean = false
    ) {
        openTextFileInteractor.open(filePath) { file, text ->
            addEditorModel(file, text, setActive)
        }
    }

    private fun addEditorModel(file: File, text: String, setActive: Boolean) {
        editorsSetModel.addEditorModel(
            EditorModel(uiScope, workDispatcher).apply {
                this.text.onTextChanged(text)
                this.file.onFileStateChanged(file.name, file.absolutePath)
                this.text.rememberTextState()
            }, setActive = setActive
        )
        recentModel.add(file.asRecentModelItem(), onTop = true)
    }
}
