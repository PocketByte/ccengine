package ru.pocketbyte.bcengine.editor.presentation.menu

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.stateIn
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.presentation.menu.MenuPresenter.ItemState

class HelpMenuPresenter(
    private val uiScope: CoroutineScope,
    private val str: StringRepository
) : MenuPresenter {

    companion object {
        private const val ITEM_TAG_HELP = "Help"
        private const val ITEM_TAG_HELP_DOCS = "$ITEM_TAG_HELP:docs"
    }

    private val state = ItemState.Group(
        str.menu_help_title,
        items = listOf(
            ItemState.Option(str.menu_help_docs, tag = ITEM_TAG_HELP_DOCS)
        )
    )

    override val stateFlow: StateFlow<ItemState.Group> = flowOf<ItemState.Group>()
        .stateIn(uiScope, SharingStarted.Eagerly, state)

    override fun onOptionClick(item: ItemState) {
        when (item.tag) {
            ITEM_TAG_HELP_DOCS -> openDocs()
        }
    }

    private fun openDocs() {

    }
}