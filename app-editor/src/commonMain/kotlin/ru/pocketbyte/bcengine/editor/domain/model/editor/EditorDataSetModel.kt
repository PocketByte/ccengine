package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.interactor.model.EditorModelDataSetLoadInteractor
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.DataSet
import ru.pocketbyte.bcengine.dataset.DataSetFactory
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.dataset.getOrNull
import ru.pocketbyte.bcengine.errors.ProjectError

class EditorDataSetModel(
    uiScope: CoroutineScope,
    workDispatcher: CoroutineDispatcher,
    val contextFlow: StateFlow<Context>
) {

    data class State(
        val factory: DataSetFactory? = null,
        val multiply: Int = 1,
        val dataSet: DataSet? = null,
        val currentItemIndex: Int = 0,
        val dataSetStatus: DataSetStatus = DataSetStatus.NotReady,
        val errors: List<ProjectError>? = null
    ) {
        fun getCurrentDataSetItem(): DataSetItem? {
            return dataSet?.getOrNull(currentItemIndex)
        }
    }

    val state: State get() = stateFlow.value
    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(State())

    private val dataSetLoadInteractor = EditorModelDataSetLoadInteractor(
        this, uiScope, workDispatcher
    )

    fun loadDataset(project: Project) {
        _stateFlow.value = state.copy(
            factory = project.dataSet,
            multiply = project.dataSetMultiply
        )
        dataSetLoadInteractor.loadDatasetAndPublish(contextFlow.value, project)
    }

    fun onDataSetStatusChanged(newStatus: DataSetStatus) {
        if (newStatus == DataSetStatus.Invalid) {
            dataSetLoadInteractor.cancel()
        }
        _stateFlow.value = state.copy(
            dataSetStatus = newStatus
        )
    }

    fun onDataSetLoaded(dataSet: DataSet) {
        val itemsCount = dataSet.size
        val newIndex = if (state.currentItemIndex >= itemsCount) {
            itemsCount - 1
        } else {
            state.currentItemIndex
        }
        _stateFlow.value = state.copy(
            dataSet = dataSet,
            currentItemIndex = newIndex,
            dataSetStatus = DataSetStatus.Loaded,
            errors = null
        )
    }

    fun onDataSetLoadError(errors: List<ProjectError>) {
        _stateFlow.value = state.copy(
            dataSetStatus = DataSetStatus.NotReady,
            errors = errors
        )
    }

    fun setCurrentItemIndex(newIndex: Int) {
        val itemsCount = state.dataSet?.size ?: return
        if (newIndex in 0 until itemsCount) {
            _stateFlow.value = state.copy(
                currentItemIndex = newIndex
            )
        }
    }
}