package ru.pocketbyte.bcengine.editor.domain

import ru.pocketbyte.bcengine.canvas.CanvasFactory
import ru.pocketbyte.bcengine.compose.canvas.MutableComposeCanvas

interface ComposeComponent {
    val window: Any
    val canvasFactory: CanvasFactory
    fun getMutableCanvas(tag: String) : MutableComposeCanvas
    fun recycleCanvas(tag: String)
    fun invalidateCache()
}