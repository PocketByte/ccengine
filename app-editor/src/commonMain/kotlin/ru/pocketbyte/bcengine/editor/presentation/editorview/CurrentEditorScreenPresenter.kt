package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.doc.ParametrisedProcessorDocDialogPresenter

class CurrentEditorScreenPresenter(
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
    private val composeComponent: ComposeComponent,
    private val editorsSetModel: EditorsSetModel,
    private val docPresenter: ParametrisedProcessorDocDialogPresenter,
) {

    private var editorViewPresenters: List<EditorViewPresenter> = listOf()

    val currentEditorFlow: StateFlow<EditorViewPresenter?> get() = _currentEditorFlow
    private val _currentEditorFlow = MutableStateFlow<EditorViewPresenter?>(null)

    init {
        uiScope.launch {
            editorsSetModel.stateFlow.collect {
                onEditorStateChanged(it)
            }
        }
    }

    private suspend fun onEditorStateChanged(state: EditorsSetModel.State) {
        val reusePresenters: MutableList<EditorViewPresenter> = editorViewPresenters.toMutableList()
        editorViewPresenters = state.editors.map { model ->
            reusePresenters.indexOfFirst { it.editorModel === model }.let {
                if (it >=0 ) {
                    reusePresenters.removeAt(it)
                } else {
                    null
                }
            } ?: EditorViewPresenter(
                uiScope, workDispatcher, composeComponent,
                model, docPresenter
            )
        }

        _currentEditorFlow.emit(
            editorViewPresenters.getOrNull(state.activeEditorIndex)
        )

        reusePresenters.forEach {
            it.recycle()
        }
    }
}