package ru.pocketbyte.bcengine.editor.presentation.save

interface SaveDialogActions {
    fun onCloseClick()
    fun onSaveClick()
    fun onForgetClick()
}