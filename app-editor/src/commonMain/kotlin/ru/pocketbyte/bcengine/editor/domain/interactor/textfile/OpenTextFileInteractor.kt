package ru.pocketbyte.bcengine.editor.domain.interactor.textfile

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.StringRepository
import java.awt.Frame
import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class OpenTextFileInteractor(
    private val workDispatcher: CoroutineDispatcher,
    private val composeComponent: ComposeComponent,
    private val stringRepository: StringRepository
) {

    suspend fun open(openAction: (file: File, text: String) -> Unit) {
        java.awt.FileDialog(
            composeComponent.window as Frame,
            stringRepository.dialog_open_title,
            java.awt.FileDialog.LOAD
        ).let {
            it.isVisible = true

            val directory = it.directory
            val fileName = it.file
            if (directory != null && fileName != null) {
                open(File(directory, fileName), openAction)
            }
        }
    }

    suspend fun open(filePath: String, openAction: (file: File, text: String) -> Unit) {
        open(File(filePath), openAction)
    }

    suspend fun open(file: File, openAction: (file: File, text: String) -> Unit) {
        withContext(workDispatcher) {
            val bytes = try {
                Files.readAllBytes(Paths.get(file.absolutePath))
            } catch (e: Exception) {
                // TODO show error
                return@withContext
            }
            openAction(file, String(bytes, StandardCharsets.UTF_8))
        }
    }
}