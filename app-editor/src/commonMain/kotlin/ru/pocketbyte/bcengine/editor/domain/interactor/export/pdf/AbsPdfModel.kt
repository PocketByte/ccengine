package ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf

abstract class AbsPdfModel(
    val id: String,
    val documentName: String,
    protected val paddingLeft: Float,
    protected val paddingRight: Float,
    protected val paddingTop: Float,
    protected val paddingBottom: Float,
    protected val horizontalSpace: Float,
    protected val verticalSpace: Float
) {

    enum class PageSize {
        A0, A1, A2, A3, A4, A5, A6
    }

    abstract val pageHeight: Float
    abstract val pageWidth: Float
    abstract val dpi: Float

    abstract fun saveAndClose(filePath: String)

    protected abstract fun prepareNextPage()

    protected abstract fun drawCard(
        imagePath: String,
        positionX: Float,
        positionY: Float,
        cardWidth: Float,
        cardHeight: Float
    )

    private var positionX: Float = -Float.MAX_VALUE
    private var positionY: Float = -Float.MAX_VALUE
    private var rowHeight: Float = 0f

    final fun drawCard(imagePath: String, cardWidth: Float, cardHeight: Float) {
        initIIfNeeded()
        if (positionX + cardWidth + paddingRight > pageWidth) {
            positionX = paddingLeft
            positionY -= rowHeight + verticalSpace
            if (positionY - cardHeight - paddingBottom < 0) {
                positionY = pageHeight - paddingTop
                prepareNextPage()
            }
            rowHeight = 0f
        }

        drawCard(imagePath, positionX, positionY - cardHeight, cardWidth, cardHeight)

        positionX += horizontalSpace + cardWidth
        rowHeight = maxOf(rowHeight, cardHeight)
    }

    private fun initIIfNeeded() {
        if (positionX < 0) { positionX = pageWidth }
        if (positionY < 0) { positionY = 0f }
    }
}