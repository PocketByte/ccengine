package ru.pocketbyte.bcengine.editor.presentation.menu

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.OpenEditorIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.SaveEditorInteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.save.SaveDialogInteractor
import ru.pocketbyte.bcengine.editor.domain.model.RecentModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportAllPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportImageDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportSingleImageDialogPresenter

class MainMenuPresenter(
    uiScope: CoroutineScope,
    stringRepository: StringRepository,
    workDispatcher: CoroutineDispatcher,
    editorsSetModel: EditorsSetModel,
    recentModel: RecentModel,
    editorOverlayPresenter: EditorOverlayPresenter,
    openEditorIteractor: OpenEditorIteractor,
    saveEditorInteractor: SaveEditorInteractor,
    exportImageDialogPresenter: ExportImageDialogPresenter,
    exportSingleImageDialogPresenter: ExportSingleImageDialogPresenter,
    exportPdfDialogPresenter: ExportPdfDialogPresenter,
    exportAllPdfDialogPresenter: ExportAllPdfDialogPresenter
) {
    val menuGroups = listOf(
        FileMenuPresenter(
            uiScope, stringRepository, workDispatcher, editorsSetModel, recentModel,
            openEditorIteractor, saveEditorInteractor,
            exportImageDialogPresenter, exportSingleImageDialogPresenter,
            exportPdfDialogPresenter, exportAllPdfDialogPresenter
        ),
        ViewMenuPresenter(uiScope, stringRepository, editorOverlayPresenter),
        HelpMenuPresenter(uiScope, stringRepository)
    )
}