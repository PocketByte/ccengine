package ru.pocketbyte.bcengine.editor.domain.model

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.Files
import ru.pocketbyte.bcengine.editor.domain.repository.StringListFileRepository
import java.io.File

class RecentModel(
    private val repository: StringListFileRepository = StringListFileRepository(Files.recent)
) {

    data class Item(
        val title: String,
        val path: String
    )

    val listFlow: StateFlow<List<Item>> get() = _listFlow

    private val _listFlow = MutableStateFlow(
        repository.loadFromFile().map {
            File(it).asRecentModelItem()
        }
    )

    fun add(item: Item, onTop: Boolean) {
        val currentState = _listFlow.value
        val fileIndex = currentState.indexOf(item)

        if (fileIndex >= 0) {
            if (onTop) {
                changeState(currentState.moveToTop(fileIndex))
            }
        } else {
            changeState(currentState.addToTop(item))
        }
    }

    fun remove(item: Item) {
        changeState(
            _listFlow.value.minusElement(item)
        )
    }

    fun clear() {
        changeState(emptyList())
    }

    private fun changeState(newState: List<Item>) {
        _listFlow.value = newState
        repository.saveToFile(newState) { it.path }
    }

    private fun List<Item>.addToTop(value: Item): List<Item> {
        return List(size + 1) {
            if (it == 0) {
                value
            } else {
                get(it - 1)
            }
        }
    }

    private fun List<Item>.moveToTop(index: Int): List<Item> {
        return List(size) {
            when {
                it == 0 -> get(index)
                it <= index -> get(it -1)
                else -> get(it)
            }
        }
    }
}