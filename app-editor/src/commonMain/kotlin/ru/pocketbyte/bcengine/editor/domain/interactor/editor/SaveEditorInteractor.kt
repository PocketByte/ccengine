package ru.pocketbyte.bcengine.editor.domain.interactor.editor

import ru.pocketbyte.bcengine.editor.domain.interactor.textfile.SaveTextFileInteractor
import ru.pocketbyte.bcengine.editor.domain.model.RecentModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.asRecentModelItem
import java.io.File

class SaveEditorInteractor(
    private val recentModel: RecentModel,
    private val saveTextFileInteractor: SaveTextFileInteractor
) {

    suspend fun save(
        activeEditor: EditorModel,
        completion: (file: File, editorModel: EditorModel) -> Unit = { _, _ -> }
    ) {
        val activeEditorFile = activeEditor.file.state
        if (activeEditorFile == null) {
            saveAs(activeEditor)
        } else {
            saveTextFileInteractor.save(
                activeEditor.text.textState,
                activeEditorFile.path
            ) { file ->
                onEditorSaved(file, activeEditor, completion)
            }
        }
    }

    suspend fun saveAs(
        activeEditor: EditorModel,
        completion: (file: File, editorModel: EditorModel) -> Unit = { _, _ -> }
    ) {
        saveTextFileInteractor.saveAs(
            activeEditor.text.textState,
            activeEditor.file.state?.path
        ) { file ->
            onEditorSaved(file, activeEditor, completion)
        }
    }

    private fun onEditorSaved(
        file: File, activeEditor: EditorModel,
        completion: (file: File, editorModel: EditorModel) -> Unit
    ) {
        activeEditor.file.onFileStateChanged(file.name, file.absolutePath)
        activeEditor.text.rememberTextState()
        recentModel.add(file.asRecentModelItem(), onTop = false)
    }
}