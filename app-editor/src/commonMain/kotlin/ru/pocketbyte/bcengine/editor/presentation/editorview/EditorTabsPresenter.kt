package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.save.SaveTabDialogPresenter

class EditorTabsPresenter(
    private val scope: CoroutineScope,
    private val editorsSetModel: EditorsSetModel,
    private val saveTabDialogPresenter: SaveTabDialogPresenter
) {
    data class TabViewState(
        val title: String,
        val isEdited: Boolean,
        val isSelected: Boolean
    )

    val stateFlow: StateFlow<List<TabViewState>> get() = _stateFlow

    private val _stateFlow = MutableStateFlow<List<TabViewState>>(emptyList())
    private var stateCollectionJob: Job? = null

    init {
        scope.launch {
            editorsSetModel.stateFlow.collect { editorState ->
                if (editorState.editors.isEmpty()) {
                    _stateFlow.value = emptyList()
                    return@collect
                }
                val flow = combine(editorState.editors.map { it.file.stateFlow }) {
                    it.mapIndexed { index, fileState ->
                        if (fileState != null) {
                            TabViewState(
                                fileState.title,
                                fileState.isEdited,
                                editorState.activeEditorIndex == index
                            )
                        } else {
                            TabViewState(
                                "(New)",
                                true,
                                editorState.activeEditorIndex == index
                            )
                        }
                    }
                }
                stateCollectionJob?.cancel()
                stateCollectionJob = scope.launch {
                    flow.collect {
                        _stateFlow.value = it
                    }
                }
            }
        }
    }

    fun onTabSelected(index: Int) {
        scope.launch {
            editorsSetModel.setActiveEditor(index)
        }
    }

    fun onTabClosed(index: Int) {
        scope.launch {
            getEditor(index)?.let { editor ->
                if (editor.file.state?.isEdited == false) {
                    editorsSetModel.removeEditorModel(editor)
                } else {
                    saveTabDialogPresenter.showDialog(index)
                }
            }
        }
    }

    private fun getEditor(index: Int): EditorModel? {
        return editorsSetModel.stateFlow.value.editors.getOrNull(index)
    }
}
