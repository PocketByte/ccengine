package ru.pocketbyte.bcengine.editor.ui.dataset

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus.Invalid
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus.Loaded
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus.Loading
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus.NotReady
import ru.pocketbyte.bcengine.editor.presentation.editorview.DataSetActions
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorDataSetViewPresenter
import ru.pocketbyte.bcengine.editor.ui.Str
import ru.pocketbyte.bcengine.editor.ui.UIDimensions

@Composable
fun DataSetStatusView(
    modifier: Modifier,
    state: EditorDataSetViewPresenter.State,
    dataSetActions: DataSetActions
) {
    Row(
        modifier = modifier
            .border(UIDimensions.borderNormal, Color.Gray)
            .padding(UIDimensions.paddingNormal),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(UIDimensions.paddingNormal)
    ) {
        val statusText = when (state.dataSetState) {
            NotReady -> Str.screen_editor_dataset_not_loaded
            Loading -> Str.screen_editor_dataset_loading
            Invalid -> Str.screen_editor_dataset_changed
            Loaded -> Str.screen_editor_dataset_item_index(
                state.currentItemIndex + 1L,
                state.dataSet?.size?.toLong() ?: 0L
            )
        }

        when (state.dataSetState) {
            NotReady, Invalid -> {
                Text(
                    state.dataSetErrorMessage ?: statusText, Modifier.weight(1f),
                    color = MaterialTheme.colors.onSurface
                )
                Button(dataSetActions::loadDataSet) {
                    Text(if (state.dataSet == null) {
                        Str.screen_editor_dataset_btn_load
                    } else {
                        Str.screen_editor_dataset_btn_refresh
                    })
                }
            }
            Loading -> {
                CircularProgressIndicator()
                Text(statusText, Modifier.weight(1f), color = MaterialTheme.colors.onSurface)
                Button(dataSetActions::loadDataSet, enabled = false) {
                    Text(Str.screen_editor_dataset_btn_refresh)
                }
            }
            Loaded -> {
                Button(
                    dataSetActions::firstDataSetItem,
                    modifier = Modifier.weight(1f),
                    enabled = state.previousItemEnabled,
                    contentPadding = PaddingValues(8.dp, 8.dp)
                ) {
                    Text("<<")
                }
                Button(
                    dataSetActions::previousDataSetItem,
                    modifier = Modifier.weight(1f),
                    enabled = state.previousItemEnabled,
                    contentPadding = PaddingValues(8.dp, 8.dp)
                ) {
                    Text("<")
                }
                Text(
                    statusText,
                    modifier = Modifier.weight(1f),
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onSurface
                )
                Button(
                    dataSetActions::nextDataSetItem,
                    modifier = Modifier.weight(1f),
                    enabled = state.nextItemEnabled,
                    contentPadding = PaddingValues(8.dp, 8.dp)
                ) {
                    Text(">")
                }
                Button(
                    dataSetActions::lastDataSetItem,
                    modifier = Modifier.weight(1f),
                    enabled = state.nextItemEnabled,
                    contentPadding = PaddingValues(8.dp, 8.dp)
                ) {
                    Text(">>")
                }
                Spacer(Modifier.weight(0.1f))
                Button(dataSetActions::loadDataSet) {
                    Text(Str.screen_editor_dataset_btn_refresh)
                }
            }
        }
    }
}
