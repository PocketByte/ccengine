package ru.pocketbyte.bcengine.editor.presentation

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainScreenPresenter(

) {

    data class State(
        val windowWidth: Int,
        val windowHeight: Int
    )

    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(State(0, 0))

    fun onWindowSizeChanged(width: Int, height: Int) {

    }

}