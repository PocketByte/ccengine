package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.EmptyCanvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.dataset.getOrNull
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.errors.ProjectError

class EditorProjectViewPresenter(
    uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
    private val editorModel: EditorModel
) {

    data class State(
        val tag: String,
        val context: Context,
        val project: Project? = null,

        val projectErrorMessage: String? = null,
    )

    val stateFlow: StateFlow<State> get() = _stateFlow
    private val _stateFlow = MutableStateFlow(
        State(editorModel.tag, editorModel.context)
    )

    private val scope = uiScope + Job()
    private val emptyCanvas = EmptyCanvas()

    init {
        scope.launch {
            editorModel.token.stateFlow.collect {
                val errors = it.errors
                if (!errors.isNullOrEmpty()) {
                    _stateFlow.emit(
                        _stateFlow.value.copy(
                            projectErrorMessage = ErrorsContainer
                                .buildErrorString(errors) ?: ""
                        )
                    )
                }
            }
        }
        scope.launch {
            editorModel.project.stateFlow.collect {
                if (!it.errors.isNullOrEmpty()) {
                    _stateFlow.emit(
                        _stateFlow.value.copy(
                            projectErrorMessage = ErrorsContainer
                                .buildErrorString(it.errors) ?: ""
                        )
                    )
                } else if (it.project != null) {
                    _stateFlow.emit(
                        _stateFlow.value.copy(
                            project = it.project,
                            projectErrorMessage = checkProject(it.project)
                        )
                    )
                }
            }
        }
        scope.launch {
            editorModel.dataSet.stateFlow.collect {
                _stateFlow.emit(_stateFlow.value.copy(
                    projectErrorMessage = checkProject(
                        _stateFlow.value.project, it.dataSet?.getOrNull(it.currentItemIndex)
                    )
                ))
            }
        }
        scope.launch {
            editorModel.contextFlow.collect {
                _stateFlow.emit(_stateFlow.value.copy(
                    context = it
                ))
            }
        }
    }


    private fun checkProject(project: Project?, dataSetItem: DataSetItem? = null): String? {
        val context = Context(editorModel.context)
        try {
            project?.measureAndDraw(
                context,
                emptyCanvas,
                dataSetItem ?: editorModel.dataSet.stateFlow.value.getCurrentDataSetItem() ?: return null
            )
        } catch (e: Throwable) {
            context.errors.addError(
                ProjectError.ExceptionError(e)
            )
        }
        return context.errors.buildErrorString()
    }
}