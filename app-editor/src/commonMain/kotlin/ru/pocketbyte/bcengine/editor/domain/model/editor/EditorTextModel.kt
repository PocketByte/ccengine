package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EditorTextModel(
    private val editorFileModel: EditorFileModel
) {

    data class SelectionState(
        val start: Int,
        val end: Int
    )

    val textState: String get() = _textStateFlow.value
    val textStateFlow: StateFlow<String>  get() = _textStateFlow
    private val _textStateFlow = MutableStateFlow("")

    val selectionState: SelectionState get() = _selectionStateFlow.value
    val selectionStateFlow: StateFlow<SelectionState>  get() = _selectionStateFlow
    private val _selectionStateFlow = MutableStateFlow(SelectionState(0, 0))

    private var rememberedText: String = ""

    fun onTextChanged(newText: String) {
        if (newText != textState) {
            _textStateFlow.value = newText
            editorFileModel.onFileStateChanged(isEdited())
        }
    }

    fun onTextSelect(start: Int, end: Int) {
        _selectionStateFlow.value = SelectionState(start = start, end = end)
    }

    fun rememberTextState() {
        rememberedText = textState
        editorFileModel.onFileStateChanged(false)
    }

    fun restoreRememberedText() {
        _textStateFlow.value = rememberedText
        editorFileModel.onFileStateChanged(false)
    }

    private fun isEdited(): Boolean {
        return rememberedText != textState
    }
}