package ru.pocketbyte.bcengine.editor.presentation.editoroverlay

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EditorOverlayPresenter {

    data class State(
        val drawDebug: Boolean
    )

    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(
        State(
            drawDebug = false
        )
    )

    fun setDebug(value: Boolean) {
        _stateFlow.value = _stateFlow.value.copy(
            drawDebug = value
        )
    }
}