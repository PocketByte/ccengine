package ru.pocketbyte.bcengine.editor.presentation.editorview

interface DataSetActions {
    fun loadDataSet()
    fun setDataSetItem(newIndex: Int)
    fun firstDataSetItem()
    fun nextDataSetItem()
    fun previousDataSetItem()
    fun lastDataSetItem()
}
