package ru.pocketbyte.bcengine.editor.domain.interactor.export

import kotlinx.coroutines.CoroutineDispatcher
import ru.pocketbyte.bcengine.canvas.CanvasFactory


expect class ExportPdfIteractor(
    workDispatcher: CoroutineDispatcher,
    canvasFactory: CanvasFactory
) : AbsExportPdfIteractor {

}
