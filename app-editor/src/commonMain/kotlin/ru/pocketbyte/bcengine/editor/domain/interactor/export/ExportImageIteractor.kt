package ru.pocketbyte.bcengine.editor.domain.interactor.export

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.ProjectOutput
import ru.pocketbyte.bcengine.canvas.CanvasFactory
import ru.pocketbyte.bcengine.dataset.DataSet
import ru.pocketbyte.bcengine.dataset.forEachIndexed
import ru.pocketbyte.bcengine.errors.ProjectError

class ExportImageIteractor(
    private val workDispatcher: CoroutineDispatcher,
    private val canvasFactory: CanvasFactory
) {

    companion object {
        const val FORMAT = "PNG"
    }

    sealed class Result {
        object Success: Result()
        class ParseError(val errors: List<ProjectError>): Result()
    }

    suspend fun export(
        editorModel: EditorModel,
        output: ProjectOutput,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        editorModel.token.stateFlow.value.errors?.let {
            return Result.ParseError(it)
        }
        editorModel.project.stateFlow.value.errors?.let {
            return Result.ParseError(it)
        }

        val project = editorModel.project.state.project
            ?: return Result.ParseError(listOf(
                ProjectError.ExceptionError(IllegalStateException("Project not parsed"))
            ))

        editorModel.context.errors.clear()
        return export(editorModel.context, project, output, progress)
    }

    suspend fun export(
        context: Context,
        project: Project,
        output: ProjectOutput,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        val dataSet = withContext(workDispatcher) {
            project.dataSet.get(context, project)
        }
        if (context.errors.hasErrors()) {
            return Result.ParseError(context.errors.list)
        }
        return export(context, project, dataSet, output, progress)
    }

    suspend fun export(
        context: Context,
        project: Project,
        dataSet: DataSet,
        output: ProjectOutput,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        val canvas = canvasFactory.createCanvas(context, project)
        withContext(workDispatcher) {
            dataSet.forEachIndexed { index, item ->
                canvas.clear()
                project.measureAndDraw(context, canvas, item)
                canvas.saveToFile(
                    output.getImagePath(context, project, item, FORMAT),
                    FORMAT
                )
                progress(index + 1, dataSet.size)
            }
        }
        return Result.Success
    }
}