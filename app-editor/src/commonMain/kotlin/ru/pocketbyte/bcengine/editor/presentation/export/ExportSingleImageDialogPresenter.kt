package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.ProjectOutput
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportSingleImageIteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.formula.formulaStaticValue

class ExportSingleImageDialogPresenter(
    uiScope: CoroutineScope,
    editorsSetModel: EditorsSetModel,
    messageDialogPresenter: MessageDialogPresenter,
    private val exportSingleImageIteractor: ExportSingleImageIteractor
): AbsExportDialogPresenter<ExportSingleImageDialogPresenter.State>(
    uiScope, editorsSetModel, messageDialogPresenter
) {

    data class State(
        override val index: Int,
        override val dir: String,
        override val file: String,
        val rowSize: String,
        override val progress: Int = -1
    ) : AbsExportDialogPresenter.State {
        override val fileEnabled = true
    }

    override fun copyCurrentState(dir: String?, file: String?): State? {
        return currentState?.copy(
            dir = dir ?: currentState?.dir ?: "",
            file = file ?: currentState?.file ?: ""
        )
    }

    override fun buildInitialState(index: Int): State? {
        return getEditor(index)?.project?.state?.project?.let { project ->
            State(
                index = index,
                dir = project.output.dir.get(project, null),
                file = project.output.singleImage.get(project, null),
                rowSize = project.output.singleImageRowSize.get(project, null).toString()
            )
        }
    }

    fun onRowSizeValueChange(value: String) {
        currentState = currentState?.copy(
            rowSize = value
        )
    }

    override fun onOkClick() {
        val state = currentState ?: return onCancelClick()
        val editor = getEditor(state.index) ?: return onCancelClick()
        val output = ProjectOutput().apply {
            dir.formula = formulaStaticValue(state.dir)
            singleImage.formula = formulaStaticValue(state.file)
            singleImageRowSize.formula = formulaStaticValue(state.rowSize.toFloat())
        }
        currentState = currentState?.copy(
            progress = 0
        )

        uiScope.launch {
            val result = exportSingleImageIteractor.export(editor, output) { count, total ->
                currentState = currentState?.copy(
                    progress = (count.toFloat() / total.toFloat() * 100).toInt()
                )
            }
            currentState = null
            when (result) {
                ExportSingleImageIteractor.Result.Success -> {
                    messageDialogPresenter.showMessage(
                        null, "Export PNG done"
                    )
                }
                is ExportSingleImageIteractor.Result.ParseError -> {
                    messageDialogPresenter.showMessage(
                        "Failed to export PNG",
                        "Error: ${ErrorsContainer.buildErrorString(result.errors)}"
                    )
                }
            }
        }
    }
}