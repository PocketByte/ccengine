package ru.pocketbyte.bcengine.editor.presentation.save

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.AppComponent
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.SaveEditorInteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel

class SaveAllDialogPresenter(
    private val scope: CoroutineScope,
    private val appComponent: AppComponent,
    private val editorsSetModel: EditorsSetModel,
    private val saveEditorInteractor: SaveEditorInteractor
) : SaveDialogActions {

    val stateFlow: StateFlow<SaveTabState?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<SaveTabState?>(null)

    fun showDialogOrCloseApp() {
        editorsSetModel.stateFlow.value.editors.forEachIndexed { index, editor ->
            if (editor.file.state?.isEdited != false) {
                _stateFlow.value = SaveTabState(index, editor.file.state)
                return
            }
        }
        appComponent.exitApplication()
    }

    override fun onCloseClick() {
        _stateFlow.value = null
    }

    override fun onSaveClick() {
        scope.launch {
            val editor = getEditor(stateFlow.value?.index)

            if (editor != null) {
                saveEditorInteractor.save(editor) { _, _ ->
                    showDialogOrCloseApp()
                }
            } else {
                showDialogOrCloseApp()
            }

        }
    }

    override fun onForgetClick() {
        getEditor(_stateFlow.value?.index)?.let {
            it.text.restoreRememberedText()
        }
        showDialogOrCloseApp()
    }

    private fun getEditor(index: Int?): EditorModel? {
        if (index == null) return null
        return editorsSetModel.stateFlow.value.editors.getOrNull(index)
    }
}