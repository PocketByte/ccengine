package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EditorsSetModel {

    data class State(
        val editors: List<EditorModel>,
        val activeEditorIndex: Int
    ) {
        val activeEditor: EditorModel? get() = editors.getOrNull(activeEditorIndex)
    }

    val stateFlow: StateFlow<State> get() = _stateFlow

    private val _stateFlow = MutableStateFlow(State(emptyList(), -1))

    fun addEditorModel(model: EditorModel, setActive: Boolean = true) {
        val activeState = stateFlow.value
        _stateFlow.value = activeState.copy(
            editors = activeState.editors + model,
            activeEditorIndex = if (setActive || activeState.activeEditorIndex < 0) {
                activeState.editors.size
            } else {
                activeState.activeEditorIndex
            }
        )
    }

    fun removeEditorModel(model: EditorModel) {
        val activeState = stateFlow.value
        val removeIndex = activeState.editors.indexOf(model)

        when {
            removeIndex < 0 -> {
                // Do nothing
            }
            removeIndex <= activeState.activeEditorIndex -> {
                _stateFlow.value = activeState.copy(
                    editors = activeState.editors - model,
                    activeEditorIndex = (activeState.activeEditorIndex - 1)
                        .adjustIndex(activeState.editors.size - 2)
                )
            }
            else -> {
                _stateFlow.value = activeState.copy(
                    editors = activeState.editors - model
                )
            }
        }
    }

    fun setActiveEditor(index: Int) {
        val activeState = stateFlow.value
        _stateFlow.value = activeState.copy(
            activeEditorIndex = index
                .adjustIndex(activeState.editors.size - 1)
        )
    }

    private fun Int.adjustIndex(maxValue: Int): Int {
        return if (maxValue == -1) {
            -1
        } else {
            coerceIn(0, maxValue)
        }
    }
}