package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.domain.interactor.export.AbsExportPdfIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportPdfIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.save.SaveDialogInteractor
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import java.io.File

class ExportAllPdfDialogPresenter(
    private val uiScope: CoroutineScope,
    private val editorsSetModel: EditorsSetModel,
    private val stringRepository: StringRepository,
    private val messageDialogPresenter: MessageDialogPresenter,
    private val saveDialogInteractor: SaveDialogInteractor,
    private val exportPdfIteractor: ExportPdfIteractor
) {

    data class State(
        val progress: Int = -1
    )

    val stateFlow: StateFlow<State?> get() = _stateFlow

    private val _stateFlow = MutableStateFlow<State?>(null)

    private var currentState: State?
        get() = _stateFlow.value
        set(value) {
            _stateFlow.value = value
        }

    fun showDialog() {
        uiScope.launch {
            saveDialogInteractor.saveAs(
                stringRepository.dialog_export_all_pdf,
                null, "deck_all.pdf"
            ) {
                saveAllPdf(it)
            }
        }
    }

    private suspend fun saveAllPdf(file: File) {
        currentState = currentState?.copy(
            progress = 0
        )
        val result = exportPdfIteractor.export(
            file.name,
            editorsSetModel.stateFlow.value.editors,
            outputFilePath = file.absolutePath
        ) { count, total ->
            currentState = currentState?.copy(
                progress = (count.toFloat() / total.toFloat() * 100).toInt()
            )
        }
        currentState = null
        when (result) {
            AbsExportPdfIteractor.Result.Success -> {
                messageDialogPresenter.showMessage(
                    null, "Export PDF done"
                )
            }
            is AbsExportPdfIteractor.Result.ParseError -> {
                messageDialogPresenter.showMessage(
                    "Failed to export PDF",
                    "Error: ${ErrorsContainer.buildErrorString(result.errors)}"
                )
            }
            is AbsExportPdfIteractor.Result.ExceptionError -> {
                messageDialogPresenter.showMessage(
                    "Failed to export PDF",
                    "Error: ${result.errors.message}"
                )
            }
        }
    }
}