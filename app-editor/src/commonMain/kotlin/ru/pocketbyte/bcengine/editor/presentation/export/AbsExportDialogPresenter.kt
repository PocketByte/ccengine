package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.MessageDialogPresenter
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.FormulaParser
import ru.pocketbyte.bcengine.provider.IntProvider
import ru.pocketbyte.bcengine.provider.StringProvider
import ru.pocketbyte.bcengine.tokenizer.Tokenizer
import kotlin.math.max
import kotlin.math.min

abstract class AbsExportDialogPresenter<State : AbsExportDialogPresenter.State>(
    protected val uiScope: CoroutineScope,
    protected val editorsSetModel: EditorsSetModel,
    protected val messageDialogPresenter: MessageDialogPresenter
) {

    interface State {
        val index: Int
        val dir: String
        val file: String
        val fileEnabled: Boolean
        val progress: Int
    }

    abstract fun copyCurrentState(dir: String? = null, file: String? = null): State?
    abstract fun buildInitialState(index: Int): State?
    abstract fun onOkClick()

    val stateFlow: StateFlow<State?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<State?>(null)
    protected var currentState: State?
        get() = _stateFlow.value
        set(value) {
            _stateFlow.value = value
        }

    fun showDialog(index: Int) {
        buildInitialState(index)?.let {
            currentState = it
        }
    }

    fun onDirValueChange(value: String) {
        currentState = copyCurrentState(
            dir = value
        )
    }

    fun onFileValueChange(value: String) {
        currentState = copyCurrentState(
            file = value
        )
    }

    fun onCancelClick() {
        currentState = null
    }

    protected fun getEditor(index: Int?): EditorModel? {
        if (index == null) return null
        return editorsSetModel.stateFlow.value.editors.getOrNull(index)
    }

    protected fun <T : Any> parseFormula(
        context: Context,
        project: Project,
        string: String,
        type: ValueType<T>
    ): Formula<T>? {
        val tokens = try {
            Tokenizer.tokenize(string)
        } catch (e: Throwable) {
            messageDialogPresenter.showMessage(
                "Failed to decode formula",
                "Failed to decode following formula: $string\n" +
                        "Error: ${e.message}"
            )
            return null
        }
        context.errors.clear()

        val formula = try {
            FormulaParser.parse(
                context, project, tokens, type,
            )
        } catch (e: Throwable) {
            messageDialogPresenter.showMessage(
                "Failed to decode formula",
                "Failed to decode following formula: $string\n" +
                        "Error: ${e.message}"
            )
            return null
        }

        if (context.errors.hasErrors()) {
            messageDialogPresenter.showMessage(
                "Failed to decode formula",
                "Failed to decode following formula: $string\n" +
                        "Error: ${context.errors.buildErrorString()}"
            )
            return null
        }

        return formula
    }

    protected fun StringProvider.getFormulaString(): String {
        val formula = this.formula ?: return "\"${defaultValue}\""
        val token = formula.token ?: return "\"${defaultValue}\""
        val range = getFormulaRange(formula)

        if (range.first > range.second) {
            return "\"${defaultValue}\""
        }

        return token.parentString.substring(range.first, range.second + 1)
    }


    protected fun IntProvider.getFormulaString(): String {
        val formula = this.formula ?: return "$defaultValue"
        val token = formula.token ?: return "$defaultValue"
        val range = getFormulaRange(formula)

        if (range.first > range.second) {
            return "$defaultValue"
        }

        return token.parentString.substring(range.first, range.second + 1)
    }

    private fun getFormulaRange(formula: Formula<Any>): Pair<Int, Int> {
        var startIndex = formula.token?.startIndex ?: Int.MAX_VALUE
        var endIndex = formula.token?.endIndex ?: Int.MIN_VALUE

        formula.parameters.forEach {
            val range = getFormulaRange(it)
            startIndex = min(startIndex, range.first)
            endIndex = max(endIndex, range.second)
        }

        return Pair(startIndex, endIndex)
    }
}