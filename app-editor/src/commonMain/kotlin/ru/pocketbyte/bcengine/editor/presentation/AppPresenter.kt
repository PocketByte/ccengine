package ru.pocketbyte.bcengine.editor.presentation

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.domain.AppComponent
import ru.pocketbyte.bcengine.editor.domain.ComposeComponent
import ru.pocketbyte.bcengine.editor.domain.Files
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.OpenEditorIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.SaveEditorInteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportImageIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportPdfIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.export.ExportSingleImageIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.textfile.OpenTextFileInteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.textfile.SaveTextFileInteractor
import ru.pocketbyte.bcengine.editor.domain.model.RecentModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.domain.repository.StringListFileRepository
import ru.pocketbyte.bcengine.editor.presentation.doc.ParametrisedProcessorDocDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.CurrentEditorScreenPresenter
import ru.pocketbyte.bcengine.editor.presentation.editorview.EditorTabsPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportImageDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportSingleImageDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.menu.MainMenuPresenter
import ru.pocketbyte.bcengine.editor.presentation.save.SaveAllDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.save.SaveTabDialogPresenter
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.doc.buildDocStringsProvider
import ru.pocketbyte.bcengine.doc.builder.ParameterDocBuilder
import ru.pocketbyte.bcengine.doc.builder.ParametrisedProcessorDocBuilder
import ru.pocketbyte.bcengine.editor.domain.interactor.save.SaveDialogInteractor
import ru.pocketbyte.bcengine.editor.presentation.export.ExportAllPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportDoneDialogPresenter

class AppPresenter(
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
    private val appComponent: AppComponent,
    private val composeComponent: ComposeComponent,
    private val stringRepository: StringRepository
) {
    private val editorsSetModel = EditorsSetModel()
    private val recentModel = RecentModel()

    private val docStringsProvider = buildDocStringsProvider()

    private val docBuilder = ParametrisedProcessorDocBuilder(
        docStringsProvider,
        ParameterDocBuilder(docStringsProvider)
    )

    private val currentProjectsRepository = StringListFileRepository(Files.current)

    private val interactors = object {
        val openFile = OpenTextFileInteractor(
            workDispatcher, composeComponent, stringRepository
        )

        val saveDialog = SaveDialogInteractor(
            composeComponent
        )

        val saveTextFile = SaveTextFileInteractor(
            saveDialog, workDispatcher, stringRepository
        )

        val openEditor = OpenEditorIteractor(
            uiScope, workDispatcher, editorsSetModel, recentModel, openFile
        )

        val saveEditor = SaveEditorInteractor(
            recentModel, saveTextFile
        )

        val export = object {
            val image = ExportImageIteractor(
                workDispatcher, composeComponent.canvasFactory
            )

            val singleImage = ExportSingleImageIteractor(
                workDispatcher, composeComponent.canvasFactory
            )

            val pdf = ExportPdfIteractor(
                workDispatcher, composeComponent.canvasFactory
            )
        }
    }

    init {
        uiScope.launch(workDispatcher) {
            currentProjectsRepository.loadFromFile().forEach {
                interactors.openEditor.open(it, setActive = false)
            }
            editorsSetModel.stateFlow.collect { state ->
                state.editors.mapNotNull { it.file.state?.path }.let {
                    currentProjectsRepository.saveToFile(it)
                }
            }
        }
    }

    // ============================================
    // Presenters

    val mainMenuPresenter by lazy {
        MainMenuPresenter(
            uiScope, stringRepository, workDispatcher,
            editorsSetModel, recentModel, overlayPresenter,
            interactors.openEditor, interactors.saveEditor,
            exportImageDialogPresenter,
            exportSingleImageDialogPresenter,
            exportPdfDialogPresenter,
            exportAllPdfDialogPresenter
        )
    }

    val overlayPresenter by lazy {
        EditorOverlayPresenter()
    }

    val currentEditorScreenPresenter by lazy {
        CurrentEditorScreenPresenter(
            uiScope, workDispatcher, composeComponent,
            editorsSetModel, parametrisedProcessorDocDialogPresenter
        )
    }

    val tabsPresenter by lazy {
        EditorTabsPresenter(uiScope, editorsSetModel, saveTabDialogPresenter)
    }

    // ============================================
    // Dialog Presenters

    val messageDialogPresenter by lazy {
        MessageDialogPresenter()
    }

    val saveTabDialogPresenter by lazy {
        SaveTabDialogPresenter(
            uiScope, editorsSetModel, interactors.saveEditor
        )
    }

    val saveAllDialogPresenter by lazy {
        SaveAllDialogPresenter(
            uiScope, appComponent, editorsSetModel, interactors.saveEditor
        )
    }

    val parametrisedProcessorDocDialogPresenter by lazy {
        ParametrisedProcessorDocDialogPresenter(
            docBuilder
        )
    }

    // ============================================
    // Export Dialog Presenters

    val exportImageDialogPresenter by lazy {
        ExportImageDialogPresenter(
            uiScope, editorsSetModel, messageDialogPresenter,
            interactors.export.image
        )
    }

    val exportSingleImageDialogPresenter by lazy {
        ExportSingleImageDialogPresenter(
            uiScope, editorsSetModel, messageDialogPresenter,
            interactors.export.singleImage
        )
    }

    val exportPdfDialogPresenter by lazy {
        ExportPdfDialogPresenter(
            uiScope, editorsSetModel, messageDialogPresenter,
            interactors.export.pdf
        )
    }

    val exportAllPdfDialogPresenter by lazy {
        ExportAllPdfDialogPresenter(
            uiScope, editorsSetModel,
            stringRepository,
            messageDialogPresenter,
            interactors.saveDialog,
            interactors.export.pdf
        )
    }

    val exportDoneDialogPresenter = ExportDoneDialogPresenter()
}