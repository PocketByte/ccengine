package ru.pocketbyte.bcengine.editor.domain.model.editor

enum class DataSetStatus {
    NotReady,
    Loading,
    Loaded,
    Invalid
}