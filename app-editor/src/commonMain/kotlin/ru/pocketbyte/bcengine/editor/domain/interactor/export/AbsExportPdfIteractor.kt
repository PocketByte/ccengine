package ru.pocketbyte.bcengine.editor.domain.interactor.export

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.CanvasFactory
import ru.pocketbyte.bcengine.dataset.DataSet
import ru.pocketbyte.bcengine.dataset.forEachIndexed
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.AbsPdfModel
import ru.pocketbyte.bcengine.editor.domain.interactor.export.pdf.AbsPdfModel.PageSize
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeSimple
import ru.pocketbyte.bcengine.errors.ProjectError

abstract class AbsExportPdfIteractor(
    protected val workDispatcher: CoroutineDispatcher,
    protected val canvasFactory: CanvasFactory
) {

    data class Paddings(
        val left: SizeSimple = SizeSimple(1.9f, Size.Dimension.CENTIMETER),
        val right: SizeSimple = SizeSimple(1.32f, Size.Dimension.CENTIMETER),
        val top: SizeSimple = SizeSimple(1.8f, Size.Dimension.CENTIMETER),
        val bottom: SizeSimple = SizeSimple(1.8f, Size.Dimension.CENTIMETER),
        val horizontalSpace: SizeSimple = Size.ZERO,
        val verticalSpace: SizeSimple = Size.ZERO
    )

    sealed class Result {
        object Success: Result()
        class ParseError(val errors: List<ProjectError>): Result()
        class ExceptionError(val errors: Throwable): Result()
    }

    abstract fun buildModel(
        documentName: String,
        pageSize: PageSize,
        pageOrientation: Orientation,
        paddings: Paddings
    ): AbsPdfModel

    abstract fun saveAndRecycle(
        pdfModel: AbsPdfModel,
        outputFilePath: String,
    ): Result

    abstract fun getTempFilePath(
        pdfModel: AbsPdfModel,
        extension: String
    ): String

    suspend fun export(
        documentName: String,
        editorModels: List<EditorModel>,
        pageSize: PageSize = PageSize.A4,
        pageOrientation: Orientation = Orientation.NONE,
        paddings: Paddings = Paddings(),
        outputFilePath: String,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        val pdfModel = buildModel(documentName, pageSize, pageOrientation, paddings)

        editorModels.forEach {
            exportIntToModel(it, pdfModel, progress).let { result ->
                if (result != Result.Success) {
                    return result
                }
            }
        }

        return saveAndRecycle(pdfModel, outputFilePath)
    }

    private suspend fun exportIntToModel(
        editorModel: EditorModel,
        pdfModel: AbsPdfModel,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        val context = editorModel.context

        editorModel.token.stateFlow.value.errors?.let {
            return Result.ParseError(it)
        }
        editorModel.project.stateFlow.value.errors?.let {
            return Result.ParseError(it)
        }

        val project = editorModel.project.state.project
            ?: return Result.ParseError(listOf(
                ProjectError.ExceptionError(IllegalStateException("Project not parsed"))
            ))

        context.errors.clear()

        val dataSet = withContext(workDispatcher) {
            project.dataSet.get(context, project)
        }
        if (context.errors.hasErrors()) {
            return Result.ParseError(context.errors.list)
        }

        return exportIntToModel(context, project, dataSet, pdfModel, progress)
    }

    private suspend fun exportIntToModel(
        context: Context,
        project: Project,
        dataSet: DataSet,
        pdfModel: AbsPdfModel,
        progress: (count: Int, total: Int) -> Unit
    ): Result {
        val canvas = canvasFactory.createCanvas(context, project)

        withContext(workDispatcher) {
            val dpiKoef = pdfModel.dpi / project.layout.dpi
            val cardWidth = project.layout.widthPx * dpiKoef
            val cardHeight = project.layout.heightPx * dpiKoef

            dataSet.forEachIndexed { index, item ->
                val itemFilePath = getTempFilePath(pdfModel, ExportImageIteractor.FORMAT)
                canvas.clear()
                project.measureAndDraw(context, canvas, item)
                canvas.saveToFile(itemFilePath, ExportImageIteractor.FORMAT)
                progress(index + 1, dataSet.size)

                pdfModel.drawCard(itemFilePath, cardWidth, cardHeight)
            }
        }

        canvas.recycle()

        return Result.Success
    }
}