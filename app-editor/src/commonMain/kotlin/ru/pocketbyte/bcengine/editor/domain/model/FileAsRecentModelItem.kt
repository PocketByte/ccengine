package ru.pocketbyte.bcengine.editor.domain.model

import java.io.File

fun File.asRecentModelItem(): RecentModel.Item {
    return RecentModel.Item(
        ".../" + parent.split('/').last() + "/" + name,
        absolutePath
    )
}