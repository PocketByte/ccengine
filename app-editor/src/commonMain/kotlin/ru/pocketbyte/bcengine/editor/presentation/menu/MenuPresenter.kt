package ru.pocketbyte.bcengine.editor.presentation.menu

import kotlinx.coroutines.flow.StateFlow

interface MenuPresenter {

    val stateFlow: StateFlow<ItemState.Group>

    sealed class ItemState {
        abstract val title: String
        abstract val isEnabled: Boolean
        abstract val tag: String

        data class Option(
            override val title: String,
            override val isEnabled: Boolean = true,
            override val tag: String,
            val extras: Any? = null
        ) : ItemState()

        data class Group(
            override val title: String,
            override val isEnabled: Boolean = true,
            val items: List<ItemState>
        ) : ItemState() {
            override val tag = "Group"
        }

        data object Separator : ItemState() {
            override val title = ""
            override val isEnabled = true
            override val tag = "Separator"
        }
    }

    fun onOptionClick(item: ItemState)

}