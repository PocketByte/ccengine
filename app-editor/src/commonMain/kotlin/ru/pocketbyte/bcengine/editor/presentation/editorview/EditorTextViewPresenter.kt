package ru.pocketbyte.bcengine.editor.presentation.editorview

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorTextModel
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class EditorTextViewPresenter(
    uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
    private val editorModel: EditorModel
) {

    data class State(
        val text: String,
        val selection: EditorTextModel.SelectionState,
        val tokens: List<ProjectToken>
    )

    val stateFlow: StateFlow<State> get() = _stateFlow
    private val _stateFlow = MutableStateFlow(
        State(
            editorModel.text.textState,
            editorModel.text.selectionState,
            editorModel.token.stateFlow.value.tokens
        )
    )

    private val scope = uiScope + Job()

    init {
        scope.launch {
            editorModel.text.textStateFlow.collect {
                _stateFlow.value = _stateFlow.value.copy(
                    text = it
                )
            }
        }
        scope.launch {
            editorModel.text.selectionStateFlow.collect {
                _stateFlow.value = _stateFlow.value.copy(
                    selection = it
                )
            }
        }
        scope.launch {
            editorModel.token.stateFlow.collect {
                _stateFlow.value = _stateFlow.value.copy(
                    tokens = it.tokens
                )
            }
        }
    }

    fun onTextChanged(newText: String) {
        scope.launch {
            editorModel.text.onTextChanged(newText)
        }
    }

    fun onTextSelect(start: Int, end: Int) {
        scope.launch {
            editorModel.text.onTextSelect(start, end)
        }
    }
}