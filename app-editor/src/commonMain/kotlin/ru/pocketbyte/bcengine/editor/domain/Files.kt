package ru.pocketbyte.bcengine.editor.domain

import java.io.File

object Files {
    val preferencesFolder = File("./prefs/")
    val recent = File(preferencesFolder, "./recent")
    val current = File(preferencesFolder, "./current")

    init {
        preferencesFolder.mkdirs()
    }
}