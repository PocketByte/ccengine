package ru.pocketbyte.bcengine.editor.domain.interactor.model

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import ru.pocketbyte.bcengine.editor.domain.model.editor.DataSetStatus
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorDataSetModel
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError

class EditorModelDataSetLoadInteractor(
    private val dataSetModel: EditorDataSetModel,
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
) {

    private var runJob: Job? = null

    fun loadDatasetAndPublish(context: Context, project: Project) {
        runJob?.cancel()
        runJob = uiScope.async(workDispatcher + Job()) {
            dataSetModel.onDataSetStatusChanged(DataSetStatus.Loading)
            context.errors.clear()
            val dataSet = try {
                project.dataSet.get(context, project)
            } catch (e: Throwable) {
                context.errors.addError(ProjectError.ExceptionError(e))
                dataSetModel.onDataSetLoadError(context.errors.list)
                return@async
            }

            if (context.errors.hasErrors()) {
                dataSetModel.onDataSetLoadError(
                    context.errors.list
                )
            } else {
                dataSetModel.onDataSetLoaded(dataSet)
            }
        }
    }

    fun cancel() {
        runJob?.cancel()
    }
}