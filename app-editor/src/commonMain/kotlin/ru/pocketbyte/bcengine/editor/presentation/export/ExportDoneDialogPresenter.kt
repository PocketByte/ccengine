package ru.pocketbyte.bcengine.editor.presentation.export

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class ExportDoneDialogPresenter {

    data class State(
        val title: String?,
        val message: String,
        val filePath: String
    )

    val stateFlow: StateFlow<State?> get() = _stateFlow
    private val _stateFlow = MutableStateFlow<State?>(null)

    fun show(title: String?, message: String, filePath: String) {
        _stateFlow.value = State(title, message, filePath)
    }

    fun dismiss() {
        _stateFlow.value = null
    }

    fun openFile() {

    }
}