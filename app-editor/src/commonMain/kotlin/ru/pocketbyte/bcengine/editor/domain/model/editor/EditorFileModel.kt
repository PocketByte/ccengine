package ru.pocketbyte.bcengine.editor.domain.model.editor

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class EditorFileModel {

    data class State(
        val title: String,
        val path: String,
        val isEdited: Boolean
    )

    val state: State? get() = stateFlow.value
    val stateFlow: StateFlow<State?> get() = _stateFlow

    private val _stateFlow = MutableStateFlow<State?>(null)

    fun onFileStateChanged(title: String, path: String) {
        _stateFlow.value = stateFlow.value?.copy(
            title = title,
            path = path
        ) ?: State(title, path, false)
    }

    fun onFileStateChanged(isEdited: Boolean) {
        stateFlow.value?.copy(
            isEdited = isEdited,
        )?.let { _stateFlow.value = it }
    }

    fun onFileStateChanged(fileState: State?) {
        _stateFlow.value = fileState
    }
}