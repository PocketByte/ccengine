package ru.pocketbyte.bcengine.editor.domain.interactor.textfile

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.domain.interactor.save.SaveDialogInteractor
import java.io.File
import java.io.PrintWriter

class SaveTextFileInteractor(
    private val saveDialogInteractor: SaveDialogInteractor,
    private val workDispatcher: CoroutineDispatcher,
    private val stringRepository: StringRepository
) {

    suspend fun saveAs(
        text: String,
        filePath: String?,
        completion: suspend (file: File) -> Unit
    ) {
        saveDialogInteractor.saveAs(
            stringRepository.dialog_save_title,
            filePath,
            "Untitled.bce"
        ) { file ->
            save(text, file, completion)
        }
    }

    suspend fun save(
        text: String,
        filePath: String,
        completion: suspend (file: File) -> Unit
    ) {
        save(text, File(filePath), completion)
    }

    suspend fun save(
        text: String,
        file: File,
        completion: suspend (file: File) -> Unit
    ) {
        withContext(workDispatcher) {
            val out = PrintWriter(file)
            out.print(text)
            out.flush()
            out.close()

            completion(file)
        }
    }
}