package ru.pocketbyte.bcengine.editor.presentation.menu

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.OpenEditorIteractor
import ru.pocketbyte.bcengine.editor.domain.interactor.editor.SaveEditorInteractor
import ru.pocketbyte.bcengine.editor.domain.model.ProjectTemplate
import ru.pocketbyte.bcengine.editor.domain.model.RecentModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorFileModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorModel
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorsSetModel
import ru.pocketbyte.bcengine.editor.presentation.export.ExportAllPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportImageDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportPdfDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.export.ExportSingleImageDialogPresenter
import ru.pocketbyte.bcengine.editor.presentation.menu.MenuPresenter.ItemState

class FileMenuPresenter(
    private val uiScope: CoroutineScope,
    private val str: StringRepository,
    private val workDispatcher: CoroutineDispatcher,
    private val editorsSetModel: EditorsSetModel,
    private val recentModel: RecentModel,
    private val openEditorIteractor: OpenEditorIteractor,
    private val saveEditorInteractor: SaveEditorInteractor,
    private val exportImageDialogPresenter: ExportImageDialogPresenter,
    private val exportSingleImageDialogPresenter: ExportSingleImageDialogPresenter,
    private val exportPdfDialogPresenter: ExportPdfDialogPresenter,
    private val exportAllPdfDialogPresenter: ExportAllPdfDialogPresenter
) : MenuPresenter {

    companion object {
        private const val ITEM_TAG_FILE = "File"
        private const val ITEM_TAG_FILE_NEW = "$ITEM_TAG_FILE:new"
        private const val ITEM_TAG_FILE_OPEN = "$ITEM_TAG_FILE:open"
        private const val ITEM_TAG_FILE_OPEN_RECENT = "$ITEM_TAG_FILE:openRecent"
        private const val ITEM_TAG_FILE_SAVE = "$ITEM_TAG_FILE:save"
        private const val ITEM_TAG_FILE_SAVE_AS = "$ITEM_TAG_FILE:saveAs"
        private const val ITEM_TAG_FILE_EXPORT = "$ITEM_TAG_FILE:export"
        private const val ITEM_TAG_FILE_EXPORT_PNG = "$ITEM_TAG_FILE_EXPORT:png"
        private const val ITEM_TAG_FILE_EXPORT_SINGLE_PNG = "$ITEM_TAG_FILE_EXPORT:singlePng"
        private const val ITEM_TAG_FILE_EXPORT_PDF = "$ITEM_TAG_FILE_EXPORT:pdf"
        private const val ITEM_TAG_FILE_EXPORT_ALL_PNG = "$ITEM_TAG_FILE_EXPORT:allPdf"
    }

    data class State(
        val isSaveEnabled: Boolean,
        val isExportEnabled: Boolean,
        val recentFiles: List<RecentModel.Item>
    )

    private val innerStateFlow = MutableStateFlow(
        State(
            isSaveEnabled = false,
            isExportEnabled = false,
            recentFiles = emptyList()
        )
    )

    override val stateFlow: StateFlow<ItemState.Group> = innerStateFlow.map { state ->
        state.asItemStateGroup()
    }.stateIn(uiScope, SharingStarted.Lazily, innerStateFlow.value.asItemStateGroup())

    private var editorFileSubscription: Job? = null

    init {
        uiScope.launch {
            editorsSetModel.stateFlow.collect {
                onActiveEditorChanged(it.activeEditor)
            }
        }
        uiScope.launch {
            recentModel.listFlow.collect {
                innerStateFlow.emit(innerStateFlow.value.copy(
                    recentFiles = it
                ))
            }
        }
    }

    override fun onOptionClick(item: ItemState) {
        when (item.tag) {
            ITEM_TAG_FILE_NEW -> newFileAction()
            ITEM_TAG_FILE_OPEN -> openFileAction()
            ITEM_TAG_FILE_OPEN_RECENT -> {
                openFileAction(
                    filePath = item
                        .let { it as? ItemState.Option }
                        ?.let { it.extras as? RecentModel.Item }
                        ?.path
                        ?: return
                )
            }
            ITEM_TAG_FILE_SAVE -> saveFileAction()
            ITEM_TAG_FILE_SAVE_AS -> saveFileAsAction()
            ITEM_TAG_FILE_EXPORT_PNG -> exportImageAction()
            ITEM_TAG_FILE_EXPORT_SINGLE_PNG -> exportSingleImageAction()
            ITEM_TAG_FILE_EXPORT_PDF -> exportPdfAction()
            ITEM_TAG_FILE_EXPORT_ALL_PNG -> exportAllPdfAction()
        }
    }

    private fun newFileAction() {
        uiScope.launch {
            editorsSetModel.addEditorModel(
                EditorModel(uiScope, workDispatcher).apply {
                    text.onTextChanged(ProjectTemplate)
                    text.rememberTextState()
                }
            )
        }
    }

    private fun openFileAction() {
        uiScope.launch {
            openEditorIteractor.open(setActive = true)
        }
    }

    private fun openFileAction(filePath: String) {
        uiScope.launch {
            openEditorIteractor.open(filePath, setActive = true)
        }
    }

    private fun saveFileAction() {
        uiScope.launch {
            editorsSetModel.stateFlow.value.activeEditor?.let {
                saveEditorInteractor.save(it)
            }
        }
    }

    private fun saveFileAsAction() {
        uiScope.launch {
            editorsSetModel.stateFlow.value.activeEditor?.let {
                saveEditorInteractor.saveAs(it)
            }
        }
    }

    private fun exportImageAction() {
        exportImageDialogPresenter.showDialog(
            editorsSetModel.stateFlow.value.activeEditorIndex
        )
    }

    private fun exportSingleImageAction() {
        exportSingleImageDialogPresenter.showDialog(
            editorsSetModel.stateFlow.value.activeEditorIndex
        )
    }

    private fun exportPdfAction() {
        exportPdfDialogPresenter.showDialog(
            editorsSetModel.stateFlow.value.activeEditorIndex
        )
    }

    private fun exportAllPdfAction() {
        exportAllPdfDialogPresenter.showDialog()
    }

    private suspend fun onActiveEditorChanged(activeEditor: EditorModel?) {
        innerStateFlow.emit(innerStateFlow.value.copy(
            isSaveEnabled = activeEditor != null
        ))

        editorFileSubscription?.cancel()
        editorFileSubscription = activeEditor?.let {
            uiScope.launch {
                it.file.stateFlow.collect {
                    onActiveEditorFileChanged(it)
                }
            }
        }
    }

    private suspend fun onActiveEditorFileChanged(fileState: EditorFileModel.State?) {
        innerStateFlow.emit(innerStateFlow.value.copy(
            isExportEnabled = fileState != null
        ))
    }

    private fun State.asItemStateGroup(): ItemState.Group {
        return ItemState.Group(
            title = str.menu_file_title,
            isEnabled = true,
            items = listOf(
                ItemState.Option(str.menu_file_new, tag = ITEM_TAG_FILE_NEW),
                ItemState.Option(str.menu_file_open, tag = ITEM_TAG_FILE_OPEN),
                ItemState.Group(str.menu_file_open_recent_title,
                    items = if (recentFiles.isEmpty()) {
                        listOf(ItemState.Option(
                            str.menu_file_open_recent_empty_message,
                            isEnabled = false, tag = ""
                        ))
                    } else recentFiles.map { item ->
                        ItemState.Option(
                            item.title,
                            tag = ITEM_TAG_FILE_OPEN_RECENT,
                            extras = item
                        )
                    }
                ),
                ItemState.Separator,
                ItemState.Option(str.menu_file_save, isSaveEnabled, tag = ITEM_TAG_FILE_SAVE),
                ItemState.Option(str.menu_file_save_as, isSaveEnabled, tag = ITEM_TAG_FILE_SAVE_AS),
                ItemState.Separator,
                ItemState.Group(str.menu_file_export_title, isExportEnabled,
                    items = listOf(
                        ItemState.Option(
                            str.menu_file_export_png, isExportEnabled,
                            tag = ITEM_TAG_FILE_EXPORT_PNG
                        ),
                        ItemState.Option(
                            str.menu_file_export_single_png, isExportEnabled,
                            tag = ITEM_TAG_FILE_EXPORT_SINGLE_PNG
                        ),
                        ItemState.Option(
                            str.menu_file_export_pdf, isExportEnabled,
                            tag = ITEM_TAG_FILE_EXPORT_PDF
                        ),
                        ItemState.Option(
                            str.menu_file_export_all_pdf, isExportEnabled,
                            tag = ITEM_TAG_FILE_EXPORT_ALL_PNG
                        )
                    )
                )
            )
        )
    }
}
