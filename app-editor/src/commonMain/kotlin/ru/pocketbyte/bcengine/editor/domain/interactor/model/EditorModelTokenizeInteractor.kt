package ru.pocketbyte.bcengine.editor.domain.interactor.model

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import ru.pocketbyte.bcengine.editor.domain.model.editor.EditorTokenModel
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.exception.TokenizeException
import ru.pocketbyte.bcengine.tokenizer.Tokenizer

class EditorModelTokenizeInteractor(
    private val tokenModel: EditorTokenModel,
    private val uiScope: CoroutineScope,
    private val workDispatcher: CoroutineDispatcher,
) {

    private var runJob: Job? = null

    fun tokenizeTextAndPublish(text: String) {
        runJob?.cancel()
        runJob = uiScope.async(workDispatcher + Job()) {
            val tokens = try {
                Tokenizer.tokenize(text)
            } catch (e: TokenizeException) {
                tokenModel.onTokenParseError(
                    listOf(
                        ProjectError.ParseError(
                            e.index, e.string,
                            e.message ?: "Failed to tokenize project text"
                        )
                    )
                )
                return@async
            } catch (e: Throwable) {
                tokenModel.onTokenParseError(
                    listOf(ProjectError.ExceptionError(e))
                )
                return@async
            }

            tokenModel.onTokenParsed(tokens)
        }
    }
}