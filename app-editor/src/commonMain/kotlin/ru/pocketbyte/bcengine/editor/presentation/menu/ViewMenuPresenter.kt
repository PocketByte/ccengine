package ru.pocketbyte.bcengine.editor.presentation.menu

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ru.pocketbyte.bcengine.editor.StringRepository
import ru.pocketbyte.bcengine.editor.presentation.editoroverlay.EditorOverlayPresenter
import ru.pocketbyte.bcengine.editor.presentation.menu.MenuPresenter.ItemState
import ru.pocketbyte.bcengine.editor.ui.Str

class ViewMenuPresenter(
    private val uiScope: CoroutineScope,
    private val str: StringRepository,
    private val editorOverlayPresenter: EditorOverlayPresenter
): MenuPresenter {

    companion object {
        private const val ITEM_TAG_MAIN = "View"
        private const val ITEM_TAG_DEBUG = "$ITEM_TAG_MAIN:debug"

    }

    data class State(
        val debug: Boolean
    )

    private val innerStateFlow = MutableStateFlow(
        State(
            debug = false
        )
    )

    override val stateFlow: StateFlow<ItemState.Group> = innerStateFlow.map { state ->
        state.asItemStateGroup()
    }.stateIn(uiScope, SharingStarted.Lazily, innerStateFlow.value.asItemStateGroup())

    init {
        uiScope.launch {
            editorOverlayPresenter.stateFlow.collect {
                innerStateFlow.emit(innerStateFlow.value.copy(
                    debug = it.drawDebug
                ))
            }
        }
    }

    override fun onOptionClick(item: ItemState) {
        when (item.tag) {
            ITEM_TAG_DEBUG -> toggleDebug()
        }
    }

    private fun toggleDebug() {
        editorOverlayPresenter.setDebug(
            !editorOverlayPresenter.stateFlow.value.drawDebug
        )
    }

    private fun State.asItemStateGroup(): ItemState.Group {
        return ItemState.Group(
            title = str.menu_view_title,
            isEnabled = true,
            items = listOf(
                ItemState.Option(
                    title = str.menu_view_debug_layer(debug),
                    isEnabled = true,
                    tag = ITEM_TAG_DEBUG
                )
            )
        )
    }

    @Suppress("FunctionName")
    private fun StringRepository.menu_view_debug_layer(isDebug: Boolean): String {
        return menu_view_debug_layer(
            if (isDebug) {
                option_on
            } else {
                option_off
            }
        )
    }
}