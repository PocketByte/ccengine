package ru.pocketbyte.bcengine

object ProjectScrumOrders {
    private const val scheetId = "1pygCKYfzWkJqPyqnIOAVDqGf3S5FpkzI6nZfn1v2KvE"
    private const val workscheetId = "0"
    private const val url = "https://docs.google.com/spreadsheets/d/${scheetId}/export?format=csv&gid=${workscheetId}"

    const val project = ""+
            "[project]\n" +
            "DATASET(csv , google_sheet_csv(\"$scheetId\"; \"$workscheetId\"))\n" +
            "LAYOUT(2.5inch, 3.5inch, #FFFFFF, )\n" +
            "FUN(FAIL_T, str, if(isEmpty(\$FAIL);\"\"; \"<b>Провал:</b> \" + \$FAIL))\n" +
            "FUN(DESC_ALL, str, join(\$P; \$DESC; \$FAIL_T))\n" +
            "FUN(P, str, <font size=8><br> <br></font>)\n" +
            "[view]\n" +
            "TEXT(0%, 0%, 17%, 17%, \$complexity, ;24sp;B, , center, center, ,)\n" +
            "TEXT(17%, 0%, 80%, 17%, \$TITLE, ;16sp;, , center, center, ,)\n" +
            "IMAGE(0%, 17%, 100%, 48%, ./photo.png, fill)\n" +
            "TEXT(2%, 69%, 96%, 30%, \$DESC_ALL, ;14sp;, html, center, center, ,)\n" +
            "LINE(-1%, 17%, 102%, 0%, 2sp)\n" +
            "LINE(-1%, 65%, 102%, 0%, 2sp)"
}