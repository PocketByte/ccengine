import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import ru.pocketbyte.locolaser.*

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose").version("1.5.10")
    id("ru.pocketbyte.locolaser")
}

group = "ru.pocketbyte.bcengine"
version = "1.0-SNAPSHOT"

kotlin {
    jvm {
        withJava()
    }
    sourceSets {
        val commonMain by getting {
            kotlin.srcDir("./build/generated/src/commonMain/kotlin/")
            dependencies {
                implementation(project(":core"))
                implementation(project(":tokenizer"))
                implementation(project(":parser"))
                implementation(project(":canvas-compose"))
                implementation(project(":doc-builder"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(libsBce.kotlin.coroutines.core)
                implementation(libsBce.locolaser.runtime)
            }
        }
        val jvmMain by getting {
            kotlin.srcDir("./build/generated/src/desktopMain/kotlin/")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation(compose.desktop.currentOs)
                implementation(libsBce.kotlin.coroutines.coreJvm)
                implementation(libsBce.locolaser.runtimeJvm)
                implementation(libsBce.jvm.pdfbox)
                implementation(libsBce.jvm.icu4j)
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "app-editor"
            packageVersion = "1.0.0"
        }
    }
}

localize {
    config {
        platform {
            kotlinMultiplatform {
                srcDir ="./build/generated/src/"
                repositoryPackage = "ru.pocketbyte.bcengine.editor"

                absKeyValue("desktop") {
                    className = "DesktopStringRepository"
                }
            }
        }
        source {
            properties {
                resourcesDir = "./src/jvmMain/resources/strings/"
            }
        }
        locales = setOf("base")
    }
}
