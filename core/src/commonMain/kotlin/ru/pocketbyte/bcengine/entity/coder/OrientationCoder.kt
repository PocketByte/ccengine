package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Orientation

object OrientationCoder {
    fun decode(string: String): Orientation {
        Orientation.values().forEach {
            if (it.name.lowercase() == string.trim()) {
                return it
            }
        }
        throw IllegalArgumentException("Unknown orientation \"$string\".")
    }

    fun encode(orientation: Orientation): String {
        return orientation.name.lowercase()
    }
}