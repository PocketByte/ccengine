package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Aligning
import ru.pocketbyte.bcengine.entity.Anchor
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size

object AnchorCoder {

    private const val SEPARATOR = ";"

    private const val INDEX_SIZE = 0
    private const val INDEX_ALIGNING = 1

    private val DEFAULT_SIZE = Size.ZERO
    private val DEFAULT_ALIGNING = Aligning.START

    private val skipBlank: (it: String) -> String? = {
        it.ifBlank { null }
    }

    fun decode(string: String, orientation: Orientation): Anchor {
        val items = string.split(SEPARATOR)

        return Anchor(
            items.getOrNull(INDEX_SIZE)
                ?.let(skipBlank)
                ?.let { SizeCoder.decode(it.trim(), orientation) }
                ?: DEFAULT_SIZE,
            items.getOrNull(INDEX_ALIGNING)
                ?.let(skipBlank)
                ?.let { AligningCoder.decode(it.trim()) }
                ?: DEFAULT_ALIGNING
        )
    }
}
