package ru.pocketbyte.bcengine.entity

class ImageAttributes(
    val scaleMode: ScaleMode,
    val aligningX: Aligning,
    val aligningY: Aligning
)