package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeSimple

object SizeCoder {

    fun decode(string: String, orientation: Orientation): Size {
        Size.Dimension.entries.forEach {
            if (string.endsWith(it.sign)) {
                if (it == Size.Dimension.PIXEL) {
                    throw IllegalArgumentException("Unknown Size format: \"$string\"")
                }
                return SizeSimple(
                    string.substring(0, string.length - it.sign.length).toFloatOrNull() ?: 0f,
                    it, orientation
                )
            }
        }
        throw IllegalArgumentException("Unknown Size format: \"$string\"")
    }
}