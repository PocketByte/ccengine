package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeRange

object SizeRangeCoder {

    private const val SEPARATOR = ";"

    private val skipBlank: (it: String) -> String? = {
        it.ifBlank { null }
    }

    fun decode(string: String, orientation: Orientation): SizeRange {
        val items = string.split(SEPARATOR)

        return when {
            items.isEmpty() -> {
                SizeRange(Size.ZERO)
            }
            items.size == 1 -> {
                items.getOrNull(0)
                    ?.let(skipBlank)
                    ?.let { SizeCoder.decode(it.trim(), orientation) }
                    ?.let { SizeRange(it) }
                    ?: SizeRange(Size.ZERO)
            }
            else -> {
                SizeRange(
                    items.getOrNull(0)
                        ?.let(skipBlank)
                        ?.let { SizeCoder.decode(it.trim(), orientation) }
                        ?: Size.ZERO,
                    items.getOrNull(1)
                        ?.let(skipBlank)
                        ?.let { SizeCoder.decode(it.trim(), orientation) }
                        ?: Size.ZERO
                )
            }
        }
    }
}