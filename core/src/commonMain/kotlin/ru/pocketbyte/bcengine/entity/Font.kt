package ru.pocketbyte.bcengine.entity

data class Font(
    val family: String,
    val style: Set<Style>,
    val size: Size,
    val color: Color
) {
    enum class Style {
        BOLD,
        ITALIC,
        UNDERLINE,
        STRIKE
    }
}