package ru.pocketbyte.bcengine.entity

val Size.isEmpty: Boolean get() {
    return when (this) {
        is SizeSimple -> value == 0f
        else -> false
    }
}

fun Size.ifEmpty(value: Size): Size {
    return if (isEmpty) {
        value
    } else {
        this
    }
}