package ru.pocketbyte.bcengine.entity

data class SizeRange(
    val min: Size,
    val max: Size
) {
    constructor(size: Size) : this(size, size)
}