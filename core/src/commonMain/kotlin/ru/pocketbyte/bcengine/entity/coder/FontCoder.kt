package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.*

object FontCoder {

    private const val SEPARATOR = ";"

    private const val DEFAULT_FAMILY = "Helvetica"
    private val DEFAULT_STYLE = emptySet<Font.Style>()
    private val DEFAULT_SIZE = SizeSimple(14f, Size.Dimension.SCALED_PIXEL, Orientation.NONE)
    private val DEFAULT_COLOR = Color.BLACK

    private const val INDEX_FAMILY = 0
    private const val INDEX_SIZE = 1
    private const val INDEX_STYLE = 2
    private const val INDEX_COLOR = 3

    private val skipBlank: (it: String) -> String? = {
        it.ifBlank { null }
    }

    fun decode(string: String): Font {
        val items = string.split(SEPARATOR)

        return Font(
            items.getOrNull(INDEX_FAMILY)
                ?.let(skipBlank)
                ?.trim()
                ?: DEFAULT_FAMILY,
            items.getOrNull(INDEX_STYLE)
                ?.let(skipBlank)
                ?.let { FontStyleCoder.decode(it.trim()) }
                ?: DEFAULT_STYLE,
            items.getOrNull(INDEX_SIZE)
                ?.let(skipBlank)
                ?.let { SizeCoder.decode(it.trim(), Orientation.NONE) }
                ?: DEFAULT_SIZE,
            items.getOrNull(INDEX_COLOR)
                ?.let(skipBlank)
                ?.let { ColorCoder.decode(it.trim()) }
                ?: DEFAULT_COLOR
        )
    }
}