package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Font

object FontStyleCoder {

    fun decode(string: String): Set<Font.Style> {
        string.trim().let {
            if (it.isEmpty()) {
                return setOf()
            } else {
                return it.toCharArray().map { char ->
                    when (char) {
                        'B' -> Font.Style.BOLD
                        'I' -> Font.Style.ITALIC
                        'U' -> Font.Style.UNDERLINE
                        'S' -> Font.Style.STRIKE
                        else -> throw IllegalArgumentException(
                            "Invalid font style \"$string\". Char '$char' not recognized"
                        )
                    }
                }.toSet()
            }
        }
    }
}