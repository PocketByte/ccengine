package ru.pocketbyte.bcengine.entity

data class Anchor(
    val size: Size,
    val aligning: Aligning
) {
    companion object {
        val ZERO = Anchor(Size.ZERO, Aligning.START)
    }
}
