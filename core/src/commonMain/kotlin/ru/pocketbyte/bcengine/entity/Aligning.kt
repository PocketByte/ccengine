package ru.pocketbyte.bcengine.entity

enum class Aligning {
    START,
    CENTER,
    END
}