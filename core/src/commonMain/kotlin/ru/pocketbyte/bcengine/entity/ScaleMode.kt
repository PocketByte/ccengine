package ru.pocketbyte.bcengine.entity

enum class ScaleMode {
    STRETCH,
    FILL,
    FIT
}