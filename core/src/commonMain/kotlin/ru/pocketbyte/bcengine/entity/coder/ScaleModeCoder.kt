package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.ScaleMode

object ScaleModeCoder {

    fun decode(string: String): ScaleMode {
        ScaleMode.values().forEach {
            if (it.name.lowercase() == string.trim()) {
                return it
            }
        }
        throw IllegalArgumentException("Unknown scale mode \"$string\".")
    }

    fun encode(aligning: ScaleMode): String {
        return aligning.name.lowercase()
    }
}