package ru.pocketbyte.bcengine.entity

enum class TextAligning {
    START,
    CENTER,
    END,
    JUSTIFY
}