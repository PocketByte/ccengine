package ru.pocketbyte.bcengine.entity

fun Int.px(): Size {
    return SizeSimple(this.toFloat(), Size.Dimension.PIXEL, Orientation.NONE)
}

fun Int.percent(): Size {
    return SizeSimple(this.toFloat(), Size.Dimension.FRAME_PERCENT, Orientation.NONE)
}