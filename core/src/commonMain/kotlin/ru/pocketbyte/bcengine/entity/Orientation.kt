package ru.pocketbyte.bcengine.entity

enum class Orientation {
    NONE,
    HORIZONTAL,
    VERTICAL
}