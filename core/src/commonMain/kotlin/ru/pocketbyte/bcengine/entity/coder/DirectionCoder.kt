package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Direction

object DirectionCoder {
    fun decode(string: String): Direction {
        Direction.values().forEach {
            if (it.name.lowercase() == string.trim()) {
                return it
            }
        }
        throw IllegalArgumentException("Unknown direction \"$string\".")
    }

    fun encode(direction: Direction): String {
        return direction.name.lowercase()
    }
}