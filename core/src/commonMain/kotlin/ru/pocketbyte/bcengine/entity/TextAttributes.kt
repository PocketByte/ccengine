package ru.pocketbyte.bcengine.entity

import ru.pocketbyte.bcengine.html.HtmlAttribute

class TextAttributes(
    val fontFamily: String,
    val fontStyle: Set<Font.Style>,
    val fontSize: Float,
    val fontColor: Color,

    val aligningX: TextAligning,
    val aligningY: Aligning,
    val htmlAttributes: List<HtmlAttribute>,
    val noCrop: Boolean = false
)