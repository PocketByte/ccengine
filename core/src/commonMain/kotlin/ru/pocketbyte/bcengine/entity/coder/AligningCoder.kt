package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Aligning

object AligningCoder {
    fun decode(string: String): Aligning {
        Aligning.values().forEach {
            if (it.name.lowercase() == string.trim()) {
                return it
            }
        }
        throw IllegalArgumentException("Unknown aligning \"$string\".")
    }

    fun encode(aligning: Aligning): String {
        return aligning.name.lowercase()
    }
}