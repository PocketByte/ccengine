package ru.pocketbyte.bcengine.entity

class Stroke(
    val size: Size,
    val color: Color,
    val style: StrokeStyle = StrokeStyle()
)