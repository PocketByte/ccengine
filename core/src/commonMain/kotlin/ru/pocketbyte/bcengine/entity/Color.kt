package ru.pocketbyte.bcengine.entity

sealed class Color {
    companion object {
        val TRANSPARENT = RGBAColor(0, 0, 0, 0)
        val BLACK = RGBAColor(0, 0, 0)
        val WHITE = RGBAColor(255, 255, 255)
        val RED = RGBAColor(255, 0, 0)
        val GREEN = RGBAColor(0, 255, 0)
        val BLUE = RGBAColor(0, 0, 255)

        val INVALID_COLOR = TRANSPARENT
    }
}

data class RGBAColor(
    val r: Int,
    val g: Int,
    val b: Int,
    val a: Int = 255
): Color() {

    private val argbInt: Int by lazy {
        (a shl 24) + (r shl 16) + (g shl 8) + b
    }

    fun toARGBInt(): Int {
        return argbInt
    }
}

fun Color.isTransparent(): Boolean {
    return when (this) {
        is RGBAColor -> a == 0
    }
}