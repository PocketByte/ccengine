package ru.pocketbyte.bcengine.entity

sealed class Size {
    abstract val dimension: Dimension
    abstract val orientation: Orientation

    enum class Dimension(
        val sign: String,
        val isPhysical: Boolean
    ) {
        PIXEL("px", false),
        SCALED_PIXEL("sp", false),
        FRAME_PERCENT("%%",false),
        FRAME_PERCENT_HEIGHT("%%h", false),
        FRAME_PERCENT_WIDTH("%%w", false),
        PERCENT("%",false),
        PERCENT_HEIGHT("%h", false),
        PERCENT_WIDTH("%w", false),
        INCH("inch", true),
        MILLIMETER("mm", true),
        CENTIMETER("cm", true),
        COMPLEX("complex", false)
    }

    operator fun plus(size: Size): Size {
        return SizeComplex(this, size) { v1, v2 ->
            v1 + v2
        }
    }

    operator fun minus(size: Size): Size {
        return SizeComplex(this, size) { v1, v2 ->
            v1 - v2
        }
    }

    companion object {
        val ZERO = SizeSimple(0f, Dimension.SCALED_PIXEL)
        val NEGATIVE = SizeSimple(-1f, Dimension.SCALED_PIXEL)
    }

}

data class SizeSimple(
    val value: Float,
    override val dimension: Dimension,
    override val orientation: Orientation = Orientation.NONE
) : Size()

data class SizeComplex(
    val size1: Size,
    val size2: Size,
    val operation: (v1: Float, v2: Float) -> Float
) : Size() {
    override val dimension: Dimension
        get() = if (size1.dimension == size2.dimension) {
            size1.dimension
        } else {
            Dimension.COMPLEX
        }

    override val orientation: Orientation
        get() = if (size1.orientation == size2.orientation) {
            size1.orientation
        } else {
            Orientation.NONE
        }
}
