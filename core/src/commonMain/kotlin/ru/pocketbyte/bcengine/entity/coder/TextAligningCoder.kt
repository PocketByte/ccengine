package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.TextAligning

object TextAligningCoder {
    fun decode(string: String): TextAligning {
        TextAligning.values().forEach {
            if (it.name.lowercase() == string.trim()) {
                return it
            }
        }
        throw IllegalArgumentException("Unknown aligning \"$string\".")
    }

    fun encode(aligning: TextAligning): String {
        return aligning.name.lowercase()
    }
}