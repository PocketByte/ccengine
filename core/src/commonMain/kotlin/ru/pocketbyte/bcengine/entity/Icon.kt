package ru.pocketbyte.bcengine.entity

import ru.pocketbyte.bcengine.provider.AligningProvider
import ru.pocketbyte.bcengine.provider.ScaleModeProvider
import ru.pocketbyte.bcengine.provider.SizeProvider
import ru.pocketbyte.bcengine.provider.StringProvider

class Icon(
    val name: String
) {
    val image = StringProvider("")
    val width = SizeProvider(Size.ZERO, Orientation.HORIZONTAL)
    val height = SizeProvider(Size.ZERO, Orientation.VERTICAL)

    val scaleMode: ScaleModeProvider = ScaleModeProvider(ScaleMode.STRETCH)
    val aligningX: AligningProvider = AligningProvider(Aligning.CENTER)
    val aligningY: AligningProvider = AligningProvider(Aligning.CENTER)
}