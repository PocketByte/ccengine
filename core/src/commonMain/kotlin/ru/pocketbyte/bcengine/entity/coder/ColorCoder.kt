package ru.pocketbyte.bcengine.entity.coder

import ru.pocketbyte.bcengine.entity.Color
import ru.pocketbyte.bcengine.entity.RGBAColor
import kotlin.math.pow

object ColorCoder {

    private val hexDigits = charArrayOf(
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    )

    val namedColors = mapOf(
        Pair("empty", Color.TRANSPARENT),
        Pair("transparent", Color.TRANSPARENT),
        Pair("black", Color.BLACK),
        Pair("white", Color.WHITE),
        Pair("red", Color.RED),
        Pair("green", Color.GREEN),
        Pair("blue", Color.BLUE)
    )

    fun decode(colorString: String): Color? {
        namedColors[colorString.lowercase()]?.let { return it }

        return decodeRGBA(colorString)
    }

    fun encode(color: RGBAColor): String {
        return encodeRGBA(color)
    }

    private fun decodeRGBA(hexString: String): RGBAColor? {
        val hex = hexString.uppercase()
        if (hex[0] != '#') {
            return null
        }
        return when (hex.length) {
            7 -> RGBAColor(
                hexToInt(hex, 1, 2) ?: return null,
                hexToInt(hex, 3, 4) ?: return null,
                hexToInt(hex, 5, 6) ?: return null
            )
            9 -> RGBAColor(
                hexToInt(hex, 3, 4) ?: return null,
                hexToInt(hex, 5, 6) ?: return null,
                hexToInt(hex, 7, 8) ?: return null,
                hexToInt(hex, 1, 2) ?: return null
            )
            else -> null
        }
    }

    private fun encodeRGBA(color: RGBAColor): String {
        val builder = StringBuilder("#")
        val putColor: (it: Int) -> Unit = {
            builder.append(hexDigits[(it and 0xf0) shr 4])
            builder.append(hexDigits[it and 0x0f])
        }
        color.a.run(putColor)
        color.r.run(putColor)
        color.g.run(putColor)
        color.b.run(putColor)
        return builder.toString()
    }

    private fun hexToInt(hexString: String, start: Int, end: Int): Int? {
        if (
            start < 0 || start > hexString.length ||
            end < 0 || end > hexString.length ||
            end < start
        ) {
            return null
        }
        var result = 0
        val len = end - start
        for (i in len downTo 0) {
            val value = hexDigits.indexOf(hexString[i + start])
            if (value == -1) {
                return null
            }
            result += value * 16.0.pow(len - i).toInt()
        }
        return result
    }
}
