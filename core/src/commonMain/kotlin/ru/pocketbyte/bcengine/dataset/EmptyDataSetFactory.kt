package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.asDataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue

class EmptyDataSetFactory: DataSetFactory {

    var size: Formula<Float> = formulaStaticValue(0f)

    override fun get(context: Context, project: Project): DataSet {
        return ListDataSet(List(size.compute(project, null).toInt()) {
            mapOf(
                Pair(DataSet.ITEM_INDEX, it.toString()),
                Pair(DataSet.ITEM_SUB_INDEX, "0"),
                Pair(DataSet.ROW_INDEX, it.toString()),
                Pair(DataSet.AMOUNT, "1")
            ).asDataSetItem()
        })
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other is EmptyDataSetFactory) {
            return size == other.size
        }
        return false
    }

    override fun hashCode(): Int {
        return size.hashCode()
    }
}