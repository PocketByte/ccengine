package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.cast.FormulaDataCast
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType

object SystemDataCast {
    fun addTo(map: MutableMap<String, Formula<*>>) {
        map[DataSet.ITEM_INDEX] = FormulaDataCast(null, DataSet.ITEM_INDEX, NumberValueType)
        map[DataSet.ITEM_SUB_INDEX] = FormulaDataCast(null, DataSet.ITEM_SUB_INDEX, NumberValueType)
        map[DataSet.ROW_INDEX] = FormulaDataCast(null, DataSet.ROW_INDEX, NumberValueType)
        map[DataSet.AMOUNT] = FormulaDataCast(null, DataSet.AMOUNT, NumberValueType)
    }
}