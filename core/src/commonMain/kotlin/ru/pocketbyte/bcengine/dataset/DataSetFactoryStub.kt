package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project

object DataSetFactoryStub: DataSetFactory {
    override fun get(context: Context, project: Project): DataSet {
        throw IllegalStateException("Project Dataset not set!")
    }
}
