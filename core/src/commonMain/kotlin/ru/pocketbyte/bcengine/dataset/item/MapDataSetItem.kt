package ru.pocketbyte.bcengine.dataset.item

class MapDataSetItem(
    private val map: Map<String, String>
): DataSetItem {

    constructor(vararg pairs: Pair<String, String>) : this(mapOf(*pairs))

    override fun get(key: String): String? {
        return map[key]
    }
}

internal fun Map<String, String>.asDataSetItem(): DataSetItem {
    return MapDataSetItem(this)
}
