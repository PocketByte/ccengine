package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.dataset.item.DataSetItem

open class ListDataSet(
    private val list: List<DataSetItem>
): DataSet {
    override val size: Int = list.size

    override fun get(index: Int): DataSetItem {
        return list[index]
    }
}
