package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.formula.Formula

expect class CSVDataSetFactory(): DataSetFactory {
    var path: Formula<String>
}