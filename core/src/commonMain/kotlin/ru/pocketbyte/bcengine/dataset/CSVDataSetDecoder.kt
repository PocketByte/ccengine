package ru.pocketbyte.bcengine.dataset

typealias DataSetMutableItem = MutableMap<String, String>

object CSVDataSetDecoder: AbsTableDataSetDecoder<String, String>() {

    private const val QUOTES_CHAR = '"'
    private const val SEPARATOR_CHAR = ','

    override fun readRow(rows: Iterator<String>): String {
        val builder = StringBuilder(rows.next())
        var quotesCount = builder.quotesCount()

        while (rows.hasNext() && quotesCount % 2 != 0) {
            builder.append("\n")
            rows.next().apply {
                quotesCount += this.quotesCount()
                builder.append(this)
            }
        }
        return builder.toString()
    }

    @Throws(RowDecodeException::class)
    override fun visitColumns(
        row: String,
        visitor: (columnIndex: Int, columnItem: String) -> Unit
    ) {
        var index = 0
        var itemBuilder = StringBuilder()
        var previousCharIsQuotes = false
        var isQuotesStart = false
        row.forEachIndexed { charIndex, char ->
            if (char == QUOTES_CHAR) {
                if (!isQuotesStart) {
                    isQuotesStart = true
                    previousCharIsQuotes = false
                    if (itemBuilder.trim().isNotEmpty()) {
                        throw RowDecodeException(charIndex)
                    }
                    itemBuilder = StringBuilder()
                } else if (previousCharIsQuotes) {
                    itemBuilder.append(char)
                    previousCharIsQuotes = false
                } else {
                    previousCharIsQuotes = true
                }
            } else if (char == SEPARATOR_CHAR) {
                if (isQuotesStart) {
                    if (previousCharIsQuotes) {
                        visitor(index++, itemBuilder.toString())
                        itemBuilder = StringBuilder()
                        isQuotesStart = false
                        previousCharIsQuotes = false
                    } else {
                        itemBuilder.append(char)
                        previousCharIsQuotes = false
                    }
                } else {
                    visitor(index++, itemBuilder.trim().toString())
                    itemBuilder = StringBuilder()
                    isQuotesStart = false
                    previousCharIsQuotes = false
                }
            } else {
                itemBuilder.append(char)
                previousCharIsQuotes = false
            }
        }
        if (isQuotesStart) {
            if (previousCharIsQuotes) {
                visitor(index, itemBuilder.toString())
            } else {
                throw RowDecodeException(row.length)
            }
        } else {
            visitor(index, itemBuilder.trim().toString())
        }
    }

    private fun CharSequence.quotesCount(): Int {
        var result = 0
        var isEscaping = false
        forEach {
            if (it == QUOTES_CHAR && !isEscaping) {
                result++
            }
            isEscaping = it == '\\' && !isEscaping
        }
        return result
    }
}