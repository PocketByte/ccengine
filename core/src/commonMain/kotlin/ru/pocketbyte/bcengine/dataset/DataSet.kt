package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.dataset.item.DataSetItem

interface DataSet {
    val size: Int
    fun get(index: Int): DataSetItem

    companion object {
        public const val ITEM_INDEX = "ITEM_INDEX"
        public const val ITEM_SUB_INDEX = "ITEM_SUB_INDEX"
        public const val ROW_INDEX = "ROW_INDEX"
        public const val AMOUNT = "AMOUNT"
    }
}

inline fun DataSet.forEachIndexed(action: (index: Int, item: DataSetItem) -> Unit) {
    for (index in 0 until size) {
        action(index, get(index))
    }
}

inline fun DataSet.forEach(action: (item: DataSetItem) -> Unit) {
    for (index in 0 until size) {
        action(get(index))
    }
}

fun DataSet.getOrNull(index: Int): DataSetItem? {
    if (index in 0 until size) {
        return get(index)
    }
    return null
}