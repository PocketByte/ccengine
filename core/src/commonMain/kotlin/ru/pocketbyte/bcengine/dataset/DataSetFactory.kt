package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project

fun interface DataSetFactory {
    fun get(context: Context, project: Project): DataSet
}