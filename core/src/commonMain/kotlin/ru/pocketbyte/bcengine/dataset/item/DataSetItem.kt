package ru.pocketbyte.bcengine.dataset.item


//typealias DataSetItem = Map<String, String>

interface DataSetItem {
    fun get(key: String): String?
}