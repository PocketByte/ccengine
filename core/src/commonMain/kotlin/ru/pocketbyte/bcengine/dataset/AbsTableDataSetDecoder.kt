package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.dataset.item.asDataSetItem

abstract class AbsTableDataSetDecoder<IteratorRow, Row> {

    class RowDecodeException(
        val position: Int,
        message: String = ""
    ): Exception(message)

    fun decode(
        rows: Iterator<IteratorRow>,
        project: Project
    ): DataSet {
        return ListDataSet(decodeToList(rows, project))
    }

    abstract fun readRow(rows: Iterator<IteratorRow>): Row

    @Throws(RowDecodeException::class)
    abstract fun visitColumns(row: Row, visitor: (columnIndex: Int, columnItem: String) -> Unit)

    private fun decodeToList(
        rows: Iterator<IteratorRow>,
        project: Project
    ): List<DataSetItem> {
        val result = mutableListOf<DataSetItem>()
        var tableIndex = 0
        var rowIndex = 0
        var itemIndex = 0

        if (!rows.hasNext()) {
            return emptyList()
        }

        val headers = mutableListOf<String>()
        try {
            visitColumns(readRow(rows)) { _, rowItem ->
                headers.add(rowItem)
            }
        } catch (e: RowDecodeException) {
            throw RuntimeException(
                "Failed to decode table at 0:${e.position}."
            )
        }

        checkFieldOverride(DataSet.ITEM_INDEX, headers, project)
        checkFieldOverride(DataSet.ITEM_SUB_INDEX, headers, project)
        checkFieldOverride(DataSet.ROW_INDEX, headers, project)

        while (rows.hasNext()) {
            tableIndex++
            val itemMap: DataSetMutableItem = mutableMapOf()
            try {
                visitColumns(readRow(rows)) { columnIndex, columnItem ->
                    itemMap[headers[columnIndex]] = columnItem
                }
            } catch (e: RowDecodeException) {
                throw RuntimeException(
                    "Failed to decode table at ${tableIndex}:${e.position}"
                )
            }

            if (headers.contains(DataSet.ITEM_INDEX).not()) {
                itemMap[DataSet.ITEM_INDEX] = itemIndex.toString()
            }
            if (headers.contains(DataSet.ITEM_SUB_INDEX).not()) {
                itemMap[DataSet.ITEM_SUB_INDEX] = "0"
            }
            if (headers.contains(DataSet.ROW_INDEX).not()) {
                itemMap[DataSet.ROW_INDEX] = rowIndex.toString()
            }
            if (headers.contains(DataSet.AMOUNT).not()) {
                itemMap[DataSet.AMOUNT] = project.dataSetMultiply.toString()
            } else {
                itemMap[DataSet.AMOUNT] = itemMap[DataSet.AMOUNT]
                    ?.toIntOrNull()
                    .let { it ?: 0 }
                    .let { it * project.dataSetMultiply }
                    .let { if (it < 0) { 0 } else { it } }
                    .toString()
            }

            val amount = itemMap[DataSet.AMOUNT]?.toIntOrNull() ?: 0

            if (amount > 0) {
                result.add(itemMap.asDataSetItem())
                rowIndex++
                itemIndex++

                if (amount > 1) {
                    for (subIndex in 1 until amount) {
                        result.add(itemMap.toMutableMap().apply {
                            if (headers.contains(DataSet.ITEM_INDEX).not()) {
                                set(DataSet.ITEM_INDEX, (itemIndex++).toString())
                            }
                            if (headers.contains(DataSet.ITEM_SUB_INDEX).not()) {
                                set(DataSet.ITEM_SUB_INDEX, (subIndex).toString())
                            }
                        }.asDataSetItem())
                    }
                }
            }

        }
        return result
    }

    private fun checkFieldOverride(field: String, headers: List<String>, project: Project) {
        if (headers.contains(field)) {
            warnFieldOverriden(field, project)
        }
    }

    private fun warnFieldOverriden(field: String, project: Project) {
        println("Warning: $field filed overriden by data set column")
    }
}