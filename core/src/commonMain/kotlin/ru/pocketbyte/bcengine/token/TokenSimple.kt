package ru.pocketbyte.bcengine.token

data class TokenSimple(
    override val parentString: String,
    override val startIndex: Int = 0,
    override val endIndex: Int = parentString.length - 1
): AbsToken(), Token