package ru.pocketbyte.bcengine.token

interface TokenRepresentation {
    val token: Token?
}