package ru.pocketbyte.bcengine.token

abstract class AbsToken : Token {

    private val metadata: MutableMap<String, Any> by lazy {
        mutableMapOf()
    }

    override fun addMetaData(key: String, value: Any) {
        metadata[key] = value
    }

    override fun getMetaData(key: String): Any? {
        return metadata[key]
    }
}