package ru.pocketbyte.bcengine.token

interface Token {
    val parentString: String
    val startIndex: Int
    val endIndex: Int

    fun addMetaData(key: String, value: Any)
    fun getMetaData(key: String): Any?

    companion object {
        const val METADATA_PROCESSOR = "PROCESSOR"
    }
}

fun Token.getTokenString(): String {
    return parentString.substring(startIndex, endIndex + 1)
}

fun Token?.isNullOrBlank(): Boolean {
    if (this == null) {
        return true
    }
    for (i in startIndex .. endIndex) {
        if (parentString[i].isWhitespace().not()) {
            return false
        }
    }
    return true
}