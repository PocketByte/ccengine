package ru.pocketbyte.bcengine.shape

interface ShapeGroup {
    val shapes: List<Shape>
    fun add(shape: Shape)
}
