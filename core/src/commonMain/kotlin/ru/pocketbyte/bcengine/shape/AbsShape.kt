package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Aligning
import ru.pocketbyte.bcengine.entity.Anchor
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeRange
import ru.pocketbyte.bcengine.entity.percent
import ru.pocketbyte.bcengine.provider.AnchorProvider
import ru.pocketbyte.bcengine.provider.FloatProvider
import ru.pocketbyte.bcengine.provider.SizeProvider
import ru.pocketbyte.bcengine.provider.SizeRangeProvider
import kotlin.math.max

abstract class AbsShape(
    defaultWidth: Size = 100.percent(),
    defaultHeight: Size = 100.percent()
): Shape {
    override val x: AnchorProvider =
        AnchorProvider(Anchor(0.percent(), Aligning.START), Orientation.HORIZONTAL)

    override val y: AnchorProvider =
        AnchorProvider(Anchor(0.percent(), Aligning.START), Orientation.VERTICAL)

    override val width = SizeRangeProvider(SizeRange(defaultWidth), Orientation.HORIZONTAL)
    override val height = SizeRangeProvider(SizeRange(defaultHeight), Orientation.VERTICAL)

    override val angle: FloatProvider = FloatProvider(0f)

    final override var positionX: Float = 0f
        private set

    final override var positionY: Float = 0f
        private set

    final override var anchorAligningX: Aligning = Aligning.START
        private set

    final override var anchorAligningY: Aligning = Aligning.START
        private set

    final override var measuredWidth: Float = 0f
        private set

    final override var measuredHeight: Float = 0f
        private set

    var parentWidth: Float = 0f
        private set

    var parentHeight: Float = 0f
        private set

    val frameWidth get() = if (measuredWidth <= 0) parentWidth else measuredWidth
    val frameHeight get() = if (measuredHeight <= 0) parentHeight else measuredHeight

    protected var minWidth: Float = 0f
        private set
    protected var maxWidth: Float = 0f
        private set
    protected var minHeight: Float = 0f
        private set
    protected var maxHeight: Float = 0f
        private set

    protected val widthIsRanged: Boolean
        get() = minWidth <= 0 || maxWidth <= 0 || minWidth != maxWidth

    protected val heightIsRanged: Boolean
        get() = minHeight <= 0 || maxHeight <= 0 || minHeight != maxHeight

    abstract fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    )

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        this.parentWidth = parentWidth
        this.parentHeight = parentHeight
        this.measuredWidth = 0f
        this.measuredHeight = 0f

        val widthRange = width.get(project, dataItem)
        val heightRange = height.get(project, dataItem)

        this.minWidth = project.layout.projectFloat(widthRange.min, parentWidth, parentHeight)
        this.maxWidth = project.layout.projectFloat(widthRange.max, parentWidth, parentHeight)
        this.minHeight = project.layout.projectFloat(heightRange.min, parentWidth, parentHeight)
        this.maxHeight = project.layout.projectFloat(heightRange.max, parentWidth, parentHeight)

        onMeasure(minWidth, minHeight)
    }

    final override fun layout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        doLayout(context, project, canvas, dataItem, positionX, positionY)

        val initialWidth = frameWidth
        val initialHeight = frameHeight
        measureAfterLayout(context, project, canvas, dataItem, positionX, positionY)

        if (initialWidth != frameWidth || initialHeight != frameHeight) {
            layout(context, project, canvas, dataItem, positionX, positionY)
//            onLayout(
//                x.getFloat(project, dataItem) + positionX,
//                y.getFloat(project, dataItem) + positionY
//            )
        }
    }

    final override fun draw(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        withTransformOf(project, canvas, dataItem) {
            drawShape(context, project, canvas, dataItem)
        }
    }

    protected fun onMeasure(width: Float, height: Float) {
        measuredWidth = if (maxWidth <= 0) {
            max(width, minWidth)
        } else {
            width.coerceIn(minWidth, maxWidth)
        }
        measuredHeight = if (maxHeight <= 0) {
            max(height, minHeight)
        } else {
            height.coerceIn(minHeight, maxHeight)
        }
    }

    protected open fun onLayout(
        positionX: Float,
        positionY: Float
    ) {
        this.positionX = positionX
        this.positionY = positionY
    }

    protected open fun doLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        onLayout(
            x.getFloat(project, dataItem) + positionX,
            y.getFloat(project, dataItem) + positionY
        )
    }

    protected open fun measureAfterLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        // Do nothing
    }

    protected fun SizeProvider.getFloat(
        dataLayer: DataSetItem?,
        project: Project,
        parentWidth: Float = frameWidth,
        parentHeight: Float = frameHeight
    ): Float {
        return project.layout.projectFloat(
            this.get(project, dataLayer),
            parentWidth, parentHeight
        )
    }

    protected fun SizeRangeProvider.getMinFloat(
        dataLayer: DataSetItem?,
        project: Project,
        parentWidth: Float = frameWidth,
        parentHeight: Float = frameHeight
    ): Float {
        return project.layout.projectFloat(
            this.get(project, dataLayer).min,
            parentWidth, parentHeight
        )
    }

    protected fun AnchorProvider.getFloat(project: Project, dataLayer: DataSetItem?): Float {
        val anchor = get(project, dataLayer)
        val position = project.layout.projectFloat(anchor.size, frameWidth, frameHeight)
        return when (anchor.aligning) {
            Aligning.START -> position
            Aligning.CENTER -> when(anchor.size.orientation) {
                Orientation.NONE -> position
                Orientation.HORIZONTAL -> position - frameWidth / 2f
                Orientation.VERTICAL -> position - frameHeight / 2f
            }
            Aligning.END -> when(anchor.size.orientation) {
                Orientation.NONE -> position
                Orientation.HORIZONTAL -> position - frameWidth
                Orientation.VERTICAL -> position - frameHeight
            }
        }
    }
}