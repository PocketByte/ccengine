package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.*
import ru.pocketbyte.bcengine.html.*
import ru.pocketbyte.bcengine.provider.AligningProvider
import ru.pocketbyte.bcengine.provider.FontProvider
import ru.pocketbyte.bcengine.provider.StringProvider
import ru.pocketbyte.bcengine.provider.TextAligningProvider

class TextShape: LayoutShape() {

    enum class Flag(
        val key: String
    ) {
        NO_HTML("nohtml"),
        NO_CROP("nocrop");

        companion object {
            const val SEPARATOR = ";"
        }
    }

    enum class FontSizeOperator(
        val char: Char,
        val operation: (fontSizeSp: Float, value: Float) -> Float
    ) {
        PLUS('+', { fontSizeSp, floatValue -> fontSizeSp + floatValue }),
        MINUS('-', { fontSizeSp, floatValue -> fontSizeSp - floatValue }),
        MULTIPLY('*', { fontSizeSp, floatValue -> fontSizeSp * floatValue }),
        DIVIDE('/', { fontSizeSp, floatValue -> fontSizeSp / floatValue });
    }

    val text: StringProvider = StringProvider("")
    val font: FontProvider = FontProvider()

    val aligningX: TextAligningProvider = TextAligningProvider(TextAligning.START)
    val aligningY: AligningProvider = AligningProvider(Aligning.START)

    val flags: StringProvider = StringProvider("")

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        super.measure(context, project, canvas, dataItem, parentWidth, parentHeight)
        val flags = decodeFlags(project, dataItem)

        val decodeResult = decodeText(project, dataItem, flags)
        val textAttributes = decodeTextAttributes(context, project, dataItem, decodeResult, flags)

        canvas.measureText(
            measuredWidth, measuredHeight,
            decodeResult.string,
            textAttributes
        ).let {
            onMeasure(it.first, it.second)
        }
    }

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        val flags = decodeFlags(project, dataItem)

        val decodeResult = decodeText(project, dataItem, flags)
        val textAttributes = decodeTextAttributes(context, project, dataItem, decodeResult, flags)

        canvas.drawText(
            measuredWidth, measuredHeight,
            decodeResult.string,
            textAttributes
        )
        super.drawShape(context, project, canvas, dataItem)
    }

    private fun decodeFlags(project: Project, dataItem: DataSetItem): List<Flag> {
        return flags.get(project, dataItem)
            .split(Flag.SEPARATOR)
            .mapNotNull {
                Flag.values()
                    .find { flag -> flag.key ==  it.trim().lowercase() }
            }
    }

    private fun decodeText(
        project: Project,
        dataItem: DataSetItem,
        flags: List<Flag>
    ): HtmlDecodeResult {
        val text = this.text.get(project, dataItem)
        return if (flags.contains(Flag.NO_HTML)) {
            HtmlDecodeResult(text, emptyList())
        } else {
            HtmlStringDecoder.decode(text)
        }
    }

    private fun decodeTextAttributes(
        context: Context,
        project: Project,
        dataItem: DataSetItem,
        decodedText: HtmlDecodeResult,
        flags: List<Flag>
    ): TextAttributes {
        val font = this.font.get(project, dataItem)
        return TextAttributes(
            fontFamily = font.family,
            fontStyle = font.style,
            fontSize = project.layout.projectSp(
                font.size, frameWidth, frameHeight
            ),
            fontColor = font.color,
            aligningX = aligningX.get(project, dataItem),
            aligningY = aligningY.get(project, dataItem),
            htmlAttributes = decodedText.attrs
                .mapAttributes(context, project, dataItem, font.size),
            noCrop = flags.contains(Flag.NO_CROP)
        )
    }

    private fun List<HtmlAttribute>.mapAttributes(
        context: Context, project: Project,
        dataItem: DataSetItem, fontSize: Size
    ): List<HtmlAttribute> {
        return map { htmlAttribute ->
            when (htmlAttribute.tag) {
                HTMLTagImage -> {
                    htmlAttribute.mapImageTag(context, project, dataItem, fontSize)
                }
                HTMLTagFont -> {
                    htmlAttribute.mapFontTag(context, project, fontSize)
                }
                else -> {
                    return@map htmlAttribute
                }
            }
        }
    }

    private fun HtmlAttribute.mapImageTag(
        context: Context, project: Project,
        dataItem: DataSetItem, fontSize: Size
    ): HtmlAttribute {
        return this.copy(
            attributes = this.attributes?.toMutableMap()?.apply {
                // Put values one more time to create entries with null values
                // if entries doesn't exists
                put(HTMLTagImage.width, getValue(HTMLTagImage.width))
                put(HTMLTagImage.height, getValue(HTMLTagImage.height))
            }?.mapValues { entry ->
                when(entry.key) {
                    HTMLTagImage.src -> {
                        getStringAttribute(HTMLTagImage.src)
                            ?.let { project.icons.getIcon(it) }
                            ?.image
                            ?.get(project, dataItem)
                            ?: entry.value
                    }
                    HTMLTagImage.width, HTMLTagImage.height -> {
                        entry.value?.toString()
                            ?.trim()
                            ?.mapFontSize(context, project, fontSize)
                            ?: project.layout.projectFloat(
                                fontSize, frameWidth, frameHeight
                            )
                    }
                    HTMLTagImage.advanceWidth, HTMLTagImage.advanceHeight -> {
                        entry.value
                            ?.toString()
                            ?.toFloatOrNull()
                            ?.let { if (it == 0f) null else it }
                    }
                    else -> entry.value
                }
            }
        )
    }

    private fun HtmlAttribute.mapFontTag(
        context: Context, project: Project, fontSize: Size
    ): HtmlAttribute {
        return this.copy(
            attributes = this.attributes?.mapValues { entry ->
                if (entry.key == HTMLTagFont.size) {
                    entry.value?.toString()
                        ?.trim()
                        ?.mapFontSize(context, project, fontSize)
                } else {
                    entry.value
                }
            }
        )
    }
    private fun String.mapFontSize(
        context: Context, project: Project, fontSize: Size
    ): Float? {
        if (this.isBlank()) {
            return null
        }

        if (first().isDigit() || first() == '.') {
            return toFloatOrNull()?.let {
                project.layout.projectSp(
                    it, Size.Dimension.SCALED_PIXEL,
                    frameWidth, frameHeight
                )
            }
        }

        val floatValue = this
            .substring(1)
            .toFloatOrNull()
            ?: return null

        val fontSizeSp = project.layout.projectSp(
            fontSize, frameWidth, frameHeight
        )

        return FontSizeOperator.values()
            .find { it.char == first() }
            ?.let { it.operation(fontSizeSp, floatValue) }
    }
}