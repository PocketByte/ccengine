package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Size
import kotlin.math.max

open class LayoutShape(
    defaultWidth: Size = Size.ZERO,
    defaultHeight: Size = Size.ZERO
): AbsShape(defaultWidth, defaultHeight), ShapeGroup {

    override val shapes: MutableList<Shape> = mutableListOf()

    override fun add(shape: Shape) {
        shapes.add(shape)
    }

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        shapes.forEach {
            it.draw(context, project, canvas, dataItem)
        }
    }

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        super.measure(context, project, canvas, dataItem, parentWidth, parentHeight)
        shapes.forEach {
            it.measure(
                context, project, canvas, dataItem,
                frameWidth, frameHeight
            )
        }
    }

    override fun doLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        super.doLayout(context, project, canvas, dataItem, positionX, positionY)
        layoutChildren(context, project, canvas, dataItem)
    }

    protected open fun layoutChildren(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
    ) {
        shapes.forEach {
            it.layout(context, project, canvas, dataItem, 0f, 0f)
        }
    }

    override fun measureAfterLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        if (shapes.isEmpty()) {
            return
        }

        var contentWidth = 0f
        var contentHeight = 0f

        shapes.forEach {
            if (it.measuredWidth > 0 && it.measuredHeight > 0) {
                contentWidth = max(contentWidth, it.positionX + it.measuredWidth)
                contentHeight = max(contentHeight, it.positionY + it.measuredHeight)
            }
        }

        onMeasure(contentWidth, contentHeight)
    }
}