package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.provider.AligningProvider
import ru.pocketbyte.bcengine.provider.ScaleModeProvider
import ru.pocketbyte.bcengine.provider.StringProvider
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.entity.Aligning
import ru.pocketbyte.bcengine.entity.ImageAttributes
import ru.pocketbyte.bcengine.entity.ScaleMode

class ImageShape: LayoutShape() {

    val image: StringProvider = StringProvider("")
    val scaleMode: ScaleModeProvider = ScaleModeProvider(ScaleMode.STRETCH)
    val aligningX: AligningProvider = AligningProvider(Aligning.CENTER)
    val aligningY: AligningProvider = AligningProvider(Aligning.CENTER)

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        canvas.drawImage(
            measuredWidth, measuredHeight,
            this.image.get(project, dataItem).trim(),
            ImageAttributes(
                scaleMode = scaleMode.get(project, dataItem),
                aligningX = aligningX.get(project, dataItem),
                aligningY = aligningY.get(project, dataItem)
            )
        )
        super.drawShape(context, project, canvas, dataItem)
    }
}