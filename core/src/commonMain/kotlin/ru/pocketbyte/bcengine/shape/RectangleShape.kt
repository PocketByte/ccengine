package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.*
import ru.pocketbyte.bcengine.provider.ColorProvider
import ru.pocketbyte.bcengine.provider.SizeProvider
import ru.pocketbyte.bcengine.provider.StrokeProvider

class RectangleShape: LayoutShape() {

    val background: ColorProvider = ColorProvider(Color.TRANSPARENT)
    var stroke: StrokeProvider = StrokeProvider()

    val cornerRadiusX: SizeProvider = SizeProvider(0.percent(), Orientation.HORIZONTAL)
    val cornerRadiusY: SizeProvider = SizeProvider(0.percent(), Orientation.VERTICAL)

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {

        val background = this.background.get(project, dataItem)
        val stroke = this.stroke.get(project, dataItem)
        val cornerRadiusX = this.cornerRadiusX.getFloat(dataItem, project)
        val cornerRadiusY = this.cornerRadiusY.getFloat(dataItem, project)

        val roundCorners = cornerRadiusX > 0f || cornerRadiusY > 0f

        if (!background.isTransparent()) {
            if (roundCorners) {
                canvas.drawRoundRect(
                    measuredWidth, measuredHeight,
                    background,
                    cornerRadiusX, cornerRadiusY
                )
            } else {
                canvas.drawRect(
                    measuredWidth, measuredHeight,
                    background
                )
            }
        }

        val strokeSize = project.layout.projectFloat(
            stroke.size, frameWidth, frameHeight
        )
        val strokeIsEmpty = strokeSize == 0f || stroke.color.isTransparent()
        if (!strokeIsEmpty) {
            if (roundCorners) {
                canvas.drawRoundRect(
                    measuredWidth, measuredHeight,
                    strokeSize, stroke.color, stroke.style,
                    cornerRadiusX, cornerRadiusY
                )
            } else {
                canvas.drawRect(
                    measuredWidth, measuredHeight,
                    strokeSize, stroke.color, stroke.style
                )
            }
        }
        super.drawShape(context, project, canvas, dataItem)
    }
}