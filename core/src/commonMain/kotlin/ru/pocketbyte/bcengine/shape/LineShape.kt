package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.provider.StrokeProvider
import ru.pocketbyte.bcengine.canvas.Canvas

class LineShape: AbsShape() {

    var stroke: StrokeProvider = StrokeProvider()

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        val x2 = this.width.getMinFloat(dataItem, project)
        val y2 = this.height.getMinFloat(dataItem, project)
        val stroke = this.stroke.get(project, dataItem)

        canvas.drawLine(
            x2, y2,
            project.layout.projectFloat(
                stroke.size, frameWidth, frameHeight
            ),
            stroke.color, stroke.style
        )
    }
}