package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem

fun Shape.withTransformOf(
    project: Project,
    canvas: Canvas,
    dataItem: DataSetItem,
    action: () -> Unit
) {
    val save = canvas.save()
    canvas.translate(positionX, positionY)
    canvas.rotate(this.angle.get(project, dataItem))
//    canvas.clipRect(0f, 0f, measuredWidth, measuredHeight)
    action()
    canvas.restore(save)
}