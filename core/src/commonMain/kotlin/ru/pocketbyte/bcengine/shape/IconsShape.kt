package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.canvas.withSave
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.*
import ru.pocketbyte.bcengine.provider.*
import kotlin.math.max

class IconsShape: LayoutShape() {

    val icons: StringListProvider = StringListProvider(emptyList())

    val orientation: OrientationProvider = OrientationProvider(Orientation.HORIZONTAL)
    val direction: DirectionProvider = DirectionProvider(Direction.NORMAL)

    val aligningX: AligningProvider = AligningProvider(Aligning.START)
    val aligningY: AligningProvider = AligningProvider(Aligning.START)

    val iconWidth: SizeProvider = SizeProvider(Size.ZERO, Orientation.HORIZONTAL)
    val iconHeight: SizeProvider = SizeProvider(Size.ZERO, Orientation.VERTICAL)
    val iconAligning: AligningProvider = AligningProvider(Aligning.END)

    private class IconItem (
        val image: String,
        val width: Float,
        val height: Float,
        val imageAttributes: ImageAttributes
    )

    private class IconGroup(
        val startIndex: Int,
        val endIndex: Int,
        val width: Float,
        val height: Float
    )

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        super.measure(context, project, canvas, dataItem, parentWidth, parentHeight)

        if (!widthIsRanged && !heightIsRanged) {
            return
        }

        val icons = projectIcons(context, project, canvas, dataItem)
        val orientation = this.orientation.get(project, dataItem)
        val (contentWidth, contentHeight) = if (orientation == Orientation.HORIZONTAL) {
            measureRows(icons)
        } else {
            measureCols(icons)
        }
        onMeasure(contentWidth, contentHeight)
    }

    private fun measureRows(
        icons: List<IconItem>
    ): Pair<Float, Float> {
        var totalHeight = 0F
        var totalWidth = 0F
        var iconIndex = 0
        while (iconIndex < icons.size) {
            val rowEnd = icons.getRowEnd(iconIndex, measuredWidth)
            totalWidth = max(totalWidth, icons.getRowWidth(iconIndex, rowEnd))
            totalHeight += icons.getRowHeight(iconIndex, rowEnd)
            iconIndex = rowEnd + 1
        }
        return Pair(totalWidth, totalHeight)
    }

    private fun measureCols(
        icons: List<IconItem>
    ): Pair<Float, Float> {
        var totalHeight = 0F
        var totalWidth = 0F
        var iconIndex = 0
        while (iconIndex < icons.size) {
            val colEnd = icons.getColEnd(iconIndex, measuredHeight)
            totalWidth += icons.getColWidth(iconIndex, colEnd)
            totalHeight = max(totalHeight, icons.getColHeight(iconIndex, colEnd))
            iconIndex = colEnd + 1
        }
        return Pair(totalWidth, totalHeight)
    }

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        val icons = projectIcons(context, project, canvas, dataItem)

        if (this.orientation.get(project, dataItem) == Orientation.HORIZONTAL) {
            drawRows(context, project, canvas, icons, dataItem)
        } else {
            drawCols(context, project, canvas, icons, dataItem)
        }
        super.drawShape(context, project, canvas, dataItem)
    }

    private fun drawRows(
        context: Context, project: Project,
        canvas: Canvas, icons: List<IconItem>,
        dataItem: DataSetItem
    ) {
        val iconAligning = this.iconAligning.get(project, dataItem)
        val aligningX = this.aligningX.get(project, dataItem)
        val aligningY = this.aligningY.get(project, dataItem)
        val direction = this.direction.get(project, dataItem)

        var totalHeight = 0F
        val groups = mutableListOf<IconGroup>()
        var iconIndex = 0
        while (iconIndex < icons.size) {
            val rowEnd = icons.getRowEnd(iconIndex, measuredWidth)
            val iconGroup = IconGroup(
                iconIndex, rowEnd,
                icons.getRowWidth(iconIndex, rowEnd),
                icons.getRowHeight(iconIndex, rowEnd)
            )
            groups.add(iconGroup)
            totalHeight += iconGroup.height
            iconIndex = rowEnd + 1
        }

        when (aligningY) {
            Aligning.START -> {
                // Do nothing
            }
            Aligning.CENTER -> {
                canvas.translate(0f, (measuredHeight - totalHeight) / 2f)
            }
            Aligning.END -> {
                canvas.translate(0f, measuredHeight - totalHeight)
            }
        }

        groups.forEach { iconGroup ->
            val canvasSave = canvas.save()
            if (direction == Direction.REVERS) {
                canvas.translate(measuredWidth, 0f)
            }

            when (aligningX) {
                Aligning.START -> {
                    // Do nothing
                }
                Aligning.CENTER -> {
                    canvas.translate(
                        (measuredWidth - iconGroup.width) / 2f * direction.multiplier, 0f
                    )
                }
                Aligning.END -> {
                    canvas.translate(
                        (measuredWidth - iconGroup.width) * direction.multiplier, 0f
                    )
                }
            }

            for (i in iconGroup.startIndex..iconGroup.endIndex) {
                val icon = icons[i]

                canvas.withSave {
                    canvas.translate(
                        if (direction == Direction.REVERS) -icon.width else 0f,
                        when (iconAligning) {
                            Aligning.START -> 0f
                            Aligning.CENTER -> (iconGroup.height - icon.height) / 2f
                            Aligning.END -> iconGroup.height - icon.height
                        }
                    )
                    canvas.drawImage(
                        icon.width, icon.height,
                        icon.image, icon.imageAttributes
                    )
                }
                canvas.translate(
                    if (direction == Direction.REVERS) 0f else icon.width,
                    0f
                )
            }
            canvas.restore(canvasSave)
            canvas.translate(0f, iconGroup.height)
        }
    }

    private fun drawCols(
        context: Context, project: Project,
        canvas: Canvas, icons: List<IconItem>,
        dataItem: DataSetItem
    ) {
        val iconAligning = this.iconAligning.get(project, dataItem)
        val aligningX = this.aligningX.get(project, dataItem)
        val aligningY = this.aligningY.get(project, dataItem)
        val direction = this.direction.get(project, dataItem)

        var totalWidth = 0F
        val groups = mutableListOf<IconGroup>()
        var iconIndex = 0
        while (iconIndex < icons.size) {
            val colEnd = icons.getColEnd(iconIndex, measuredHeight)
            val iconGroup = IconGroup(
                iconIndex, colEnd,
                icons.getColWidth(iconIndex, colEnd),
                icons.getColHeight(iconIndex, colEnd)
            )
            groups.add(iconGroup)
            totalWidth += iconGroup.width
            iconIndex = colEnd + 1
        }

        when (aligningX) {
            Aligning.START -> {
                // Do nothing
            }
            Aligning.CENTER -> {
                canvas.translate((measuredWidth - totalWidth) / 2f, 0f)
            }
            Aligning.END -> {
                canvas.translate(measuredWidth - totalWidth, 0f)
            }
        }

        groups.forEach { iconGroup ->
            val canvasSave = canvas.save()
            if (direction == Direction.REVERS) {
                canvas.translate(0f, measuredHeight)
            }

            when (aligningY) {
                Aligning.START -> {
                    // Do nothing
                }
                Aligning.CENTER -> {
                    canvas.translate(
                        0f, (measuredHeight - iconGroup.height) / 2f * direction.multiplier
                    )
                }
                Aligning.END -> {
                    canvas.translate(
                        0f, (measuredHeight - iconGroup.height) * direction.multiplier
                    )
                }
            }

            for (i in iconGroup.startIndex..iconGroup.endIndex) {
                val icon = icons[i]

                canvas.withSave {
                    canvas.translate(
                        when (iconAligning) {
                            Aligning.START -> 0f
                            Aligning.CENTER -> (iconGroup.width - icon.width) / 2f
                            Aligning.END -> iconGroup.width - icon.width
                        },
                        if (direction == Direction.REVERS) -icon.height else 0f,
                    )
                    canvas.drawImage(
                        icon.width, icon.height,
                        icon.image, icon.imageAttributes
                    )
                }
                canvas.translate(
                    0f,
                    if (direction == Direction.REVERS) 0f else icon.height,
                )
            }
            canvas.restore(canvasSave)
            canvas.translate(iconGroup.width, 0f)
        }
    }
    private fun projectIcons(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ): List<IconItem> {
        val iconWidth = this.iconWidth.getFloat(dataItem, project)
        val iconHeight = this.iconHeight.getFloat(dataItem, project)

        return this.icons.get(project, dataItem).map {
            project.icons.getOrCreate(context, canvas.imageRepository, it).let { icon ->
                IconItem(
                    icon.image.get(project, dataItem),
                    if (iconWidth > 0f) iconWidth else icon.width.getFloat(dataItem, project),
                    if (iconHeight > 0f) iconHeight else icon.height.getFloat(dataItem, project),
                    ImageAttributes(
                        scaleMode = icon.scaleMode.get(project, dataItem),
                        aligningX = icon.aligningX.get(project, dataItem),
                        aligningY = icon.aligningY.get(project, dataItem)
                    )
                )
            }
        }
    }

    private fun List<IconItem>.getRowEnd(startIndex: Int, viewWidth: Float): Int {
        var totalSize = 0f
        var index = startIndex
        while (index < this.size) {
            if (viewWidth > 0 && totalSize + this[index].width > viewWidth) {
                return max(startIndex, index - 1)
            }
            totalSize += this[index].width
            index++
        }
        return max(startIndex, index - 1)
    }


    private fun List<IconItem>.getRowHeight(startIndex: Int, endIndex: Int): Float {
        var maxSize = 0f
        for (i in startIndex..endIndex) {
            maxSize = max(maxSize, this[i].height)
        }
        return maxSize
    }

    private fun List<IconItem>.getRowWidth(startIndex: Int, endIndex: Int): Float {
        var result = 0f
        for (i in startIndex..endIndex) {
            result += this[i].width
        }
        return result
    }

    private fun List<IconItem>.getColEnd(startIndex: Int, viewHeight: Float): Int {
        var totalSize = 0f
        var index = startIndex
        while (index < this.size) {
            if (viewHeight > 0 && totalSize + this[index].height > viewHeight) {
                return max(startIndex, index - 1)
            }
            totalSize += this[index].height
            index++
        }
        return max(startIndex, index - 1)
    }

    private fun List<IconItem>.getColWidth(startIndex: Int, endIndex: Int): Float {
        var maxSize = 0f
        for (i in startIndex..endIndex) {
            maxSize = max(maxSize, this[i].width)
        }
        return maxSize
    }
    private fun List<IconItem>.getColHeight(startIndex: Int, endIndex: Int): Float {
        var result = 0f
        for (i in startIndex..endIndex) {
            result += this[i].height
        }
        return result
    }

    private val Direction.multiplier: Float get() = when(this) {
        Direction.NORMAL -> 1f
        Direction.REVERS -> -1f
    }

}