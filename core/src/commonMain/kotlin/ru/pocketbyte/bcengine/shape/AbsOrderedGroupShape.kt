package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeRange
import ru.pocketbyte.bcengine.provider.SizeRangeProvider

abstract class AbsOrderedGroupShape: LayoutShape() {

    val space: SizeRangeProvider =
        SizeRangeProvider(SizeRange(Size.ZERO, Size.NEGATIVE), Orientation.VERTICAL)

    protected var minSpace: Float = 0f
        private set
    protected var maxSpace: Float = 0f
        private set

    protected val spaceIsRanged: Boolean
        get() = maxSpace < 0 || minSpace != maxSpace

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        val spaceRange = space.get(project, dataItem)

        minSpace = project.layout.projectFloat(spaceRange.min, parentWidth, parentHeight)
        maxSpace = project.layout.projectFloat(spaceRange.max, parentWidth, parentHeight)

        super.measure(context, project, canvas, dataItem, parentWidth, parentHeight)
    }
}