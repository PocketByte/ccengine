package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeRange
import ru.pocketbyte.bcengine.entity.percent
import ru.pocketbyte.bcengine.provider.SizeProvider
import ru.pocketbyte.bcengine.provider.SizeRangeProvider

class ColumnShape: AbsOrderedGroupShape() {

    override fun layoutChildren(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        if (shapes.isEmpty()) {
            return
        }

        val totalHeight = layoutChildren(context, project, canvas, dataItem, minSpace)

        if (totalHeight < measuredHeight && spaceIsRanged && shapes.size > 1) {
            val totalSpace = (measuredHeight - totalHeight + minSpace * (shapes.size - 1))
            val space = totalSpace / (shapes.size - 1)

            layoutChildren(context, project, canvas, dataItem, space)
        }
    }

    private fun layoutChildren(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        space: Float
    ): Float {
        var totalHeight = -space

        shapes.forEach {
            it.layout(context, project, canvas, dataItem,
                positionX = 0f,
                positionY = totalHeight + space
            )

            if (it.measuredHeight > 0) {
                totalHeight = it.positionY + it.measuredHeight
            }
        }

        return totalHeight
    }
}