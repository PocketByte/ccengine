package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.provider.BooleanProvider

class IfLayoutShape: LayoutShape() {

    val condition: BooleanProvider = BooleanProvider(false)

    override fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    ) {
        if (condition.get(project, dataItem)) {
            super.measure(context, project, canvas, dataItem, parentWidth, parentHeight)
        } else {
            onMeasure(0f, 0f)
        }
    }

    override fun measureAfterLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        if (condition.get(project, dataItem)) {
            super.measureAfterLayout(context, project, canvas, dataItem, positionX, positionY)
        } else {
            onMeasure(0f, 0f)
        }
    }

    override fun doLayout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    ) {
        if (condition.get(project, dataItem)) {
            super.doLayout(context, project, canvas, dataItem, positionX, positionY)
        } else {
            onLayout(positionX, positionY)
        }
    }

    override fun drawShape(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        if (condition.get(project, dataItem)) {
            super.drawShape(context, project, canvas, dataItem)
        }
    }
}