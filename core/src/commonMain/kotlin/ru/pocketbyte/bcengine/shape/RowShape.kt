package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.percent
import ru.pocketbyte.bcengine.provider.SizeProvider

class RowShape: AbsOrderedGroupShape() {

    override fun layoutChildren(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        if (shapes.isEmpty()) {
            return
        }

        val totalWidth = layoutChildren(context, project, canvas, dataItem, minSpace)

        if (totalWidth < measuredWidth && spaceIsRanged && shapes.size > 1) {
            val totalSpace = (measuredWidth - totalWidth + minSpace * (shapes.size - 1))
            val space = totalSpace / (shapes.size - 1)

            layoutChildren(context, project, canvas, dataItem, space)
        }
    }

    private fun layoutChildren(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        space: Float
    ): Float {
        var totalWidth = -space

        shapes.forEach {
            it.layout(context, project, canvas, dataItem,
                positionX = totalWidth + space,
                positionY = 0f
            )

            if (it.measuredWidth > 0) {
                totalWidth = it.positionX + it.measuredWidth
            }
        }

        return totalWidth
    }
}
