package ru.pocketbyte.bcengine.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Aligning
import ru.pocketbyte.bcengine.provider.AnchorProvider
import ru.pocketbyte.bcengine.provider.FloatProvider
import ru.pocketbyte.bcengine.provider.SizeRangeProvider

interface Shape {
    val x: AnchorProvider
    val y: AnchorProvider
    val width: SizeRangeProvider
    val height: SizeRangeProvider
    val angle: FloatProvider

    val measuredWidth: Float
    val measuredHeight: Float

    val positionX: Float
    val positionY: Float
    val anchorAligningX: Aligning
    val anchorAligningY: Aligning

    fun measure(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        parentWidth: Float,
        parentHeight: Float
    )

    fun layout(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem,
        positionX: Float,
        positionY: Float
    )

    fun draw(
        context: Context,
        project: Project,
        canvas: Canvas,
        dataItem: DataSetItem
    )
}