package ru.pocketbyte.bcengine.canvas

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project

interface CanvasFactory {
    fun createCanvas(
        context: Context,
        project: Project,
        widthMultiply: Int = 1,
        heightMultiply: Int = 1
    ): Canvas
}