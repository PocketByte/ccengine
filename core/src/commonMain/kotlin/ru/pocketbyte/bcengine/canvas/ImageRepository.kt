package ru.pocketbyte.bcengine.canvas

import ru.pocketbyte.bcengine.Context

interface ImageRepository {

    open class Item(
        val width: Int,
        val height: Int
    )

    fun get(context: Context, imagePath: String): Item
    fun clear()
}