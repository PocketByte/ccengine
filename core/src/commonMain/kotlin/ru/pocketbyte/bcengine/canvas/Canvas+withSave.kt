package ru.pocketbyte.bcengine.canvas

inline fun Canvas.withSave(action: (canvas: Canvas) -> Unit) {
    val save = save()
    action(this)
    restore(save)
}