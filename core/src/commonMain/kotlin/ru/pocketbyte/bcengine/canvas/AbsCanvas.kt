package ru.pocketbyte.bcengine.canvas

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.ProjectLayoutOld
import ru.pocketbyte.bcengine.entity.Color

abstract class AbsCanvas: Canvas {

    abstract val layout: ProjectLayoutOld
    abstract val context: Context

    protected open val widthMultiply: Int = 1
    protected open val heightMultiply: Int = 1

    override val width: Float
        get() = layout.widthPx * widthMultiply
    override val height: Float
        get() = layout.heightPx * heightMultiply

    override var background: Color = Color.WHITE
}