package ru.pocketbyte.bcengine.canvas

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.entity.*

class EmptyCanvas : Canvas {

    override val imageRepository: ImageRepository = object : ImageRepository {
        override fun get(context: Context, imagePath: String): ImageRepository.Item {
            return ImageRepository.Item(0, 0)
        }

        override fun clear() { }
    }

    override val width: Float = 1f
    override val height: Float = 1f

    override var background: Color = Color.WHITE

    override fun saveToFile(filePath: String, format: String) { }

    override fun recycle() { }

    override fun clipRect(left: Float, top: Float, right: Float, bottom: Float) { }

    override fun translate(x: Float, y: Float) { }

    override fun rotate(angle: Float) { }

    override fun save(): Int = 0

    override fun restore(save: Int) { }

    override fun clear() { }

    override fun clearRect(width: Float, height: Float) { }

    override fun drawLine(x: Float, y: Float, strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle) { }

    override fun drawRect(
        width: Float, height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle
    ) { }

    override fun drawRect(width: Float, height: Float, color: Color) { }

    override fun drawRoundRect(
        width: Float, height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle,
        cornerRadiusX: Float,
        cornerRadiusY: Float
    ) { }

    override fun drawRoundRect(
        width: Float,
        height: Float,
        color: Color,
        cornerRadiusX: Float,
        cornerRadiusY: Float
    ) { }

    override fun drawImage(
        width: Float,
        height: Float,
        imagePath: String,
        attributes: ImageAttributes
    ) { }

    override fun measureText(
        width: Float,
        height: Float,
        text: String,
        attributes: TextAttributes
    ): Pair<Float, Float> {
        return Pair(0f, 0f)
    }

    override fun drawText(width: Float, height: Float, text: String, attributes: TextAttributes) { }

    override fun drawDebugRect(x: Float, y: Float, width: Float, height: Float) { }
}