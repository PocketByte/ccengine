package ru.pocketbyte.bcengine.canvas

import ru.pocketbyte.bcengine.entity.*

interface Canvas {
    val imageRepository: ImageRepository

    val width: Float
    val height: Float

    fun saveToFile(filePath: String, format: String)
    fun recycle()

    var background: Color

    fun clipRect(left: Float, top: Float, right: Float, bottom: Float)
    fun translate(x: Float, y: Float)
    fun rotate(angle: Float)
    fun save(): Int
    fun restore(save: Int)

    /**
     * Clears whole canvas by filling it with the background color
     * of the current drawing surface.
     */
    fun clear()

    /**
     * Clears the specified rectangle by filling it with the background color
     * of the current drawing surface.
     */
    fun clearRect(width: Float, height: Float)

    fun drawLine(
        x: Float, y: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle
    )

    fun drawRect(
        width: Float, height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle
    )

    fun drawRoundRect(
        width: Float, height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle,
        cornerRadiusX: Float, cornerRadiusY: Float,
    )

    fun drawRect(
        width: Float, height: Float,
        color: Color
    )

    fun drawRoundRect(
        width: Float, height: Float,
        color: Color,
        cornerRadiusX: Float, cornerRadiusY: Float,
    )

    fun drawImage(
        width: Float, height: Float,
        imagePath: String,
        attributes: ImageAttributes
    )

    fun drawText(
        width: Float, height: Float,
        text: String, attributes: TextAttributes
    )

    fun measureText(
        width: Float, height: Float,
        text: String, attributes: TextAttributes
    ): Pair<Float, Float>

    fun drawDebugRect(
        x: Float, y: Float,
        width: Float, height: Float
    )
}
