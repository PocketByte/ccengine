package ru.pocketbyte.bcengine

import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.kydra.log.KydraLog
import ru.pocketbyte.kydra.log.Logger

class Context(
    val workDir: String,
    val tempDir: String
) {

    constructor(copy: Context) : this(copy.workDir, copy.tempDir)

    val logger: Logger = KydraLog
    val errors = ErrorsContainer()

    val flags = mutableSetOf<String>()
}