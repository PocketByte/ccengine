package ru.pocketbyte.bcengine

import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.dataset.DataSetFactory
import ru.pocketbyte.bcengine.dataset.DataSetFactoryStub
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.dataset.SystemDataCast
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.shape.Shape
import ru.pocketbyte.bcengine.shape.ShapeGroup
import ru.pocketbyte.bcengine.shape.withTransformOf

class Project: ShapeGroup, FormulaContext {
    var workDir: String? = null

    var dataSetMultiply = 1
    var dataSet: DataSetFactory = DataSetFactoryStub

    val output = ProjectOutput()

    val icons: ProjectIcons = ProjectIcons()
    val functions: MutableMap<String, Formula<*>> = mutableMapOf<String, Formula<*>>().apply {
        SystemDataCast.addTo(this)
    }
    val parameters: MutableMap<String, FormulaStaticValue<*>> = mutableMapOf()

    val layout: ProjectLayoutOld = ProjectLayoutOld()
    override val shapes: MutableList<Shape> = mutableListOf()

    override fun add(shape: Shape) {
        shapes.add(shape)
    }

    override fun findFormula(name: String): Formula<*>? {
        return parameters[name] ?: functions[name]
    }

    fun measureAndDraw(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        measure(context, canvas, dataItem)
        layout(context, canvas, dataItem)
        draw(context, canvas, dataItem)
    }

    private fun measure(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        val layoutWidth = layout.widthPx
        val layoutHeight = layout.heightPx
        shapes.forEach {
            it.measure(
                context, this, canvas, dataItem,
                layoutWidth, layoutHeight
            )
        }
    }

    private fun layout(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        shapes.forEach {
            it.layout(
                context, this, canvas, dataItem,
                0f, 0f
            )
        }
    }

    private fun draw(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        canvas.background = layout.background.get(this, dataItem)
        canvas.clear()
        shapes.forEach {
            it.draw(context, this, canvas, dataItem)
        }
    }

    fun drawDebugShapeBorders(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem
    ) {
        shapes.forEach {
            drawDebugShapeBorder(context, canvas, dataItem, it)
        }
    }

    private fun drawDebugShapeBorder(
        context: Context,
        canvas: Canvas,
        dataItem: DataSetItem,
        shape: Shape
    ) {
        shape.withTransformOf(this, canvas, dataItem) {
            canvas.drawDebugRect(
                0f, 0f,
                shape.measuredWidth,
                shape.measuredHeight
            )
        }
        if (shape is ShapeGroup) {
            shape.withTransformOf(this, canvas, dataItem) {
                shape.shapes.forEach {
                    drawDebugShapeBorder(context, canvas, dataItem, it)
                }
            }
        }
    }
}