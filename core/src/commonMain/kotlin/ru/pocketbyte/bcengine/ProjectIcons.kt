package ru.pocketbyte.bcengine

import ru.pocketbyte.bcengine.canvas.ImageRepository
import ru.pocketbyte.bcengine.entity.Icon
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.formula.formulaStaticValue

class ProjectIcons {

    private val icons: MutableMap<String, Icon> = mutableMapOf()

    fun addIcon(icon: Icon) {
        icons[icon.name] = icon
    }

    fun getIcon(name: String): Icon? {
        return icons[name]
    }

    fun getOrCreate(context: Context, imageRepository: ImageRepository, name: String): Icon {
        icons[name]?.let { return it }
        return Icon(name).apply {
            val imageItem = imageRepository.get(context, name)
            image.formula = formulaStaticValue(name)
            width.formula = formulaStaticValue(
                "${imageItem.width}${Size.Dimension.SCALED_PIXEL.sign}"
            )
            height.formula = formulaStaticValue(
                "${imageItem.height}${Size.Dimension.SCALED_PIXEL.sign}"
            )
            icons[name] = this
        }
    }
}
