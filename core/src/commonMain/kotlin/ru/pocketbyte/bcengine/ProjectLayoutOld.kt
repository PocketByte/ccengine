package ru.pocketbyte.bcengine

import ru.pocketbyte.bcengine.entity.*
import ru.pocketbyte.bcengine.provider.ColorProvider
import kotlin.math.max
import kotlin.math.min

class ProjectLayoutOld {

    var dpi: Float = DEFAULT_DPI
        set(value) {
            field = value
            calculateSizes()
        }
    var width: SizeSimple = SizeSimple(0f, Size.Dimension.INCH)
        set(value) {
            value.assertPhysical()
            field = value
            calculateSizes()
        }
    var height: SizeSimple = SizeSimple(0f, Size.Dimension.INCH)
        set(value) {
            value.assertPhysical()
            field = value
            calculateSizes()
        }

    var background: ColorProvider = ColorProvider(Color.WHITE)

    var widthPx: Float = projectFloat(width, dpi)
        private set
    var heightPx: Float = projectFloat(height, dpi)
        private set

    fun projectFloat(
        size: Size,
        frameWidth: Float,
        frameHeight: Float
    ): Float {
        return when (size) {
            is SizeSimple -> projectFloat(
                size.value, size.dimension, frameWidth,
                frameHeight, size.orientation
            )
            is SizeComplex -> {
                size.operation(
                    projectFloat(size.size1, frameWidth, frameHeight),
                    projectFloat(size.size2, frameWidth, frameHeight)
                )
            }
        }
    }

    fun projectFloat(size: Size): Float {
        return projectFloat(size, widthPx, heightPx)
    }

    fun projectFloat(
        value: Float, dimension: Size.Dimension,
        frameWidth: Float,
        frameHeight: Float,
        orientation: Orientation = Orientation.NONE
    ): Float {
        return when (dimension) {
            Size.Dimension.PERCENT -> {
                getSizeFor(orientation, widthPx, heightPx) * value / 100f
            }
            Size.Dimension.PERCENT_HEIGHT -> heightPx * value / 100f
            Size.Dimension.PERCENT_WIDTH -> widthPx * value / 100f

            Size.Dimension.FRAME_PERCENT -> {
                getSizeFor(orientation, frameWidth, frameHeight) * value / 100f
            }
            Size.Dimension.FRAME_PERCENT_HEIGHT -> frameHeight * value / 100f
            Size.Dimension.FRAME_PERCENT_WIDTH -> frameWidth * value / 100f
            else -> projectFloat(value, dimension, dpi)
        }
    }

    fun projectSp(
        size: Size,
        frameWidth: Float,
        frameHeight: Float
    ): Float {
        return when (size) {
            is SizeSimple -> projectSp(
                size.value, size.dimension, frameWidth,
                frameHeight, size.orientation
            )
            is SizeComplex -> {
                size.operation(
                    projectSp(size.size1, frameWidth, frameHeight),
                    projectSp(size.size2, frameWidth, frameHeight)
                )
            }
        }
    }

    fun projectSp(
        value: Float, dimension: Size.Dimension,
        frameWidth: Float,
        frameHeight: Float,
        orientation: Orientation = Orientation.NONE
    ): Float {
        return if (dimension == Size.Dimension.SCALED_PIXEL) {
            value
        } else {
            pixelToSp(projectFloat(value, dimension, frameWidth, frameHeight, orientation))
        }
    }

    fun pixelToSp(value: Float): Float {
        return value / dpi * DEFAULT_DPI
    }

    private fun getSizeFor(
        orientation: Orientation,
        frameWidth: Float,
        frameHeight: Float
    ): Float {
        return when (orientation) {
            Orientation.NONE -> min(frameWidth, frameHeight)
            Orientation.HORIZONTAL -> frameWidth
            Orientation.VERTICAL -> frameHeight
        }
    }

    private fun calculateSizes() {
        widthPx = max(projectFloat(width, dpi), 1f)
        heightPx = max(projectFloat(height, dpi), 1f)
    }

    private fun Size.assertPhysical() {
        if (dimension.isPhysical.not()) {
            throw IllegalArgumentException("Size should represent physical metrics.")
        }
    }

    companion object {
        const val DEFAULT_DPI = 96f
        const val INC_TO_MM = 1 / (10 * 2.54f)

        fun projectFloat(size: SizeSimple, dpi: Float): Float {
            return projectFloat(size.value, size.dimension, dpi)
        }

        fun projectFloat(value: Float, dimension: Size.Dimension, dpi: Float): Float {
            return when (dimension) {
                Size.Dimension.PIXEL -> value
                Size.Dimension.SCALED_PIXEL -> value * dpi / DEFAULT_DPI
                Size.Dimension.FRAME_PERCENT,
                Size.Dimension.FRAME_PERCENT_HEIGHT,
                Size.Dimension.FRAME_PERCENT_WIDTH,
                Size.Dimension.PERCENT,
                Size.Dimension.PERCENT_HEIGHT,
                Size.Dimension.PERCENT_WIDTH -> throw IllegalArgumentException(
                    "Can't be converted to pixel without canvas size."
                )
                Size.Dimension.INCH -> value * dpi
                Size.Dimension.MILLIMETER -> value * dpi * INC_TO_MM
                Size.Dimension.CENTIMETER -> value * dpi * INC_TO_MM * 10f
                Size.Dimension.COMPLEX -> throw IllegalArgumentException(
                    "Complex size can't be converted to pixel."
                )
            }
        }
    }
}