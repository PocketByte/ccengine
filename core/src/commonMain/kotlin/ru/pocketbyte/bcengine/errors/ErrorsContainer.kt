package ru.pocketbyte.bcengine.errors

class ErrorsContainer {

    private val mutableList = mutableListOf<ProjectError>()

    val list: List<ProjectError>
        get() = mutableList

    fun addError(error: ProjectError) {
        mutableList.add(error)
    }

    fun hasErrors(): Boolean {
        return list.isNotEmpty()
    }

    fun clear() {
        mutableList.clear()
    }

    fun buildErrorString(): String? {
        return buildErrorString(list)
    }

    companion object {
        fun buildErrorString(list: List<ProjectError>): String? {
            if (list.isEmpty()) {
                return null
            }
            val builder = StringBuilder()
            list.forEach {
                if (builder.isNotEmpty()) {
                    builder.append("\n\n")
                }
                when (it) {
                    is ProjectError.ParseError -> {
                        builder.append(it.message)
                        builder.append("\n")
                        builder.append(pointStringLocation(it.string, it.index))
                    }
                    is ProjectError.ParseTokenError -> {
                        builder.append(it.message)
                        builder.append("\n")
                        builder.append(pointStringLocation(it.token.parentString, it.token.startIndex))
                    }
                    is ProjectError.ExceptionError -> {
                        builder.append(it.exception.message)
                        it.exception.printStackTrace()
                    }
                }
            }
            return builder.toString()
        }

        fun buildErrorString(it: ProjectError): String {
            val builder = StringBuilder()

            when (it) {
                is ProjectError.ParseError -> {
                    builder.append(it.message)
                    builder.append("\n")
                    builder.append(pointStringLocation(it.string, it.index))
                }
                is ProjectError.ParseTokenError -> {
                    builder.append(it.message)
                    builder.append("\n")
                    builder.append(pointStringLocation(it.token.parentString, it.token.startIndex))
                }
                is ProjectError.ExceptionError -> {
                    builder.append(it.exception.message)
                    it.exception.printStackTrace()
                }
            }
            return builder.toString()
        }

        private fun pointStringLocation(string: String, index: Int): String {
            var currentIndex = index
            while (currentIndex >= 0 && string[currentIndex] != '\n') {
                currentIndex--
            }
            val rowStart = currentIndex + 1

            var rowNumber = 1
            for (i in currentIndex downTo 0) {
                if (string[i] == '\n') {
                    rowNumber++
                }
            }

            currentIndex = index
            while (currentIndex < string.length && string[currentIndex] != '\n') { currentIndex++ }
            val rowEnd = currentIndex

            val rowNumberString = "[$rowNumber,${currentIndex - rowStart}]: "

            return rowNumberString +
                    string.substring(rowStart, maxOf(rowEnd, rowStart)) + "\n" +
                    space(index - rowStart + rowNumberString.length) + "^"
        }

        private fun space(size: Int): String {
            return String(ByteArray(maxOf(size, 0)) { ' '.code.toByte() })
        }
    }

}