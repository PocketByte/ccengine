package ru.pocketbyte.bcengine.errors

import ru.pocketbyte.bcengine.token.Token

sealed class ProjectError {

    class ParseError(
        val index: Int,
        val string: String,
        val message: String
    ): ProjectError()

    class ParseTokenError(
        val token: Token,
        val message: String
    ): ProjectError()

    class ExceptionError(
        val exception: Throwable
    ): ProjectError()

}