package ru.pocketbyte.bcengine.provider

class BooleanProvider(
    defaultValue: Boolean
): FormulaProvider<Boolean, Boolean>(defaultValue) {

    override val fallbackValue: Boolean = false

    override fun decodeValue(value: Boolean): Boolean {
        return value
    }
}
