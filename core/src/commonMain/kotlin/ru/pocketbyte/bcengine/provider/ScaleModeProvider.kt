package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.ScaleModeCoder
import ru.pocketbyte.bcengine.entity.ScaleMode

class ScaleModeProvider(
    defaultValue: ScaleMode
): FormulaProvider<ScaleMode, String>(defaultValue) {

    override fun decodeValue(value: String): ScaleMode {
        return ScaleModeCoder.decode(value)
    }

    override val fallbackValue: ScaleMode = ScaleMode.STRETCH
}
