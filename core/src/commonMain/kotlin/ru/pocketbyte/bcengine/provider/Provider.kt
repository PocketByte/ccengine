package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.FormulaContext

interface Provider<Type> {
    val isDefault: Boolean
    fun resetToDefault()

    fun get(context: FormulaContext, dataLayer: DataSetItem?): Type
}