package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.Anchor
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.coder.AnchorCoder
import ru.pocketbyte.bcengine.entity.coder.SizeCoder

class AnchorProvider(
    defaultValue: Anchor,
    val orientation: Orientation
): FormulaProvider<Anchor, String>(defaultValue) {

    constructor(orientation: Orientation): this(
        AnchorCoder.decode("", orientation),
        orientation
    )

    override fun decodeValue(value: String): Anchor? {
        return try {
            AnchorCoder.decode(value, orientation)
        } catch (e: IllegalArgumentException) {
            null
        }
    }

}