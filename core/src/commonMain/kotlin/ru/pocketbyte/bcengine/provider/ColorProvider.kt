package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.ColorCoder
import ru.pocketbyte.bcengine.entity.Color

class ColorProvider(
    defaultValue: Color
): FormulaProvider<Color, String>(defaultValue) {

    override fun decodeValue(value: String): Color? {
        return ColorCoder.decode(value.trim())
    }

    override val fallbackValue: Color = Color.INVALID_COLOR
}