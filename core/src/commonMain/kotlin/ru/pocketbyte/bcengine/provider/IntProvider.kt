package ru.pocketbyte.bcengine.provider

class IntProvider(
    defaultValue: Int
): FormulaProvider<Int, Float>(defaultValue) {

    override fun decodeValue(value: Float): Int {
        return value.toInt()
    }

    override val fallbackValue: Int = 0
}
