package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.AligningCoder
import ru.pocketbyte.bcengine.entity.Aligning

class AligningProvider(
    defaultValue: Aligning
): FormulaProvider<Aligning, String>(defaultValue) {

    override fun decodeValue(value: String): Aligning {
        return AligningCoder.decode(value)
    }

    override val fallbackValue: Aligning = Aligning.START
}
