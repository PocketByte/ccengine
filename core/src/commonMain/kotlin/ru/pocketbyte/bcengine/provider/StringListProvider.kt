package ru.pocketbyte.bcengine.provider

class StringListProvider(
    defaultValue: List<String>
): FormulaProvider<List<String>, List<String>>(defaultValue) {

    override fun decodeValue(value: List<String>): List<String>? {
        return value
    }
}
