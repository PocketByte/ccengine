package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.SizeCoder
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeSimple

class SizeProvider(
    defaultValue: Size,
    val orientation: Orientation
): FormulaProvider<Size, String>(defaultValue) {

    override fun decodeValue(value: String): Size? {
        return try {
            SizeCoder.decode(value.trim(), orientation)
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    override val fallbackValue: Size = SizeSimple(0f, Size.Dimension.FRAME_PERCENT)
}
