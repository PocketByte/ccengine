package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.Direction
import ru.pocketbyte.bcengine.entity.coder.DirectionCoder

class DirectionProvider(
    defaultValue: Direction
): FormulaProvider<Direction, String>(defaultValue) {

    override fun decodeValue(value: String): Direction {
        return DirectionCoder.decode(value)
    }
}
