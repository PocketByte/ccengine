package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.TextAligningCoder
import ru.pocketbyte.bcengine.entity.TextAligning

class TextAligningProvider(
    defaultValue: TextAligning
): FormulaProvider<TextAligning, String>(defaultValue) {

    override fun decodeValue(value: String): TextAligning {
        return TextAligningCoder.decode(value)
    }

    override val fallbackValue: TextAligning = TextAligning.START
}
