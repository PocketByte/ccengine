package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.asFormulaRoot

abstract class FormulaProvider<Type, FormulaType : Any>(
    val defaultValue: Type
) : Provider<Type> {

    abstract fun decodeValue(value: FormulaType): Type?

    open val fallbackValue: Type = defaultValue

    var formula: Formula<FormulaType>? = null

    override val isDefault: Boolean = formula == null

    override fun get(context: FormulaContext, dataLayer: DataSetItem?): Type {
        return formula?.let {
            decodeValue(it.compute(context, dataLayer)) ?: fallbackValue
        } ?: defaultValue
    }

    override fun resetToDefault() {
        formula = null
    }
}