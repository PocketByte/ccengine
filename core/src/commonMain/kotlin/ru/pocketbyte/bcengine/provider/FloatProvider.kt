package ru.pocketbyte.bcengine.provider

class FloatProvider(
    defaultValue: Float
): FormulaProvider<Float, Float>(defaultValue) {

    override fun decodeValue(value: Float): Float {
        return value
    }

    override val fallbackValue: Float = 0f
}
