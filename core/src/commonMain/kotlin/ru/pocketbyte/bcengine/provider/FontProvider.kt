package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.coder.FontCoder
import ru.pocketbyte.bcengine.entity.Font

class FontProvider(
    defaultValue: Font
): FormulaProvider<Font, String>(defaultValue) {

    constructor(): this(FontCoder.decode(""))

    override fun decodeValue(value: String): Font {
        return FontCoder.decode(value)
    }

    override val fallbackValue: Font = FontCoder.decode("")
}