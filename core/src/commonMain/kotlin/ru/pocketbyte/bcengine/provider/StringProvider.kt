package ru.pocketbyte.bcengine.provider

class StringProvider(
    defaultValue: String
): FormulaProvider<String, Any>(defaultValue) {

    override fun decodeValue(value: Any): String? {
        return value.toString()
    }
}
