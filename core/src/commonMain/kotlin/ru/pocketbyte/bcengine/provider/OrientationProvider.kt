package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.coder.OrientationCoder

class OrientationProvider(
    defaultValue: Orientation
): FormulaProvider<Orientation, String>(defaultValue) {

    override fun decodeValue(value: String): Orientation {
        return OrientationCoder.decode(value)
    }
}
