package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.entity.Color
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Stroke
import ru.pocketbyte.bcengine.entity.px
import ru.pocketbyte.bcengine.formula.FormulaContext

class StrokeProvider: Provider<Stroke> {

    override val isDefault: Boolean
        get() = size.isDefault && color.isDefault

    val size: SizeProvider = SizeProvider(0.px(), Orientation.NONE)
    val color: ColorProvider = ColorProvider(Color.BLACK)

    override fun get(context: FormulaContext, dataLayer: DataSetItem?) : Stroke {
        return Stroke(
            size.get(context, dataLayer),
            color.get(context, dataLayer)
        )
    }

    override fun resetToDefault() {
        size.resetToDefault()
        color.resetToDefault()
    }
}
