package ru.pocketbyte.bcengine.provider

import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.SizeRange
import ru.pocketbyte.bcengine.entity.coder.SizeRangeCoder

class SizeRangeProvider(
    defaultValue: SizeRange,
    val orientation: Orientation
): FormulaProvider<SizeRange, String>(defaultValue) {

    override fun decodeValue(value: String): SizeRange? {
        return try {
            SizeRangeCoder.decode(value.trim(), orientation)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}