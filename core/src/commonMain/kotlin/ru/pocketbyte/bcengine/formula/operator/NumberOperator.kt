package ru.pocketbyte.bcengine.formula.operator

enum class NumberOperator(
    val char: Char
) {
    PLUS('+'),
    MINUS('-'),
    MULTIPLY('*'),
    DIVIDE('/')
}