package ru.pocketbyte.bcengine.formula.valuetype

object BooleanValueType: ValueType<Boolean> {
    override val name: String = "Boolean"
    override val shortName: String = "bool"

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): Boolean {
        return when (valueType) {
            is StringValueType -> {
                (value as String).toBoolean()
            }
            is NumberValueType -> {
                (value as Float) > 0
            }
            is BooleanValueType -> {
                value as Boolean
            }
            is ListValueType<*> -> {
                (value as List<Any>).isNotEmpty()
            }
            is DictValueType<*> -> {
                (value as Map<Any, Any>).isNotEmpty()
            }
            is UndefinedValueType -> throw RuntimeException("Unknown type")
        }
    }
}