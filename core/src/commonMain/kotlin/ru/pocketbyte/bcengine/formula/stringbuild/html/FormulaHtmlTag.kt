package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

open class FormulaHtmlTag(
    override val token: Token?,
    private val tag: Formula<String>,
    private val content: Formula<String>
): Formula<String> {
    companion object {
        const val NAME = "htmlTag"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(tag, content)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        val contentString = content.compute(context, dataLayer)
        if (contentString.isEmpty()) {
            return ""
        }

        val tagString = tag.compute(context, dataLayer)
        if (tagString.isBlank()) {
            return contentString
        }

        return "<$tagString>$contentString</$tagString>"

    }
}