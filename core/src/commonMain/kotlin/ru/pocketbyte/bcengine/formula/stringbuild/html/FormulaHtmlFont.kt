package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlFont(
    override val token: Token?,
    private val content: Formula<String>,
    private val face: Formula<String>?,
    private val size: Formula<String>?,
    private val color: Formula<String>?
): Formula<String> {
    companion object {
        const val NAME = "htmlFont"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(content, face, size, color)
        .filterNotNull().toTypedArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        val contentString = content.compute(context, dataLayer)
        if (contentString.isEmpty()) {
            return ""
        }

        val builder = StringBuilder("<font")

        val faceString = face?.compute(context, dataLayer)
        if (faceString?.isNotBlank() == true) {
            builder.append(" face=\"")
                .append(faceString)
                .append("\"")
        }
        val sizeString = size?.compute(context, dataLayer)
        if (sizeString?.isNotBlank() == true) {
            builder.append(" size=\"")
                .append(sizeString)
                .append("\"")
        }
        val colorString = color?.compute(context, dataLayer)
        if (colorString?.isNotBlank() == true) {
            builder.append(" color=\"")
                .append(colorString)
                .append("\"")
        }

        builder.append(">")
            .append(contentString)
            .append("</font>")

        return builder.toString()
    }
}