package ru.pocketbyte.bcengine.formula.compare

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.extension.computeToBoolean
import ru.pocketbyte.bcengine.token.Token

class FormulaBooleanCompare(
    override val token: Token?,
    private val value1: Formula<*>,
    private val comparison: ComparisonOperator,
    private val value2: Formula<*>
): Formula<Boolean> {

    override val type = BooleanValueType
    override val parameters = arrayOf(value1, value2)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Boolean {
        return compare(
            value1.computeToBoolean(context, dataLayer),
            value2.computeToBoolean(context, dataLayer)
        )
    }

    private fun compare(v1: Boolean, v2: Boolean): Boolean {
        return when (comparison) {
            ComparisonOperator.EQUAL -> v1 == v2
            ComparisonOperator.GREATER_OR_EQUAL -> v1 >= v2
            ComparisonOperator.GREATER -> v1 > v2
            ComparisonOperator.LESS_OR_EQUAL -> v1 <= v2
            ComparisonOperator.LESS -> v1 < v2
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaBooleanCompare)
                && parameters.contentEquals(other.parameters)
                && comparison == other.comparison
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + comparison.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}