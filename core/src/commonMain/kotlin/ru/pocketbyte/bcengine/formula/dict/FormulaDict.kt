package ru.pocketbyte.bcengine.formula.dict

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaDict<out Type : Any>(
    override val token: Token?,
    override val type: DictValueType<Type>,
    val map: Map<String, Formula<Type>>,
): Formula<Map<String, Type>> {

    companion object {
        const val NAME = "dict"
    }

    override val parameters: Array<Formula<*>> = map.values.toTypedArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Map<String, Type> {
        return map.mapValues {
            it.value.compute(context, dataLayer)
        }
    }
}