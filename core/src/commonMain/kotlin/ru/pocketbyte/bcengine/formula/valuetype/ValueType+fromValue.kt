package ru.pocketbyte.bcengine.formula.valuetype

fun <Type : Any> ValueType<Type>.fromString(value: String): Type {
    return from(StringValueType, value)
}

fun <Type : Any> ValueType<Type>.fromNumber(value: Float): Type {
    return from(NumberValueType, value)
}

fun <Type : Any> ValueType<Type>.fromBoolean(value: Boolean): Type {
    return from(BooleanValueType, value)
}