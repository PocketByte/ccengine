package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaNamedFunctionCall<Type : Any>(
    override val token: Token?,
    val name: String,
    private val arguments: List<Formula<*>>,
    override val type: ValueType<Type>
): Formula<Type> {

    override val parameters: Array<Formula<*>> = arguments.toTypedArray()

    private val overrideMap = mutableMapOf<String, Formula<*>>().apply {
        arguments.forEachIndexed { index, formula ->
            set("$index", formula)
            set("${index}_$name", formula)
        }
    } as Map<String, Formula<*>>

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        return context.findFormula(name)
            ?.let { it as? Formula<Type> }
            ?.compute(context.contextOverride(overrideMap), dataLayer)
            ?: type.from(StringValueType, "")
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaNamedFunctionCall<*>)
                && name == other.name
                && arguments == other.arguments
                && type == other.type
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + arguments.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}