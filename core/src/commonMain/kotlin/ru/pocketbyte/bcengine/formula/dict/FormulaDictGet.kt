package ru.pocketbyte.bcengine.formula.dict

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaDictGet<out Type : Any>(
    override val token: Token?,
    override val type: ValueType<Type>,
    val dict: Formula<Map<String, Type>>,
    val key: Formula<String>,
    val fallbackValue: Formula<Type>
): Formula<Type> {

    companion object {
        const val NAME = "dictGet"
    }

    override val parameters: Array<Formula<*>> = arrayOf(dict, key)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        val keyValue = key.compute(context, dataLayer)
        return dict.compute(context, dataLayer)[keyValue]
            ?: fallbackValue.compute(context, dataLayer)
    }

}