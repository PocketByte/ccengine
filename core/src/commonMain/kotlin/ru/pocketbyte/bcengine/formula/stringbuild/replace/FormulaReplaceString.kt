package ru.pocketbyte.bcengine.formula.stringbuild.replace

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaReplaceString(
    override val token: Token?,
    val string: Formula<String>,
    val oldString: Formula<String>,
    val newString: Formula<String>
): Formula<String> {

    companion object {
        const val NAME = "replace"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(string, oldString, newString)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        return string.compute(context, dataLayer).replace(
            oldString.compute(context, dataLayer),
            newString.compute(context, dataLayer)
        )
    }
}
