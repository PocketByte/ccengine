package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class LazyFormula<Type : Any>(
    override val type: ValueType<Type>,
    val arguments: List<ValueType<*>>,
    private val formulaProvider: () -> Formula<Type>
): Formula<Type> {

    override val token: Token?
        get() = innerFormula.token

    override val parameters: Array<Formula<*>> by lazy {
        innerFormula.parameters
    }

    private val innerFormula: Formula<Type> by lazy {
        formulaProvider()
    }

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        return innerFormula.compute(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return innerFormula == other
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + innerFormula.hashCode()
        return result
    }
}