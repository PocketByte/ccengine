package ru.pocketbyte.bcengine.formula.cast

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.extension.computeToString
import ru.pocketbyte.bcengine.token.Token

class FormulaToString(
    override val token: Token?,
    private val value: Formula<*>,
): Formula<String> {

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : String {
        return value.computeToString(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaToString)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}