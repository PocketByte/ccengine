package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaGroup<Type : Any>(
    override val token: Token?,
    private val value: Formula<Type>
): Formula<Type> {

    override val type: ValueType<Type>
        get() = value.type

    override val parameters: Array<Formula<*>> = arrayOf(value)

    companion object {
        const val NAME = ""
    }

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        return value.compute(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaGroup<*>)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}