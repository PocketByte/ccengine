package ru.pocketbyte.bcengine.formula.extension

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.toBoolean
import ru.pocketbyte.bcengine.formula.valuetype.toNumber
import ru.pocketbyte.bcengine.formula.valuetype.toString

fun <Type : Any> Formula<Type>.computeToString(
    context: FormulaContext,
    dataLayer: DataSetItem?
): String {
    return type.toString(compute(context, dataLayer))
}

fun <Type : Any> Formula<Type>.computeToNumber(
    context: FormulaContext,
    dataLayer: DataSetItem?
): Float {
    return type.toNumber(compute(context, dataLayer))
}

fun <Type : Any> Formula<Type>.computeToBoolean(
    context: FormulaContext,
    dataLayer: DataSetItem?
): Boolean {
    return type.toBoolean(compute(context, dataLayer))
}