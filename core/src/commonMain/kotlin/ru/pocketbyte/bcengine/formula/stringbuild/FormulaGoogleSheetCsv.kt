package ru.pocketbyte.bcengine.formula.stringbuild

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaGoogleSheetCsv(
    override val token: Token?,
    private val scheetId: Formula<String>,
    private val workscheetId: Formula<String>
): Formula<String> {

    companion object {
        const val NAME = "googleSheetCsv"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(scheetId, workscheetId)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        return "https://docs.google.com/spreadsheets/d/${
            scheetId.compute(context, dataLayer)
        }/export?format=csv&gid=${
            workscheetId.compute(context, dataLayer)
        }"
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaGoogleSheetCsv)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}