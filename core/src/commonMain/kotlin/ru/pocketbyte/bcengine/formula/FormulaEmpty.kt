package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.token.Token

class FormulaEmpty<Type : Any>(
    override val type: ValueType<Type>
): Formula<Type> {
    override val token: Token? = null
    override val parameters: Array<Formula<*>> = emptyArray()

    private val value = type.fromString("")

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        return value
    }
}