package ru.pocketbyte.bcengine.formula.stringbuild.character

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.extension.computeToNumber
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token
import kotlin.math.roundToInt

open class FormulaChar(
    override val token: Token?,
    private val charCode: Formula<Float>
): Formula<String> {

    companion object {
        const val NAME = "char"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(charCode)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        return Char(
            charCode.computeToNumber(context, dataLayer).roundToInt()
        ).toString()
    }
}