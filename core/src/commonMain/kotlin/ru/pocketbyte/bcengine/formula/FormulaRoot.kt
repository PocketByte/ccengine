package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaRoot<Type : Any>(
    private val value: Formula<Type>
): Formula<Type> {
    override val token: Token? = null

    override val type: ValueType<Type>
        get() = value.type

    override val parameters: Array<Formula<*>> = arrayOf(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        return value.compute(context.rootContext(), dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaGroup<*>)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}

fun <Type : Any> Formula<Type>.asFormulaRoot(): FormulaRoot<Type> {
    return FormulaRoot(this)
}