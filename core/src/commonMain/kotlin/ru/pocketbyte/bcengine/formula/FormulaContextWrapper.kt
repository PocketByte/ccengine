package ru.pocketbyte.bcengine.formula

open class FormulaContextWrapper(
    private val wrappedContext: FormulaContext
) : FormulaContext {
    override fun findFormula(name: String): Formula<*>? {
        return wrappedContext.findFormula(name)
    }

    fun rootContext(): FormulaContext {
        return if (wrappedContext is FormulaContextWrapper) {
            wrappedContext.rootContext()
        } else {
            wrappedContext
        }
    }
}

fun FormulaContext.rootContext(): FormulaContext {
    return if (this is FormulaContextWrapper) {
        rootContext()
    } else {
        this
    }
}