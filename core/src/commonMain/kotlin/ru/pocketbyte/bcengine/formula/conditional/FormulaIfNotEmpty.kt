package ru.pocketbyte.bcengine.formula.conditional

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.contextOverride
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaIfNotEmpty<Type : Any>(
    override val token: Token?,
    private val value: Formula<Type>,
    private val valueTransform: Formula<Type>
): Formula<Type> {

    companion object {
        const val NAME = "ifNotEmpty"
    }

    override val type: ValueType<Type>
        get() = value.type

    override val parameters: Array<Formula<*>> = arrayOf(value, valueTransform)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        val computedValue = value.compute(context, dataLayer)
        return if (computedValue is String && computedValue.isEmpty()) {
            computedValue
        } else {
            valueTransform.compute(
                context.contextOverride(
                    mapOf(
                        Pair("0", value),
                        Pair("0_$NAME", value)
                    )
                ),
                dataLayer
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaIfNotEmpty<*>)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}