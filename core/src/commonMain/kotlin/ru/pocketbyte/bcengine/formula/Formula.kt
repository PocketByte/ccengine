package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.TokenRepresentation

interface Formula<out Type : Any>: TokenRepresentation {
    val type: ValueType<Type>
    val parameters: Array<Formula<*>>
    fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type
}