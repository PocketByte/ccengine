package ru.pocketbyte.bcengine.formula.extension

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.*
import ru.pocketbyte.bcengine.formula.valuetype.*

fun <Type : Any> ValueType<Type>.compute(
    formula: Formula<*>,
    context: FormulaContext,
    dataLayer: DataSetItem?
): Type {
    return when(formula.type) {
        StringValueType -> {
            fromString(formula.computeToString(context, dataLayer))
        }
        NumberValueType -> {
            fromNumber(formula.computeToNumber(context, dataLayer))
        }
        BooleanValueType -> {
            fromBoolean(formula.computeToBoolean(context, dataLayer))
        }
        UndefinedValueType -> {
            fromString(formula.computeToString(context, dataLayer))
        }
        is ListValueType<*> -> {
            return from(formula.type, formula.compute(context, dataLayer))
        }
        is DictValueType<*> -> {
            return from(formula.type, formula.compute(context, dataLayer))
        }
    }
}