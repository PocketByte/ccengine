package ru.pocketbyte.bcengine.formula.valuetype

sealed interface ValueType<out Type : Any> {
    val name: String
    val shortName: String
    fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): Type
}