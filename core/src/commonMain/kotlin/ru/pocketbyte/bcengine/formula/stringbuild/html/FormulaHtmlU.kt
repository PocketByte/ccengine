package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlU(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("u"), content) {
    companion object {
        const val NAME = "htmlU"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}