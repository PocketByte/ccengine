package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlS(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("s"), content) {
    companion object {
        const val NAME = "htmlS"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}