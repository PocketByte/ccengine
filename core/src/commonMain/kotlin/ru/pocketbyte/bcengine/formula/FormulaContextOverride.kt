package ru.pocketbyte.bcengine.formula

class FormulaContextOverride(
    wrappedContext: FormulaContext,
    private val overrideMap: Map<String, Formula<*>>
) : FormulaContextWrapper(wrappedContext) {

    override fun findFormula(name: String): Formula<*>? {
        return overrideMap[name] ?: super.findFormula(name)
    }
}

internal fun FormulaContext.contextOverride(
    overrideMap: Map<String, Formula<*>>
): FormulaContext {
    return FormulaContextOverride(this,overrideMap)
}