package ru.pocketbyte.bcengine.formula.stringbuild.join

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.extension.computeToString
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaJoinNotEmptyString(
    override val token: Token?,
    val separator: Formula<String>,
    val values: List<Formula<*>>
): Formula<String> {

    companion object {
        const val NAME = "joinNotEmpty"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(separator, *values.toTypedArray())

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        val builder = StringBuilder()
        val separatorString = separator.compute(context, dataLayer)
        values.forEach {
            val valueString = it.computeToString(context, dataLayer)

            if (valueString.isNotEmpty()) {
                if (builder.isNotEmpty()) {
                    builder.append(separatorString)
                }
                builder.append(valueString)
            }
        }
        return builder.toString()
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaJoinString)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}