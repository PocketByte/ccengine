package ru.pocketbyte.bcengine.formula.stringbuild

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.extension.computeToString
import ru.pocketbyte.bcengine.token.Token

class FormulaStringConcat(
    override val token: Token?,
    private val value1: Formula<*>,
    private val value2: Formula<*>
): Formula<String> {

    override val type = StringValueType
    override val parameters = arrayOf(value1, value2)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : String {
        return value1.computeToString(context, dataLayer) +
                value2.computeToString(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaStringConcat)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}