package ru.pocketbyte.bcengine.formula.stringbuild.character

import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaCharNewLine(
    token: Token?,
) : FormulaChar(token, NEW_LINE_CODE_FORMULA) {

    companion object {
        const val NAME = "charNl"
        private const val NEW_LINE_CODE = '\n'.code.toFloat()
        private val NEW_LINE_CODE_FORMULA = formulaStaticValue(NEW_LINE_CODE)
    }
}
