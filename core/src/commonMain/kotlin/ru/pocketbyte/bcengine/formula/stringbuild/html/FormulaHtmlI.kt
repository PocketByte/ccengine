package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlI(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("i"), content) {
    companion object {
        const val NAME = "htmlI"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}
