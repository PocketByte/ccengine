package ru.pocketbyte.bcengine.formula.operator

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.extension.computeToBoolean
import ru.pocketbyte.bcengine.token.Token

class FormulaBooleanOperator(
    override val token: Token?,
    private val value1: Formula<*>,
    private val operator: Type,
    private val value2: Formula<*>
): Formula<Boolean> {

    enum class Type(
        val char: Char,
        val operation: (v1: Boolean, v2: Boolean) -> Boolean
    ) {
        AND('&', { v1, v2 -> v1 && v2 }) ,
        OR('|', { v1, v2 -> v1 || v2 })
    }

    override val type = BooleanValueType
    override val parameters = arrayOf(value1, value2)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Boolean {
        return operator.operation(
            value1.computeToBoolean(context, dataLayer),
            value2.computeToBoolean(context, dataLayer)
        )
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaBooleanOperator)
                && parameters.contentEquals(other.parameters)
                && operator == other.operator
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + operator.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}