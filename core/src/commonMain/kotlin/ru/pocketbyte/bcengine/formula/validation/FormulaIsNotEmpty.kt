package ru.pocketbyte.bcengine.formula.validation

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.isEmpty
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaIsNotEmpty(
    override val token: Token?,
    private val value: Formula<*>,
): Formula<Boolean> {

    companion object {
        const val NAME = "isNotEmpty"
    }

    override val type = BooleanValueType
    override val parameters: Array<Formula<*>> = arrayOf(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Boolean {
        return value.isEmpty(context, dataLayer).not()
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaIsNotEmpty)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + value.hashCode()
        return result
    }
}