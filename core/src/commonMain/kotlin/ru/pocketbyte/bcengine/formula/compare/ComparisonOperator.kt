package ru.pocketbyte.bcengine.formula.compare

enum class ComparisonOperator(
    val sign: String
) {
    EQUAL("=="),
    GREATER_OR_EQUAL(">="),
    GREATER(">"),
    LESS_OR_EQUAL("<="),
    LESS("<")
}