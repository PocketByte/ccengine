package ru.pocketbyte.bcengine.formula.cast

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.token.Token

class FormulaDataCast<Type : Any>(
    override val token: Token?,
    val name: String,
    override val type: ValueType<Type>
): Formula<Type> {

    override val parameters: Array<Formula<*>> = emptyArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        dataLayer?.get(name)?.let {
            return type.fromString(it)
        }
        return type.fromString("\$$name")
    }
}