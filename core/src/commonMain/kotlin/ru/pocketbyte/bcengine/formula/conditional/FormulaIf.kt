package ru.pocketbyte.bcengine.formula.conditional

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaIf<Type : Any>(
    override val token: Token?,
    private val condition: Formula<Boolean>,
    private val thenValue: Formula<Type>,
    private val elseValue: Formula<Type>
): Formula<Type> {

    companion object {
        const val NAME = "if"
    }

    override val type: ValueType<Type>
        get() = thenValue.type

    override val parameters = arrayOf(condition, thenValue, elseValue)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        return if (condition.compute(context, dataLayer)) {
            thenValue.compute(context, dataLayer)
        } else {
            elseValue.compute(context, dataLayer)
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaIf<*>)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}