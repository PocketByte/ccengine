package ru.pocketbyte.bcengine.formula.valuetype

class DictValueType<out Type : Any> private constructor(
    val itemType: ValueType<Type>,
): ValueType<Map<String, Type>> {

    override val name = "${itemType.name}Dict"
    override val shortName: String = "${itemType.shortName}_dict"

    companion object {
        val String = DictValueType(StringValueType)
        val Number = DictValueType(NumberValueType)
        val Boolean = DictValueType(BooleanValueType)
        val Undefined = DictValueType(UndefinedValueType)

        fun forItemType(itemType: ValueType<Any>): DictValueType<Any> {
            return when (itemType) {
                StringValueType -> String
                NumberValueType -> Number
                BooleanValueType -> Boolean
                else -> Undefined
            }
        }
    }

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): Map<String, Type> {
        return if (valueType is DictValueType<*>) {
            if (itemType == valueType.itemType) {
                value as Map<String, Type>
            } else {
                fromDict(valueType, value as Map<String, *>)
            }
        } else {
            emptyMap()
        }
    }


    private fun fromDict(
        valueType: DictValueType<*>,
        value: Map<String, *>
    ): Map<String, Type> {
        return value.mapValues {
            when (valueType.itemType) {
                BooleanValueType -> itemType.from(BooleanValueType, it as Boolean)
                NumberValueType -> itemType.from(NumberValueType, it as Float)
                StringValueType -> itemType.from(StringValueType, it as String)
                else -> {
                    throw IllegalArgumentException(
                        "Failed to convert $value. From $valueType to $this"
                    )
                }
            }
        }
    }
}