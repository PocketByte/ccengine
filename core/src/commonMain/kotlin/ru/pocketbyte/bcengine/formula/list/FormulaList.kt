package ru.pocketbyte.bcengine.formula.list

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaList<out Type : Any>(
    override val token: Token?,
    override val type: ListValueType<Type>,
    val list: List<Formula<Type>>,
): Formula<List<Type>> {

    override val parameters: Array<Formula<*>> = list.toTypedArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): List<Type> {
        return list.map {
            it.compute(context, dataLayer)
        }
    }
}