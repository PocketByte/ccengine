package ru.pocketbyte.bcengine.formula.valuetype

fun <Type : Any> ValueType<Type>.toString(value: Type): String {
    return StringValueType.from(this, value)
}

fun <Type : Any> ValueType<Type>.toNumber(value: Type): Float {
    return NumberValueType.from(this, value)
}

fun <Type : Any> ValueType<Type>.toBoolean(value: Type): Boolean {
    return BooleanValueType.from(this, value)
}