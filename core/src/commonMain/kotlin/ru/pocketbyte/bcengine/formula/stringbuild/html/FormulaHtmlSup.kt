package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlSup(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("sup"), content) {
    companion object {
        const val NAME = "htmlSup"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}