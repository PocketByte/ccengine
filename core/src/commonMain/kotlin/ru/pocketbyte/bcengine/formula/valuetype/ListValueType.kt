package ru.pocketbyte.bcengine.formula.valuetype

class ListValueType<out Type : Any> private constructor(
    val itemType: ValueType<Type>,
): ValueType<List<Type>> {

    override val name = "${itemType.name}List"
    override val shortName: String = "${itemType.shortName}_list"

    companion object {
        val String = ListValueType(StringValueType)
        val Number = ListValueType(NumberValueType)
        val Boolean = ListValueType(BooleanValueType)
    }

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): List<Type> {
        return if (valueType is ListValueType<*>) {
            if (itemType == valueType.itemType) {
                value as List<Type>
            } else {
                fromList(valueType, value as List<*>)
            }
        } else {
            listOf(itemType.from(valueType, value))
        }
    }

    private fun fromList(
        valueType: ListValueType<*>,
        value: List<*>
    ): List<Type> {
        return value.map {
            when (valueType.itemType) {
                BooleanValueType -> itemType.from(BooleanValueType, it as Boolean)
                NumberValueType -> itemType.from(NumberValueType, it as Float)
                StringValueType -> itemType.from(StringValueType, it as String)
                else -> {
                    throw IllegalArgumentException(
                        "Failed to convert $value. From $valueType to $this"
                    )
                }
            }
        }
    }
}



