package ru.pocketbyte.bcengine.formula.operator

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.extension.computeToBoolean
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaBooleanNot(
    override val token: Token?,
    private val value: Formula<Boolean>
): Formula<Boolean> {

    companion object {
        const val NAME = "not"
    }

    override val type = BooleanValueType
    override val parameters = arrayOf<Formula<*>>(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Boolean {
        return value.computeToBoolean(context, dataLayer).not()
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaBooleanOperator)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}