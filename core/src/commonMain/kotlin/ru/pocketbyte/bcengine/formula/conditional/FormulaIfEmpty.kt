package ru.pocketbyte.bcengine.formula.conditional

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaIfEmpty<Type : Any>(
    override val token: Token?,
    private val value: Formula<Type>,
    private val defaultValue: Formula<Type>
): Formula<Type> {

    companion object {
        const val NAME = "ifEmpty"
    }

    override val type: ValueType<Type>
        get() = value.type

    override val parameters: Array<Formula<*>> = arrayOf(value, defaultValue)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): Type {
        val computedValue = value.compute(context, dataLayer)
        return if (computedValue is String && computedValue.isNotEmpty()) {
            computedValue
        } else {
            defaultValue.compute(context, dataLayer)
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaIfEmpty<*>)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}