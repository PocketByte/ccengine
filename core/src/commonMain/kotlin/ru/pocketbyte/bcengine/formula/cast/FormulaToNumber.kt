package ru.pocketbyte.bcengine.formula.cast

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.extension.computeToNumber
import ru.pocketbyte.bcengine.token.Token

class FormulaToNumber(
    override val token: Token?,
    private val value: Formula<*>,
): Formula<Float> {

    override val type = NumberValueType
    override val parameters: Array<Formula<*>> = arrayOf(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Float {
        return value.computeToNumber(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaToNumber)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}