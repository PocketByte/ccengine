package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.formula.extension.compute
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString

class FormulaNamedProperty<Type : Any>(
    override val token: Token?,
    val name: String,
    override val type: ValueType<Type>
): Formula<Type> {

    override val parameters: Array<Formula<*>> = emptyArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        context.findFormula(name)?.let {
            return type.compute(it, context, dataLayer)
        }
        dataLayer?.get(name)?.let {
            return type.fromString(it)
        }
        return type.fromString("\$$name")
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaNamedProperty<*>)
                && name == other.name
                && type == other.type
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}