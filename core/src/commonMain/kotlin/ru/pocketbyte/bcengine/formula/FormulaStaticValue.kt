package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaStaticValue<Type : Any>(
    override val token: Token?,
    val value: Type,
    override val type: ValueType<Type>
): Formula<Type> {

    override val parameters: Array<Formula<*>> = emptyArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Type {
        return value
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaStaticValue<*>)
                && value == other.value
                && type == other.type
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + value.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}

fun formulaStaticValue(value: String): FormulaStaticValue<String> {
    return FormulaStaticValue(null, value, StringValueType)
}

fun formulaStaticValue(token: Token, value: String): FormulaStaticValue<String> {
    return FormulaStaticValue(token, value, StringValueType)
}

fun formulaStaticValue(value: Float): FormulaStaticValue<Float> {
    return FormulaStaticValue(null, value, NumberValueType)
}

fun formulaStaticValue(token: Token, value: Float): FormulaStaticValue<Float> {
    return FormulaStaticValue(token, value, NumberValueType)
}