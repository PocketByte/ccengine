package ru.pocketbyte.bcengine.formula.stringbuild.replace

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.contextOverride
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaReplaceRegexString(
    override val token: Token?,
    private val string: Formula<String>,
    private val regex: Formula<String>,
    private val newString: Formula<String>
): Formula<String> {

    companion object {
        const val NAME = "replaceR"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(string, regex, newString)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        return string.compute(context, dataLayer).replace(
            regex.compute(context, dataLayer).toRegex()
        ) { match ->
            newString.compute(context.contextOverride(match), dataLayer)
        }
    }

    private fun FormulaContext.contextOverride(match: MatchResult): FormulaContext {
        val overrideMap = mutableMapOf<String, Formula<*>>()
        match.groupValues.forEachIndexed { index, value ->
            val formula = formulaStaticValue(value)
            overrideMap["$index"] = formula
            overrideMap["${index}_$NAME"] = formula
        }
        return contextOverride(overrideMap)
    }
}
