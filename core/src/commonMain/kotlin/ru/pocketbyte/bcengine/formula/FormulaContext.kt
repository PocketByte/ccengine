package ru.pocketbyte.bcengine.formula

interface FormulaContext {
    fun findFormula(name: String): Formula<*>?
}