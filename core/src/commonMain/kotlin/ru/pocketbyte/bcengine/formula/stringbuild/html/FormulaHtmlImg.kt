package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.stringbuild.FormulaGoogleSheetCsv
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.html.HTMLTagImage
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlImg(
    override val token: Token?,
    private val image: Formula<String>,
    private val align: Formula<String>?,
    private val width: Formula<String>?,
    private val height: Formula<String>?,
    private val advanceWidth: Formula<Float>?,
    private val advanceHeight: Formula<Float>?,
    private val originX: Formula<Float>?,
    private val originY: Formula<Float>?
): Formula<String> {
    companion object {
        const val NAME = "htmlImg"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(image, align, width, height)
        .filterNotNull().toTypedArray()

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        val builder = StringBuilder("<img ")
            .append(HTMLTagImage.src)
            .append("=\"")
            .append(image.compute(context, dataLayer))
            .append("\"")

        align?.compute(context, dataLayer)?.also {
            if (it.isBlank()) return@also
            builder.append(" ")
                .append(HTMLTagImage.align)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        width?.compute(context, dataLayer)?.also {
            if (it.isBlank()) return@also
            builder.append(" ")
                .append(HTMLTagImage.width)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        height?.compute(context, dataLayer)?.also {
            if (it.isBlank()) return@also
            builder.append(" ")
                .append(HTMLTagImage.height)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        advanceWidth?.compute(context, dataLayer)?.also {
            builder.append(" ")
                .append(HTMLTagImage.advanceWidth)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        advanceHeight?.compute(context, dataLayer)?.also {
            builder.append(" ")
                .append(HTMLTagImage.advanceHeight)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        originX?.compute(context, dataLayer)?.also {
            builder.append(" ")
                .append(HTMLTagImage.originX)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        originY?.compute(context, dataLayer)?.also {
            builder.append(" ")
                .append(HTMLTagImage.originY)
                .append("=\"")
                .append(it)
                .append("\"")
        }

        builder.append(">")

        return builder.toString()
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaGoogleSheetCsv)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}