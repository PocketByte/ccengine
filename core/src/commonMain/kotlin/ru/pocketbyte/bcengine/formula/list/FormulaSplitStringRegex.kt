package ru.pocketbyte.bcengine.formula.list

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaSplitStringRegex(
    override val token: Token?,
    val string: Formula<String>,
    val separator: Formula<String>
): Formula<List<String>> {

    companion object {
        const val NAME = "splitR"
    }

    override val type = ListValueType.String
    override val parameters: Array<Formula<*>> = arrayOf(string, separator)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): List<String> {
        return string
            .compute(context, dataLayer)
            .split(separator.compute(context, dataLayer).toRegex())
    }
}