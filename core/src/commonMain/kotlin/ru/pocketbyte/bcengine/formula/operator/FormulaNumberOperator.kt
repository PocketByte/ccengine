package ru.pocketbyte.bcengine.formula.operator

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.extension.computeToNumber
import ru.pocketbyte.bcengine.token.Token

class FormulaNumberOperator(
    override val token: Token?,
    private val value1: Formula<*>,
    private val operator: NumberOperator,
    private val value2: Formula<*>
): Formula<Float> {

    override val type = NumberValueType
    override val parameters = arrayOf(value1, value2)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Float {
        return calculate(
            value1.computeToNumber(context, dataLayer),
            value2.computeToNumber(context, dataLayer)
        )
    }

    private fun calculate(v1: Float, v2: Float): Float {
        return when (operator) {
            NumberOperator.PLUS -> v1 + v2
            NumberOperator.MINUS -> v1 - v2
            NumberOperator.MULTIPLY -> v1 * v2
            NumberOperator.DIVIDE -> v1 / v2
        }
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaNumberOperator)
                && parameters.contentEquals(other.parameters)
                && operator == other.operator
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + operator.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}