package ru.pocketbyte.bcengine.formula

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.extension.computeToString
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType

internal fun Formula<*>.isEmpty(context: FormulaContext, dataLayer: DataSetItem?): Boolean {
    return when(type) {
        is StringValueType -> computeToString(context, dataLayer).isEmpty()
        else -> false
    }
}
