package ru.pocketbyte.bcengine.formula.valuetype

object StringValueType: ValueType<String> {
    override val name: String = "String"
    override val shortName: String = "str"

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): String {
        return when (valueType) {
            is StringValueType -> {
                value as String
            }
            is NumberValueType -> {
                val floatValue = value as Float
                val intValue = floatValue.toInt()
                return if (intValue.toFloat() == floatValue) {
                    intValue.toString()
                } else {
                    floatValue.toString()
                }
            }
            is BooleanValueType -> {
                (value as Boolean).toString()
            }
            is ListValueType<*> -> {
                (value as List<Any>).joinToString("; ") {
                    from(valueType.itemType, it)
                }
            }
            is DictValueType<*> -> {
                (value as Map<String, Any>).entries.joinToString("; ") {
                    "${it.key}:${from(valueType.itemType, it.value)}"
                }
            }
            is UndefinedValueType -> throw RuntimeException("Unknown type")
        }
    }
}