package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlSub(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("sub"), content) {
    companion object {
        const val NAME = "htmlSub"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}