package ru.pocketbyte.bcengine.formula.cast

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.extension.computeToBoolean
import ru.pocketbyte.bcengine.token.Token

class FormulaToBoolean(
    override val token: Token?,
    private val value: Formula<*>,
): Formula<Boolean> {

    override val type = BooleanValueType
    override val parameters: Array<Formula<*>> = arrayOf(value)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?) : Boolean {
        return value.computeToBoolean(context, dataLayer)
    }

    override fun equals(other: Any?): Boolean {
        return (other is FormulaToBoolean)
                && parameters.contentEquals(other.parameters)
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + parameters.contentHashCode()
        return result
    }
}