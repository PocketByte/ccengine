package ru.pocketbyte.bcengine.formula.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.token.Token

class FormulaHtmlB(
    token: Token?,
    content: Formula<String>
): FormulaHtmlTag(token, formulaStaticValue("b"), content) {
    companion object {
        const val NAME = "htmlB"
    }

    override val parameters: Array<Formula<*>> = arrayOf(content)
}
