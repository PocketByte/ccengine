package ru.pocketbyte.bcengine.formula.stringbuild.replace

import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaContext
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.Token

class FormulaReplaceDict(
    override val token: Token?,
    val string: Formula<String>,
    val dict: Formula<Map<String, String>>,
): Formula<String> {

    companion object {
        const val NAME = "replaceDict"
    }

    override val type = StringValueType
    override val parameters: Array<Formula<*>> = arrayOf(string, dict)

    override fun compute(context: FormulaContext, dataLayer: DataSetItem?): String {
        var result = string.compute(context, dataLayer)
        val map = dict.compute(context, dataLayer)

        map.entries.forEach {
            result = result.replace(it.key, it.value)
        }
        return result
    }
}
