package ru.pocketbyte.bcengine.formula.valuetype

object UndefinedValueType: ValueType<String> {
    override val name: String = "Undefined"
    override val shortName: String = "any"

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): String {
        throw RuntimeException("Value type undefined")
    }
}
