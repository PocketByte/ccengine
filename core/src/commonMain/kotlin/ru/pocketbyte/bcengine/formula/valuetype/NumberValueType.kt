package ru.pocketbyte.bcengine.formula.valuetype

object NumberValueType: ValueType<Float> {
    override val name: String = "Number"
    override val shortName: String = "num"

    override fun <AnotherType : Any> from(
        valueType: ValueType<AnotherType>,
        value: AnotherType
    ): Float {
        return when (valueType) {
            is StringValueType -> {
                (value as String).toFloatOrNull() ?: 0f
            }
            is NumberValueType -> {
                value as Float
            }
            is BooleanValueType -> {
                if (value as Boolean) 1f else 0f
            }
            is ListValueType<*> -> {
                (value as List<Any>).size.toFloat()
            }
            is DictValueType<*> -> {
                (value as Map<Any, Any>).size.toFloat()
            }
            is UndefinedValueType -> throw RuntimeException("Unknown type")
        }
    }
}