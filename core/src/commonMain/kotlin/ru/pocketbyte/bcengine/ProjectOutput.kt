package ru.pocketbyte.bcengine

import ru.pocketbyte.bcengine.dataset.DataSet
import ru.pocketbyte.bcengine.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.formula.FormulaNamedProperty
import ru.pocketbyte.bcengine.formula.cast.FormulaToString
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.FormulaStringConcat
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.provider.IntProvider
import ru.pocketbyte.bcengine.provider.StringProvider
import ru.pocketbyte.bcengine.token.TokenSimple

class ProjectOutput {
    val dir: StringProvider = StringProvider("./output/")
    val image: StringProvider = StringProvider("")
    val pdf: StringProvider = StringProvider("")
    val singleImage: StringProvider = StringProvider("")
    val singleImageRowSize: IntProvider = IntProvider(0)

    private val defaultImageName = FormulaStringConcat(
        TokenSimple("\"card_\" + str(\$ITEM_INDEX)"),
        formulaStaticValue("card_"),
        FormulaToString(
            null,
            FormulaNamedProperty(null, DataSet.ITEM_INDEX, NumberValueType)
        )
    ).apply {
        image.formula = this
    }

    private val defaultSingleImageName = formulaStaticValue("deck").apply {
        singleImage.formula = this
    }

    private val defaultPdfName = formulaStaticValue("deck").apply {
        pdf.formula = this
    }

    fun getImagePath(
        context: Context, project: Project, dataLayer: DataSetItem?, format: String
    ): String {
        val result = createInitialPathBuilder(context, project, dataLayer)
        result.append(
            image.get(project, dataLayer).trim().ifBlank {
                defaultImageName.compute(project, dataLayer)
            }
        )
        result.append(".${format.lowercase()}")
        return result.toString()
    }

    fun getSingleImagePath(
        context: Context, project: Project, format: String
    ): String {
        val result = createInitialPathBuilder(context, project, null)
        result.append(
            singleImage.get(project, null).trim().ifBlank {
                defaultSingleImageName.compute(project, null)
            }
        )
        result.append(".${format.lowercase()}")
        return result.toString()
    }

    fun getPdfPath(context: Context, project: Project): String {
        val result = createInitialPathBuilder(context, project, null)
        result.append(
            pdf.get(project, null).trim().ifBlank {
                defaultPdfName.compute(project, null)
            }
        )
        result.append(".pdf")
        return result.toString()
    }

    private fun createInitialPathBuilder(
        context: Context, project: Project, dataLayer: DataSetItem?
    ): StringBuilder {
        val dirPath = dir.get(project, dataLayer).trim()
        val result = StringBuilder()

        if (dirPath.startsWith('.')) {
            result.append(context.workDir)
            if (result.last() != '/') {
                result.append('/')
            }
        }

        result.append(dirPath)
        if (result.last() != '/') {
            result.append('/')
        }
        return result
    }
}