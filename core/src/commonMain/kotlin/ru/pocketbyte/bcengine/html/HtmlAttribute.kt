package ru.pocketbyte.bcengine.html

data class HtmlAttribute(
    val tag: HTMLTag,
    val attributes: Map<String, Any?>?,
    val startIndex: Int,
    val endIndex: Int
) {
    fun getStringAttribute(attribute: String): String? {
        return attributes?.get(attribute) as? String
    }

    fun getFloatAttribute(attribute: String): Float? {
        return attributes?.get(attribute)?.let {
            (it as? Float) ?: it.toString().toFloatOrNull()
        }
    }

    fun getAttribute(attribute: String): Any? {
        return attributes?.get(attribute)
    }
}