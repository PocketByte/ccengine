package ru.pocketbyte.bcengine.html

class HtmlDecodeResult(
    val string: String,
    val attrs: List<HtmlAttribute>
)