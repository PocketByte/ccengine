package ru.pocketbyte.bcengine.html

expect object HtmlStringDecoder {
    fun decode(string: String): HtmlDecodeResult
}