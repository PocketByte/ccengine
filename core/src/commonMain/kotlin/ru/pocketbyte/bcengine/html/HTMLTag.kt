package ru.pocketbyte.bcengine.html

sealed interface HTMLTag {
    val startString: String
        get() = ""
    val endString: String
        get() = ""
}

object HTMLTagBr : HTMLTag {
    override val startString = "\n"
}

object HTMLTagQ: HTMLTag {
    override val startString = "\""
    override val endString = "\""
}

object HTMLTagImage: HTMLTag {
    override val startString = " "

    const val src = "src"
    const val width = "width"
    const val height = "height"
    const val align = "align"
    const val originX = "originx"
    const val originY = "originy"
    const val advanceWidth = "advancewidth"
    const val advanceHeight = "advanceheight"

}

object HTMLTagFont: HTMLTag {
    const val face = "face"
    const val size = "size"
    const val color = "color"
}
object HTMLTagB: HTMLTag
object HTMLTagI: HTMLTag
object HTMLTagS: HTMLTag
object HTMLTagU: HTMLTag
object HTMLTagSup: HTMLTag
object HTMLTagSub: HTMLTag

// --- Tag Names -----------------------------------

// h1 - h6 = Font size change ???
// small, big = Font size change ???
// The <p> tag defines a paragraph.


// May be
// The <hr> tag defines a thematic break in an HTML page (e.g. a shift of topic).
// The <bdo> tag is used to override the current text direction.
// ol, li, ul = lists
