package ru.pocketbyte.bcengine.entity.coder

import org.junit.Test
import ru.pocketbyte.bcengine.entity.RGBAColor
import kotlin.test.assertEquals

class ColorCoderTest {


    @Test
    fun test() {
        assertEquals(
            RGBAColor(255, 255, 255, 255),
            ColorCoder.decode("#FFFFFF")
        )
        assertEquals(
            RGBAColor(0, 0, 0, 255),
            ColorCoder.decode("#000000")
        )
        assertEquals(
            RGBAColor(16, 32, 48, 255),
            ColorCoder.decode("#102030")
        )
        assertEquals(
            RGBAColor(1, 2, 3, 255),
            ColorCoder.decode("#010203")
        )
        assertEquals(
            RGBAColor(206, 246, 255, 255),
            ColorCoder.decode("#CEF6FF")
        )
    }
}