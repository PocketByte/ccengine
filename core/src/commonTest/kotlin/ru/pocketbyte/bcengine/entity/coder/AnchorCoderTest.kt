package ru.pocketbyte.bcengine.entity.coder

import org.junit.Test
import ru.pocketbyte.bcengine.entity.Aligning
import ru.pocketbyte.bcengine.entity.Anchor
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeSimple
import kotlin.test.assertEquals

class AnchorCoderTest {

    @Test
    fun testEmpty() {
        assertEquals(
            Anchor.ZERO,
            AnchorCoder.decode("", Orientation.NONE)
        )
    }

    @Test
    fun testOnlySize() {
        assertEquals(
            Anchor(SizeSimple(10f, Size.Dimension.PERCENT), Aligning.START),
            AnchorCoder.decode("10%", Orientation.NONE)
        )
    }
}