package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.CSVDataSetDecoder
import ru.pocketbyte.bcengine.dataset.item.MapDataSetItem
import kotlin.test.Test
import kotlin.test.assertEquals

class CSVDataSetTest {

    @Test
    fun test() {
        val sequence: Sequence<String> = listOf(
            "INDEX, SHMINDEX, \"Hello world\"",
            "1, 20.5 , \"A string, ...\"",
            "\" ddd\"\"qqq \", 4, true "
        ).asSequence()
        val dataSet = CSVDataSetDecoder.decode(sequence.iterator(), Project())

        assertEquals(2, dataSet.size)
        assertEquals(
            MapDataSetItem(
                Pair("INDEX", "1"),
                Pair("SHMINDEX", "20.5", ),
                Pair("Hello world", "A string, ..."),
                Pair("ITEM_INDEX", "0"),
                Pair("ITEM_SUB_INDEX", "0"),
                Pair("ROW_INDEX", "0"),
                Pair("AMOUNT", "1")
            ),
            dataSet.get(0)
        )
        assertEquals(
            MapDataSetItem(
                Pair("INDEX", " ddd\"qqq "),
                Pair("SHMINDEX", "4", ),
                Pair("Hello world", "true"),
                Pair("ITEM_INDEX", "1"),
                Pair("ITEM_SUB_INDEX", "0"),
                Pair("ROW_INDEX", "1"),
                Pair("AMOUNT", "1")
            ),
            dataSet.get(1)
        )
    }
}