package ru.pocketbyte.bcengine.html

import javax.swing.text.MutableAttributeSet
import javax.swing.text.html.HTML
import javax.swing.text.html.HTMLEditorKit
import javax.swing.text.html.parser.ParserDelegator

actual object HtmlStringDecoder {

    class ActiveTag(
        val tag: HTMLTag,
        val attributes: Map<String, Any>?,
        val startIndex: Int,
        val parent: ActiveTag?
    )

    private val knownTags = mutableMapOf<String, HTMLTag>()

    init {
        knownTags["b"] = HTMLTagB
        knownTags["strong"] = HTMLTagB

        knownTags["i"] = HTMLTagI
        knownTags["cite"] = HTMLTagI
        knownTags["em"] = HTMLTagI

        knownTags["s"] = HTMLTagS
        knownTags["strike"] = HTMLTagS
        knownTags["del"] = HTMLTagS

        knownTags["u"] = HTMLTagU
        knownTags["ins"] = HTMLTagU

        knownTags["img"] = HTMLTagImage
        knownTags["font"] = HTMLTagFont

        knownTags["sup"] = HTMLTagSup
        knownTags["sub"] = HTMLTagSub

        knownTags["br"] = HTMLTagBr
        knownTags["q"] = HTMLTagQ
    }

    actual fun decode(string: String): HtmlDecodeResult {
        val callback = CallbackImpl(knownTags)

        ParserDelegator().parse(
            string.encodeToByteArray().inputStream().reader(),
            callback, true
        )

        return HtmlDecodeResult(callback.getString(), callback.getAttributes())
    }

    private class CallbackImpl(
        val knownTags: Map<String, HTMLTag>
    ): HTMLEditorKit.ParserCallback() {

        private val stringBuilder = StringBuilder()
        private val activeTags = mutableMapOf<String, ActiveTag?>()
        private val attributesList = mutableListOf<HtmlAttribute>()

        fun getString(): String {
            return stringBuilder.toString()
        }

        fun getAttributes(): List<HtmlAttribute> {
            return attributesList
        }

        override fun handleText(data: CharArray?, pos: Int) {
            data?.let {
                stringBuilder.append(it)
            }
        }

        override fun handleSimpleTag(tag: HTML.Tag?, attributes: MutableAttributeSet?, pos: Int) {
            knownTags[tag.toString()]?.let { htmlTag ->
                attributesList.add(
                    HtmlAttribute(
                        htmlTag,
                        attributes?.toMap(),
                        stringBuilder.length,
                        stringBuilder.length + htmlTag.startString.length + htmlTag.endString.length
                    )
                )
                stringBuilder.append(htmlTag.startString).append(htmlTag.endString)
            }
        }

        override fun handleStartTag(tag: HTML.Tag?, attributes: MutableAttributeSet?, pos: Int) {
            var activeTag = activeTags[tag.toString()]
            if (activeTag != null) {
                activeTag = ActiveTag(
                    activeTag.tag, attributes?.toMap(),
                    stringBuilder.length,
                    activeTag
                )
            } else {
                knownTags[tag.toString()]?.let {
                    activeTag = ActiveTag(
                        it, attributes?.toMap(),
                        stringBuilder.length,
                        null
                    )
                }
            }

            activeTags[tag.toString()] = activeTag

            activeTag?.let {
                stringBuilder.append(it.tag.startString)
            }
        }

        override fun handleEndTag(tag: HTML.Tag?, pos: Int) {
            activeTags[tag.toString()]?.let { activeTag ->
                stringBuilder.append(activeTag.tag.endString)

                val start = activeTag.startIndex
                val end = stringBuilder.length
                if (start < end) {
                    attributesList.add(
                        HtmlAttribute(
                            activeTag.tag,
                            activeTag.attributes,
                            start, end
                        )
                    )
                }

                activeTags[tag.toString()] = activeTag.parent
            }
        }

        private fun MutableAttributeSet.toMap(): Map<String, Any> {
            val result = mutableMapOf<String, Any>()
            attributeNames.iterator().forEach {
                result[it.toString()] = getAttribute(it)
            }
            return result
        }
    }
}