package ru.pocketbyte.bcengine.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.kydra.log.info

actual class CSVDataSetFactory : DataSetFactory {

    actual var path: Formula<String> = FormulaStaticValue(null, "", StringValueType)

    override fun get(context: Context, project: Project): DataSet {
        val path = path.compute(project, null)
        context.logger.info { "Getting CSV DataSet from: $path" }
        return CSVDataSetDecoder.decode(
            InputStringIterator.from(path),
            project
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other is CSVDataSetFactory) {
            return path == other.path
        }
        return false
    }

    override fun hashCode(): Int {
        return path.hashCode()
    }
}