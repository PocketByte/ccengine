// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    val kotlinVersion = "1.9.20"
    val localaserVersion = "2.3.0-alpha2"

    repositories {
        google()
        mavenCentral()
        mavenLocal()
        maven("https://maven.google.com")
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")

        classpath("ru.pocketbyte.locolaser:plugin:$localaserVersion")
        classpath("ru.pocketbyte.locolaser:resource-kotlin-mpp:$localaserVersion")
        classpath("ru.pocketbyte.locolaser:resource-properties:$localaserVersion")
        classpath("ru.pocketbyte.locolaser:runtime:$localaserVersion")
    }
}

allprojects {
    repositories {
        mavenLocal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}