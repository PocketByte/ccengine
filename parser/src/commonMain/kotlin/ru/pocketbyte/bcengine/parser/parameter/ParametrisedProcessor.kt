package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

interface ParametrisedProcessor {
    val name: String
    val descriptionKey: String

    /**
     * Parameters array that defines set and order of parameters.
     */
    val parameters: Array<Parameter<*>>

    /**
     * Parameters separator type for given Processor.
     */
    val parameterSeparator: SeparatorToken.Type
}