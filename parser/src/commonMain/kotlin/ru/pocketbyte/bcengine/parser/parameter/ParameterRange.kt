package ru.pocketbyte.bcengine.parser.parameter

data class ParameterRange(
    val start: Int,
    val end: Int
) {
    val length: Int get() = end - start + 1
}