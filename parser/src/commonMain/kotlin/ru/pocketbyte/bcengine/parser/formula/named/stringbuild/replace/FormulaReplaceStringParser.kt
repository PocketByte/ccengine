package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.replace.FormulaReplaceString
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaReplaceStringParser: AbsNamedFormulaParser() {

    val PARAM_String = Parameter(
        name = "String",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_OldString = Parameter(
        name = "OldString",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_NewString = Parameter(
        name = "NewString",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaReplaceString.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_String, PARAM_OldString, PARAM_NewString
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val string = getFormula(context, project, PARAM_String, token, strict = true)
        val oldString = getFormula(context, project, PARAM_OldString, token, strict = string != null)
        val newString = getFormula(context, project, PARAM_NewString, token, strict = oldString != null)

        return if (string != null && oldString != null && newString != null) {
            assertParametersCount(context, token)
            FormulaReplaceString(token, string, oldString, newString)
        } else {
            FormulaStaticValue(token, "", StringValueType)
        }
    }
}