package ru.pocketbyte.bcengine.parser

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.OperatorToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

abstract class AbsObjectParser<Type> :
    ObjectParser<Type> {

    protected abstract fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): Type

    protected open fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: Type
    ) {
//        parameters.forEach { parameter ->
//            getFormula(context, project, parameter, token)?.let {
//                parameter.applyAction(instance, it)
//            }
//        }
    }

    protected open fun applyObjectContent(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        instance: Type,
        index: Int
    ): Int {
        return 0
//        val token = tokens.getOrNull(index) as? NamedPropertyToken ?: return 0
//
//        val parameter = getParameter<Type, Any>(token.name)
//        val formulaEnd = lookUpFormulaEnd(tokens, index + 1)
//
//        if (parameter == null) {
//            context.errors.addError(
//                ProjectError.ParseTokenError(token, "Property \"${token.name}\" not found.")
//            )
//            return formulaEnd - index
//        }
//
//        val value = FormulaParser.forProject(project).parse<Any>(
//            context, tokens, parameter.type, index + 1, formulaEnd,
//        )
//
//        parameter.applyAction(instance, value)
//
//        return formulaEnd - index
    }

    final override fun parse(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        currentIndex: Int
    ): ParseResult<Type> {
        val token = tokens.getOrNull(currentIndex) as? NamedGroupToken
            ?: return ParseResult(null, 1)

        val instance = createInstance(context, project, token).apply {
            applyParameters(context, project, token, this)
        }

        var hasCurlyGroup = true
        var index = currentIndex + 1
        while (index < tokens.size && hasCurlyGroup) {
            hasCurlyGroup = applyCurlyBracersContent(
                context, project,
                tokens[index], instance,
                index - currentIndex
            )
            if (hasCurlyGroup) {
                index++
            }
        }
        return ParseResult(instance, index - currentIndex)
    }

    private fun applyCurlyBracersContent(
        context: Context,
        project: Project,
        token: ProjectToken,
        instance: Type,
        tokenNumber: Int
    ): Boolean {
        val subToken = token as? NamedGroupToken ?: return false
        if (subToken.bracers != NamedGroupToken.Bracers.CURLY) {
            return false
        }
        if (tokenNumber > 1) {
            return false
        }
        if (subToken.name.isNotEmpty()) {
            return false
        }

        var index = 0
        while (index < subToken.subTokens.size) {
            val tokensConsumed = applyObjectContent(
                context, project, subToken.subTokens, instance, index
            )
            index += if (tokensConsumed > 0) { tokensConsumed } else { 1 }
        }
        return true
    }

    private fun lookUpFormulaEnd(tokens: List<ProjectToken>, startIndex: Int): Int {
        var isPreviousOperator = tokens.getOrNull(startIndex) is OperatorToken
        var currentIndex = startIndex + 1
        while (currentIndex < tokens.size) {
            if (isPreviousOperator) {
                if (tokens.getOrNull(currentIndex) is OperatorToken) {
                    return currentIndex - 1
                }
            } else {
                if (tokens.getOrNull(currentIndex) !is OperatorToken) {
                    return currentIndex - 1
                }
            }
            isPreviousOperator = !isPreviousOperator
            currentIndex += 1
        }
        return currentIndex - 1
    }
}
