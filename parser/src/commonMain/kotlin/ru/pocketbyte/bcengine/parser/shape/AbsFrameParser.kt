package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.parser.project.group.ProjectViewGroupHandler
import ru.pocketbyte.bcengine.shape.LayoutShape
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

abstract class AbsFrameParser<Type : LayoutShape>: AbsShapeParser<Type>() {

    override fun applyObjectContent(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        instance: Type,
        index: Int
    ): Int {
        super.applyObjectContent(context, project, tokens, instance, index)
            .let { if (it > 0) return it }

        val result = ProjectViewGroupHandler.buildShape(
            context, project, tokens, index
        )
        if (result != null) {
            if (result.instance != null) {
                instance.add(result.instance)
            }
        }
        return result?.tokensConsumed ?: 0
    }
}
