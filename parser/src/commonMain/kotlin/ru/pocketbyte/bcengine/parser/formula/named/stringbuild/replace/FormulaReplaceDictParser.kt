package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.replace.FormulaReplaceDict
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaReplaceDictParser: AbsNamedFormulaParser() {

    val PARAM_String = Parameter(
        name = "String",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Dict = Parameter(
        name = "Dictionary",
        type = DictValueType.String,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaReplaceDict.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_String, PARAM_Dict
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val string = getFormula(context, project, PARAM_String, token, strict = true)
        val dict = getFormula(context, project, PARAM_Dict, token, strict = string != null)

        return if (string != null && dict != null) {
            assertParametersCount(context, token)
            FormulaReplaceDict(token, string, dict)
        } else {
            FormulaStaticValue(token, "", StringValueType)
        }
    }
}