package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlB
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlBParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlB.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlB(token, content)
    }
}
