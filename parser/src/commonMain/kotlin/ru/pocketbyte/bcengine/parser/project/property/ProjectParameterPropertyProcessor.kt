package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.*
import ru.pocketbyte.bcengine.formula.valuetype.*
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getParameterToken
import ru.pocketbyte.bcengine.parser.parameter.getStringValue
import ru.pocketbyte.bcengine.parser.project.*
import ru.pocketbyte.bcengine.parser.utils.trimQuotes
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectParameterPropertyProcessor: AbsProjectPropertyProcessor() {

    override val name = "PARAM"
    override val isExclusive = false

    private val PARAM_Name = Parameter(
        name = "Name",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Type = Parameter(
        name = "Type",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Value = Parameter(
        name = "Value",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Name, PARAM_Type, PARAM_Value
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return
        val name = getStringValue(context, PARAM_Name, token, strict = true)?.trim()
            ?: return

        if (project.findFormula(name) != null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token,
                    "Double declaration! " +
                            "Function or parameter with name'$name' already declared."
                )
            )
            return
        }

        val typeName = getStringValue(context, PARAM_Type, token, strict = true)
            ?.lowercase()?.trim()

        val type: ValueType<Any> =
            when (typeName) {
                StringValueType.shortName -> StringValueType
                NumberValueType.shortName -> NumberValueType
                BooleanValueType.shortName -> BooleanValueType
                null -> {
                    return
                }
                else -> {
                    context.errors.addError(
                        ProjectError.ParseTokenError(
                            token,
                            "Unknown parameter type: $typeName"
                        )
                    )
                    return
                }
            }

        val valueToken = getParameterToken(context, PARAM_Value, token, strict = true)

        if (valueToken == null) {
            project.parameters[name] = FormulaStaticValue(null, type.fromString(""), type)
            return
        }

        project.parameters[name] = when (type) {
            StringValueType -> FormulaStaticValue(
                valueToken,
                type.fromString(valueToken.getTokenString().trim().trimQuotes()),
                type
            )
            else -> FormulaStaticValue(
                valueToken,
                type.fromString(valueToken.getTokenString().trim()),
                type
            )
        }
    }
}
