package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.formula.valuetype.ValueType

data class Parameter<Type : Any>(
    val name: String,
    val type: ValueType<Type>,
    val formulaType: ParameterFormulaType,
    val varArg: Boolean = false,

    @Deprecated("Must be removed")
    val description: String? = null
)