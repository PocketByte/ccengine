package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.TextShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object TextShapeParser: AbsFrameParser<TextShape>() {

    val PARAM_Text = Parameter(
        name = "Text",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Font = Parameter(
        name = "Font",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Flags = Parameter(
        name = "Flags",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_AligningX = Parameter(
        name = "AligningX",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_AligningY = Parameter(
        name = "AligningY",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = "TEXT"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height,
        PARAM_Text, PARAM_Font, PARAM_Flags,
        PARAM_AligningX, PARAM_AligningY, PARAM_Angle
    )

    override fun createInstance(
        context: Context, project: Project, token: NamedGroupToken
    ): TextShape = TextShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: TextShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_Text, token)
                ?.let { text.formula = it }

            getFormula(context, project, PARAM_Font, token)
                ?.let { font.formula = it }

            getFormula(context, project, PARAM_Flags, token)
                ?.let { flags.formula = it }

            getFormula(context, project, PARAM_AligningX, token)
                ?.let { aligningX.formula = it }

            getFormula(context, project, PARAM_AligningY, token)
                ?.let { aligningY.formula = it }
        }
    }
}