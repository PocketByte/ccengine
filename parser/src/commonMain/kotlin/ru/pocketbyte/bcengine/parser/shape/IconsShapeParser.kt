package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.IconsShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object IconsShapeParser: AbsFrameParser<IconsShape>() {

    override val name: String = "ICONS"

    val PARAM_Icons = Parameter(
        name = "Icons",
        type = ListValueType.String,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_IconWidth = Parameter(
        name = "Icon Width",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_IconHeight = Parameter(
        name = "Icon Height",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Orientation = Parameter(
        name = "Orientation",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_IconAligning = Parameter(
        name = "IconAligning",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Direction = Parameter(
        name = "Direction",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_AligningX = Parameter(
        name = "AligningX",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_AligningY = Parameter(
        name = "AligningY",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height,
        PARAM_Icons, PARAM_Orientation, PARAM_Direction,
        PARAM_AligningX, PARAM_AligningY,
        PARAM_IconWidth, PARAM_IconHeight, PARAM_IconAligning, PARAM_Angle
    )

    override fun createInstance(
        context: Context, project: Project, token: NamedGroupToken
    ): IconsShape = IconsShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: IconsShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {

            getFormula(context, project, PARAM_Icons, token)
                ?.let { icons.formula = it }

            getFormula(context, project, PARAM_Orientation, token)
                ?.let { orientation.formula = it }

            getFormula(context, project, PARAM_Direction, token)
                ?.let { direction.formula = it }

            getFormula(context, project, PARAM_AligningX, token)
                ?.let { aligningX.formula = it }

            getFormula(context, project, PARAM_AligningY, token)
                ?.let { aligningY.formula = it }

            getFormula(context, project, PARAM_IconWidth, token)
                ?.let { iconWidth.formula = it }

            getFormula(context, project, PARAM_IconHeight, token)
                ?.let { iconHeight.formula = it }

            getFormula(context, project, PARAM_IconAligning, token)
                ?.let { iconAligning.formula = it }
        }
    }
}