package ru.pocketbyte.bcengine.parser.dataset

import ru.pocketbyte.bcengine.dataset.DataSetFactory
import ru.pocketbyte.bcengine.parser.AbsObjectParser
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

abstract class AbsDataSetParser<Type: DataSetFactory>
    : AbsObjectParser<Type>(), DataSetParser<Type> {

    override val descriptionKey get() = "dataseet_${name.lowercase()}"
    override val parameterSeparator = SeparatorToken.Type.COMMA
}