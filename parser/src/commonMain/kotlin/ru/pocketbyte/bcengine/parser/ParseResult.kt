package ru.pocketbyte.bcengine.parser

class ParseResult<Type>(
    val instance: Type?,
    val tokensConsumed: Int
)