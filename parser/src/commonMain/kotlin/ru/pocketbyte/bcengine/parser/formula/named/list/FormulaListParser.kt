package ru.pocketbyte.bcengine.parser.formula.named.list

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.list.FormulaList
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.forEachVarargParameterRange
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.parameter.getParameterToken
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

class FormulaListParser<Type : Any>(
    private val listType: ListValueType<Type>,
): AbsNamedFormulaParser() {

    val PARAM_Values = Parameter(
        name = "Values",
        type = listType.itemType,
        formulaType = ParameterFormulaType.FORMULA,
        varArg = true
    )

    override val name: String = listType.shortName

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Values
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val valuesList = mutableListOf<Formula<Type>>()

        forEachVarargParameterRange(PARAM_Values, token) { _, range ->
            valuesList.add(
                getFormula(context, project, range, token, PARAM_Values.type)
                    ?: FormulaStaticValue(
                        getParameterToken(range, token),
                        PARAM_Values.type.from(StringValueType, ""),
                        PARAM_Values.type
                    )
            )
        }

        return FormulaList(token, listType, valuesList)
    }
}