package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.character

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.character.FormulaChar
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaCharParser: AbsNamedFormulaParser() {

    val PARAM_CharCode = Parameter(
        name = "CharCode",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaChar.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(PARAM_CharCode)

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val charCode = getFormula(context, project, PARAM_CharCode, token, strict = true)

        return if (charCode != null) {
            assertParametersCount(context, token)
            FormulaChar(token, charCode)
        } else {
            FormulaStaticValue(token, token.getTokenString(), StringValueType)
        }
    }
}