package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.entity.Icon
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.parameter.getStringValue
import ru.pocketbyte.bcengine.parser.project.*
import ru.pocketbyte.bcengine.parser.utils.trimQuotes
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectIconPropertyProcessor: AbsProjectPropertyProcessor() {

    override val name = "ICON"
    override val isExclusive = false

    private val PARAM_Name = Parameter(
        name = "Name",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )
    private val PARAM_Path = Parameter(
        name = "Path",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    private val PARAM_Width = Parameter(
        name = "Width",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Height = Parameter(
        name = "Height",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_ScaleMode = Parameter(
        name = "ScaleMode",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AligningX = Parameter(
        name = "AligningX",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AligningY = Parameter(
        name = "AligningY",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Name, PARAM_Path, PARAM_Width, PARAM_Height,
        PARAM_ScaleMode, PARAM_AligningX, PARAM_AligningY
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return
        val name = getStringValue(context, PARAM_Name, token, strict = true)
            ?.trim()
            ?.trimQuotes()
            ?: return

        project.icons.addIcon(Icon(name).apply {
            image.formula = getFormula(context, project, PARAM_Path, token, strict = true)
            width.formula = getFormula(context, project, PARAM_Width, token, strict = true)
            height.formula = getFormula(context, project, PARAM_Height, token, strict = true)

            getFormula(context, project, PARAM_ScaleMode, token)
                ?.let { scaleMode.formula = it }

            getFormula(context, project, PARAM_AligningX, token)
                ?.let { aligningX.formula = it }

            getFormula(context, project, PARAM_AligningY, token)
                ?.let { aligningY.formula = it }
        })
    }
}