package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.project.AbsProjectPropertyProcessor
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectOutputImagePropertyProcessor: AbsProjectPropertyProcessor() {

    override val name = "OUTPUT_IMG"
    override val isExclusive = true

    private val PARAM_Path = Parameter(
        name = "File Path",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Path
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return

        getFormula(context, project, PARAM_Path, token)?.let {
            project.output.image.formula = it
        }
    }
}