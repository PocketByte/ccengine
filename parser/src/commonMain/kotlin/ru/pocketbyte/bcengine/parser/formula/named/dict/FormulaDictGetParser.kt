package ru.pocketbyte.bcengine.parser.formula.named.dict

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.dict.FormulaDictGet
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaDictGetParser: AbsNamedFormulaParser() {

    val PARAM_Dict = Parameter(
        name = "Dict",
        type = DictValueType.Undefined,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Key = Parameter(
        name = "Dict",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_FallbackValue = Parameter(
        name = "Fallback value",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaDictGet.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Dict, PARAM_Key, PARAM_FallbackValue
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val dict = getFormula(context, project, PARAM_Dict, token,
            strict = true,
            type = DictValueType.forItemType(formulaType)
        )
        val key = getFormula(context, project, PARAM_Key, token, strict = true)
        val fallbackValue = getFormula(context, project, PARAM_FallbackValue, token,
            strict = true,
            type = formulaType
        )
        return if (dict != null && key != null && fallbackValue != null) {
            FormulaDictGet(token, formulaType, dict, key, fallbackValue)
        } else {
            fallbackValue
                ?: FormulaStaticValue(token, formulaType.fromString(""), formulaType)
        }
    }
}