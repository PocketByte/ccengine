package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.entity.coder.SizeCoder
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.entity.Orientation
import ru.pocketbyte.bcengine.entity.Size
import ru.pocketbyte.bcengine.entity.SizeSimple
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.parser.parameter.*
import ru.pocketbyte.bcengine.parser.project.*
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectLayoutPropertyProcessor: AbsProjectPropertyProcessor() {
    override val name = "LAYOUT"
    override val isExclusive = true

    private val PARAM_Width = Parameter(
        name = "Width",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Height = Parameter(
        name = "Height",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Background = Parameter(
        name = "Background",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    private val PARAM_Dpi = Parameter(
        name = "Dpi",
        type = NumberValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Width, PARAM_Height, PARAM_Background, PARAM_Dpi
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return
        getParameterToken(context, PARAM_Width, token, strict = true)?.let {
            project.layout.width = decodeLayoutSize(context, it)
        }

        getParameterToken(context, PARAM_Height, token, strict = true)?.let {
            project.layout.height = decodeLayoutSize(context, it)
        }

        getFormula(context, project, PARAM_Background, token)?.let {
            project.layout.background.formula = it
        }

        getStringValue(PARAM_Dpi, token)?.let { dpiStr ->
            if (dpiStr.isBlank()) {
                return@let null
            }

            val dpi = dpiStr.toFloatOrNull()
            if (dpi == null) {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        getParameterToken(PARAM_Dpi, token) ?: token,
                        "Layout dpi should be a number. Formula not allowed."
                    )
                )
            } else {
                project.layout.dpi = dpi
            }
        }
    }

    private fun decodeLayoutSize(
        context: Context,
        token: ProjectToken
    ): SizeSimple {
        val size = try {
            SizeCoder.decode(
                token.getTokenString().trim(),
                Orientation.NONE
            )
        } catch (e: Exception) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token,
                    e.message ?: "Invalid size"
                )
            )
            return SizeSimple(0f, Size.Dimension.INCH)
        } as? SizeSimple

        if (size == null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token,
                    "Size of Layout can't be a complex or a formula"
                )
            )
            return SizeSimple(0f, Size.Dimension.INCH)
        }

        return size
    }
}