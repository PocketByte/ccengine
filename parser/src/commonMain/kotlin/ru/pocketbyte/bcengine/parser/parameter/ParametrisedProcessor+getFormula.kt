package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.parser.formula.FormulaParser
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.EmptyStringToken
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken


fun <T : Any> ParametrisedProcessor.getFormula(
    context: Context,
    project: Project,
    parameter: Parameter<T>,
    token: NamedGroupToken,
    strict: Boolean = false
): Formula<T>? {
    return getFormula(context, project, parameter, token, parameter.type, strict)
}

fun <T : Any> ParametrisedProcessor.getFormula(
    context: Context,
    project: Project,
    parameter: Parameter<*>,
    token: NamedGroupToken,
    type: ValueType<T>,
    strict: Boolean = false
): Formula<T>? {
    return getParameterRange(parameter, token)?.let {
        getFormula(context, project, it, token, type)
    }.apply {
        if (this == null && strict) {
            onParameterMissing(context, parameter, token)
        }
    }
}

fun <T : Any> ParametrisedProcessor.getFormula(
    context: Context,
    project: Project,
    parameterRange: ParameterRange,
    token: NamedGroupToken,
    type: ValueType<T>
): Formula<T>? {
    return getFormula(context, project, parameterRange, token.subTokens, type)
}

fun <T : Any> ParametrisedProcessor.getFormula(
    context: Context,
    project: Project,
    parameter: Parameter<T>,
    tokens: List<ProjectToken>
): Formula<T>? {
    return getParameterRange(parameter, tokens)?.let {
        getFormula(context, project, it, tokens, parameter.type)
    }
}

fun <T : Any> ParametrisedProcessor.getFormula(
    context: Context,
    project: Project,
    parameterRange: ParameterRange,
    tokens: List<ProjectToken>,
    type: ValueType<T>
): Formula<T>? {
    if (parameterRange.length == 0) {
        return null
    }
    if (parameterRange.length == 1) {
        val token = tokens.getOrNull(parameterRange.start)
        if (token == null || token is EmptyStringToken) {
            return null
        }
    }
    try {
        return FormulaParser.parse(
            context, project, tokens, type, parameterRange.start, parameterRange.end
        )
    } catch (e: Throwable) {
        if (type is StringValueType) {
            val token = getParameterToken(parameterRange, tokens)
            return FormulaStaticValue(
                token, type.fromString(token?.getTokenString() ?: ""), type
            )
        } else {
            context.errors.addError(
                ProjectError.ExceptionError(e)
            )
        }
    }
    return null
}