package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.parser.dataset.CSVDataSetParser
import ru.pocketbyte.bcengine.parser.dataset.DataSetParser
import ru.pocketbyte.bcengine.parser.dataset.EmptyDataSetParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.project.*
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectDataSetPropertyProcessor: AbsProjectPropertyProcessor() {

    companion object {
        val PARSERS = mapOf(
            EmptyDataSetParser.asMapPair(),
            CSVDataSetParser.asMapPair()
        )

        private fun DataSetParser<*>.asMapPair(): Pair<String, DataSetParser<*>> {
            return Pair(name.lowercase(), this)
        }
    }

    override val name = "DATASET"
    override val isExclusive = true

    private val PARAM_Value = Parameter(
        name = "Value",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA,
        description = "Type of the value depends on DetaSet type."
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Value
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return

        assertParametersCount(context, token, 1) { return }

        val datasetToken = token.subTokens[0] as? NamedGroupToken
        if (datasetToken == null) {
            ProjectError.ParseTokenError(
                token.subTokens[0], "Unknown type of DataSet."
            )
            return
        }

        val parser = PARSERS[datasetToken.name.lowercase()]
        if (parser == null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token, "Unknown type of DataSet: '${datasetToken.name}'."
                )
            )
            return
        }

        val dataSet = parser.parse(context, project, token.subTokens, 0).instance
        if (dataSet == null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token, "Failed to build DataSet."
                )
            )
            return
        }
        datasetToken.addMetaData(Token.METADATA_PROCESSOR, parser)

        project.dataSet = dataSet
    }
}