package ru.pocketbyte.bcengine.parser.formula.named.validation

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.validation.FormulaIsEmpty
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaIsEmptyParser: AbsNamedFormulaParser() {

    val PARAM_Value = Parameter(
        name = "Value",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaIsEmpty.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(PARAM_Value)

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        return getFormula(context, project, PARAM_Value, token, strict = true)?.let {
            assertParametersCount(context, token)
            FormulaIsEmpty(token, it)
        } ?: FormulaStaticValue(token, true, BooleanValueType)
    }
}