package ru.pocketbyte.bcengine.parser.project

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.parser.project.group.ProjectGroupHandler
import ru.pocketbyte.bcengine.parser.project.group.ProjectNoneHandler
import ru.pocketbyte.bcengine.parser.project.group.ProjectPropertiesHandler
import ru.pocketbyte.bcengine.parser.project.group.ProjectViewGroupHandler
import ru.pocketbyte.bcengine.parser.project.group.SkipGroupHandler
import ru.pocketbyte.bcengine.tokenizer.CommentToken
import ru.pocketbyte.bcengine.tokenizer.EmptyStringToken
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.Tokenizer
import ru.pocketbyte.bcengine.tokenizer.exception.TokenizeException
import ru.pocketbyte.bcengine.tokenizer.subTokensToString

class ProjectParser {

    private var groupHandler: ProjectGroupHandler = ProjectNoneHandler

    fun parse(context: Context, string: String): Project {
        val project = Project()

        val tokens = try {
            Tokenizer.tokenize(string, 0, string.length - 1)
        } catch (e: TokenizeException) {
            context.errors.addError(
                ProjectError.ParseError(
                    e.index, e.string, e.message ?: "Failed to tokenize project text"
                )
            )
            return project
        } catch (e: Throwable) {
            context.errors.addError(ProjectError.ExceptionError(e))
            return project
        }

        parse(context, project, tokens)

        return project
    }

    fun parse(context: Context, tokens: List<ProjectToken>): Project {
        return Project().apply {
            parse(context, this, tokens)
        }
    }

    private fun parse(context: Context, project: Project, tokens: List<ProjectToken>) {
        var i = 0
        while (i < tokens.size) {
            val token = tokens[i]
            when {
                token is EmptyStringToken || token is CommentToken -> {
                    /* Skip it */
                    i += 1
                }
                token is NamedGroupToken && token.isProjectGroupToken() -> {
                    if(!switchGroup(context, project, token, tokens)) {
                        context.errors.addError(
                            ProjectError.ParseTokenError(
                                token, "Unknown group: ${token.name}"
                            )
                        )
                    }
                    i += 1
                }
                else -> {
                    i += groupHandler.handle(context, project, tokens, i)
                }
            }
        }
    }

    private fun switchGroup(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        tokens: List<ProjectToken>
    ): Boolean {
        groupHandler.finish(context, project, tokens)

        val groupName = token.subTokensToString().lowercase().trim()
        if (groupHandler == ProjectNoneHandler && groupName != GROUP_PROJECT) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token, "Project must start from [${GROUP_PROJECT}] group."
                )
            )
            groupHandler = SkipGroupHandler
            return true
        }
        groupHandler = when (groupName) {
            GROUP_PROJECT -> ProjectPropertiesHandler()
            GROUP_VIEW -> ProjectViewGroupHandler
            else -> SkipGroupHandler
        }
        return groupHandler != SkipGroupHandler
    }

    private fun NamedGroupToken.isProjectGroupToken(): Boolean {
        return name.isEmpty() && bracers == NamedGroupToken.Bracers.SQUARE && subTokens.size == 1
    }

    companion object {
        private const val GROUP_PROJECT = "project"
        private const val GROUP_VIEW = "view"
    }
}
