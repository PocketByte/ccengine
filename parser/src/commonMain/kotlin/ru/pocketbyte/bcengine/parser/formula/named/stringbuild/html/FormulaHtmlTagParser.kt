package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlTag
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertMaxParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaHtmlTagParser: AbsNamedFormulaParser() {

    val PARAM_Tag = Parameter(
        name = "Tag",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Content = Parameter(
        name = "Content",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaHtmlTag.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Tag, PARAM_Content
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val content = getFormula(context, project, PARAM_Content, token, strict = true)
        val tag = getFormula(context, project, PARAM_Tag, token, strict = true)

        return if (tag != null && content != null) {
            assertMaxParametersCount(context, token)
            FormulaHtmlTag(token, tag, content)
        } else {
            FormulaStaticValue(token, token.getTokenString(), StringValueType)
        }
    }
}
