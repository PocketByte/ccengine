package ru.pocketbyte.bcengine.parser.formula.named.conditional

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.conditional.FormulaIfEmpty
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaIfEmptyParser: AbsNamedFormulaParser() {

    val PARAM_Value = Parameter(
        name = "Value",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_DefaultValue = Parameter(
        name = "DefaultValue",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaIfEmpty.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Value, PARAM_DefaultValue
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val value = getFormula(context, project, PARAM_Value, token, formulaType, strict = true)
        val defaultValue = getFormula(context, project, PARAM_DefaultValue, token, formulaType, strict = true)

        return if (value != null && defaultValue != null) {
            assertParametersCount(context, token)
            FormulaIfEmpty(token, value, defaultValue)
        } else {
            FormulaStaticValue(token, formulaType.fromString(""), formulaType)
        }
    }
}