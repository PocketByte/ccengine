package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.StringToken

fun ParametrisedProcessor.getStringValue(
    context: Context,
    parameter: Parameter<*>,
    token: NamedGroupToken,
    strict: Boolean
): String? {
    return getStringValue(parameter, token).apply {
        if (this == null && strict) {
            onParameterMissing(context, parameter, token)
        }
    }
}

fun ParametrisedProcessor.getStringValue(
    parameter: Parameter<*>,
    token: NamedGroupToken
): String? {
    return getParameterRange(parameter, token)?.let {
        getStringValue(it, token)
    }
}

fun ParametrisedProcessor.getStringValue(
    parameterRange: ParameterRange,
    token: NamedGroupToken
): String {
    return token.parentString.substring(
        token.subTokens[parameterRange.start].startIndex,
        token.subTokens[parameterRange.end].endIndex + 1,
    )
}