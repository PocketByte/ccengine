package ru.pocketbyte.bcengine.parser.formula.named.list

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.list.FormulaSplitString
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaSplitStringParser: AbsNamedFormulaParser() {

    val PARAM_String = Parameter(
        name = "String",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Separator = Parameter(
        name = "Separator",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaSplitString.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_String, PARAM_Separator
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val string = getFormula(context, project, PARAM_String, token, strict = true)
        val separator = getFormula(context, project, PARAM_Separator, token, strict = string != null)

        return if (string != null && separator != null) {
            assertParametersCount(context, token)
            FormulaSplitString(token, string, separator)
        } else {
            FormulaStaticValue(token, emptyList(), ListValueType.String)
        }
    }
}