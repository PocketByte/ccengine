package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.replace.FormulaReplaceRegexString
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaReplaceRegexStringParser: AbsNamedFormulaParser() {

    val PARAM_String = Parameter(
        name = "String",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Regex = Parameter(
        name = "Regex",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_NewString = Parameter(
        name = "NewString",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaReplaceRegexString.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_String, PARAM_Regex, PARAM_NewString
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val string = getFormula(context, project, PARAM_String, token, strict = true)
        val regex = getFormula(context, project, PARAM_Regex, token, strict = string != null)
        val newString = getFormula(context, project, PARAM_NewString, token, strict = regex != null)

        return if (string != null && regex != null && newString != null) {
            assertParametersCount(context, token)
            FormulaReplaceRegexString(token, string, regex, newString)
        } else {
            FormulaStaticValue(token, "", StringValueType)
        }
    }
}