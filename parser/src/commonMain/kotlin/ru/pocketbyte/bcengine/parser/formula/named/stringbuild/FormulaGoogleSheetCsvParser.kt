package ru.pocketbyte.bcengine.parser.formula.named.stringbuild

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.FormulaGoogleSheetCsv
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaGoogleSheetCsvParser: AbsNamedFormulaParser() {

    val PARAM_SheetId = Parameter(
        name = "SheetId",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_WorksheetId = Parameter(
        name = "WorksheetId",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaGoogleSheetCsv.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_SheetId, PARAM_WorksheetId
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val sheetId = getFormula(context, project, PARAM_SheetId, token, strict = true)
        val worksheetId = getFormula(context, project, PARAM_WorksheetId, token, strict = sheetId != null)

        return if (sheetId != null && worksheetId != null) {
            assertParametersCount(context, token)
            FormulaGoogleSheetCsv(token, sheetId, worksheetId)
        } else {
            FormulaStaticValue(token, "", StringValueType)
        }
    }
}