package ru.pocketbyte.bcengine.parser.formula.named.dict

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.dict.FormulaDict
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.forEachVarargParametersRange
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.parameter.getParameterToken
import ru.pocketbyte.bcengine.parser.parameter.getStringValue
import ru.pocketbyte.bcengine.parser.utils.trimQuotes
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

class FormulaDictParser<Type : Any>(
    val dictType: DictValueType<Type>,
): AbsNamedFormulaParser() {

    val PARAM_Keys = Parameter(
        name = "Keys",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA,
        varArg = true
    )

    val PARAM_Values = Parameter(
        name = "Values",
        type = dictType.itemType,
        formulaType = ParameterFormulaType.FORMULA,
        varArg = true
    )

    override val name: String = dictType.shortName

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Keys, PARAM_Values
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val valuesMap = mutableMapOf<String, Formula<Type>>()

        forEachVarargParametersRange(PARAM_Keys, 2, token) { _, ranges ->
            valuesMap[
                getFormula(context, project, ranges[0], token, PARAM_Keys.type)
                    ?.compute(project, null)
                    ?: getStringValue(ranges[0], token).trimQuotes()
            ] =
                getFormula(context, project, ranges[1], token, PARAM_Values.type)
                    ?: FormulaStaticValue(
                        getParameterToken(ranges[0], token),
                        PARAM_Values.type.from(StringValueType, ""),
                        PARAM_Values.type
                    )
        }

        return FormulaDict(token, dictType, valuesMap)
    }
}