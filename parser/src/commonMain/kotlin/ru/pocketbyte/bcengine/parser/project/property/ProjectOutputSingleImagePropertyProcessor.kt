package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.project.AbsProjectPropertyProcessor
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

class ProjectOutputSingleImagePropertyProcessor: AbsProjectPropertyProcessor() {

    override val name = "OUTPUT_SIMG"
    override val isExclusive = true

    private val PARAM_Path = Parameter(
        name = "File Path",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA
    )

    private val PARAM_RowSize = Parameter(
        name = "Items count in the row",
        type = NumberValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Path, PARAM_RowSize
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return

        getFormula(context, project, PARAM_Path, token)?.let {
            project.output.singleImage.formula = it
        }

        getFormula(context, project, PARAM_RowSize, token)?.let {
            project.output.singleImageRowSize.formula = it
        }
    }
}