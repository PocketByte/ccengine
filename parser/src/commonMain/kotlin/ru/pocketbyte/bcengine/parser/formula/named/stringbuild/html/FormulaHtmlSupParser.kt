package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlSup
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlSupParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlSup.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlSup(token, content)
    }
}
