package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.LineShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object LineShapeParser: AbsShapeParser<LineShape>() {

    val PARAM_StrokeSize = Parameter(
        name = "StrokeSize",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_StrokeColor = Parameter(
        name = "StrokeColor",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = "LINE"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height,
        PARAM_StrokeSize, PARAM_StrokeColor, PARAM_Angle
    )

    override fun createInstance(
        context: Context, project: Project, token: NamedGroupToken
    ): LineShape = LineShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: LineShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_StrokeSize, token)
                ?.let { stroke.size.formula = it }

            getFormula(context, project, PARAM_StrokeColor, token)
                ?.let { stroke.color.formula = it }
        }
    }
}