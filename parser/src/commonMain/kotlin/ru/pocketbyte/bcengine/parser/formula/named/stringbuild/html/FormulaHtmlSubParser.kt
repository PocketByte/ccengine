package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlSub
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlSubParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlSub.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlSub(token, content)
    }
}
