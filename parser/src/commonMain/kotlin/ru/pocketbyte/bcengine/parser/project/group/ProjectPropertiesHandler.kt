package ru.pocketbyte.bcengine.parser.project.group

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.parser.project.AbsProjectPropertyProcessor
import ru.pocketbyte.bcengine.parser.project.ProjectPropertyProcessor
import ru.pocketbyte.bcengine.parser.project.property.*
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.CommentToken
import ru.pocketbyte.bcengine.tokenizer.EmptyStringToken
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.NamedPropertyToken
import ru.pocketbyte.bcengine.tokenizer.OperatorToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken
import ru.pocketbyte.bcengine.tokenizer.StringToken
import ru.pocketbyte.bcengine.tokenizer.WordToken

class ProjectPropertiesHandler: ProjectGroupHandler {

    companion object {
        fun buildProcessorsArray(): Array<AbsProjectPropertyProcessor> {
            return arrayOf(
                ProjectParameterPropertyProcessor(),
                ProjectFunctionPropertyProcessor(),
                ProjectIconPropertyProcessor(),
                ProjectLayoutPropertyProcessor(),
                ProjectDataSetMultiplyPropertyProcessor(),
                ProjectDataSetPropertyProcessor(),
                ProjectOutputDirPropertyProcessor(),
                ProjectOutputImagePropertyProcessor(),
                ProjectOutputSingleImagePropertyProcessor(),
                ProjectOutputPdfPropertyProcessor()
            )
        }
    }

    private val handlersMap = mapOf<String, ProjectPropertyProcessor>(
        *buildProcessorsArray().map {
            Pair(it.name.lowercase(), it)
        }.toTypedArray()
    )

    override fun handle(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        return when (val token = tokens[index]) {
            is NamedGroupToken -> {
                handleObject(context, project, token, tokens, index)
            }
            is CommentToken,
            is EmptyStringToken -> {
                1 // Do nothing
            }
            is NamedPropertyToken,
            is OperatorToken,
            is SeparatorToken,
            is StringToken,
            is WordToken -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        token, "Unknown command"
                    ))
                1
            }
        }
    }

    override fun finish(context: Context, project: Project, tokens: List<ProjectToken>) {
        handlersMap.values.forEach {
            it.process(context, project, tokens)
        }
    }

    private fun handleObject(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        val tokenName = token.name.lowercase()
        val handler = handlersMap[tokenName]

        return if (handler == null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                token, "Unknown project property"
            ))
            1
        } else {
            token.addMetaData(Token.METADATA_PROCESSOR, handler)
            handler.add(context, project, tokens, index)
        }
    }
}