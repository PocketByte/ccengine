package ru.pocketbyte.bcengine.parser.project.group

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

object SkipGroupHandler: ProjectGroupHandler {
    override fun handle(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        // Do nothing
        return 1
    }
}