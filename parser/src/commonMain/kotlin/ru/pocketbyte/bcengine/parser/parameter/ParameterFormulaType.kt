package ru.pocketbyte.bcengine.parser.parameter

enum class ParameterFormulaType {
    STATIC_VALUE,
    STATIC_FORMULA,
    FORMULA;

    fun isStatic(): Boolean {
        return this != FORMULA
    }
}