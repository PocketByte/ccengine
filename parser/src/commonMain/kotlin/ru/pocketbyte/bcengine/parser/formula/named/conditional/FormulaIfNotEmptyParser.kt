package ru.pocketbyte.bcengine.parser.formula.named.conditional

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.conditional.FormulaIfNotEmpty
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaIfNotEmptyParser: AbsNamedFormulaParser() {

    val PARAM_Value = Parameter(
        name = "Value",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Transformation = Parameter(
        name = "Transformation",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaIfNotEmpty.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Value, PARAM_Transformation
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val value = getFormula(context, project, PARAM_Value, token, formulaType, strict = true)
        val transformation = getFormula(context, project, PARAM_Transformation, token, formulaType, strict = true)

        return if (value != null && transformation != null) {
            assertParametersCount(context, token)
            FormulaIfNotEmpty(token, value, transformation)
        } else {
            FormulaStaticValue(token, formulaType.fromString(""), formulaType)
        }
    }
}