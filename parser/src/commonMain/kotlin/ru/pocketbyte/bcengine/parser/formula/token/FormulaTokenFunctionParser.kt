package ru.pocketbyte.bcengine.parser.formula.token

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaEmpty
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.NamedFormulaParser
import ru.pocketbyte.bcengine.parser.formula.named.FormulaGroupParser
import ru.pocketbyte.bcengine.parser.formula.named.FormulaNamedPropertyParser
import ru.pocketbyte.bcengine.parser.formula.named.conditional.FormulaIfParser
import ru.pocketbyte.bcengine.parser.formula.named.FormulaNotParser
import ru.pocketbyte.bcengine.parser.formula.named.cast.FormulaToBooleanParser
import ru.pocketbyte.bcengine.parser.formula.named.cast.FormulaToNumberParser
import ru.pocketbyte.bcengine.parser.formula.named.cast.FormulaToStringParser
import ru.pocketbyte.bcengine.parser.formula.named.conditional.FormulaIfEmptyParser
import ru.pocketbyte.bcengine.parser.formula.named.conditional.FormulaIfNotEmptyParser
import ru.pocketbyte.bcengine.parser.formula.named.dict.FormulaDictParser
import ru.pocketbyte.bcengine.parser.formula.named.dict.FormulaDictGetParser
import ru.pocketbyte.bcengine.parser.formula.named.list.FormulaListParser
import ru.pocketbyte.bcengine.parser.formula.named.list.FormulaSplitStringParser
import ru.pocketbyte.bcengine.parser.formula.named.list.FormulaSplitStringRegexParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.FormulaGoogleSheetCsvParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.FormulaJoinNotEmptyStringParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.FormulaJoinStringParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.character.FormulaCharParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.character.FormulaCharNewLine
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlBParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlFontParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlIParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlImgParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlSParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlSubParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlSupParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlTagParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html.FormulaHtmlUParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace.FormulaReplaceDictParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace.FormulaReplaceRegexStringParser
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.replace.FormulaReplaceStringParser
import ru.pocketbyte.bcengine.parser.formula.named.validation.FormulaIsEmptyParser
import ru.pocketbyte.bcengine.parser.formula.named.validation.FormulaIsNotEmptyParser
import ru.pocketbyte.bcengine.parser.formula.token.FormulaTokenParser.Companion.PROPERTY_CHAR
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import java.lang.RuntimeException

object FormulaTokenFunctionParser : FormulaTokenParser<NamedGroupToken, Any> {

    private val builders = mutableMapOf<String, NamedFormulaParser>().apply {
        put(FormulaGroupParser)
        put(FormulaIfParser)
        put(FormulaIfNotEmptyParser)
        put(FormulaIfEmptyParser)
        put(FormulaNotParser)

        // Validation
        put(FormulaIsEmptyParser)
        put(FormulaIsNotEmptyParser)

        // Cast
        put(FormulaToStringParser)
        put(FormulaToNumberParser)
        put(FormulaToBooleanParser)

        // String builders
        put(FormulaGoogleSheetCsvParser)
        put(FormulaJoinStringParser)
        put(FormulaJoinNotEmptyStringParser)
        put(FormulaReplaceRegexStringParser)
        put(FormulaReplaceStringParser)
        put(FormulaReplaceDictParser)
        put(FormulaCharParser)
        put(FormulaCharNewLine)

        // Html
        put(FormulaHtmlBParser)
        put(FormulaHtmlFontParser)
        put(FormulaHtmlIParser)
        put(FormulaHtmlImgParser)
        put(FormulaHtmlSParser)
        put(FormulaHtmlSubParser)
        put(FormulaHtmlSupParser)
        put(FormulaHtmlTagParser)
        put(FormulaHtmlUParser)

        // List builders
        put(FormulaListParser(ListValueType.String))
        put(FormulaListParser(ListValueType.Number))
        put(FormulaListParser(ListValueType.Boolean))
        put(FormulaSplitStringParser)
        put(FormulaSplitStringRegexParser)

        // Dict builders
        put(FormulaDictParser(DictValueType.String))
        put(FormulaDictParser(DictValueType.Number))
        put(FormulaDictParser(DictValueType.Boolean))
        put(FormulaDictGetParser)
    } as Map<String, NamedFormulaParser>

    override fun decode(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        if (token.name.length >= 2 && token.name.first() == PROPERTY_CHAR) {
            return FormulaNamedPropertyParser
                .parse(context, project, token, formulaType)
        }

        return builders[token.name.lowercase()]?.let {
            it.parse(context, project, token, formulaType).apply {
                token.addMetaData(Token.METADATA_PROCESSOR, it)
            }
        } ?: FormulaEmpty(formulaType).also {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token, "Unknown formula"
                )
            )
        }
    }

    private fun MutableMap<String, NamedFormulaParser>.put(builder: NamedFormulaParser) {
        val name = builder.name.lowercase()
        if (contains(name)) {
            throw RuntimeException("Duplicate formula builder: $name")
        }
        set(name, builder)
    }
}
