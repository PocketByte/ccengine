package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlI
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlIParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlI.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlI(token, content)
    }
}
