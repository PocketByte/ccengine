package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlS
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlSParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlS.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlS(token, content)
    }
}
