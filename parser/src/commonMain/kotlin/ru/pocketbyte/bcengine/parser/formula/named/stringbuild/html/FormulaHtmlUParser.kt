package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlU
import ru.pocketbyte.bcengine.token.Token

object FormulaHtmlUParser: AbsFormulaHtmlTagParser() {

    override val name: String = FormulaHtmlU.NAME

    override fun buildFormula(token: Token?, content: Formula<String>): Formula<Any> {
        return FormulaHtmlU(token, content)
    }
}
