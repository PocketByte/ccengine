package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.ImageShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object ImageShapeParser: AbsFrameParser<ImageShape>() {

    private val PARAM_Image = Parameter(
        name = "Image",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_ScaleMode = Parameter(
        name = "ScaleMode",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AligningX = Parameter(
        name = "AligningX",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AligningY = Parameter(
        name = "AligningY",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = "IMAGE"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height,
        PARAM_Image, PARAM_ScaleMode,
        PARAM_AligningX, PARAM_AligningY, PARAM_Angle
    )

    override fun createInstance(
        context: Context, project: Project, token: NamedGroupToken
    ): ImageShape = ImageShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: ImageShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_Image, token)
                ?.let { image.formula = it }

            getFormula(context, project, PARAM_ScaleMode, token)
                ?.let { scaleMode.formula = it }

            getFormula(context, project, PARAM_AligningX, token)
                ?.let { aligningX.formula = it }

            getFormula(context, project, PARAM_AligningY, token)
                ?.let { aligningY.formula = it }
        }
    }
}