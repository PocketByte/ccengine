package ru.pocketbyte.bcengine.parser.project.group

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

object ProjectNoneHandler: ProjectGroupHandler {
    override fun handle(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        context.errors.addError(
            ProjectError.ParseTokenError(
                tokens[index], "No group defined."
            )
        )
        throw IllegalStateException("No group defined.")
    }
}