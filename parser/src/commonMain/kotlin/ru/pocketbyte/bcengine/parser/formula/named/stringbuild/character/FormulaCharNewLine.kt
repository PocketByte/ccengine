package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.character

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.character.FormulaCharNewLine
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaCharNewLine: AbsNamedFormulaParser() {

    override val name: String = FormulaCharNewLine.NAME

    override val parameters: Array<Parameter<*>> = emptyArray()

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        assertParametersCount(context, token)
        return FormulaCharNewLine(token)
    }
}