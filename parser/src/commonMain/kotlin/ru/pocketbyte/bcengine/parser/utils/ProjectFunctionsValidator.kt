package ru.pocketbyte.bcengine.parser.utils

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaNamedProperty

object ProjectFunctionsValidator {

    fun validate(context: Context, project: Project) {
        project.functions.values.forEach {
            checkCyclicDependencies(context, project, it)
        }
    }

    private fun checkCyclicDependencies(
        context: Context,
        project: Project, formula: Formula<*>,
        chain: Array<String> = emptyArray()
    ) {
        if (formula is FormulaNamedProperty) {
            if (chain.contains(formula.name)) {
                if (chain[0] == formula.name) {
                    onCyclicDependency(context, project, formula, chain + formula.name)
                } else {
                    // Here is the cyclic dependency,
                    // but initial formula is not the function that starts the cycle
                }
                return
            }
            val nextFormula = project.functions[formula.name]
            if (nextFormula != null) {
                checkCyclicDependencies(context, project, nextFormula, chain + formula.name)
            }
            return
        }
        formula.parameters.forEach {
            checkCyclicDependencies(context, project, it, chain)
        }
    }

    private fun onCyclicDependency(
        context: Context, project: Project, formula: Formula<*>, chain: Array<String>
    ) {
        formula.token?.let {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    it, "Cyclic formula dependency ${chain.joinToString("->")}"
                )
            )
        }
    }
}