package ru.pocketbyte.bcengine.parser.dataset

import ru.pocketbyte.bcengine.dataset.DataSetFactory
import ru.pocketbyte.bcengine.parser.ObjectParser

interface DataSetParser<Type: DataSetFactory> : ObjectParser<Type>