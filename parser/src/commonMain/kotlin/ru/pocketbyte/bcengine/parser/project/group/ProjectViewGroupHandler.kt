package ru.pocketbyte.bcengine.parser.project.group

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.parser.ParseResult
import ru.pocketbyte.bcengine.parser.shape.ColumnShapeParser
import ru.pocketbyte.bcengine.parser.shape.LayoutShapeParser
import ru.pocketbyte.bcengine.parser.shape.IconsShapeParser
import ru.pocketbyte.bcengine.parser.shape.IfLayoutShapeParser
import ru.pocketbyte.bcengine.parser.shape.ImageShapeParser
import ru.pocketbyte.bcengine.parser.shape.LineShapeParser
import ru.pocketbyte.bcengine.parser.shape.RectangleShapeParser
import ru.pocketbyte.bcengine.parser.shape.RowShapeParser
import ru.pocketbyte.bcengine.parser.shape.ShapeParser
import ru.pocketbyte.bcengine.parser.shape.TextShapeParser
import ru.pocketbyte.bcengine.shape.Shape
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.*

object ProjectViewGroupHandler: ProjectGroupHandler {

    private val builders = mapOf(
        RectangleShapeParser.asMapPair(),
        LineShapeParser.asMapPair(),
        ImageShapeParser.asMapPair(),
        TextShapeParser.asMapPair(),
        IconsShapeParser.asMapPair(),
        LayoutShapeParser.asMapPair(),
        IfLayoutShapeParser.asMapPair(),
        ColumnShapeParser.asMapPair(),
        RowShapeParser.asMapPair()
    )

    override fun handle(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        val result = buildShape(context, project, tokens, index) ?: return 1

        result.instance?.let { project.add(it) }

        return result.tokensConsumed
    }

    fun buildShape(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): ParseResult<out Shape>? {
        when (val token = tokens[index]) {
            is NamedGroupToken -> {
                val builder = builders[token.name.lowercase()]

                if (builder == null) {
                    context.errors.addError(
                        ProjectError.ParseTokenError(
                            token, "Unknown shape"
                        )
                    )
                    return null
                }

                token.addMetaData(Token.METADATA_PROCESSOR, builder)

                return try {
                    builder.parse(context, project, tokens, index)
                } catch (e: Exception) {
                    context.errors.addError(
                        ProjectError.ParseTokenError(
                            token, e.message ?: "Invalid shape"
                        )
                    )
                    null
                }
            }
            is CommentToken,
            is EmptyStringToken -> {
                // Do nothing
            }
            is OperatorToken,
            is SeparatorToken,
            is NamedPropertyToken,
            is StringToken,
            is WordToken -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        token, "Unknown command"
                    )
                )
            }
        }
        return null
    }

    private fun ShapeParser<*>.asMapPair(): Pair<String, ShapeParser<*>> {
        return Pair(name.lowercase(), this)
    }
}