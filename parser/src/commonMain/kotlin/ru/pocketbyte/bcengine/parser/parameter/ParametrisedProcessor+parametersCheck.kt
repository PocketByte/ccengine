package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.EmptyStringToken
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

inline fun ParametrisedProcessor.assertParametersCount(
    context: Context,
    token: NamedGroupToken,
    exact: Int = parameters.size,
    fallbackAction: () -> Unit = {}
) {
    val count = getParametersCount(token)
    if (exact != count) {
        context.errors.addError(
            ProjectError.ParseTokenError(
                token, "Invalid parameters count. Expected $exact, but found $count."
            )
        )
        fallbackAction()
    }
}

fun ParametrisedProcessor.assertMaxParametersCount(
    context: Context,
    token: NamedGroupToken,
    max: Int = parameters.size,
    fallbackAction: () -> Unit = {}
) {
    val count = getParametersCount(token)
    if (count > max) {
        context.errors.addError(
            ProjectError.ParseTokenError(
                token, "Invalid parameters count. Maximum $max, but found $count."
            )
        )
        fallbackAction()
    }
}

fun ParametrisedProcessor.getParametersCount(
    token: NamedGroupToken
): Int {
    return getParametersCount(token.subTokens)
}

fun ParametrisedProcessor.getParametersCount(
    tokens: List<ProjectToken>
): Int {
    if (tokens.isEmpty() || (tokens.size == 1 && tokens[0] is EmptyStringToken)) {
        return 0
    }

    return tokens.count { it is SeparatorToken && it.type == parameterSeparator } + 1
}