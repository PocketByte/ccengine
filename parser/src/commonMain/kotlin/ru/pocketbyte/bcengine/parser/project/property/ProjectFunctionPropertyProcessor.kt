package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.*
import ru.pocketbyte.bcengine.formula.valuetype.*
import ru.pocketbyte.bcengine.parser.utils.ProjectFunctionsValidator
import ru.pocketbyte.bcengine.parser.formula.ValueTypeCoder
import ru.pocketbyte.bcengine.parser.parameter.*
import ru.pocketbyte.bcengine.parser.project.*
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.StringToken

class ProjectFunctionPropertyProcessor: AbsProjectPropertyProcessor() {

    override val name = "FUN"
    override val isExclusive = false

    private val PARAM_Name = Parameter(
        name = "Name",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Type = Parameter(
        name = "Type",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_VALUE
    )

    private val PARAM_Function = Parameter(
        name = "Function",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Name, PARAM_Type, PARAM_Function
    )

    override fun process(context: Context, project: Project, tokens: List<ProjectToken>) {
        super.process(context, project, tokens)
        ProjectFunctionsValidator.validate(context, project)
    }

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return
        val name = getStringValue(context, PARAM_Name, token, strict = true)?.trim()
            ?: return

        if (project.findFormula(name) != null) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    token,
                    "Double declaration! " +
                        "Function or parameter with name'$name' already declared."
                )
            )
            return
        }

        val typeName = getStringValue(context, PARAM_Type, token, strict = true)
            ?.lowercase()?.trim()
        val type: ValueType<Any> = ValueTypeCoder.decode(typeName).let {
            if (it == null) {
                if (typeName != null) {
                    context.errors.addError(
                        ProjectError.ParseTokenError(
                            token,
                            "Unknown function type: $typeName"
                        )
                    )
                }
                return
            } else {
                it
            }
        }

        val functionRange = getParameterRange(context, PARAM_Function, token, strict = true)

        if (functionRange == null) {
            project.functions[name] = FormulaStaticValue(null, type.fromString(""), type)
            return
        }

        project.functions[name] = LazyFormula(type, emptyList()) {
            getFormula(context, project, functionRange, token, type)?.let {
                if (it !is FormulaEmpty) {
                    return@LazyFormula it
                }
            }
            getStringValue(functionRange, token).let { value ->
                FormulaStaticValue(
                    StringToken(
                        value, token.parentString,
                        token.subTokens[functionRange.start].startIndex,
                        token.subTokens[functionRange.end].endIndex,
                    ),
                    type.fromString(value),
                    type
                )
            }
        }.asFormulaRoot()
    }
}