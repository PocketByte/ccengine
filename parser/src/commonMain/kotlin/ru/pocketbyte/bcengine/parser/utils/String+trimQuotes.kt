package ru.pocketbyte.bcengine.parser.utils

fun String.trimQuotes(): String {
    if (length >= 2 && first() == '"' && last() == '"') {
        return substring(1, length - 1)
    }
    return this
}