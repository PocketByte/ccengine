package ru.pocketbyte.bcengine.parser.formula.token

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

interface FormulaTokenParser<TokenType : ProjectToken, out Type : Any> {
    fun decode(
        context: Context,
        project: Project,
        token: TokenType,
        formulaType: ValueType<Any>
    ): Formula<Type>

    companion object {
        internal const val PROPERTY_CHAR = '$'
    }
}