package ru.pocketbyte.bcengine.parser.project.property

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.project.AbsProjectPropertyProcessor
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import kotlin.math.roundToInt

class ProjectDataSetMultiplyPropertyProcessor: AbsProjectPropertyProcessor() {
    override val name = "DATASET_MULT"
    override val isExclusive: Boolean = true

    private val PARAM_Value = Parameter(
        name = "Value",
        type = NumberValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA
    )


    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Value
    )

    override fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ) {
        val token = tokens.getOrNull(index) as? NamedGroupToken ?: return

        project.dataSetMultiply = getFormula(context, project, PARAM_Value, token, strict = true)
            ?.compute(project, null)
            ?.roundToInt() ?: 1
    }
}