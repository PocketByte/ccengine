package ru.pocketbyte.bcengine.parser.formula.named

import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

abstract class AbsNamedFormulaParser : NamedFormulaParser {

    override val descriptionKey get() = "formula_${name.lowercase()}"
    override val parameterSeparator = SeparatorToken.Type.SEMICOLON

}