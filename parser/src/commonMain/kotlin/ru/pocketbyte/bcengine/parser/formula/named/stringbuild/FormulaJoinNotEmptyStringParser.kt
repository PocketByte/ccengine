package ru.pocketbyte.bcengine.parser.formula.named.stringbuild

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.join.FormulaJoinNotEmptyString
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.join.AbsFormulaJoinStringParser
import ru.pocketbyte.bcengine.token.Token

object FormulaJoinNotEmptyStringParser: AbsFormulaJoinStringParser() {

    override val name: String = FormulaJoinNotEmptyString.NAME

    override fun buildFormula(
        token: Token?,
        separator: Formula<String>,
        values: List<Formula<*>>
    ): Formula<Any> {
        return FormulaJoinNotEmptyString(token, separator, values)
    }
}