package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken
import kotlin.math.min

fun getParameterRange(
    parameter: Parameter<*>,
    parameters: Array<Parameter<*>>,
    parameterSeparator: SeparatorToken.Type,
    tokens: List<ProjectToken>
): ParameterRange? {
    val index = parameters.indexOf(parameter)
    if (index < 0) {
        return null
    }
    return getParameterRange(index, parameterSeparator, tokens)
}

fun getParameterRange(
    index: Int,
    parameterSeparator: SeparatorToken.Type,
    tokens: List<ProjectToken>
): ParameterRange? {

    var parameterIndex = 0
    var startParameterIndex = 0

    // Looking for parameter start
    while (parameterIndex < index && startParameterIndex < tokens.size) {
        val subToken = tokens.getOrNull(startParameterIndex)
        if (subToken is SeparatorToken && subToken.type == parameterSeparator) {
            parameterIndex += 1
        }
        startParameterIndex += 1
    }

    if (parameterIndex != index) {
        return null
    }

    var endParameterIndex = startParameterIndex
    // Looking for parameter end
    while (parameterIndex == index && endParameterIndex < tokens.size) {
        val subToken = tokens.getOrNull(endParameterIndex)
        if (subToken is SeparatorToken && subToken.type == parameterSeparator) {
            parameterIndex += 1
            endParameterIndex -= 1
        } else {
            endParameterIndex += 1
        }
    }

    return ParameterRange(startParameterIndex, min(tokens.size - 1, endParameterIndex))
}