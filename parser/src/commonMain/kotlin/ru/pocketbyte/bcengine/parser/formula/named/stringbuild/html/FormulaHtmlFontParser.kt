package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlFont
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertMaxParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaHtmlFontParser: AbsNamedFormulaParser() {

    val PARAM_Content = Parameter(
        name = "Content",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Face = Parameter(
        name = "Face",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Size = Parameter(
        name = "Size",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Color = Parameter(
        name = "Color",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaHtmlFont.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Content, PARAM_Face, PARAM_Size, PARAM_Color
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val content = getFormula(context, project, PARAM_Content, token, strict = true)
        val face = getFormula(context, project, PARAM_Face, token)
        val size = getFormula(context, project, PARAM_Size, token)
        val color = getFormula(context, project, PARAM_Color, token)

        return if (content != null) {
            assertMaxParametersCount(context, token)
            FormulaHtmlFont(token, content, face, size, color)
        } else {
            FormulaStaticValue(token, token.getTokenString(), StringValueType)
        }
    }
}