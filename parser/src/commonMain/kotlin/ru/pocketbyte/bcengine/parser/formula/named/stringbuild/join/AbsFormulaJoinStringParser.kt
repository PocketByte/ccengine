package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.join

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.*
import ru.pocketbyte.bcengine.token.Token
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

abstract class AbsFormulaJoinStringParser: AbsNamedFormulaParser() {

    val PARAM_Separator = Parameter(
        name = "Separator",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Values = Parameter(
        name = "Values",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA,
        varArg = true
    )

    abstract override val name: String
    abstract fun buildFormula(
        token: Token?,
        separator: Formula<String>,
        values: List<Formula<*>>
    ): Formula<Any>

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Separator, PARAM_Values
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val separator = getFormula(context, project, PARAM_Separator, token, strict = true)
            ?: return FormulaStaticValue(token, "", StringValueType)

        val valuesList = mutableListOf<Formula<*>>()

        forEachVarargParameterRange(PARAM_Values, token) { _, range ->
            valuesList.add(
                getFormula(context, project, range, token, PARAM_Values.type)
                    ?: FormulaStaticValue(
                        getParameterToken(range, token),
                        "", StringValueType
                    )
            )
        }

        return buildFormula(token, separator, valuesList)
    }
}