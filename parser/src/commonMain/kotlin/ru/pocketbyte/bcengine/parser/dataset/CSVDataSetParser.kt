package ru.pocketbyte.bcengine.parser.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.CSVDataSetFactory
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object CSVDataSetParser : AbsDataSetParser<CSVDataSetFactory>() {

    val PARAM_Path = Parameter(
        name = "Path",
        type = StringValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA
    )

    override val name: String = "CSV"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Path
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): CSVDataSetFactory {
        return CSVDataSetFactory()
    }

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: CSVDataSetFactory
    ) {
        getFormula(context, project, PARAM_Path, token, strict = true)?.let {
            instance.path = it
        }
    }

}