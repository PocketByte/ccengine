package ru.pocketbyte.bcengine.parser.formula

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaEmpty
import ru.pocketbyte.bcengine.formula.compare.ComparisonOperator
import ru.pocketbyte.bcengine.formula.compare.FormulaBooleanCompare
import ru.pocketbyte.bcengine.formula.compare.FormulaNumberCompare
import ru.pocketbyte.bcengine.formula.compare.FormulaStringCompare
import ru.pocketbyte.bcengine.formula.operator.FormulaBooleanOperator
import ru.pocketbyte.bcengine.formula.operator.FormulaNumberOperator
import ru.pocketbyte.bcengine.formula.operator.NumberOperator
import ru.pocketbyte.bcengine.formula.stringbuild.FormulaStringConcat
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.token.FormulaTokenFunctionParser
import ru.pocketbyte.bcengine.parser.formula.token.FormulaTokenStringParser
import ru.pocketbyte.bcengine.parser.formula.token.FormulaTokenWordParser
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.OperatorToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.StringToken
import ru.pocketbyte.bcengine.tokenizer.WordToken

object FormulaParser {

    fun <T : Any> parse(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        type: ValueType<T>
    ) : Formula<T> {
        return parse(context, project, tokens, type, 0, tokens.size - 1)
    }

    fun <T : Any> parse(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        type: ValueType<T>,
        startIndex: Int,
        endIndex: Int
    ) : Formula<T> {
        var index = startIndex

        // If first token is operator, Merge it with next WordToken
        val parameter1Token = (tokens[index] as? OperatorToken)?.let {
            index += 1
            val nextToken = if (index <= endIndex) {
                tokens.getOrNull(index) as? WordToken
            } else {
                null
            } ?: return it.onWrongPlace(context, type)

            WordToken(
                it.parentString.substring(it.startIndex, nextToken.endIndex),
                it.parentString,
                it.startIndex, nextToken.endIndex
            )
        } ?: tokens[index]

        var parameter1: Formula<T> = parameter1Token.toFormula(context, project, type)

        index += 1
        while (index < tokens.size && index <= endIndex) {
            val operator: OperatorToken? = tokens[index] as? OperatorToken

            if (operator == null) {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        tokens[index],
                        "Missing operator"
                    )
                )
                return FormulaEmpty(type)
            }

            index += 1
            if (index >= tokens.size || index > endIndex) {
                return operator.onWrongPlace(context, type)
            }

            val nexOperator: OperatorToken? = if (index + 1 <= endIndex) {
                tokens.getOrNull(index + 1) as? OperatorToken
            } else {
                null
            }

            if (nexOperator != null && nexOperator.type.priority > operator.type.priority) {
                return operator.toFormula(
                    context, type, parameter1,
                    parse(context, project, tokens, type, index, endIndex)
                )
            } else {
                parameter1 = operator.toFormula(
                    context, type, parameter1,
                    tokens[index].toFormula(context, project, type)
                )
            }
            index += 1
        }

        if (type != parameter1.type && type !is UndefinedValueType) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    tokens[index],
                    "Expected value type ${type.name}, but found ${parameter1.type.name}"
                )
            )
            return FormulaEmpty(type)
        }

        return parameter1
    }

    private fun <T : Any> ProjectToken.toFormula(
        context: Context,
        project: Project,
        formulaType: ValueType<T>
    ): Formula<T> {
        return when {
            this is OperatorToken -> {
                onWrongPlace(context, formulaType)
            }
            this is StringToken -> {
                FormulaTokenStringParser.decode(context, project, this, formulaType)
            }
            this is WordToken -> {
                FormulaTokenWordParser.decode(context, project, this, formulaType)
            }
            this is NamedGroupToken && this.isFunctionToken() -> {
                FormulaTokenFunctionParser.decode(context, project , this, formulaType)
            }
            else -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        this, "Unknown formula token."
                    )
                )
                FormulaEmpty(formulaType)
            }
        } as Formula<T>
    }

    private fun NamedGroupToken.isFunctionToken(): Boolean {
        return this.bracers == NamedGroupToken.Bracers.ROUND
    }

    private fun <T : Any> OperatorToken.toFormula(
        context: Context,
        type: ValueType<T>,
        parameter1: Formula<*>,
        parameter2: Formula<*>
    ): Formula<T> {
        if (parameter1.type != parameter2.type) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    this,
                    "Operator ${this.type.string} can't be applied to different types: " +
                            "${parameter1.type.name} and ${parameter2.type.name}."
                )
            )
            return FormulaEmpty(type)
        }
        return when(this.type) {
            OperatorToken.Type.PLUS,
            OperatorToken.Type.MINUS,
            OperatorToken.Type.MULTIPLY,
            OperatorToken.Type.DIVIDE -> {
                when (parameter1.type) {
                    StringValueType -> {
                        if (this.type == OperatorToken.Type.PLUS) {
                            FormulaStringConcat(this, parameter1, parameter2)
                        } else {
                            onCantBeApplied(context, parameter1.type)
                        }
                    }
                    NumberValueType -> FormulaNumberOperator(
                        this, parameter1, this.toNumberOperator(context), parameter2
                    )
                    else -> onCantBeApplied(context,parameter1.type)
                }
            }
            OperatorToken.Type.AND,
            OperatorToken.Type.OR -> {
                when (parameter1.type) {
                    BooleanValueType -> FormulaBooleanOperator(
                        this, parameter1, this.toBooleanOperatorType(context), parameter2
                    )
                    else -> onCantBeApplied(context, parameter1.type)
                }
            }
            OperatorToken.Type.EQUAL,
            OperatorToken.Type.GREATER_OR_EQUAL,
            OperatorToken.Type.GREATER,
            OperatorToken.Type.LESS_OR_EQUAL,
            OperatorToken.Type.LESS -> {
                when (parameter1.type) {
                    StringValueType -> FormulaStringCompare(
                        this, parameter1, this.toComparisonOperator(context), parameter2
                    )
                    NumberValueType -> FormulaNumberCompare(
                        this, parameter1, this.toComparisonOperator(context), parameter2
                    )
                    BooleanValueType -> FormulaBooleanCompare(
                        this, parameter1, this.toComparisonOperator(context), parameter2
                    )
                    else -> onCantBeApplied(context, parameter1.type)
                }
            }
        } as Formula<T>
    }

    private fun OperatorToken.toNumberOperator(context: Context): NumberOperator {
        return when (type) {
            OperatorToken.Type.PLUS -> NumberOperator.PLUS
            OperatorToken.Type.MINUS -> NumberOperator.MINUS
            OperatorToken.Type.MULTIPLY -> NumberOperator.MULTIPLY
            OperatorToken.Type.DIVIDE -> NumberOperator.DIVIDE
            else -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        this, "Number operator expected"
                    )
                )
                NumberOperator.PLUS
            }
        }
    }

    private fun OperatorToken.toBooleanOperatorType(context: Context): FormulaBooleanOperator.Type {
        return when (type) {
            OperatorToken.Type.AND -> FormulaBooleanOperator.Type.AND
            OperatorToken.Type.OR -> FormulaBooleanOperator.Type.OR
            else -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        this, "Boolean operator expected"
                    )
                )
                FormulaBooleanOperator.Type.AND
            }
        }
    }

    private fun OperatorToken.toComparisonOperator(context: Context): ComparisonOperator {
        return when (type) {
            OperatorToken.Type.EQUAL -> ComparisonOperator.EQUAL
            OperatorToken.Type.GREATER_OR_EQUAL -> ComparisonOperator.GREATER_OR_EQUAL
            OperatorToken.Type.GREATER -> ComparisonOperator.GREATER
            OperatorToken.Type.LESS_OR_EQUAL -> ComparisonOperator.LESS_OR_EQUAL
            OperatorToken.Type.LESS -> ComparisonOperator.LESS
            else -> {
                context.errors.addError(
                    ProjectError.ParseTokenError(
                        this, "Comparison operator expected"
                    )
                )
                ComparisonOperator.EQUAL
            }
        }
    }

    private fun <T : Any> OperatorToken.onCantBeApplied(
        context: Context,
        formulaType: ValueType<T>
    ): Formula<T> {
        context.errors.addError(
            ProjectError.ParseTokenError(
                this,
                "Operator ${this.type.string} can't be applied to ${formulaType.name}."
            )
        )
        return FormulaEmpty(formulaType)
    }

    private fun <T : Any> OperatorToken.onWrongPlace(
        context: Context,
        formulaType: ValueType<T>
    ): Formula<T> {
        context.errors.addError(
            ProjectError.ParseTokenError(
                this,
                "Operator '${this.type.string}' can be placed only between 2 values or functions"
            )
        )
        return FormulaEmpty(formulaType)
    }
}
