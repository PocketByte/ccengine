package ru.pocketbyte.bcengine.parser.formula.named

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaNamedFunctionCall
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.LazyFormula
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.forEachVarargParameterRange
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.parser.parameter.getParameterToken
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

object FormulaNamedPropertyParser: NamedFormulaParser {

    val PARAM_Arguments = Parameter(
        name = "Arguments",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA,
        varArg = true
    )

    override val name: String = "namedProperty"
    override val descriptionKey: String = "namedProperty"

    override val parameters: Array<Parameter<*>> = arrayOf(PARAM_Arguments)
    override val parameterSeparator = SeparatorToken.Type.SEMICOLON

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val arguments = mutableListOf<Formula<*>>()
        val name = token.name.substring(1)
        val formula = project.findFormula(name)
            as? LazyFormula<*>
            ?: return formulaStaticValue(token, token.getTokenString())

        forEachVarargParameterRange(PARAM_Arguments, token) { index, range ->
            formula.arguments.getOrNull(index)?.let { argument ->
                getFormula(context, project, range, token, argument)
            }.let {
                arguments.add(
                    it ?: FormulaStaticValue(
                        getParameterToken(range, token),
                        PARAM_Arguments.type.from(StringValueType, ""),
                        PARAM_Arguments.type
                        )
                    )
            }
        }

        return FormulaNamedFunctionCall(token, name, arguments, formulaType)
    }

}