package ru.pocketbyte.bcengine.parser.project

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

abstract class AbsProjectPropertyProcessor: ProjectPropertyProcessor {

    private val indexes = mutableListOf<Int>()

    override val descriptionKey get() = "project_${name.lowercase()}"

    override val parameterSeparator = SeparatorToken.Type.COMMA

    override fun add(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int {
        this.indexes.add(index)
        return 1
    }

    override fun process(context: Context, project: Project, tokens: List<ProjectToken>) {
        if (indexes.size > 1 && isExclusive) {
            context.errors.addError(
                ProjectError.ParseTokenError(
                    tokens[indexes[1]], "Double declaration"
                )
            )
        }
        indexes.forEach { processItem(context, project, tokens, it) }
    }

    protected abstract fun processItem(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    )
}
