package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.AbsObjectParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.Shape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.SeparatorToken

abstract class AbsShapeParser<ShapeType : Shape>
    : AbsObjectParser<ShapeType>(), ShapeParser<ShapeType> {

    val PARAM_X = Parameter(
        name = "X",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Y = Parameter(
        name = "Y",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Width = Parameter(
        name = "Width",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Height = Parameter(
        name = "Height",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_Angle = Parameter(
        name = "Angle",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val descriptionKey get() = "view_${name.lowercase()}"

    override val parameterSeparator = SeparatorToken.Type.COMMA

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: ShapeType
    ) {
        instance.apply {
            getFormula(context, project, PARAM_X, token)
                ?.let { x.formula = it }

            getFormula(context, project, PARAM_Y, token)
                ?.let { y.formula = it }

            getFormula(context, project, PARAM_Width, token)
                ?.let { width.formula = it }

            getFormula(context, project, PARAM_Height, token)
                ?.let { height.formula = it }

            getFormula(context, project, PARAM_Angle, token)
                ?.let { angle.formula = it }
        }
    }
}
