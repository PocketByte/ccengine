package ru.pocketbyte.bcengine.parser.dataset

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.dataset.EmptyDataSetFactory
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object EmptyDataSetParser: AbsDataSetParser<EmptyDataSetFactory>() {

    val PARAM_Size = Parameter(
        name = "Size",
        type = NumberValueType,
        formulaType = ParameterFormulaType.STATIC_FORMULA
    )

    override val name: String = "EMPTY"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Size
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): EmptyDataSetFactory {
        return EmptyDataSetFactory()
    }

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: EmptyDataSetFactory
    ) {
        getFormula(context, project, PARAM_Size, token, strict = true)?.let {
            instance.size = it
        }
    }

}