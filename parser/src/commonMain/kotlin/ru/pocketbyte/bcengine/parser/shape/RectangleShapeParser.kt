package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.RectangleShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object RectangleShapeParser: AbsFrameParser<RectangleShape>() {

    val PARAM_Background = Parameter(
        name = "Background",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_StrokeSize = Parameter(
        name = "StrokeSize",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_StrokeColor = Parameter(
        name = "StrokeColor",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_CornerRadiusX = Parameter(
        name = "CornerRadiusX",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    val PARAM_CornerRadiusY = Parameter(
        name = "CornerRadiusY",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = "RECT"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height,
        PARAM_Background, PARAM_StrokeSize, PARAM_StrokeColor,
        PARAM_CornerRadiusX, PARAM_CornerRadiusY, PARAM_Angle
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): RectangleShape = RectangleShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: RectangleShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_Background, token)
                ?.let { background.formula = it }

            getFormula(context, project, PARAM_StrokeSize, token)
                ?.let { stroke.size.formula = it }

            getFormula(context, project, PARAM_StrokeColor, token)
                ?.let { stroke.color.formula = it }

            getFormula(context, project, PARAM_CornerRadiusX, token)
                ?.let { cornerRadiusX.formula = it }

            getFormula(context, project, PARAM_CornerRadiusY, token)
                ?.let { cornerRadiusY.formula = it }
        }
    }
}