package ru.pocketbyte.bcengine.parser.formula.named.conditional

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.conditional.FormulaIf
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.fromString
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaIfParser: AbsNamedFormulaParser() {

    val PARAM_Condition = Parameter(
        name = "Condition",
        type = BooleanValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Then = Parameter(
        name = "Then",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    val PARAM_Else = Parameter(
        name = "Else",
        type = UndefinedValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaIf.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Condition, PARAM_Then, PARAM_Else
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val condition = getFormula(context, project, PARAM_Condition, token, strict = true)
        val thenValue = getFormula(context, project, PARAM_Then, token, formulaType, strict = condition != null)
        val elseValue = getFormula(context, project, PARAM_Else, token, formulaType, strict = thenValue != null)

        return if (condition != null && thenValue != null && elseValue != null) {
            assertParametersCount(context, token)
            FormulaIf(token, condition, thenValue, elseValue)
        } else {
            FormulaStaticValue(token, formulaType.fromString(""), formulaType)
        }
    }
}