package ru.pocketbyte.bcengine.parser.formula.token

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.tokenizer.StringToken

object FormulaTokenStringParser : FormulaTokenParser<StringToken, String> {
    override fun decode(
        context: Context,
        project: Project,
        token: StringToken,
        formulaType: ValueType<Any>
    ): Formula<String> {
        return FormulaStaticValue(token, token.value, StringValueType)
    }
}