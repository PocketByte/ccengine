package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.IfLayoutShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object IfLayoutShapeParser: AbsFrameParser<IfLayoutShape>() {

    override val name: String = "IF"

    val PARAM_Condition = Parameter(
        name = "Condition",
        type = BooleanValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Condition, PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height, PARAM_Angle
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): IfLayoutShape = IfLayoutShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: IfLayoutShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_Condition, token)
                ?.let { condition.formula = it }
        }
    }
}
