package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.errors.ProjectError
import ru.pocketbyte.bcengine.token.Token

inline fun <T> ParametrisedProcessor.onParameterMissing(
    context: Context,
    parameter: Parameter<*>,
    token: Token,
    finishAction: () -> T
): T {
    onParameterMissing(context, parameter, token)
    return finishAction()
}

fun ParametrisedProcessor.onParameterMissing(
    context: Context,
    parameter: Parameter<*>,
    token: Token
) {
    context.errors.addError(
        ProjectError.ParseTokenError(
            token, "$name: Missing ${parameters.indexOf(parameter) + 1} parameter '${parameter.name}'"
        )
    )
}