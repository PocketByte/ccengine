package ru.pocketbyte.bcengine.parser.formula.named.stringbuild

import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.stringbuild.join.FormulaJoinString
import ru.pocketbyte.bcengine.parser.formula.named.stringbuild.join.AbsFormulaJoinStringParser
import ru.pocketbyte.bcengine.token.Token

object FormulaJoinStringParser: AbsFormulaJoinStringParser() {

    override val name: String = FormulaJoinString.NAME

    override fun buildFormula(
        token: Token?,
        separator: Formula<String>,
        values: List<Formula<*>>
    ): Formula<Any> {
        return FormulaJoinString(token, separator, values)
    }
}