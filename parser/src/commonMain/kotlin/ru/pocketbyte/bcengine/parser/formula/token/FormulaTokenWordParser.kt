package ru.pocketbyte.bcengine.parser.formula.token

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.*
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.formula.token.FormulaTokenParser.Companion.PROPERTY_CHAR
import ru.pocketbyte.bcengine.tokenizer.WordToken

object FormulaTokenWordParser : FormulaTokenParser<WordToken, Any> {
    override fun decode(
        context: Context,
        project: Project,
        token: WordToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        return when (formulaType) {
            is StringValueType -> {
                decodePropertyFormula(context, project, token)
                    ?: decodeStringFormula(context, project, token)
            }
            else -> {
                decodePropertyFormula(context, project, token)
                    ?: decodeBooleanFormula(context, project, token)
                    ?: decodeFloatFormula(context, project, token)
                    ?: decodeStringFormula(context, project, token)
            }
        }
    }

    private fun decodePropertyFormula(
        context: Context,
        project: Project,
        token: WordToken
    ): Formula<Any>? {
        if (token.value.length < 2) {
            return null
        }
        if (token.value.first() == PROPERTY_CHAR) {
            val property = token.value.substring(1)
            return FormulaNamedProperty(
                token, property,
                project.findFormula(property)?.type ?: StringValueType
            )
        }
        return null
    }

    private fun decodeBooleanFormula(
        context: Context,
        project: Project,
        token: WordToken
    ): Formula<Boolean>? {
        return when (token.value.lowercase()) {
            BOOL_TRUE -> true
            BOOL_FALSE -> false
            else -> null
        }?.let {
            FormulaStaticValue(
                token, it, BooleanValueType
            )
        }
    }

    private fun decodeFloatFormula(
        context: Context,
        project: Project,
        token: WordToken
    ): Formula<Float>? {
        return token.value.toFloatOrNull()?.let {
            FormulaStaticValue(
                token, it, NumberValueType
            )
        }
    }

    private fun decodeStringFormula(
        context: Context,
        project: Project,
        token: WordToken
    ): Formula<String> {
        return FormulaStaticValue(token, token.value, StringValueType)
    }

    private const val BOOL_TRUE = "true"
    private const val BOOL_FALSE = "false"
}