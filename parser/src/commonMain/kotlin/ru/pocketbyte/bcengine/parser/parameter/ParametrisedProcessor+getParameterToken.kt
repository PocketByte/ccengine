package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken
import ru.pocketbyte.bcengine.tokenizer.StringToken

fun ParametrisedProcessor.getParameterToken(
    context: Context,
    parameter: Parameter<*>,
    token: NamedGroupToken,
    strict: Boolean
): ProjectToken? {
    return getParameterToken(parameter, token).apply {
        if (this == null && strict) {
            onParameterMissing(context, parameter, token)
        }
    }
}

fun ParametrisedProcessor.getParameterToken(
    parameter: Parameter<*>,
    token: NamedGroupToken
): ProjectToken? {
    return getParameterToken(
        getParameterRange(parameter, token) ?: return null,
        token
    )
}

fun ParametrisedProcessor.getParameterToken(
    range: ParameterRange,
    token: NamedGroupToken
): ProjectToken? {
    return getParameterToken(range, token.subTokens)
}

fun ParametrisedProcessor.getParameterToken(
    range: ParameterRange,
    tokens: List<ProjectToken>
): ProjectToken? {
    if (range.length == 0) {
        return null
    }
    if (range.length == 1) {
        return tokens.getOrNull(range.start)
    }
    val startToken = tokens.getOrNull(range.start) ?: return null
    val startIndex = startToken.startIndex
    val endIndex = tokens.getOrNull(range.end)?.endIndex ?: return null
    return StringToken(
        startToken.parentString.substring(startIndex, endIndex + 1),
        startToken.parentString, startIndex, endIndex
    )
}