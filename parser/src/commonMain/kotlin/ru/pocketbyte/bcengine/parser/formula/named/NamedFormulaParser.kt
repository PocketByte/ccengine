package ru.pocketbyte.bcengine.parser.formula.named

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

interface NamedFormulaParser : ParametrisedProcessor {
    fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any>
}
