package ru.pocketbyte.bcengine.parser.formula

import ru.pocketbyte.bcengine.formula.valuetype.BooleanValueType
import ru.pocketbyte.bcengine.formula.valuetype.DictValueType
import ru.pocketbyte.bcengine.formula.valuetype.ListValueType
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType

object ValueTypeCoder {

    fun decode(string: String?): ValueType<Any>? {
        return when (string) {
            StringValueType.shortName -> StringValueType
            NumberValueType.shortName -> NumberValueType
            BooleanValueType.shortName -> BooleanValueType
            ListValueType.String.shortName -> ListValueType.String
            ListValueType.Number.shortName -> ListValueType.Number
            ListValueType.Boolean.shortName -> ListValueType.Boolean
            DictValueType.String.shortName -> DictValueType.String
            DictValueType.Number.shortName -> DictValueType.Number
            DictValueType.Boolean.shortName -> DictValueType.Boolean
            else -> null
        }
    }
}