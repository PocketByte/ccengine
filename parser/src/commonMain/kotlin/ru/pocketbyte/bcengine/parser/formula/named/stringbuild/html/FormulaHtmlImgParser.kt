package ru.pocketbyte.bcengine.parser.formula.named.stringbuild.html

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.html.FormulaHtmlImg
import ru.pocketbyte.bcengine.formula.valuetype.NumberValueType
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.parser.formula.named.AbsNamedFormulaParser
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.assertMaxParametersCount
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.token.getTokenString
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object FormulaHtmlImgParser: AbsNamedFormulaParser() {

    private val PARAM_Image = Parameter(
        name = "Image",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_Align = Parameter(
        name = "Align",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_Width = Parameter(
        name = "Width",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_Height = Parameter(
        name = "Height",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AdvancedWidth = Parameter(
        name = "AdvancedWidth",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_AdvancedHeight = Parameter(
        name = "AdvancedHeight",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_OriginX = Parameter(
        name = "OriginX",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )
    private val PARAM_OriginY = Parameter(
        name = "OriginY",
        type = NumberValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val name: String = FormulaHtmlImg.NAME

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_Image, PARAM_Align, PARAM_Width, PARAM_Height,
        PARAM_AdvancedWidth, PARAM_AdvancedHeight,
        PARAM_OriginX, PARAM_OriginY
    )

    override fun parse(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        formulaType: ValueType<Any>
    ): Formula<Any> {
        val image = getFormula(context, project, PARAM_Image, token, strict = true)
        return if (image != null) {
            assertMaxParametersCount(context, token)
            FormulaHtmlImg(
                token, image,
                getFormula(context, project, PARAM_Align, token),
                getFormula(context, project, PARAM_Width, token),
                getFormula(context, project, PARAM_Height, token),
                getFormula(context, project, PARAM_AdvancedWidth, token),
                getFormula(context, project, PARAM_AdvancedHeight, token),
                getFormula(context, project, PARAM_OriginX, token),
                getFormula(context, project, PARAM_OriginY, token)
            )
        } else {
            FormulaStaticValue(token, token.getTokenString(), StringValueType)
        }
    }
}