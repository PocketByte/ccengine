package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.shape.LayoutShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object LayoutShapeParser: AbsFrameParser<LayoutShape>() {

    override val name: String = "LAYOUT"

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height, PARAM_Angle
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): LayoutShape = LayoutShape()
}
