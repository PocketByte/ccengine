package ru.pocketbyte.bcengine.parser.parameter

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

fun ParametrisedProcessor.getParameterRange(
    context: Context,
    parameter: Parameter<*>,
    token: NamedGroupToken,
    strict: Boolean
): ParameterRange? {
    return getParameterRange(parameter, token).apply {
        if (this == null && strict) {
            onParameterMissing(context, parameter, token)
        }
    }
}

fun ParametrisedProcessor.getParameterRange(
    parameter: Parameter<*>,
    token: NamedGroupToken
): ParameterRange? {
    return getParameterRange(
        parameter, token.subTokens
    )
}

fun ParametrisedProcessor.getParameterRange(
    parameter: Parameter<*>,
    tokens: List<ProjectToken>,
): ParameterRange? {
    return getParameterRange(
        parameter,
        parameters,
        parameterSeparator,
        tokens
    )
}

fun ParametrisedProcessor.forEachVarargParameterRange(
    parameter: Parameter<*>,
    token: NamedGroupToken,
    action: (index: Int, range: ParameterRange) -> Unit
) {
    var index = parameters.indexOf(parameter)
    var range = getParameterRange(index, parameterSeparator, token.subTokens)
    while (range != null) {
        action(index, range)
        index += 1
        range = getParameterRange(index, parameterSeparator, token.subTokens)
    }
}

fun ParametrisedProcessor.forEachVarargParametersRange(
    firstParameter: Parameter<*>,
    size: Int,
    token: NamedGroupToken,
    action: (index: Int, ranges: List<ParameterRange>) -> Unit
) {
    var index = parameters.indexOf(firstParameter)
    var ranges = List(size){
        getParameterRange(index++, parameterSeparator, token.subTokens)
    }.filterNotNull()

    while (ranges.size == size) {
        action(index - 1, ranges)
        ranges = List(size){
            getParameterRange(index++, parameterSeparator, token.subTokens)
        }.filterNotNull()
    }
}