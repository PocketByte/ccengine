package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.parser.parameter.Parameter
import ru.pocketbyte.bcengine.parser.parameter.ParameterFormulaType
import ru.pocketbyte.bcengine.parser.parameter.getFormula
import ru.pocketbyte.bcengine.shape.ColumnShape
import ru.pocketbyte.bcengine.tokenizer.NamedGroupToken

object ColumnShapeParser: AbsFrameParser<ColumnShape>() {

    override val name: String = "COLUMN"

    val PARAM_Space = Parameter(
        name = "Space",
        type = StringValueType,
        formulaType = ParameterFormulaType.FORMULA
    )

    override val parameters: Array<Parameter<*>> = arrayOf(
        PARAM_X, PARAM_Y, PARAM_Width, PARAM_Height, PARAM_Space, PARAM_Angle
    )

    override fun createInstance(
        context: Context,
        project: Project,
        token: NamedGroupToken
    ): ColumnShape = ColumnShape()

    override fun applyParameters(
        context: Context,
        project: Project,
        token: NamedGroupToken,
        instance: ColumnShape
    ) {
        super.applyParameters(context, project, token, instance)
        instance.apply {
            getFormula(context, project, PARAM_Space, token)
                .let { space.formula = it }
        }
    }
}
