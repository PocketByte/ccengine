package ru.pocketbyte.bcengine.parser.shape

import ru.pocketbyte.bcengine.parser.ObjectParser
import ru.pocketbyte.bcengine.shape.Shape

interface ShapeParser<ShapeType : Shape> : ObjectParser<ShapeType>