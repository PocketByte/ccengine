package ru.pocketbyte.bcengine.parser

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

interface ObjectParser<Type>: ParametrisedProcessor {
    fun parse(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        currentIndex: Int
    ): ParseResult<Type>
}