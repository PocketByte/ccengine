package ru.pocketbyte.bcengine.parser.project

import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.parser.parameter.ParametrisedProcessor
import ru.pocketbyte.bcengine.tokenizer.ProjectToken

interface ProjectPropertyProcessor: ParametrisedProcessor {
    val isExclusive: Boolean
    fun add(
        context: Context,
        project: Project,
        tokens: List<ProjectToken>,
        index: Int
    ): Int

    fun process(context: Context, project: Project, tokens: List<ProjectToken>)
}