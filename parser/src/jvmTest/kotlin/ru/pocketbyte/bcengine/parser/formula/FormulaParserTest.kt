package ru.pocketbyte.bcengine.parser.formula

import org.junit.Test
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.entity.assertNoErrors
import ru.pocketbyte.bcengine.formula.Formula
import ru.pocketbyte.bcengine.formula.FormulaStaticValue
import ru.pocketbyte.bcengine.formula.formulaStaticValue
import ru.pocketbyte.bcengine.formula.stringbuild.FormulaGoogleSheetCsv
import ru.pocketbyte.bcengine.formula.valuetype.StringValueType
import ru.pocketbyte.bcengine.formula.valuetype.UndefinedValueType
import ru.pocketbyte.bcengine.formula.valuetype.ValueType
import ru.pocketbyte.bcengine.tokenizer.Tokenizer
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FormulaParserTest {

    @Test
    fun test() {
        val context = Context("./", "./tmp/")
        val project = Project().apply {
            functions["x"] = formulaStaticValue(2f)
            functions["a"] = formulaStaticValue("5")
        }
        val parser = FormulaParser(
            project
        )

        var formula: Formula<Any>? = parser
            .parse(context, "IF(\$x == 2; \"15\"; \"no\") + \$a", UndefinedValueType)

        context.errors.assertNoErrors()

        assertEquals(
            "155", formula?.compute(project, null)
        )

        formula = parser.parse<Any>(context, "\$x == 2 & true", UndefinedValueType)

        context.errors.assertNoErrors()

        assertEquals(
            true, formula.compute(project, null)
        )
    }

    @Test
    fun testParseFormulaGoogleSheetCsv() {
        val context = Context("./", "./tmp/")
        val project = Project()
        val parser = FormulaParser(project)

        val scheetId = "1pygCKYfzWkJqPyqnIOAVDqGf3S5FpkzI6nZfn1v2KvE"
        val workscheetId = "0"
        val formula = parser.parse(
            context,
            "googleSheetCsv(\"$scheetId\"; \"$workscheetId\")",
            StringValueType
        ) as? FormulaGoogleSheetCsv

        assertNotNull(formula)
        assertEquals(
            scheetId,
            (formula.parameters[0] as? FormulaStaticValue<String>)?.value
        )
        assertEquals(
            workscheetId,
            (formula.parameters[1] as? FormulaStaticValue<String>)?.value
        )
    }

    private fun <T : Any> FormulaParser.parse(
        context: Context,
        formulaString: String, type: ValueType<T>
    ) : Formula<T> {
        return parse(context, Tokenizer.tokenize(formulaString), type)
    }
}