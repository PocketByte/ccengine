package ru.pocketbyte.bcengine.entity

import ru.pocketbyte.bcengine.errors.ErrorsContainer
import ru.pocketbyte.bcengine.errors.ProjectError

fun ErrorsContainer.assertNoErrors() {
    list.forEach {
        when (it) {
            is ProjectError.ExceptionError -> {
                throw it.exception
            }
            else -> throw RuntimeException(
                ErrorsContainer.buildErrorString(it)
            )
        }
    }
}