plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose").version("1.5.10")
}

group = "ru.pocketbyte.bcengine"
version = "1.0-SNAPSHOT"

kotlin {
    jvm()
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":core"))
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(libsBce.kotlin.coroutines.core)
                implementation(libsBce.kydra.log)
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(compose.desktop.currentOs)
            }
        }
    }
}