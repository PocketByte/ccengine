package ru.pocketbyte.compose

import androidx.compose.ui.text.font.FontFamily
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.Project
import ru.pocketbyte.bcengine.canvas.Canvas
import ru.pocketbyte.bcengine.canvas.CanvasFactory
import ru.pocketbyte.bcengine.compose.canvas.ComposeImageRepository
import ru.pocketbyte.compose.canvas.ComposeCanvas

class ComposeCanvasFactory(
    private val imageRepository: ComposeImageRepository,
    private val fontFamilyResolver: FontFamily.Resolver
): CanvasFactory {
    override fun createCanvas(
        context: Context,
        project: Project,
        widthMultiply: Int,
        heightMultiply: Int
    ): Canvas {
        return ComposeCanvas(
            project.layout, context, imageRepository, fontFamilyResolver,
            widthMultiply, heightMultiply
        )
    }
}