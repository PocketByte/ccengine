package ru.pocketbyte.compose.canvas

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.compose.canvas.AbsComposeImageRepository
import ru.pocketbyte.kydra.log.KydraLog
import ru.pocketbyte.kydra.log.error
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO

class ComposeImageRepositoryImpl : AbsComposeImageRepository() {

    override fun loadBitmap(context: Context, imagePath: String): ImageBitmap? {
        val imageFile = if (imagePath.startsWith(".")) {
            File(context.workDir, imagePath)
        } else {
            File(imagePath)
        }

        if (!imageFile.exists()) {
            KydraLog.error { "File not found: ${imageFile.absolutePath}" }
            return null
        }

        try {
            return ImageIO
                .read(imageFile)
                .toComposeImageBitmap()

        } catch (e: IOException) {
            KydraLog.error { "Failed to read file ${imageFile.absolutePath}" }
            e.message?.let { KydraLog.error(it) }
        }
        return null
    }
}