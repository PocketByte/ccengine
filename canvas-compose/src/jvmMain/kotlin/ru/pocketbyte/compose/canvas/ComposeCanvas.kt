package ru.pocketbyte.compose.canvas

import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toAwtImage
import androidx.compose.ui.text.font.FontFamily
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.ProjectLayoutOld
import ru.pocketbyte.bcengine.compose.canvas.AbsComposeCanvas
import ru.pocketbyte.bcengine.compose.canvas.ComposeImageRepository
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.*
import javax.imageio.metadata.IIOMetadata
import javax.imageio.metadata.IIOMetadataNode
import kotlin.math.roundToInt

class ComposeCanvas(
    override val layout: ProjectLayoutOld,
    override val context: Context,
    imageRepository: ComposeImageRepository,
    fontFamilyResolver: FontFamily.Resolver,
    widthMultiply: Int = 1,
    heightMultiply: Int = 1
): AbsComposeCanvas(imageRepository, fontFamilyResolver, widthMultiply, heightMultiply) {

    private var bitmap = ImageBitmap(
        width.roundToInt(), height.roundToInt(),
    )

    override val canvas = Canvas(bitmap)

    override fun saveToFile(filePath: String, format: String) {
        val file = File(filePath)
        file.parentFile?.mkdirs()
        saveImage(file, bitmap.toAwtImage(), format)
    }

    private fun saveImage(output: File, image: BufferedImage, formatName: String) {
        output.delete()
        val iw: Iterator<ImageWriter> = ImageIO.getImageWritersByFormatName(formatName)
        while (iw.hasNext()) {
            val writer: ImageWriter = iw.next()
            val writeParam = writer.defaultWriteParam
            val typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB)
            val metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam)
            if (metadata.isReadOnly || !metadata.isStandardMetadataFormatSupported) {
                continue
            }
            setDPI(metadata)
            ImageIO.createImageOutputStream(output).use { stream ->
                writer.output = stream
                writer.write(metadata, IIOImage(image, null, metadata), writeParam)
            }
            break
        }
    }

    private fun setDPI(metadata: IIOMetadata) {
        // for PMG, it's dots per millimeter
        val dotsPerMilli: Double = 1.0 * layout.dpi * ProjectLayoutOld.INC_TO_MM
        val horiz = IIOMetadataNode("HorizontalPixelSize")
        horiz.setAttribute("value", dotsPerMilli.toString())
        val vert = IIOMetadataNode("VerticalPixelSize")
        vert.setAttribute("value", dotsPerMilli.toString())
        val dim = IIOMetadataNode("Dimension")
        dim.appendChild(horiz)
        dim.appendChild(vert)
        val root = IIOMetadataNode("javax_imageio_1.0")
        root.appendChild(dim)
        metadata.mergeTree("javax_imageio_1.0", root)
    }

}