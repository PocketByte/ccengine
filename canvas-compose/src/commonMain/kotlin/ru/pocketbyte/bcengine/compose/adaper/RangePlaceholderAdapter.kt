package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.Placeholder
import ru.pocketbyte.bcengine.html.HtmlAttribute

interface RangePlaceholderAdapter {
    fun adapt(attribute: HtmlAttribute): AnnotatedString.Range<Placeholder>?
}