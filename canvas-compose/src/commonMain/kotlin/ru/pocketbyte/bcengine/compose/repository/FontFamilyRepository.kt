package ru.pocketbyte.bcengine.compose.repository

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.platform.Typeface
import org.jetbrains.skia.FontMgr
import org.jetbrains.skia.FontStyle

class FontFamilyRepository {

    private val cache = mutableMapOf<String, FontFamily?>()

    fun get(
        familyName: String,
        fontStyle: FontStyle = FontStyle.NORMAL
    ): FontFamily? {
        cache[familyName]?.let { return it }

        val skStyle = FontMgr.default
            .matchFamilyStyle(familyName, fontStyle)
            ?: return null

        return Typeface(skStyle, "${familyName}_Style:${fontStyle._value}")
            .fontFamily.apply {
                cache[familyName] = this
            }
    }

    fun clear() {
        cache.clear()
    }
}