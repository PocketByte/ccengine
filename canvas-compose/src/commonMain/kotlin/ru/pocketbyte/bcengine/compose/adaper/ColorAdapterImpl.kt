package ru.pocketbyte.bcengine.compose.adaper

import ru.pocketbyte.bcengine.entity.Color
import ru.pocketbyte.bcengine.entity.RGBAColor

class ColorAdapterImpl : ColorAdapter {
    override fun adapt(color: Color): androidx.compose.ui.graphics.Color {
        return when (color) {
            is RGBAColor -> androidx.compose.ui.graphics.Color(
                color.r, color.g, color.b, color.a
            )
        }
    }
}