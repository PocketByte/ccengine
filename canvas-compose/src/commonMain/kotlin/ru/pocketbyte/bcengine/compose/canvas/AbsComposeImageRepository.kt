package ru.pocketbyte.bcengine.compose.canvas

import androidx.compose.ui.graphics.ImageBitmap
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.compose.canvas.ComposeImageRepository.ComposeItem

abstract class AbsComposeImageRepository : ComposeImageRepository {

    private val imageStub = ComposeItem(
        1, 1,
        ImageBitmap(1, 1)
    )

    private val cache = mutableMapOf<String, ComposeItem>()

    abstract fun loadBitmap(context: Context, imagePath: String): ImageBitmap?

    override fun get(context: Context, imagePath: String): ComposeItem {
        cache[imagePath]?.let { return it }

        val image = loadBitmap(context, imagePath) ?: return imageStub
        val item = ComposeItem(
            image.width,
            image.height,
            image
        )
        cache[imagePath] = item
        return item
    }

    override fun clear() {
        cache.clear()
    }
}