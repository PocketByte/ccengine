package ru.pocketbyte.bcengine.compose.canvas

import androidx.compose.ui.graphics.ImageBitmap
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.canvas.ImageRepository

interface ComposeImageRepository : ImageRepository {

    class ComposeItem(
        width: Int,
        height: Int,
        val image: ImageBitmap
    ) : ImageRepository.Item(width, height)

    override fun get(context: Context, imagePath: String): ComposeItem
}