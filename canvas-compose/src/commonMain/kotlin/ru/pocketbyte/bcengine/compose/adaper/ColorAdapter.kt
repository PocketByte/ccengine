package ru.pocketbyte.bcengine.compose.adaper

import ru.pocketbyte.bcengine.entity.Color

interface ColorAdapter {
    fun adapt(color: Color): androidx.compose.ui.graphics.Color
}