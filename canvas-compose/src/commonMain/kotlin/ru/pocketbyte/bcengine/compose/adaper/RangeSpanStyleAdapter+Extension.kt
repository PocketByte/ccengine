package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import ru.pocketbyte.bcengine.html.HtmlAttribute

fun RangeSpanStyleAdapter.adapt(
    attributes: List<HtmlAttribute>
): List<AnnotatedString.Range<SpanStyle>> {
    return attributes.mapNotNull {
        adapt(it)
    }
}