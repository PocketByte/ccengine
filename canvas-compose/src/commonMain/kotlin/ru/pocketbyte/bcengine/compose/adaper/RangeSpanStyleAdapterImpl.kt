package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.BaselineShift
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import ru.pocketbyte.bcengine.compose.repository.FontFamilyRepository
import ru.pocketbyte.bcengine.entity.coder.ColorCoder
import ru.pocketbyte.bcengine.html.*

class RangeSpanStyleAdapterImpl(
    private val fontFamilyRepository: FontFamilyRepository,
    private val colorAdapter: ColorAdapter
) : RangeSpanStyleAdapter {
    override fun adapt(attribute: HtmlAttribute): AnnotatedString.Range<SpanStyle>? {
        return attribute.toComposeSpanStyle()?.let {
            AnnotatedString.Range(it, attribute.startIndex, attribute.endIndex)
        }
    }

    @OptIn(ExperimentalUnitApi::class)
    private fun HtmlAttribute.toComposeSpanStyle(): SpanStyle? {
        return when (tag) {
            is HTMLTagImage,
            is HTMLTagBr,
            is HTMLTagQ -> {
                // Do Nothing
                null
            }
            is HTMLTagFont -> {
                val family = getStringAttribute(HTMLTagFont.face)?.let {
                    fontFamilyRepository.get(it)
                }
                val size = getFloatAttribute(HTMLTagFont.size)?.let {
                    TextUnit(it, TextUnitType.Sp)
                }
                val color = getStringAttribute(HTMLTagFont.color)?.let {
                    ColorCoder.decode(it.trim())?.let { color ->
                        colorAdapter.adapt(color)
                    }
                }
                SpanStyle(
                    color = color ?: androidx.compose.ui.graphics.Color.Unspecified,
                    fontSize = size ?: TextUnit.Unspecified,
                    fontFamily = family
                )
            }
            is HTMLTagB -> {
                SpanStyle(
                    fontWeight = FontWeight.Bold
                )
            }
            is HTMLTagI -> {
                SpanStyle(
                    fontStyle = FontStyle.Italic
                )
            }
            is HTMLTagS -> {
                SpanStyle(
                    textDecoration = TextDecoration.LineThrough
                )
            }
            is HTMLTagU -> {
                SpanStyle(
                    textDecoration = TextDecoration.Underline
                )
            }
            is HTMLTagSup -> {
                SpanStyle(
                    baselineShift = BaselineShift.Superscript
                )
            }
            is HTMLTagSub -> {
                SpanStyle(
                    baselineShift = BaselineShift.Subscript
                )
            }
        }
    }
}