package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import ru.pocketbyte.bcengine.entity.Color
import ru.pocketbyte.bcengine.entity.StrokeStyle

class PaintAdapterImpl(
    private val colorAdapter: ColorAdapter
) : PaintAdapter {

    override fun adapt(color: Color): Paint {
        return Paint().apply {
            this.color = colorAdapter.adapt(color)
        }
    }

    override fun adapt(color: Color, size: Float, style: StrokeStyle): Paint {
        return Paint().apply {
            this.color = colorAdapter.adapt(color)
            this.style = PaintingStyle.Stroke
            this.strokeWidth = size
        }
    }
}