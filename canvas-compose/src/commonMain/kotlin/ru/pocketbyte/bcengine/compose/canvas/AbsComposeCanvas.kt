package ru.pocketbyte.bcengine.compose.canvas

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.PaintingStyle
import androidx.compose.ui.text.*
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.*
import ru.pocketbyte.bcengine.ProjectLayoutOld
import ru.pocketbyte.bcengine.canvas.AbsCanvas
import ru.pocketbyte.bcengine.compose.adaper.*
import ru.pocketbyte.bcengine.compose.adaper.RangeSpanStyleAdapterImpl
import ru.pocketbyte.bcengine.compose.repository.FontFamilyRepository
import ru.pocketbyte.bcengine.entity.*
import ru.pocketbyte.bcengine.html.HTMLTagImage
import ru.pocketbyte.bcengine.html.HtmlAttribute
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

@OptIn(ExperimentalUnitApi::class)
abstract class AbsComposeCanvas(
    override val imageRepository: ComposeImageRepository,
    private val fontFamilyResolver: FontFamily.Resolver,
    override val widthMultiply: Int,
    override val heightMultiply: Int
) : AbsCanvas() {

    companion object {
        const val MAX_CONSTRAINT_SIZE = 0x3FFFF - 8 // MaxFocusMask - 8
    }

    protected abstract val canvas: Canvas

    private val fontFamilyRepository = FontFamilyRepository()

    private val colorAdapter: ColorAdapter by lazy {
        ColorAdapterImpl()
    }

    private val paintAdapter: PaintAdapter by lazy {
        PaintAdapterImpl(colorAdapter)
    }

    private val rangeSpanStyleAdapter: RangeSpanStyleAdapter by lazy {
        RangeSpanStyleAdapterImpl(this.fontFamilyRepository, colorAdapter)
    }

    private val rangePlaceholderAdapter: RangePlaceholderAdapter by lazy {
        RangePlaceholderAdapterImpl()
    }

    override var background: Color
        get() = super.background
        set(value) {
            super.background = value
            backgroundPaint.color = colorAdapter.adapt(value)
        }

    private var saveCounter = 0

    protected val defaultPaint = Paint()

    protected var backgroundPaint: Paint = Paint().apply {
        color = colorAdapter.adapt(background)
        blendMode = BlendMode.Src
    }

    protected val debugPaint = Paint().apply {
        color = androidx.compose.ui.graphics.Color.Red
        style = PaintingStyle.Stroke
    }
    get() {
        field.strokeWidth = layout.projectFloat(
            0.5f, Size.Dimension.SCALED_PIXEL,
            0f, 0f
        )
        return field
    }

    override fun recycle() {
        imageRepository.clear()
        fontFamilyRepository.clear()
    }

    override fun clipRect(left: Float, top: Float, right: Float, bottom: Float) {
        canvas.clipRect(left, top, right, bottom)
    }

    override fun translate(x: Float, y: Float) {
        canvas.translate(x, y)
    }

    override fun rotate(angle: Float) {
        canvas.rotate(angle)
    }

    override fun save(): Int {
        canvas.save()
        return saveCounter++
    }

    override fun restore(save: Int) {
        while (saveCounter > save) {
            canvas.restore()
            saveCounter--
        }
    }

    override fun clear() {
        canvas.drawRect(
            0f, 0f,
            width, height,
            backgroundPaint
        )
    }

    override fun clearRect(width: Float, height: Float) {
        canvas.drawRect(
            0f, 0f,
            width, height,
            backgroundPaint
        )
    }

    override fun drawLine(x: Float, y: Float, strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle) {
        canvas.drawLine(
            Offset.Zero,
            Offset(x, y),
            paintAdapter.adapt(strokeColor, strokeSize, strokeStyle)
        )
    }

    override fun drawRect(
        width: Float, height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle
    ) {
        canvas.drawRect(
            0f, 0f,
            width, height,
            paintAdapter.adapt(strokeColor, strokeSize, strokeStyle)
        )
    }

    override fun drawRoundRect(
        width: Float,
        height: Float,
        strokeSize: Float, strokeColor: Color, strokeStyle: StrokeStyle,
        cornerRadiusX: Float,
        cornerRadiusY: Float
    ) {
        canvas.drawRoundRect(
            0f, 0f,
            width, height,
            cornerRadiusX, cornerRadiusY,
            paintAdapter.adapt(strokeColor, strokeSize, strokeStyle)
        )
    }

    override fun drawRect(width: Float, height: Float, color: Color) {
        canvas.drawRect(
            0f, 0f,
            width, height,
            paintAdapter.adapt(color)
        )
    }

    override fun drawRoundRect(
        width: Float,
        height: Float,
        color: Color,
        cornerRadiusX: Float,
        cornerRadiusY: Float
    ) {
        canvas.drawRoundRect(
            0f, 0f,
            width, height,
            cornerRadiusX, cornerRadiusY,
            paintAdapter.adapt(color)
        )
    }

    override fun drawImage(
        width: Float, height: Float,
        imagePath: String,
        attributes: ImageAttributes
    ) {
        val image = imageRepository.get(context, imagePath).image
        var posX = 0f
        var posY = 0f
        canvas.save()
        canvas.clipRect(posX, posY, posX + width, posY + height)

        var imgWidth = width
        var imgHeight = height
        if (attributes.scaleMode != ScaleMode.STRETCH) {
            val scaleFactorX = width / image.width
            val scaleFactorY = height / image.height
            val scaleFactor = when (attributes.scaleMode) {
                ScaleMode.FILL -> max(scaleFactorX, scaleFactorY)
                ScaleMode.FIT -> min(scaleFactorX, scaleFactorY)
                else -> throw IllegalStateException()
            }
            imgWidth = image.width * scaleFactor
            imgHeight = image.height * scaleFactor

            when(attributes.aligningX) {
                Aligning.START -> {
                    // Do nothing
                }
                Aligning.CENTER -> {
                    posX += (width - imgWidth) / 2f
                }
                Aligning.END -> {
                    posX += width - imgWidth
                }
            }

            when(attributes.aligningY) {
                Aligning.START -> {
                    // Do nothing
                }
                Aligning.CENTER -> {
                    posY += (height - imgHeight) / 2f
                }
                Aligning.END -> {
                    posY += height - imgHeight
                }
            }
        }

        canvas.drawImageRect(
            imageRepository.get(context, imagePath).image,
            dstOffset = IntOffset(posX.roundToInt(), posY.roundToInt()),
            dstSize = IntSize(imgWidth.roundToInt(), imgHeight.roundToInt()),
            paint = defaultPaint
        )
        canvas.restore()
    }

    override fun measureText(
        width: Float, height: Float,
        text: String, attributes: TextAttributes
    ): Pair<Float, Float> {
        if (text.isEmpty()) {
            return Pair(
                if (width <= 0) 0f else width,
                if (height <= 0) 0f else height
            )
        }

        if (width > 0 && height > 0) {
            return Pair(width, height)
        }

        val placeHolders = attributes.htmlAttributes.filter { it.tag == HTMLTagImage }
        val paragraph = buildParagraph(width, height, text, attributes, placeHolders)

        return Pair(
            if (width <= 0) paragraph.width else width,
            if (height <= 0) paragraph.height else height
        )
    }

    override fun drawText(
        width: Float, height: Float,
        text: String, attributes: TextAttributes
    ) {
        if (text.isEmpty()) {
            return
        }

        canvas.save()

        if (!attributes.noCrop) {
            canvas.clipRect(
                0f, 0f,
                width, height
            )
        }

        val placeHolders = attributes.htmlAttributes.filter { it.tag == HTMLTagImage }
        val paragraph = buildParagraph(width, height, text, attributes, placeHolders)

        val yOffset = when (attributes.aligningY) {
            Aligning.START -> 0f
            Aligning.CENTER -> (height - paragraph.height) / 2f
            Aligning.END -> height - paragraph.height
        }

        val xOffset = when (attributes.aligningX) {
            TextAligning.START, TextAligning.JUSTIFY -> 0f
            TextAligning.CENTER -> (width - paragraph.width) / 2f
            TextAligning.END -> width - paragraph.width
        }

        withTranslate(xOffset, yOffset) {
            paragraph.paint(canvas)
            paragraph.placeholderRects.forEachIndexed { index, rect ->
                if (rect == null) return@forEachIndexed
                val attr = placeHolders.getOrNull(index)
                    ?: return@forEachIndexed
                val imagePath = attr.getStringAttribute(HTMLTagImage.src)
                    ?: return@forEachIndexed

                val originX = attr.getFloatAttribute(HTMLTagImage.originX) ?: 0f
                val originY = attr.getFloatAttribute(HTMLTagImage.originY) ?: 0f

                val advanceWidth = attr.getFloatAttribute(HTMLTagImage.advanceWidth) ?: 1f
                val advanceHeight = attr.getFloatAttribute(HTMLTagImage.advanceHeight) ?: 1f

                val imgWidth = rect.width / advanceWidth
                val imgHeight = rect.height / advanceHeight

                val icon = imageRepository.get(context, imagePath)
                canvas.drawImageRect(
                    icon.image,
                    dstOffset = IntOffset(
                        (rect.left - imgWidth * originX).roundToInt(),
                        (rect.bottom - imgHeight * (1 - originY)).roundToInt()
                    ),
                    dstSize = IntSize(
                        imgWidth.roundToInt(),
                        imgHeight.roundToInt()
                    ),
                    paint = defaultPaint
                )
            }
        }

        canvas.restore()
    }

    private fun buildParagraph(
        width: Float, height: Float,
        text: String, attributes: TextAttributes,
        placeHolders: List<HtmlAttribute>
    ): Paragraph {
        val intrinsics = ParagraphIntrinsics(
            text,
            attributes.toTextStyle(),
            rangeSpanStyleAdapter.adapt(attributes.htmlAttributes),
            rangePlaceholderAdapter.adapt(placeHolders),
            Density(layout.dpi / ProjectLayoutOld.DEFAULT_DPI),
            fontFamilyResolver
        )

        return Paragraph(
            intrinsics,
            Constraints(
                minWidth = 0,
                maxWidth = if (width <= 0) {
                    Constraints.Infinity
                } else {
                    min(width.roundToInt(), MAX_CONSTRAINT_SIZE)
                },
                minHeight = 0,
                maxHeight = if (height <= 0) {
                    Constraints.Infinity
                } else {
                    min(height.roundToInt(), MAX_CONSTRAINT_SIZE)
                }
            ),
            ellipsis = true
        )
    }

    override fun drawDebugRect(x: Float, y: Float, width: Float, height: Float) {
        if (width < 0 || height < 0) {
            return
        }
        if (width == 0f && height == 0f) {
            return
        }
        canvas.drawRect(
            x, y,
            x + width,
            y + height,
            debugPaint
        )
    }

    private fun TextAttributes.toTextStyle(): TextStyle {
        return TextStyle(
            color = colorAdapter.adapt(fontColor),
            fontSize = TextUnit(fontSize, TextUnitType.Sp),
            fontFamily = fontFamilyRepository.get(fontFamily),
            fontStyle = if (fontStyle.contains(Font.Style.ITALIC)) {
                FontStyle.Italic
            } else {
                FontStyle.Normal
            },
            fontWeight = if (fontStyle.contains(Font.Style.BOLD)) {
                FontWeight.Bold
            } else {
                FontWeight.Normal
            },
            textAlign = when (aligningX) {
                TextAligning.START -> TextAlign.Start
                TextAligning.CENTER -> TextAlign.Center
                TextAligning.END -> TextAlign.End
                TextAligning.JUSTIFY -> TextAlign.Justify
            }
        )
    }

    private inline fun withTranslate(dx: Float, dy: Float, block: () -> Unit) {
        val save = save()
        canvas.translate(dx, dy)
        block()
        restore(save)
    }
}