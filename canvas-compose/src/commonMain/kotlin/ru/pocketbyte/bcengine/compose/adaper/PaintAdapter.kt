package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.graphics.Paint
import ru.pocketbyte.bcengine.entity.Color
import ru.pocketbyte.bcengine.entity.StrokeStyle

interface PaintAdapter {
    fun adapt(color: Color): Paint
    fun adapt(color: Color, size: Float, style: StrokeStyle): Paint
}