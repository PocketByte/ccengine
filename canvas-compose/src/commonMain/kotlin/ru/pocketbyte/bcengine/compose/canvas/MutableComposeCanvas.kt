package ru.pocketbyte.bcengine.compose.canvas

import androidx.compose.ui.graphics.Canvas
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.text.font.FontFamily
import ru.pocketbyte.bcengine.Context
import ru.pocketbyte.bcengine.ProjectLayoutOld
import kotlin.math.roundToInt

class MutableComposeCanvas(
    imageRepository: ComposeImageRepository,
    fontFamilyResolver: FontFamily.Resolver,
    widthMultiply: Int = 1,
    heightMultiply: Int = 1
): AbsComposeCanvas(imageRepository, fontFamilyResolver, widthMultiply, heightMultiply) {

    override val canvas: Canvas
        get() = _canvas ?: throw IllegalStateException("Canvas not set")

    override val layout: ProjectLayoutOld
        get() = _layout ?: throw IllegalStateException("Layout not set")

    override val context: Context
        get() = _context ?: throw IllegalStateException("Context not set")

    var bitmap: ImageBitmap? = null
        private set

    private var _canvas: Canvas? = null
    private var _layout: ProjectLayoutOld? = null
    private var _context: Context? = null

    override fun saveToFile(filePath: String, format: String) {
        throw RuntimeException("Save operation not supported for current canvas")
    }

    fun setLayout(layout: ProjectLayoutOld) {
        _layout = layout
        val width = width.roundToInt()
        val height = height.roundToInt()
        if (bitmap?.width != width || bitmap?.height != height) {
            ImageBitmap(width, height).let {
                bitmap = it
                _canvas = Canvas(it)
            }
        }
    }

    fun setContext(context: Context) {
        if (_context?.workDir != context.workDir) {
            imageRepository.clear()
        }
        _context = context
    }

    override fun recycle() {
        super.recycle()
        _canvas = null
        _layout = null
        bitmap = null
    }

}