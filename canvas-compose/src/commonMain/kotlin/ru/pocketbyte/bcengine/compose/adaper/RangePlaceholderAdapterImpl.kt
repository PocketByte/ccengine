package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.Placeholder
import androidx.compose.ui.text.PlaceholderVerticalAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import ru.pocketbyte.bcengine.html.HTMLTagImage
import ru.pocketbyte.bcengine.html.HtmlAttribute

class RangePlaceholderAdapterImpl : RangePlaceholderAdapter {
    override fun adapt(attribute: HtmlAttribute): AnnotatedString.Range<Placeholder>? {
        return attribute.toComposePlaceholder()?.let {
            AnnotatedString.Range(it, attribute.startIndex, attribute.endIndex)
        }
    }

    private fun HtmlAttribute.toComposePlaceholder(): Placeholder? {
        return when (tag) {
            is HTMLTagImage -> {
                val widthSp = getAttribute(HTMLTagImage.width) as? Float ?: 20f
                val heightSp = getAttribute(HTMLTagImage.height) as? Float ?: 20f
                val advanceWidth = getFloatAttribute(HTMLTagImage.advanceWidth) ?: 1f
                val advanceHeight = getFloatAttribute(HTMLTagImage.advanceHeight) ?: 1f

                val align = getStringAttribute(HTMLTagImage.align)

                Placeholder(
                    TextUnit(widthSp * advanceWidth, TextUnitType.Sp),
                    TextUnit(heightSp * advanceHeight, TextUnitType.Sp),
                    align.toPlaceholderVerticalAlign()
                )
            }
            else -> null
        }
    }

    private fun String?.toPlaceholderVerticalAlign(): PlaceholderVerticalAlign {
        return when (this?.lowercase()){
            "baseline" -> PlaceholderVerticalAlign.AboveBaseline
            "top" -> PlaceholderVerticalAlign.Top
            "bottom" -> PlaceholderVerticalAlign.Bottom
            "center" -> PlaceholderVerticalAlign.Center
            "texttop" -> PlaceholderVerticalAlign.TextTop
            "textbottom" -> PlaceholderVerticalAlign.TextBottom
            "textcenter" -> PlaceholderVerticalAlign.TextCenter
            else -> PlaceholderVerticalAlign.AboveBaseline
        }
    }
}