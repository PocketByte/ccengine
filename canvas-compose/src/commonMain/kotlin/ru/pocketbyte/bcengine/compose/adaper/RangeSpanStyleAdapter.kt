package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import ru.pocketbyte.bcengine.html.HtmlAttribute

interface RangeSpanStyleAdapter {
    fun adapt(attribute: HtmlAttribute): AnnotatedString.Range<SpanStyle>?
}
