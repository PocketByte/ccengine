package ru.pocketbyte.bcengine.compose.adaper

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.Placeholder
import ru.pocketbyte.bcengine.html.HtmlAttribute

fun RangePlaceholderAdapter.adapt(
    attributes: List<HtmlAttribute>
): List<AnnotatedString.Range<Placeholder>> {
    return attributes.mapNotNull {
        adapt(it)
    }
}
