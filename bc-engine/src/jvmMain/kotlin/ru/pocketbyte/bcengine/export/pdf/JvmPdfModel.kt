package ru.pocketbyte.bcengine.export.pdf

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import ru.pocketbyte.bcengine.core.PhysicalDimension
import ru.pocketbyte.bcengine.core.config.ProjectLayout
import java.io.File
import kotlin.math.max


internal class JvmPdfModel(
    private val pageSize: PDRectangle,
    private val documentName: String,
    private val paddingLeft: Float,
    private val paddingRight: Float,
    private val paddingTop: Float,
    private val paddingBottom: Float,
    private val horizontalSpace: Float,
    private val horizontalAlignment: Alignment,
    private val verticalSpace: Float
) {

    companion object {
        const val PDF_DPI = 72f

        private const val HEADER_FONT_SIZE = 8f
    }

    private var positionX: Float = -Float.MAX_VALUE
    private var positionY: Float = -Float.MAX_VALUE
    private var rowHeight: Float = 0f

    private var pagesCount = 0
    private val document = PDDocument()
    private var contentStream: PDPageContentStream? = null

    private val textOffset = max(
        paddingLeft,
        ProjectLayout.projectPx(5f, PhysicalDimension.MILLIMETER, PDF_DPI)
    )

    private class PendingCard(
        val image: PDImageXObject,
        val positionX: Float,
        val positionY: Float,
        val cardWidth: Float,
        val cardHeight: Float
    )

    private var pendingCards = mutableListOf<PendingCard>()

    fun drawCard(imageFile: File, cardWidth: Float, cardHeight: Float) {
        initIIfNeeded()

        if (positionX + cardWidth + paddingRight > pageSize.width) {
            positionX = paddingLeft
            positionY -= rowHeight + verticalSpace
            if (positionY - cardHeight - paddingBottom < 0) {
                positionY = pageSize.height - paddingTop
                prepareNextPage()
            }
            rowHeight = 0f
        }

        pendingCards.add(
            PendingCard(
                PDImageXObject.createFromFileByExtension(imageFile, document),
                positionX, positionY - cardHeight,
                cardWidth, cardHeight
            )
        )

        positionX += horizontalSpace + cardWidth
        rowHeight = maxOf(rowHeight, cardHeight)
    }

    fun saveAndClose(file: File) {
        drawPendingCards()
        contentStream?.close()
        document.save(file)
        document.close()
    }

    private fun prepareNextPage() {
        drawPendingCards()
        contentStream?.close()

        pagesCount++
        val page = PDPage(pageSize)

        document.addPage(page)
        contentStream = PDPageContentStream(document, page).apply {
            beginText()
            setFont(PDType1Font.COURIER, HEADER_FONT_SIZE)
            newLineAtOffset(textOffset, paddingBottom - HEADER_FONT_SIZE * 1.5f)
            showText("$documentName (Page $pagesCount)")
            endText()
        }
    }

    private fun drawPendingCards() {
        if (pendingCards.isEmpty()) {
            return
        }

        val contentWidth = pageSize.width - paddingLeft - paddingRight
        var index = 0
        var rowWidth = 0f
        do {
            val rowSize = getPendingRowSize(index)

            rowWidth = pendingCards.getOrNull(index + rowSize - 1)
                ?.let { it.positionX + it.cardWidth }
                ?.let { max(rowWidth, it - pendingCards[index].positionX) }
                ?: rowWidth

            drawPendingCards(
                index,
                index + rowSize - 1,
                when (horizontalAlignment) {
                    Alignment.START -> 0f
                    Alignment.END -> contentWidth - rowWidth
                    Alignment.CENTER -> (contentWidth - rowWidth) / 2f
                }
            )

            index += rowSize
        } while (index < pendingCards.size)
        pendingCards.clear()
    }


    private fun drawPendingCards(start: Int, end: Int, offsetX: Float) {
        for (index in start .. end) {
            pendingCards[index].let {
                contentStream?.drawImage(
                    it.image,
                    it.positionX + offsetX,
                    it.positionY,
                    it.cardWidth,
                    it.cardHeight
                )
            }
        }
    }

    private fun initIIfNeeded() {
        if (positionX < 0) { positionX = pageSize.width }
        if (positionY < 0) { positionY = 0f }
    }

    private fun getPendingRowSize(offset: Int = 0): Int {
        val initialY = pendingCards.getOrNull(offset)?.positionY ?: return 0
        var size = 1
        while (initialY == pendingCards.getOrNull(offset + size)?.positionY) {
            size += 1
        }
        return size
    }
}