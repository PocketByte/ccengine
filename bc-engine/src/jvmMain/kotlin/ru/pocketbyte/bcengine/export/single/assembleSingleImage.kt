package ru.pocketbyte.bcengine.export.single

import org.jetbrains.skia.Surface
import org.jetbrains.skiko.toImage
import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.config.output.SingleImageOutput
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.forEachIndexed
import ru.pocketbyte.bcengine.export.image.SaveImageInteractor
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.ceil
import kotlin.math.roundToInt
import kotlin.math.sqrt


fun assembleSingleImage(
    projectConfig: ProjectConfig,
    singleImageOutput: SingleImageOutput,
    context: Context,
    dataSet: DataSet,
    outputFile: File? = null
) {
    val rowSize = singleImageOutput.rowSize.let {
        if (it <= 0) {
            sqrt(dataSet.size.toDouble()).roundToInt().coerceAtLeast(1)
        } else {
            it
        }
    }

    val surface = Surface.makeRasterN32Premul(
        rowSize * projectConfig.layout.widthPx(),
        ceil(dataSet.size.toDouble() / rowSize.toDouble())
            .toInt() * projectConfig.layout.heightPx()
    )

    surface.canvas.clear(org.jetbrains.skia.Color.WHITE)

    var positionX = 0f
    var positionY = 0f
    dataSet.forEachIndexed { index, item ->
        val bi: BufferedImage = ImageIO.read(
            projectConfig.output.deck.getItemFile(context, index, item)
        )

        surface.canvas.drawImage(
            bi.toImage(),
            positionX,
            positionY,
            null
        )
        positionX += projectConfig.layout.widthPx()
        if (positionX + projectConfig.layout.widthPx() > surface.width) {
            positionX = 0f
            positionY += projectConfig.layout.heightPx()
        }
    }

    SaveImageInteractor.saveToPngFile(
        surface.makeImageSnapshot(),
        outputFile ?: singleImageOutput.getFile(context),
        projectConfig.layout.dpi
    )
}
