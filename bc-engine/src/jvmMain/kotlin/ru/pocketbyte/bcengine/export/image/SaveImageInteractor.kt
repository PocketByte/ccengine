package ru.pocketbyte.bcengine.export.image

import org.jetbrains.skia.Bitmap
import org.jetbrains.skia.Canvas
import org.jetbrains.skia.ColorAlphaType
import org.jetbrains.skia.Image
import org.jetbrains.skia.ImageInfo
import java.awt.Transparency
import java.awt.color.ColorSpace
import java.awt.image.BufferedImage
import java.awt.image.ComponentColorModel
import java.awt.image.DataBuffer
import java.awt.image.DataBufferByte
import java.awt.image.Raster
import java.io.File
import javax.imageio.IIOImage
import javax.imageio.ImageIO
import javax.imageio.ImageIO.getImageWritersByFormatName
import javax.imageio.ImageTypeSpecifier.createFromBufferedImageType
import javax.imageio.ImageWriter

object SaveImageInteractor {

    fun saveToPngFile(
        image: Image,
        output: File,
        dpi: Float
    ) {
        output.delete()
        val iw: Iterator<ImageWriter> = getImageWritersByFormatName("png")
        while (iw.hasNext()) {
            val writer: ImageWriter = iw.next()
            val writeParam = writer.defaultWriteParam
            val typeSpecifier = createFromBufferedImageType(BufferedImage.TYPE_INT_RGB)
            val metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam)
            if (metadata.isReadOnly || !metadata.isStandardMetadataFormatSupported) {
                continue
            }
            metadata.setDPI(dpi)
            ImageIO.createImageOutputStream(output).use { stream ->
                writer.output = stream
                writer.write(
                    metadata,
                    IIOImage(image.toBufferedImage(), null, metadata),
                    writeParam
                )
            }
            break
        }
    }

    private fun Image.toBufferedImage(): BufferedImage {
        val storage = Bitmap()
        storage.allocPixelsFlags(ImageInfo.makeS32(this.width, this.height, ColorAlphaType.PREMUL), false)
        Canvas(storage).drawImage(this, 0f, 0f)

        val bytes = storage.readPixels(storage.imageInfo, (this.width * 4), 0, 0)!!
        val buffer = DataBufferByte(bytes, bytes.size)
        val raster = Raster.createInterleavedRaster(
            buffer,
            this.width,
            this.height,
            this.width * 4, 4,
            intArrayOf(2, 1, 0, 3),     // BGRA order
            null
        )
        val colorModel = ComponentColorModel(
            ColorSpace.getInstance(ColorSpace.CS_sRGB),
            true,
            false,
            Transparency.TRANSLUCENT,
            DataBuffer.TYPE_BYTE
        )

        return BufferedImage(colorModel, raster!!, false, null)
    }
}