package ru.pocketbyte.bcengine.export.image

import org.w3c.dom.Node
import org.w3c.dom.NodeList
import ru.pocketbyte.bcengine.core.config.ProjectLayout
import javax.imageio.metadata.IIOMetadata
import javax.imageio.metadata.IIOMetadataNode


fun IIOMetadata.setDPI(dpi: Float) {
    // for PMG, it's dots per millimeter
    val dotsPerMilli: Double = 1.0 * dpi * ProjectLayout.INC_TO_MM
    val horiz = IIOMetadataNode("HorizontalPixelSize")
    horiz.setAttribute("value", dotsPerMilli.toString())
    val vert = IIOMetadataNode("VerticalPixelSize")
    vert.setAttribute("value", dotsPerMilli.toString())
    val dim = IIOMetadataNode("Dimension")
    dim.appendChild(horiz)
    dim.appendChild(vert)
    val root = IIOMetadataNode("javax_imageio_1.0")
    root.appendChild(dim)
    mergeTree("javax_imageio_1.0", root)
}

fun IIOMetadata.getDPI(): Float {
    val root = getAsTree("javax_imageio_1.0") as? IIOMetadataNode
        ?: return ProjectLayout.DEFAULT_DPI

    val dim = root.childNodes.find { it.nodeName == "Dimension" } as? IIOMetadataNode
        ?: return ProjectLayout.DEFAULT_DPI

    val horiz = dim.find { it.nodeName == "HorizontalPixelSize" } as? IIOMetadataNode
        ?: return ProjectLayout.DEFAULT_DPI

    val vert = dim.find { it.nodeName == "VerticalPixelSize" } as? IIOMetadataNode
        ?: return ProjectLayout.DEFAULT_DPI

    if (horiz.getAttribute("value") != vert.getAttribute("value")) {
        return ProjectLayout.DEFAULT_DPI
    }

    val dotsPerMilli = horiz.getAttribute("value").toFloatOrNull()
        ?: return ProjectLayout.DEFAULT_DPI

    return dotsPerMilli / ProjectLayout.INC_TO_MM
}

private inline fun NodeList.find(condition: (item: Node) -> Boolean): Node? {
    for (i in 0 until length) {
        if (condition(item(i))) {
            return item(i)
        }
    }
    return null
}