package ru.pocketbyte.bcengine.export.pdf

import ru.pocketbyte.bcengine.core.PhysicalDimension

data class Paddings(
    val left: Float = 19f,
    val right: Float = 13.2f,
    val top: Float = 16f,
    val bottom: Float = 16f,
    val horizontalSpace: Float = 0f,
    val verticalSpace: Float = 0f,
    var dimension: PhysicalDimension = PhysicalDimension.MILLIMETER
)