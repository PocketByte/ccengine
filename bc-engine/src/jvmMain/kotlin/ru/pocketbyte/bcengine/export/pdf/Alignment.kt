package ru.pocketbyte.bcengine.export.pdf

enum class Alignment {
    START,
    END,
    CENTER
}