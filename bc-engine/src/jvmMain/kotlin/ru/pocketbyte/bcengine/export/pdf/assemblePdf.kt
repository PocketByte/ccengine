package ru.pocketbyte.bcengine.export.pdf

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.config.ProjectLayout
import ru.pocketbyte.bcengine.core.config.output.PdfOutput
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.forEachIndexed
import java.io.File

fun assemblePdf(
    projectConfig: ProjectConfig,
    pdfOutput: PdfOutput,
    context: Context,
    dataSet: DataSet
) {
    val dpiKoef = JvmPdfModel.PDF_DPI / projectConfig.layout.dpi
    val cardWidth = projectConfig.layout.widthPx() * dpiKoef
    val cardHeight = projectConfig.layout.heightPx() * dpiKoef

    val paddings = pdfOutput.paddings
    val pdfModel = JvmPdfModel(
        pdfOutput.pageSize,
        projectConfig.name,
        paddingLeft = ProjectLayout.projectPx(paddings.left, paddings.dimension, JvmPdfModel.PDF_DPI),
        paddingRight = ProjectLayout.projectPx(paddings.right, paddings.dimension, JvmPdfModel.PDF_DPI),
        paddingTop = ProjectLayout.projectPx(paddings.top, paddings.dimension, JvmPdfModel.PDF_DPI),
        paddingBottom = ProjectLayout.projectPx(paddings.bottom, paddings.dimension, JvmPdfModel.PDF_DPI),
        horizontalSpace = ProjectLayout.projectPx(paddings.horizontalSpace, paddings.dimension, JvmPdfModel.PDF_DPI),
        horizontalAlignment = pdfOutput.horizontalAlignment,
        verticalSpace = ProjectLayout.projectPx(paddings.verticalSpace, paddings.dimension, JvmPdfModel.PDF_DPI),
    )
    dataSet.forEachIndexed { index, item ->
        pdfModel.drawCard(
            projectConfig.output.deck.getItemFile(context, index, item),
            cardWidth, cardHeight
        )
    }
    pdfModel.saveAndClose(pdfOutput.getFile(context))
}