package ru.pocketbyte.bcengine

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import ru.pocketbyte.bcengine.compose.DataSetItemCompositionLocal
import ru.pocketbyte.bcengine.compose.ProjectConfigCompositionLocal
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.config.ProjectLayout
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

@Composable
fun bcEnginePreview(
    dataSetItem: DataSetItem,
    projectConfig: ProjectConfig,
    view: @Composable BoxScope.() -> Unit
) {
    CompositionLocalProvider(
        DataSetItemCompositionLocal provides dataSetItem,
        ProjectConfigCompositionLocal provides projectConfig
    ) {
        Box(Modifier
            .fillMaxSize()
            .background(Color.Gray)
        ) {
            Box(
                modifier = Modifier
                    .size(projectConfig.layout.widthDp(), projectConfig.layout.heightDp())
                    .align(Alignment.Center)
                    .background(Color.White)
                    .clip(RectangleShape),
                content = view
            )
        }
    }
}