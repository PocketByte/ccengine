package ru.pocketbyte.bcengine.utils

fun Int.toString(length: Int): String {
    val intString = this.toString()
    val builder = StringBuilder()
    for (i in intString.length until length) {
        builder.append("0")
    }

    return builder
        .append(intString)
        .toString()
}