package ru.pocketbyte.bcengine.utils

import java.io.File
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.utils.io.*
import io.ktor.utils.io.jvm.javaio.*
import kotlinx.coroutines.runBlocking

fun InputStringIterator.Companion.from(path: String): InputStringIterator {
    val file = File(path)
    return if (file.exists()) {
        fromFile(path)
    } else {
        fromUrl(path)
    }
}

fun InputStringIterator.Companion.fromUrl(urlString: String): InputStringIterator {
    return runBlocking {
        val client = HttpClient(CIO)
        val response: HttpResponse = client.get(urlString)
        val channel: ByteReadChannel = response.body()
        return@runBlocking InputStringIterator(channel.toInputStream())
    }
}

fun InputStringIterator.Companion.fromFile(file: File): InputStringIterator {
    return InputStringIterator(file.inputStream())
}

fun InputStringIterator.Companion.fromFile(filePath: String): InputStringIterator {
    return fromFile(File(filePath))
}