package ru.pocketbyte.bcengine.utils

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class InputStringIterator(
    inputStream: InputStream
): Iterator<String> {

    private val reader = BufferedReader(InputStreamReader(inputStream))
    private var currentLine: String? = reader.readLine()

    override fun hasNext(): Boolean {
        return currentLine != null
    }

    override fun next(): String {
        val result = currentLine
        currentLine = reader.readLine()
        return result ?: throw IndexOutOfBoundsException()
    }

    companion object
}