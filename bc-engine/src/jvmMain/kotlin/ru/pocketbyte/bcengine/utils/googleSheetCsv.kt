package ru.pocketbyte.bcengine.utils

fun googleSheetCsv(scheetId: String, workscheetId: String): String {
    return "https://docs.google.com/spreadsheets/d/${
        scheetId
    }/export?format=csv&gid=${
        workscheetId
    }"
}