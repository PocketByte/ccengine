package ru.pocketbyte.bcengine.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.MapDictionary

val DictionaryCompositionLocal = compositionLocalOf<Dictionary> {
    MapDictionary(emptyMap())
}

@Composable
fun dict(key: String, ifEmpty: String = key): String {
    return DictionaryCompositionLocal.current
        .get(key)
        ?.ifBlank { ifEmpty }
        ?: ifEmpty
}

