package ru.pocketbyte.bcengine.compose.formatter

import androidx.compose.runtime.Composable
import ru.pocketbyte.bcengine.compose.DictionaryCompositionLocal
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.get

open class HtmlTextFormatter private constructor(
    private val transforms: List<Transform>
) {

    sealed interface Transform {
        class Text(
            val regex: Regex,
            val action: TextTransformAction
        ) : Transform

        class Tag(
            val name: String,
            val action: TagTransformAction
        ) : Transform
    }

    private val cachedDictionary = ThreadLocal<Dictionary?>()
    private var cachedContext = ThreadLocal<TransformContext?>()

    @Composable
    fun formatText(text: String): String {
        return formatText(
            context = DictionaryCompositionLocal.current.asContext(),
            text = text
        )
    }

    open fun formatText(context: TransformContext, text: String): String {
        var result = text
        transforms.forEach { transform ->
            result = when (transform) {
                is Transform.Text -> result.applyTransform(context, transform)
                is Transform.Tag -> result.applyTransform(context, transform)
            }
        }
        return result
    }

    private fun String.applyTransform(
        context: TransformContext,
        textTransform: Transform.Text,
    ): String {
        return replace(textTransform.regex) { match ->
            textTransform.action.transform(context, match.groupValues[0], match.groupValues)
        }
    }

    private fun String.applyTransform(
        context: TransformContext,
        tagTransform: Transform.Tag,
    ): String {
        var result = this
        val openingTag = "<${tagTransform.name}>"
        val closingTag = "</${tagTransform.name}>"
        var openingIndex = result.indexOf(openingTag)
        while (openingIndex >= 0) {
            val closingIndex = result.indexOf(closingTag, openingIndex)
            if (closingIndex >= 0) {
                val substring = result.substring(openingIndex + openingTag.length, closingIndex)

                result = result.replaceRange(
                    openingIndex,
                    closingIndex + closingTag.length,
                    tagTransform.action.transform(context, substring)
                )
            } else {
                result = result.replaceRange(
                    openingIndex,
                    openingIndex + openingTag.length,
                    tagTransform.action.transform(context, null)
                )
            }
            openingIndex = result.indexOf(openingTag, openingIndex)
        }
        return result
    }

    // TODO: Potential memory leeks here.
    private fun Dictionary.asContext(): TransformContext {
        if (cachedDictionary.get() == this) {
            cachedContext.get()?.let { return it }
        }
        cachedDictionary.set(this)
        return object : TransformContext {
            override fun dict(key: String, fallback: String): String {
                return this@asContext.get(key, fallback)
            }
        }.also {
            cachedContext.set(it)
        }
    }

    class Builder {

        private val transforms = mutableListOf<Transform>()

        private val textTransform = TextsTransformBuilder(transforms)
        private val objectsTransform = ObjectsTransformBuilder(textTransform)
        private val tagsTransform = TagsTransformBuilder(transforms)

        fun objects(objectsConfig: ObjectsTransformBuilder.() -> Unit) {
            objectsTransform.apply(objectsConfig)
        }

        fun tags(tagsConfig: TagsTransformBuilder.() -> Unit) {
            tagsTransform.apply(tagsConfig)
        }

        fun texts(textsConfig: TextsTransformBuilder.() -> Unit) {
            textTransform.apply(textsConfig)
        }

        fun build(): HtmlTextFormatter {
            return HtmlTextFormatter(transforms)
        }
    }
}

inline fun HtmlTextFormatter(
    config: HtmlTextFormatter.Builder.() -> Unit
): HtmlTextFormatter {
    return HtmlTextFormatter.Builder().apply(config).build()
}
