package ru.pocketbyte.bcengine.compose.formatter

import androidx.compose.ui.graphics.Color
import ru.pocketbyte.bcengine.core.html.htmlFont

class ObjectsTransformBuilder(
    private val textsTransformBuilder: TextsTransformBuilder
) {

    infix fun String.transformTo(value: TransformContext.() -> String) {
        this transformTo static(value)
    }

    infix fun String.transformTo(transform: ObjectTransform) {
        textsTransformBuilder.apply {
            "\\((?:((?:.(?<!\\)))+) )?(${this@transformTo})\\)".toRegex() transformTo
                    TextTransformAction { context, _, groupValues ->
                        transform.transform(
                            context,
                            groupValues.getOrNull(1)?.ifEmpty { null },
                        )
                    }
        }
    }

    fun static(value: TransformContext.() -> String): ObjectTransform {
        return ObjectTransform { context, _ -> value(context) }
    }

    fun fontTransform(
        face: String? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? =  { null }
    ): ObjectTransform {
        @Suppress("CAST_NEVER_SUCCEEDS")
        return fontTransform(face, null as? String, color, withIcon)
    }

    fun fontTransform(
        face: String? = null,
        size: Float? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? =  { null }
    ): ObjectTransform {
        return fontTransform(face, size.toString(), color, withIcon)
    }

    fun fontTransform(
        face: String? = null,
        size: String? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? =  { null }
    ): ObjectTransform {
        return ObjectTransform { context, it ->
            if (it == null) {
                return@ObjectTransform withIcon(context) ?: ""
            }
            htmlFont(it, face, size, color) + (withIcon(context) ?: "")
        }
    }
}
