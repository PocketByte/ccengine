package ru.pocketbyte.bcengine.compose.formatter

internal class ObjectTransformInputMapper(
    private val mappedTransform: ObjectTransform,
    private val mapping: TransformContext.(String?) -> String?
): ObjectTransform {
    override fun transform(context: TransformContext, value: String?): String {
        return mappedTransform.transform(context, mapping(context, value))
    }
}

fun ObjectTransform.mapInput(mapping: TransformContext.(String?) -> String?): ObjectTransform {
    return ObjectTransformInputMapper(this, mapping)
}
