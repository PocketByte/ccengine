package ru.pocketbyte.bcengine.compose.formatter

interface TransformContext {
    fun dict(key: String, fallback: String = key): String
}