package ru.pocketbyte.bcengine.compose.formatter

fun interface TextTransformAction {
    fun transform(context: TransformContext, value: String, groupValues: List<String>): String
}