package ru.pocketbyte.bcengine.compose.htmldecoder

enum class FontSizeOperator(
    val char: Char,
    val operation: (fontSizeSp: Float, value: Float) -> Float
) {
    PLUS('+', { fontSizeSp, floatValue -> fontSizeSp + floatValue }),
    MINUS('-', { fontSizeSp, floatValue -> fontSizeSp - floatValue }),
    MULTIPLY('*', { fontSizeSp, floatValue -> fontSizeSp * floatValue }),
    DIVIDE('/', { fontSizeSp, floatValue -> fontSizeSp / floatValue });
}