package ru.pocketbyte.bcengine.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.ComposeScene
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.delay
import org.jetbrains.skia.Image
import org.jetbrains.skia.Surface
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.forEachIndexed
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

@OptIn(ExperimentalComposeUiApi::class)
class BcScene(
    val width: Int,
    val height: Int,
    val density: Density = Density(1f),
    val layoutDirection: LayoutDirection = LayoutDirection.Ltr,
    private val content: @Composable () -> Unit
) {
    var exception: Throwable? = null
        private set

    private val surface = Surface.makeRasterN32Premul(width, height)
    private val composeScene = ComposeScene(
        coroutineContext = CoroutineExceptionHandler { _, throwable ->
            exception = throwable
        },
        density = density,
        layoutDirection = layoutDirection
    )

    suspend fun process(
        dataSet: DataSet,
        processor: (index: Int, dataSetItem: DataSetItem, snapshot: Image) -> Unit
    ) {
        var currentDataSetItem by mutableStateOf<DataSetItem?>(null)

        composeScene.setContent {
            val dataSetItem = currentDataSetItem
            if (dataSetItem != null) {
                CompositionLocalProvider(
                    DataSetItemCompositionLocal provides dataSetItem
                ) {
                    content()
                }
            }
        }

        dataSet.forEachIndexed { index: Int, item: DataSetItem ->
            currentDataSetItem = item

            //FIXME: Hot fix for situation when rendering render previous data item.
            // This fix gives to 'currentDataSetItem' some time to notify 'composeScene'.
            delay(50)

            surface.canvas.clear(Color.Transparent.value.toInt())
            composeScene.render(surface.canvas, 0L)

            if (exception != null) {
                return
            }

            processor(index, item, surface.makeImageSnapshot())
        }
    }
}
