package ru.pocketbyte.bcengine.compose.htmldecoder

class HtmlDecodeResult(
    val string: String,
    val attrs: List<HtmlAttribute>
)