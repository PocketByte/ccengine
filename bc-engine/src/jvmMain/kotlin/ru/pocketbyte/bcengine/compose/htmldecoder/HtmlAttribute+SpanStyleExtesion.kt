package ru.pocketbyte.bcengine.compose.htmldecoder

import androidx.compose.runtime.Composable
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.platform.Typeface
import androidx.compose.ui.text.style.BaselineShift
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.sp
import org.jetbrains.skia.FontMgr
import ru.pocketbyte.bcengine.core.color.ColorCoder

@Composable
fun List<HtmlAttribute>.asRangeSpanStyles(): List<AnnotatedString.Range<SpanStyle>> {
    return mapNotNull {
        it.asRangeSpanStyle()
    }
}

@Composable
fun HtmlAttribute.asRangeSpanStyle(): AnnotatedString.Range<SpanStyle>? {
    return asSpanStyle()?.let {
        AnnotatedString.Range(it, startIndex, endIndex)
    }
}

@Composable
fun HtmlAttribute.asSpanStyle(): SpanStyle? {
    return when (tag) {
        is HTMLTagImage,
        is HTMLTagBr,
        is HTMLTagQ -> {
            // Do Nothing
            null
        }
        is HTMLTagFont -> {
            val family = getStringAttribute(HTMLTagFont.face)?.let {
                findFontFamily(it)
            }
            val size = getFloatAttribute(HTMLTagFont.size)?.let {
                TextUnit(it, TextUnitType.Sp)
            }
            val color = getStringAttribute(HTMLTagFont.color)?.let {
                ColorCoder.decode(it.trim())
            }
            SpanStyle(
                color = color ?: androidx.compose.ui.graphics.Color.Unspecified,
                fontSize = size ?: TextUnit.Unspecified,
                fontFamily = family
            )
        }
        is HTMLTagB -> {
            SpanStyle(
                fontWeight = FontWeight.Bold
            )
        }
        is HTMLTagI -> {
            SpanStyle(
                fontStyle = FontStyle.Italic
            )
        }
        is HTMLTagS -> {
            SpanStyle(
                textDecoration = TextDecoration.LineThrough
            )
        }
        is HTMLTagU -> {
            SpanStyle(
                textDecoration = TextDecoration.Underline
            )
        }
        is HTMLTagSup -> {
            SpanStyle(
                fontSize = 5.sp,
                baselineShift = BaselineShift.Superscript
            )
        }
        is HTMLTagSub -> {
            SpanStyle(
                baselineShift = BaselineShift.Subscript
            )
        }
    }
}

private val fontCache = mutableMapOf<String, FontFamily?>()

private fun findFontFamily(
    familyName: String,
    fontStyle: org.jetbrains.skia.FontStyle = org.jetbrains.skia.FontStyle.NORMAL
): FontFamily? {
    fontCache[familyName]?.let { return it }

    val skStyle = FontMgr.default
        .matchFamilyStyle(familyName, fontStyle)
        ?: return null

    return Typeface(skStyle, "${familyName}_Style:${fontStyle._value}")
        .fontFamily.apply {
            fontCache[familyName] = this
        }
}
