package ru.pocketbyte.bcengine.compose.formatter

fun interface ObjectTransform {
    fun transform(context: TransformContext, value: String?): String
}
