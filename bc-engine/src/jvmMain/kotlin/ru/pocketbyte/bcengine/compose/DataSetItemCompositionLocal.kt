package ru.pocketbyte.bcengine.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.compositionLocalOf
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

val DataSetItemCompositionLocal = compositionLocalOf<DataSetItem> {
    throw IllegalStateException("DataSetItemCompositionLocal must be initialised by CompositionLocalProvider")
}

@Composable
fun data(key: String, ifEmpty: String = key): String {
    return DataSetItemCompositionLocal.current
        .get(key)
        ?.ifBlank { ifEmpty }
        ?: ifEmpty
}

@Composable
fun dataOrNull(key: String): String? {
    return DataSetItemCompositionLocal.current
        .get(key)
        ?.ifBlank { null }
}