package ru.pocketbyte.bcengine.compose

import androidx.compose.runtime.compositionLocalOf
import ru.pocketbyte.bcengine.core.config.ProjectConfig

val ProjectConfigCompositionLocal = compositionLocalOf<ProjectConfig> {
    throw IllegalStateException("ProjectConfigCompositionLocal must be initialised by CompositionLocalProvider")
}
