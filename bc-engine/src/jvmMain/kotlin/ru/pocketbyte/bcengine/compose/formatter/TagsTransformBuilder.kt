package ru.pocketbyte.bcengine.compose.formatter

import androidx.compose.ui.graphics.Color
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.html.htmlFont

class TagsTransformBuilder(
    private val transforms: MutableList<HtmlTextFormatter.Transform>
) {

    infix fun String.transformTo(value: TransformContext.() -> String) {
        this transformTo static(value)
    }

    infix fun String.transformTo(transform: TagTransformAction) {
        transforms.add(HtmlTextFormatter.Transform.Tag(this, transform))
    }

    fun static(value: TransformContext.() -> String): TagTransformAction {
        return TagTransformAction { context, _ -> value(context) }
    }

    fun fontTransform(
        face: String? = null,
        color: Color? = null
    ): TagTransformAction {
        @Suppress("CAST_NEVER_SUCCEEDS")
        return fontTransform(face, null as? String, color)
    }

    fun fontTransform(
        face: String? = null,
        size: Float? = null,
        color: Color? = null
    ): TagTransformAction {
        return fontTransform(face, size.toString(), color)
    }

    fun fontTransform(
        face: String? = null,
        size: String? = null,
        color: Color? = null
    ): TagTransformAction {
        return TagTransformAction { _, it ->
            if (it == null) {
                return@TagTransformAction ""
            }
            htmlFont(it, face, size, color)
        }
    }
}
