package ru.pocketbyte.bcengine.compose.htmldecoder

fun List<HtmlAttribute>.mapAttributes(fontSize: Float): List<HtmlAttribute> {
    return map { htmlAttribute ->
        when (htmlAttribute.tag) {
            HTMLTagImage -> {
                htmlAttribute.mapImageTag(fontSize)
            }
            HTMLTagFont -> {
                htmlAttribute.mapFontTag(fontSize)
            }
            else -> {
                return@map htmlAttribute
            }
        }
    }
}

private fun HtmlAttribute.mapImageTag(fontSize: Float): HtmlAttribute {
    return this.copy(
        attributes = this.attributes?.toMutableMap()?.apply {
            // Put values one more time to create entries with null values
            // if entries doesn't exists
            put(HTMLTagImage.width, getValue(HTMLTagImage.width))
            put(HTMLTagImage.height, getValue(HTMLTagImage.height))
        }?.mapValues { entry ->
            when(entry.key) {
//                HTMLTagImage.src -> {
//                    getStringAttribute(HTMLTagImage.src)
//                        ?.let { project.icons.getIcon(it) }
//                        ?.image
//                        ?.get(project, dataItem)
//                        ?: entry.value
//                }
                HTMLTagImage.width, HTMLTagImage.height -> {
                    entry.value?.toString()
                        ?.trim()
                        ?.mapFontSize(fontSize)
                        ?: fontSize
                }
                HTMLTagImage.advanceWidth, HTMLTagImage.advanceHeight -> {
                    entry.value
                        ?.toString()
                        ?.toFloatOrNull()
                        ?.let { if (it == 0f) null else it }
                }
                else -> entry.value
            }
        }
    )
}

private fun HtmlAttribute.mapFontTag(fontSize: Float): HtmlAttribute {
    return this.copy(
        attributes = this.attributes?.mapValues { entry ->
            if (entry.key == HTMLTagFont.size) {
                entry.value?.toString()
                    ?.trim()
                    ?.mapFontSize(fontSize)
            } else {
                entry.value
            }
        }
    )
}
private fun String.mapFontSize(fontSize: Float): Float? {
    if (this.isBlank()) {
        return null
    }

    if (first().isDigit() || first() == '.') {
        return toFloatOrNull()
    }

    val floatValue = this
        .substring(1)
        .toFloatOrNull()
        ?: return null

    return FontSizeOperator.entries
        .find { it.char == first() }
        ?.let { it.operation(fontSize, floatValue) }
}