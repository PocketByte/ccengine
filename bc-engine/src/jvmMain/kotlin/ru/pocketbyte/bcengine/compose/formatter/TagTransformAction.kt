package ru.pocketbyte.bcengine.compose.formatter

fun interface TagTransformAction {
    fun transform(context: TransformContext, value: String?): String
}
