package ru.pocketbyte.bcengine.compose.htmldecoder

import androidx.compose.ui.text.Placeholder
import androidx.compose.ui.text.PlaceholderVerticalAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType

data class ImageAttribute(
    val src: String,
    val width: Float,
    val height: Float,
    val align: String?,
    val originX: Float,
    val originY: Float,
    val advanceWidth: Float,
    val advanceHeight: Float,
)

fun HtmlAttribute.asImageAttribute(): ImageAttribute? {
    if (tag != HTMLTagImage) {
        return null
    }

    return ImageAttribute(
        getStringAttribute(HTMLTagImage.src)
            ?: throw RuntimeException("Missing src attribute"),
        getFloatAttribute(HTMLTagImage.width) ?: 20f,
        getFloatAttribute(HTMLTagImage.height) ?: 20f,
        getStringAttribute(HTMLTagImage.align),
        getFloatAttribute(HTMLTagImage.originX) ?: 0f,
        getFloatAttribute(HTMLTagImage.originY) ?: 0f,
        getFloatAttribute(HTMLTagImage.advanceWidth) ?: 1f,
        getFloatAttribute(HTMLTagImage.advanceHeight) ?: 1f
    )
}

fun ImageAttribute.asPlaceholder(): Placeholder {
    return Placeholder(
        TextUnit(width * advanceWidth, TextUnitType.Sp),
        TextUnit(height * advanceHeight, TextUnitType.Sp),
        align.toPlaceholderVerticalAlign()
    )
}

private fun String?.toPlaceholderVerticalAlign(): PlaceholderVerticalAlign {
    return when (this?.lowercase()){
        "baseline" -> PlaceholderVerticalAlign.AboveBaseline
        "top" -> PlaceholderVerticalAlign.Top
        "bottom" -> PlaceholderVerticalAlign.Bottom
        "center" -> PlaceholderVerticalAlign.Center
        "texttop" -> PlaceholderVerticalAlign.TextTop
        "textbottom" -> PlaceholderVerticalAlign.TextBottom
        "textcenter" -> PlaceholderVerticalAlign.TextCenter
        else -> PlaceholderVerticalAlign.AboveBaseline
    }
}