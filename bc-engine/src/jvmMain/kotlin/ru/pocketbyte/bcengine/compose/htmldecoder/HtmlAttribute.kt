package ru.pocketbyte.bcengine.compose.htmldecoder

data class HtmlAttribute(
    val tag: HTMLTag,
    val attributes: Map<String, Any?>?,
    val startIndex: Int,
    val endIndex: Int
) {

    fun getAttribute(attribute: String): Any? {
        return attributes?.get(attribute)
    }

    fun getStringAttribute(attribute: String): String? {
        return getAttribute(attribute)?.toString()
    }

    fun getFloatAttribute(attribute: String): Float? {
        return getAttribute(attribute)?.let { value ->
            if (value is Float) {
                return value
            }
            value.toString()
                .toFloatOrNull()
                .let { if (it?.isNaN() == true) null else it }
        }
    }
}