package ru.pocketbyte.bcengine.compose.formatter

import androidx.compose.ui.graphics.Color
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.html.htmlFont

class TextsTransformBuilder(
    private val transforms: MutableList<HtmlTextFormatter.Transform>
) {

    infix fun String.transformTo(value: TransformContext.() -> String) {
        this transformTo static(value)
    }

    infix fun String.transformTo(transform: TextTransformAction) {
        this.toRegex() transformTo transform
    }

    infix fun Regex.transformTo(value: TransformContext.() -> String) {
        this transformTo static(value)
    }

    infix fun Regex.transformTo(transform: TextTransformAction) {
        transforms.add(HtmlTextFormatter.Transform.Text(this, transform))
    }

    fun static(value: TransformContext.() -> String): TextTransformAction {
        return TextTransformAction { context, _, _ -> value(context) }
    }

    fun fontTransform(
        face: String? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? = { null }
    ): TextTransformAction {
        @Suppress("CAST_NEVER_SUCCEEDS")
        return fontTransform(face, null as? String, color, withIcon)
    }

    fun fontTransform(
        face: String? = null,
        size: Float? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? = { null }
    ): TextTransformAction {
        return fontTransform(face, size.toString(), color, withIcon)
    }

    fun fontTransform(
        face: String? = null,
        size: String? = null,
        color: Color? = null,
        withIcon: TransformContext.() -> String? = { null }
    ): TextTransformAction {
        return TextTransformAction { context, value, _ ->
            htmlFont(value, face, size, color) + (withIcon(context) ?: "")
        }
    }
}
