package ru.pocketbyte.bcengine.compose.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.foundation.text.appendInlineContent
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.isUnspecified
import ru.pocketbyte.bcengine.compose.htmldecoder.asRangeSpanStyle
import ru.pocketbyte.bcengine.compose.htmldecoder.HTMLTagImage
import ru.pocketbyte.bcengine.compose.htmldecoder.HtmlStringDecoder
import ru.pocketbyte.bcengine.compose.htmldecoder.asImageAttribute
import ru.pocketbyte.bcengine.compose.htmldecoder.asPlaceholder
import ru.pocketbyte.bcengine.compose.htmldecoder.mapAttributes

@Composable
fun HtmlText(
    text: String,
    modifier: Modifier = Modifier,
    color: Color = Color.Unspecified,
    fontSize: TextUnit = TextUnit.Unspecified,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    onTextLayout: ((TextLayoutResult) -> Unit)? = null,
    style: TextStyle = LocalTextStyle.current
) {
    val decodeResult = HtmlStringDecoder.decode(text)
    val fontFloatSize = if (fontSize.isUnspecified) {
        style.fontSize.value
    } else {
        fontSize.value
    }
    val htmlAttributes = decodeResult.attrs.mapAttributes(fontFloatSize)

    val inlineContent = mutableMapOf<String, InlineTextContent>()
    val annotatedText = buildAnnotatedString {
        var index = 0
        var imageIndex = 0
        htmlAttributes.forEach { attribute ->
            if (attribute.tag is HTMLTagImage) {
                if (attribute.startIndex > index) {
                    append(decodeResult.string, index, attribute.startIndex)
                }
                index = attribute.startIndex + HTMLTagImage.startString.length

                attribute.asImageAttribute()?.let { imageAttribute ->
                    appendInlineContent("img_$imageIndex")
                    inlineContent["img_$imageIndex"] = InlineTextContent(
                        imageAttribute.asPlaceholder()
                    ) {
                        val offsetX = if (imageAttribute.advanceWidth < 1f) {
                            imageAttribute.width * ((1 - imageAttribute.advanceWidth) / 2f + imageAttribute.originX)
                        } else {
                            imageAttribute.width * imageAttribute.originX
                        }
                        val offsetY = if (imageAttribute.advanceHeight < 1f) {
                            imageAttribute.height * ((imageAttribute.advanceHeight - 1) / 2f + imageAttribute.originY)
                        } else {
                            imageAttribute.height * (imageAttribute.advanceHeight - 1 + imageAttribute.originY)
                        }
                        Image(
                            modifier = Modifier
                                .requiredSize(
                                    width = imageAttribute.width.dp,
                                    height = imageAttribute.height.dp
                                ).offset(
                                    x = offsetX.dp,
                                    y = offsetY.dp,
                                ),
                            painter = painterResource(imageAttribute.src),
                            contentScale = ContentScale.FillBounds,
                            contentDescription = ""
                        )
                    }
                    imageIndex += 1
                }
            } else {
                attribute.asRangeSpanStyle()?.let {
                    addStyle(it.item, it.start, it.end)
                }
            }
        }

        append(decodeResult.string, index, decodeResult.string.length)
    }

    Text(
        annotatedText, modifier, color,
        fontSize, fontStyle, fontWeight, fontFamily,
        letterSpacing, textDecoration, textAlign,
        lineHeight, overflow, softWrap,
        maxLines, minLines, inlineContent,
        { layoutResult ->
            onTextLayout?.let { it(layoutResult) }
        }, style
    )
}