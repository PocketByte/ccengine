package ru.pocketbyte.bcengine

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.MonotonicFrameClock
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield
import ru.pocketbyte.bcengine.compose.BcScene
import ru.pocketbyte.bcengine.compose.DictionaryCompositionLocal
import ru.pocketbyte.bcengine.compose.ProjectConfigCompositionLocal
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.config.ProjectLayout
import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.export.image.SaveImageInteractor
import ru.pocketbyte.bcengine.export.pdf.assemblePdf
import ru.pocketbyte.bcengine.export.single.assembleSingleImage

fun bcEngineApplication(
    projectConfig: ProjectConfig,
    onComplete: (context: Context, dataSet: DataSet) -> Throwable? = { _, _ -> null }
): Throwable? {
    val context = Context("./build/bcengine/", "./build/tmp/")

    val dataSet = projectConfig.dataSet.get(context, projectConfig)
    val dictionary = projectConfig.dictionary.get(context, projectConfig)

    return runBlocking {
        withContext(YieldFrameClock) {
            val layout = projectConfig.layout
            val scene = BcScene(
                layout.widthPx(),
                layout.heightPx(),
                density = Density(layout.dpi / ProjectLayout.DEFAULT_DPI),
                layoutDirection = LayoutDirection.Ltr,
                content = {
                    CompositionLocalProvider(
                        ProjectConfigCompositionLocal provides projectConfig,
                        DictionaryCompositionLocal provides dictionary
                    ) {
                        Box(
                            modifier = Modifier
                                .size(
                                    width = layout.widthDp(),
                                    height = layout.heightDp()
                                )
                                .background(layout.backgroundColor),
                            content = projectConfig.view
                        )
                    }
                }
            )

            scene.process(dataSet) { index, dataSetItem, snapshot ->
                SaveImageInteractor.saveToPngFile(
                    snapshot,
                    projectConfig.output.deck.getItemFile(context, index, dataSetItem),
                    projectConfig.layout.dpi
                )
            }

            scene.exception?.let {
                return@withContext it
            }

            projectConfig.output.singleImage?.let {
                assembleSingleImage(projectConfig, it, context, dataSet)
            }

            projectConfig.output.pdf?.let {
                assemblePdf(projectConfig, it, context, dataSet)
            }

            onComplete(context, dataSet)
        }
    }
}

private object YieldFrameClock : MonotonicFrameClock {
    override suspend fun <R> withFrameNanos(
        onFrame: (frameTimeNanos: Long) -> R
    ): R {
        // We call `yield` to avoid blocking UI thread. If we don't call this then application
        // can be frozen for the user in some cases as it will not receive any input events.
        //
        // Swing dispatcher will process all pending events and resume after `yield`.
        yield()
        return onFrame(System.nanoTime())
    }
}