package ru.pocketbyte.bcengine.core.dataset.factory.csv

class RowDecodeException(
    val position: Int,
    message: String = ""
): Exception(message)