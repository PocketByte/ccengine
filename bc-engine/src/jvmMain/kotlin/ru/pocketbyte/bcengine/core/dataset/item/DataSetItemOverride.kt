package ru.pocketbyte.bcengine.core.dataset.item

open class DataSetItemOverride(
    private val dataSetItem: DataSetItem,
    private val overrideMap: Map<String, String>
) : DataSetItem {
    override fun get(key: String): String? {
        return overrideMap[key] ?: dataSetItem.get(key)
    }
}
