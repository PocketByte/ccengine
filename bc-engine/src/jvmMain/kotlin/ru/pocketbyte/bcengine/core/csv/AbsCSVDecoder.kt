package ru.pocketbyte.bcengine.core.csv

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.factory.csv.RowDecodeException

abstract class AbsCSVDecoder<Type> {

    companion object {
        private const val QUOTES_CHAR = '"'
        private const val SEPARATOR_CHAR = ','
    }

    abstract fun buildEmptyResult(): Type

    abstract fun decode(
        context: Context,
        project: ProjectConfig,
        headers: List<String>,
        nextRow: () -> MutableMap<String, String>?
    ): Type

    fun decode(
        context: Context,
        project: ProjectConfig,
        stringIterator: Iterator<String>
    ): Type {
        if (!stringIterator.hasNext()) {
            return buildEmptyResult()
        }

        val headers = mutableListOf<String>()
        try {
            visitColumns(readRow(stringIterator)) { _, rowItem ->
                headers.add(rowItem)
            }
        } catch (exception: RowDecodeException) {
            throw RuntimeException(
                "Failed to decode table at 0:${exception.position}.",
                exception
            )
        }

        var tableIndex = 0
        return decode(context, project, headers) {
            if (stringIterator.hasNext()) {
                tableIndex++
                val itemMap = mutableMapOf<String, String>()
                try {
                    visitColumns(readRow(stringIterator)) { columnIndex, columnItem ->
                        itemMap[headers[columnIndex]] = columnItem
                    }
                } catch (exception: RowDecodeException) {
                    throw RuntimeException(
                        "Failed to decode table at ${tableIndex}:${exception.position}",
                        exception
                    )
                }

                itemMap
            } else {
                null
            }
        }
    }

    private fun readRow(rows: Iterator<String>): String {
        val builder = StringBuilder(rows.next())
        var quotesCount = builder.quotesCount()

        while (rows.hasNext() && quotesCount % 2 != 0) {
            builder.append("\n")
            rows.next().apply {
                quotesCount += this.quotesCount()
                builder.append(this)
            }
        }
        return builder.toString()
    }

    @Throws(RowDecodeException::class)
    private fun visitColumns(
        row: String,
        visitor: (columnIndex: Int, columnItem: String) -> Unit
    ) {
        var index = 0
        var itemBuilder = StringBuilder()
        var previousCharIsQuotes = false
        var isQuotesStart = false
        row.forEachIndexed { charIndex, char ->
            if (char == QUOTES_CHAR) {
                if (!isQuotesStart) {
                    isQuotesStart = true
                    previousCharIsQuotes = false
                    if (itemBuilder.trim().isNotEmpty()) {
                        throw RowDecodeException(
                            charIndex,
                            "Item is $itemBuilder. But empty string expected"
                        )
                    }
                    itemBuilder = StringBuilder()
                } else if (previousCharIsQuotes) {
                    itemBuilder.append(char)
                    previousCharIsQuotes = false
                } else {
                    previousCharIsQuotes = true
                }
            } else if (char == SEPARATOR_CHAR) {
                if (isQuotesStart) {
                    if (previousCharIsQuotes) {
                        visitor(index++, itemBuilder.toString())
                        itemBuilder = StringBuilder()
                        isQuotesStart = false
                        previousCharIsQuotes = false
                    } else {
                        itemBuilder.append(char)
                        previousCharIsQuotes = false
                    }
                } else {
                    visitor(index++, itemBuilder.trim().toString())
                    itemBuilder = StringBuilder()
                    isQuotesStart = false
                    previousCharIsQuotes = false
                }
            } else {
                itemBuilder.append(char)
                previousCharIsQuotes = false
            }
        }
        if (isQuotesStart) {
            if (previousCharIsQuotes) {
                visitor(index, itemBuilder.toString())
            } else {
                throw RowDecodeException(row.length)
            }
        } else {
            visitor(index, itemBuilder.trim().toString())
        }
    }

    private fun CharSequence.quotesCount(): Int {
        var result = 0
        var isEscaping = false
        forEach {
            if (it == QUOTES_CHAR && !isEscaping) {
                result++
            }
            isEscaping = it == '\\' && !isEscaping
        }
        return result
    }
}