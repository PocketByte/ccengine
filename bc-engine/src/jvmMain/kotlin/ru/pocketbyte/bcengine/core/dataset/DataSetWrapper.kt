package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

open class DataSetWrapper(
    private val dataSet: DataSet
): DataSet {
    override val size: Int
        get() = dataSet.size

    override fun get(index: Int): DataSetItem {
        return dataSet.get(index)
    }
}
