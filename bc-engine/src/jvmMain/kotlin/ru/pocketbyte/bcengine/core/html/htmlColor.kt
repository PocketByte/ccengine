package ru.pocketbyte.bcengine.core.html

import androidx.compose.ui.graphics.Color
import ru.pocketbyte.bcengine.core.color.ColorCoder

fun htmlColor(color: Color): String {
    return ColorCoder.encode(color)
}