package ru.pocketbyte.bcengine.core.dict

interface Dictionary {
    fun get(key: String): String?
}
