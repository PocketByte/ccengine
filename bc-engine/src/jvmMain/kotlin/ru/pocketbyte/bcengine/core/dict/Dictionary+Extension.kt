package ru.pocketbyte.bcengine.core.dict

fun Dictionary.get(key: String, fallback: String): String {
    return get(key) ?: fallback
}