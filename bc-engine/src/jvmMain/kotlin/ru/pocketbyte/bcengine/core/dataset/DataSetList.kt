package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

class DataSetList(
    private val dataSets: List<DataSet>
) : DataSet {
    override val size: Int
        get() = dataSets.fold(0) { acc, set -> acc + set.size}

    override fun get(index: Int): DataSetItem {
        var itemIndex = index
        var dataSetIndex = 0
        while (itemIndex >= dataSets[dataSetIndex].size) {
            itemIndex -= dataSets[dataSetIndex].size
            dataSetIndex += 1
        }
        return dataSets[dataSetIndex].get(itemIndex)
    }
}