package ru.pocketbyte.bcengine.core.config

import ru.pocketbyte.bcengine.core.config.output.DeckOutput
import ru.pocketbyte.bcengine.core.config.output.PdfOutput
import ru.pocketbyte.bcengine.core.config.output.SingleImageOutput

class ProjectOutputs {

    var dir = "./"

    internal val deck: DeckOutput
        get() = deckOutputInstance ?: DeckOutput.Builder(this)
                .build()
                .apply { deckOutputInstance = this }

    internal val pdf: PdfOutput?
        get() = pdfOutputInstance

    internal val singleImage: SingleImageOutput?
        get() = singleImageOutput

    private var deckOutputInstance: DeckOutput? = null
    private var pdfOutputInstance: PdfOutput? = null
    private var singleImageOutput: SingleImageOutput? = null

    fun deck(configuration: DeckOutput.Builder.() -> Unit) {
        if (deckOutputInstance != null) {
            throw IllegalStateException("Deck already configured!")
        }
        deckOutputInstance = DeckOutput.Builder(this)
            .apply(configuration)
            .build()
    }

    fun pdf() {
        pdf { /* Do nothing */ }
    }

    fun pdf(configuration: PdfOutput.Builder.() -> Unit) {
        if (pdfOutputInstance != null) {
            throw IllegalStateException("Pdf already configured.")
        }
        pdfOutputInstance = PdfOutput.Builder(this)
            .apply(configuration)
            .build()
    }

    fun singleImage() {
        singleImage { /* Do nothing */ }
    }

    fun singleImage(configuration: SingleImageOutput.Builder.() -> Unit) {
        if (singleImageOutput != null) {
            throw IllegalStateException("Single Image already configured.")
        }
        singleImageOutput = SingleImageOutput.Builder(this)
            .apply(configuration)
            .build()
    }

    companion object {
        internal const val PNG_EXTENSION = ".png"
        internal const val PDF_EXTENSION = ".pdf"
    }
}