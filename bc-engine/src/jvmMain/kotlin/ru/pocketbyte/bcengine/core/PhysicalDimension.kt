package ru.pocketbyte.bcengine.core

enum class PhysicalDimension(
    val sign: String
) {
    INCH("inch"),
    MILLIMETER("mm"),
    CENTIMETER("cm")
}