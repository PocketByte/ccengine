package ru.pocketbyte.bcengine.core.dict

class MapDictionary(
    val map: Map<String, String?>
) : Dictionary {
    override fun get(key: String): String? {
        return map[key]
    }
}
