package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.factory.csv.CSVDataSetDecoder
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.utils.InputStringIterator
import ru.pocketbyte.bcengine.utils.from
import ru.pocketbyte.kydra.log.info

class CSVDataSetFactory(
    val path: String
) : DataSetFactory {

    override fun get(context: Context, project: ProjectConfig): DataSet {
        context.logger.info { "Getting CSV DataSet from: $path" }
        return CSVDataSetDecoder.decode(
            context, project,
            InputStringIterator.from(path)
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other is CSVDataSetFactory) {
            return path == other.path
        }
        return false
    }

    override fun hashCode(): Int {
        return path.hashCode()
    }
}