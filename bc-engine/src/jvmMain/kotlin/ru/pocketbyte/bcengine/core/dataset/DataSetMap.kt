package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

class DataSetMap(
    private val dataSet: DataSet,
    private val mapTransform: (DataSetItem) -> DataSetItem
) : DataSet {
    override val size: Int
        get() = dataSet.size

    override fun get(index: Int): DataSetItem {
        return mapTransform(dataSet.get(index))
    }
}
