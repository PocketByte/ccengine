package ru.pocketbyte.bcengine.core.dataset.item

class DataSetItemMerge(
    private val primaryItem: DataSetItem,
    private val secondaryItem: DataSetItem
) : DataSetItem {
    override fun get(key: String): String? {
        return primaryItem.get(key) ?: secondaryItem.get(key)
    }
}
