package ru.pocketbyte.bcengine.core.dataset.factory.csv

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.csv.AbsCSVDecoder
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.ListDataSet

object CSVDataSetDecoder : AbsCSVDecoder<DataSet>() {

    override fun buildEmptyResult(): DataSet {
        return ListDataSet(emptyList())
    }

    override fun decode(
        context: Context,
        project: ProjectConfig,
        headers: List<String>,
        nextRow: () -> MutableMap<String, String>?,
    ): DataSet {
        return TableDataSetDecoder.decode(context, project, headers, nextRow)
    }
}
