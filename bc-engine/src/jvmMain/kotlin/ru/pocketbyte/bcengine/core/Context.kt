package ru.pocketbyte.bcengine.core

import ru.pocketbyte.kydra.log.KydraLog
import ru.pocketbyte.kydra.log.Logger

class Context(
    val workDir: String,
    val tempDir: String
) {

    constructor(copy: Context) : this(copy.workDir, copy.tempDir)

    val logger: Logger = KydraLog
}