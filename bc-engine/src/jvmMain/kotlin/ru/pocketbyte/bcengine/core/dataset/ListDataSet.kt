package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

open class ListDataSet(
    private val list: List<DataSetItem>
): DataSet {
    override val size: Int = list.size

    override fun get(index: Int): DataSetItem {
        return list[index]
    }
}
