package ru.pocketbyte.bcengine.core.dataset.factory.extension

import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.factory.DataSetFactory
import ru.pocketbyte.bcengine.core.dataset.factory.DataSetInflateFactory
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItemOverride

fun DataSetFactory.inflate(
    inflater: (index: Int, item: DataSetItem) -> Collection<DataSetItem>?
): DataSetFactory {
    return DataSetInflateFactory(
        this,
        inflater
    )
}

fun DataSetFactory.inflateAmount(multiply: Int = 1): DataSetFactory {
    var itemIndex = 0
    return inflate { index, item ->
        val amount = item.get(DataSet.AMOUNT)
            .let { it ?: "1" }
            .toIntOrNull() ?: 0

        if (amount > 0) {
            val amountValue = amount.toString()
            val rowIndex = index.toString()
            return@inflate List(amount * multiply) { subIndex ->
                DataSetItemOverride(
                    dataSetItem = item,
                    overrideMap = mapOf(
                        DataSet.AMOUNT to amountValue,
                        DataSet.ROW_INDEX to rowIndex,
                        DataSet.ITEM_INDEX to (itemIndex++).toString(),
                        DataSet.ITEM_SUB_INDEX to subIndex.toString()
                    )
                )
            }
        }
        return@inflate null
    }
}