package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.DataSetMap
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

class DataSetMapFactory(
    private val dataSetFactory: DataSetFactory,
    private val mapTransform: (DataSetItem) -> DataSetItem
) : DataSetFactory {
    override fun get(context: Context, project: ProjectConfig): DataSet {
        return DataSetMap(dataSetFactory.get(context, project), mapTransform)
    }
}