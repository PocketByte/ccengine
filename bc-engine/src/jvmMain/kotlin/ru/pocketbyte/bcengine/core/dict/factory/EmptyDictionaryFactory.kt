package ru.pocketbyte.bcengine.core.dict.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.MapDictionary

object EmptyDictionaryFactory : DictionaryFactory {
    override fun get(context: Context, project: ProjectConfig): Dictionary {
        return MapDictionary(emptyMap())
    }
}
