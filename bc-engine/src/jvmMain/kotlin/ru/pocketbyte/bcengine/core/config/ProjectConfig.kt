package ru.pocketbyte.bcengine.core.config

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.runtime.Composable
import ru.pocketbyte.bcengine.core.dataset.factory.DataSetFactory
import ru.pocketbyte.bcengine.core.dict.factory.DictionaryFactory
import ru.pocketbyte.bcengine.core.dict.factory.EmptyDictionaryFactory

class ProjectConfig(
    val name: String
) {
    var dataSet: DataSetFactory = dataSetStub

    var dictionary: DictionaryFactory = EmptyDictionaryFactory

    val output = ProjectOutputs()
    val layout = ProjectLayout()

    val view: @Composable BoxScope.() -> Unit = {
        if (views.isEmpty()) {
            throw NotImplementedError("Project view doesn't provided")
        }
        views.forEach {
            it.invoke(this)
        }
    }

    private var views: MutableList<@Composable BoxScope.() -> Unit> = mutableListOf()

    fun view(view: @Composable BoxScope.() -> Unit) {
        views.add(view)
    }

    inline fun output(configurator: ProjectOutputs.() -> Unit) {
        output.apply(configurator)
    }

    inline fun layout(configurator: ProjectLayout.() -> Unit) {
        layout.apply(configurator)
    }

    companion object {
        private val dataSetStub = DataSetFactory { _, _ ->
            throw IllegalStateException("Project Dataset not set!")
        }
    }
}