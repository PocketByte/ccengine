package ru.pocketbyte.bcengine.core.dataset.factory.extension

import ru.pocketbyte.bcengine.core.dataset.factory.DataSetFactory
import ru.pocketbyte.bcengine.core.dataset.factory.DataSetFilterFactory
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

fun DataSetFactory.filter(
    filter: (index: Int, item: DataSetItem) -> Boolean
): DataSetFactory {
    return DataSetFilterFactory(
        this,
        filter
    )
}