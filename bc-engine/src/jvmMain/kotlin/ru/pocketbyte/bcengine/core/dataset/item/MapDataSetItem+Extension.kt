package ru.pocketbyte.bcengine.core.dataset.item

internal fun Map<String, String>.asDataSetItem(): DataSetItem {
    return MapDataSetItem(this)
}
