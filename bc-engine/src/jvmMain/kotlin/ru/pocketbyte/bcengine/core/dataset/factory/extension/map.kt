package ru.pocketbyte.bcengine.core.dataset.factory.extension

import ru.pocketbyte.bcengine.core.dataset.factory.DataSetFactory
import ru.pocketbyte.bcengine.core.dataset.factory.DataSetMapFactory
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

fun DataSetFactory.map(transform: (DataSetItem) -> DataSetItem): DataSetFactory {
    return DataSetMapFactory(this, transform)
}
