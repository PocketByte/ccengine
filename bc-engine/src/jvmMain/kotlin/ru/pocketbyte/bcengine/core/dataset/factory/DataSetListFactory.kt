package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.DataSetList

class DataSetListFactory(
    private vararg val factories: DataSetFactory
): DataSetFactory {
    override fun get(context: Context, project: ProjectConfig): DataSet {
        return DataSetList(factories.map { it.get(context, project) })
    }
}