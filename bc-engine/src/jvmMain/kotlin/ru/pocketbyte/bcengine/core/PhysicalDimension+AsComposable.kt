package ru.pocketbyte.bcengine.core

import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.Dp
import ru.pocketbyte.bcengine.core.config.ProjectLayout

inline val Float.mm: Dp
    @Composable
    get() = Dp(
        ProjectLayout.projectDp(this, PhysicalDimension.MILLIMETER)
    )

inline val Int.mm: Dp
    @Composable
    get() = toFloat().mm

inline val Double.mm: Dp
    @Composable
    get() = toFloat().mm