package ru.pocketbyte.bcengine.core.dict.factory.csv

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.csv.AbsCSVDecoder
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.MapDictionary

class CSVDictionaryDecoder(
    private val keyColumn: String,
    private val valueColumn: String,
    private val valueFallbackColumn: String?
) : AbsCSVDecoder<Dictionary>() {

    override fun buildEmptyResult(): Dictionary {
        return MapDictionary(emptyMap())
    }

    override fun decode(
        context: Context,
        project: ProjectConfig,
        headers: List<String>,
        nextRow: () -> MutableMap<String, String>?,
    ): Dictionary {
        val map = mutableMapOf<String, String?>()

        assertFieldExists(keyColumn, headers)
        assertFieldExists(valueColumn, headers)

        var itemMap = nextRow()

        while (itemMap != null) {
            val key = itemMap[keyColumn]
            if (!key.isNullOrBlank()) {
                map[key] = itemMap[valueColumn]
                    ?.ifBlank { null }
                    ?: if (valueFallbackColumn != null) {
                        itemMap[valueFallbackColumn]
                    } else {
                        null
                    }
            }

            itemMap = nextRow()
        }

        return MapDictionary(map)
    }

    private fun assertFieldExists(field: String, headers: List<String>) {
        if (!headers.contains(field)) {
            throw RuntimeException(
                "Field $field doesn't exists."
            )
        }
    }
}
