package ru.pocketbyte.bcengine.core.dict.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dict.Dictionary

fun interface DictionaryFactory {
    fun get(context: Context, project: ProjectConfig): Dictionary
}