package ru.pocketbyte.bcengine.core.html

enum class HtmlAlign {
    Baseline,
    Top,
    Bottom,
    Center,
    TextTop,
    TextBottom,
    TextCenter;

    override fun toString(): String {
        return when(this) {
            Baseline -> "baseline"
            Top -> "top"
            Bottom -> "bottom"
            Center -> "center"
            TextTop -> "texttop"
            TextBottom -> "textbottom"
            TextCenter -> "textcenter"
        }
    }

    companion object {
        fun fromString(string: String): HtmlAlign {
            return when (string.lowercase()){
                "baseline" -> Baseline
                "top" -> Top
                "bottom" -> Bottom
                "center" -> Center
                "texttop" -> TextTop
                "textbottom" -> TextBottom
                "textcenter" -> TextCenter
                else -> Baseline
            }
        }
    }
}