package ru.pocketbyte.bcengine.core.dataset.item


//typealias DataSetItem = Map<String, String>

interface DataSetItem {
    fun get(key: String): String?
}