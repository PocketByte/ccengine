package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.filter
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

class DataSetFilterFactory(
    private val factory: DataSetFactory,
    private val filter: (index: Int, item: DataSetItem) -> Boolean
): DataSetFactory {

    override fun get(context: Context, project: ProjectConfig): DataSet {
        return factory.get(context, project).filter(filter)
    }
}
