package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.DataSetMerge

class DataSetMergeFactory(
    private val primaryDataSetFactory: DataSetFactory,
    private val secondaryDataSetFactory: DataSetFactory,
    private val indexKey: String
) : DataSetFactory {
    override fun get(context: Context, project: ProjectConfig): DataSet {
        return DataSetMerge(
            primaryDataSetFactory.get(context, project),
            secondaryDataSetFactory.get(context, project),
            indexKey
        )
    }
}