package ru.pocketbyte.bcengine.core.color

import androidx.compose.ui.graphics.Color
import kotlin.math.pow

object ColorCoder {

    private val hexDigits = charArrayOf(
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F'
    )

    val namedColors = mapOf(
        Pair("empty", Color.Transparent),
        Pair("transparent", Color.Transparent),
        Pair("black", Color.Black),
        Pair("white", Color.White),
        Pair("red", Color.Red),
        Pair("green", Color.Green),
        Pair("blue", Color.Blue)
    )

    fun decode(colorString: String): Color? {
        namedColors[colorString.lowercase()]?.let { return it }

        return decodeRGBA(colorString)
    }

    fun encode(color: Color): String {
        val builder = StringBuilder("#")
        val putColor: (it: Float) -> Unit = {
            val intValue = (it * 255f).toInt().coerceIn(0, 255)
            builder.append(hexDigits[(intValue and 0xf0) shr 4])
            builder.append(hexDigits[intValue and 0x0f])
        }
        color.alpha.run(putColor)
        color.red.run(putColor)
        color.green.run(putColor)
        color.blue.run(putColor)
        return builder.toString()
    }

    private fun decodeRGBA(hexString: String): Color? {
        val hex = hexString.uppercase()
        if (hex[0] != '#') {
            return null
        }
        return when (hex.length) {
            7 -> Color(
                hexToInt(hex, 1, 2) ?: return null,
                hexToInt(hex, 3, 4) ?: return null,
                hexToInt(hex, 5, 6) ?: return null
            )
            9 -> Color(
                hexToInt(hex, 3, 4) ?: return null,
                hexToInt(hex, 5, 6) ?: return null,
                hexToInt(hex, 7, 8) ?: return null,
                hexToInt(hex, 1, 2) ?: return null
            )
            else -> null
        }
    }

    private fun hexToInt(hexString: String, start: Int, end: Int): Int? {
        if (
            start < 0 || start > hexString.length ||
            end < 0 || end > hexString.length ||
            end < start
        ) {
            return null
        }
        var result = 0
        val len = end - start
        for (i in len downTo 0) {
            val value = hexDigits.indexOf(hexString[i + start])
            if (value == -1) {
                return null
            }
            result += value * 16.0.pow(len - i).toInt()
        }
        return result
    }
}
