package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.ListDataSet
import ru.pocketbyte.bcengine.core.dataset.forEachIndexed
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

class DataSetInflateFactory(
    private val factory: DataSetFactory,
    private val inflater: (index: Int, item: DataSetItem) -> Collection<DataSetItem>?
): DataSetFactory {

    override fun get(context: Context, project: ProjectConfig): DataSet {
        val dataSet = factory.get(context, project)

        val list = mutableListOf<DataSetItem>()

        dataSet.forEachIndexed { index, item ->
            inflater(index, item)
                ?.let { list.addAll(it) }
        }

        return ListDataSet(list)
    }
}
