package ru.pocketbyte.bcengine.core.dict

class DictionaryWrapper(
    private val dictionary: Dictionary
) : Dictionary {
    override fun get(key: String): String? {
        return dictionary.get(key)
    }
}
