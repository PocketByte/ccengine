package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.ListDataSet
import ru.pocketbyte.bcengine.core.dataset.item.asDataSetItem

class EmptyDataSetFactory(
    private val size: Int = 0
): DataSetFactory {

    override fun get(context: Context, project: ProjectConfig): DataSet {
        return ListDataSet(List(size) {
            mapOf(
                Pair(DataSet.ITEM_INDEX, it.toString()),
                Pair(DataSet.ITEM_SUB_INDEX, "0"),
                Pair(DataSet.ROW_INDEX, it.toString()),
                Pair(DataSet.AMOUNT, "1")
            ).asDataSetItem()
        })
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other is EmptyDataSetFactory) {
            return size == other.size
        }
        return false
    }

    override fun hashCode(): Int {
        return size.hashCode()
    }
}