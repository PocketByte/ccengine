package ru.pocketbyte.bcengine.core.config

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import ru.pocketbyte.bcengine.core.PhysicalDimension
import kotlin.math.max
import kotlin.math.roundToInt

class ProjectLayout {

    var dpi: Float = DEFAULT_DPI

    var backgroundColor: Color = Color.Transparent

    var width: Float = 0f
        private set

    var height: Float = 0f
        private set

    var dimension: PhysicalDimension = PhysicalDimension.MILLIMETER
        private set

    fun widthPx(): Int {
        return max(projectPx(width, dimension).roundToInt(), 1)
    }

    fun heightPx(): Int {
        return max(projectPx(height, dimension).roundToInt(), 1)
    }

    fun widthDp(): Dp {
        return max(projectDp(width, dimension).roundToInt(), 1).dp
    }

    fun heightDp(): Dp {
        return max(projectDp(height, dimension).roundToInt(), 1).dp
    }

    fun size(width: Float, height: Float, dimension: PhysicalDimension) {
        this.width = width
        this.height = height
        this.dimension = dimension
    }

    fun projectPx(value: Float, dimension: PhysicalDimension): Float {
        return projectPx(value, dimension, dpi)
    }

    companion object {
        const val DEFAULT_DPI = 96f
        const val INC_TO_MM = 1 / (10 * 2.54f)

        fun projectPx(value: Float, dimension: PhysicalDimension, dpi: Float): Float {
            return when (dimension) {
                PhysicalDimension.INCH -> value * dpi
                PhysicalDimension.MILLIMETER -> value * dpi * INC_TO_MM
                PhysicalDimension.CENTIMETER -> value * dpi * INC_TO_MM * 10f
            }
        }

        fun projectDp(value: Float, dimension: PhysicalDimension): Float {
            return when (dimension) {
                PhysicalDimension.INCH -> value * DEFAULT_DPI
                PhysicalDimension.MILLIMETER -> value * DEFAULT_DPI * INC_TO_MM
                PhysicalDimension.CENTIMETER -> value * DEFAULT_DPI * INC_TO_MM * 10f
            }
        }
    }
}