package ru.pocketbyte.bcengine.core.html

import androidx.compose.ui.graphics.Color
import ru.pocketbyte.bcengine.compose.htmldecoder.HTMLTagFont

fun htmlFont(
    content: String,
    face: String? = null,
    color: Color? = null
): String {
    @Suppress("CAST_NEVER_SUCCEEDS")
    return htmlFont(content, face, null as? String, color)
}

fun htmlFont(
    content: String,
    face: String? = null,
    size: Float? = null,
    color: Color? = null
): String {
    return htmlFont(content, face, size?.toString(), color)
}

fun htmlFont(
    content: String,
    face: String? = null,
    size: String? = null,
    color: Color? = null
): String {
    return htmlFontOpenTag(face, size, color) + content + htmlFontCloseTag()
}

private fun htmlFontOpenTag(
    face: String? = null,
    size: String? = null,
    color: Color? = null
): String {
    val builder = StringBuilder("<font")

    if (face != null) {
        builder.append(" ")
            .append(HTMLTagFont.face)
            .append("=\"")
            .append(face)
            .append("\"")
    }

    if (size != null) {
        builder.append(" ")
            .append(HTMLTagFont.size)
            .append("=")
            .append(size)
    }

    if (color != null) {
        builder.append(" ")
            .append(HTMLTagFont.color)
            .append("=")
            .append(htmlColor(color))
    }

    builder.append(">")

    return builder.toString()
}

private fun htmlFontCloseTag(): String = "</font>"
