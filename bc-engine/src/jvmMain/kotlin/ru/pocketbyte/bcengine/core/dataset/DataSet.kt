package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

interface DataSet {
    val size: Int
    fun get(index: Int): DataSetItem

    companion object {
        const val ITEM_INDEX = "ITEM_INDEX"
        const val ITEM_SUB_INDEX = "ITEM_SUB_INDEX"
        const val ROW_INDEX = "ROW_INDEX"
        const val AMOUNT = "AMOUNT"
    }
}