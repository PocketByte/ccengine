package ru.pocketbyte.bcengine.core.html

import ru.pocketbyte.bcengine.compose.htmldecoder.HTMLTagImage

fun htmlImg(
    image: String,
    width: String? = null,
    height: String? = null,
    align: HtmlAlign? = null,
    advanceWidth: Float? = null,
    advanceHeight: Float? = null,
    originX: Float? = null,
    originY: Float? = null
): String {
    return StringBuilder().apply {
        append("<img")
        append(" ${HTMLTagImage.src}=\"")
        append(image)
        append("\"")

        if (align != null) {
            append(" ${HTMLTagImage.align}=")
            append(align.toString())
        }

        append(" ${HTMLTagImage.width}=")
        append(width ?: "*1")

        append(" ${HTMLTagImage.height}=")
        append(height ?: "*1")

        if (advanceWidth != null) {
            append(" ${HTMLTagImage.advanceWidth}=")
            append(advanceWidth.toString())
        }

        if (advanceHeight != null) {
            append(" ${HTMLTagImage.advanceHeight}=")
            append(advanceHeight.toString())
        }

        if (originX != null) {
            append(" ${HTMLTagImage.originX}=")
            append(originX.toString())
        }

        if (originY != null) {
            append(" ${HTMLTagImage.originY}=")
            append(originY.toString())
        }
        append(">")
    }.toString()
}