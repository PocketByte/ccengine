package ru.pocketbyte.bcengine.core.dict

class DictionaryList(
    private val dictionaries: List<Dictionary>
) : Dictionary {
    override fun get(key: String): String? {
        dictionaries.forEach { dictionary ->
            dictionary.get(key)
                ?.let { return it }
        }
        return null
    }
}
