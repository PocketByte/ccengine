package ru.pocketbyte.bcengine.core.dict.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.DictionaryList

class DictionaryListFactory(
    private vararg val factories: DictionaryFactory
) : DictionaryFactory {
    override fun get(context: Context, project: ProjectConfig): Dictionary {
        return DictionaryList(factories.map { it.get(context, project) })
    }
}
