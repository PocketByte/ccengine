package ru.pocketbyte.bcengine.core.dict.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.factory.CSVDataSetFactory
import ru.pocketbyte.bcengine.core.dict.Dictionary
import ru.pocketbyte.bcengine.core.dict.factory.csv.CSVDictionaryDecoder
import ru.pocketbyte.bcengine.utils.InputStringIterator
import ru.pocketbyte.bcengine.utils.from
import ru.pocketbyte.kydra.log.info

class CSVDictionaryFactory(
    private val path: String,
    keyColumn: String = DEFAULT_KEY_COLUMN,
    valueColumn: String = DEFAULT_VALUE_COLUMN,
    valueFallbackColumn: String? = null
) : DictionaryFactory {

    companion object {
        private const val DEFAULT_KEY_COLUMN = "key"
        private const val DEFAULT_VALUE_COLUMN = "value"
    }

    private val decoder = CSVDictionaryDecoder(keyColumn, valueColumn, valueFallbackColumn)

    override fun get(context: Context, project: ProjectConfig): Dictionary {
        context.logger.info { "Getting CSV Dictionary from: $path" }

        return decoder.decode(
            context, project,
            InputStringIterator.from(path)
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other is CSVDataSetFactory) {
            return path == other.path
        }
        return false
    }

    override fun hashCode(): Int {
        return path.hashCode()
    }
}
