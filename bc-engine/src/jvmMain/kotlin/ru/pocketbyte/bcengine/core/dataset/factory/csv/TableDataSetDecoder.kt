package ru.pocketbyte.bcengine.core.dataset.factory.csv

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet
import ru.pocketbyte.bcengine.core.dataset.ListDataSet
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.core.dataset.item.asDataSetItem
import ru.pocketbyte.kydra.log.warn

object TableDataSetDecoder {

    fun decode(
        context: Context,
        project: ProjectConfig,
        headers: List<String>,
        nextRow: () -> MutableMap<String, String>?,
    ): DataSet {
        checkFieldOverride(context, DataSet.ROW_INDEX, headers)
        checkFieldOverride(context, DataSet.ITEM_INDEX, headers)
        checkFieldOverride(context, DataSet.ITEM_SUB_INDEX, headers)

        val list = mutableListOf<DataSetItem>()
        var rowIndex = 0

        var itemMap = nextRow()
        while (itemMap != null) {
            if (headers.contains(DataSet.ROW_INDEX).not()) {
                itemMap[DataSet.ROW_INDEX] = rowIndex.toString()
            }
            if (headers.contains(DataSet.ITEM_INDEX).not()) {
                itemMap[DataSet.ITEM_INDEX] = rowIndex.toString()
            }
            if (headers.contains(DataSet.ITEM_SUB_INDEX).not()) {
                itemMap[DataSet.ITEM_SUB_INDEX] = "0"
            }
            if (headers.contains(DataSet.AMOUNT).not()) {
                itemMap[DataSet.AMOUNT] = "1"
            }

            list.add(itemMap.asDataSetItem())
            rowIndex++

            itemMap = nextRow()
        }
        return ListDataSet(list)
    }

    private fun checkFieldOverride(context: Context, field: String, headers: List<String>) {
        if (headers.contains(field)) {
            context.logger.warn("Warning: $field filed overriden by data set column")
        }
    }
}
