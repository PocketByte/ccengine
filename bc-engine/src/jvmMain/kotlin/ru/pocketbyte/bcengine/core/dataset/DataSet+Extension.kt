package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem

inline fun DataSet.forEachIndexed(action: (index: Int, item: DataSetItem) -> Unit) {
    for (index in 0 until size) {
        action(index, get(index))
    }
}

inline fun DataSet.forEach(action: (item: DataSetItem) -> Unit) {
    for (index in 0 until size) {
        action(get(index))
    }
}

fun DataSet.getOrNull(index: Int): DataSetItem? {
    if (index in 0 until size) {
        return get(index)
    }
    return null
}

fun DataSet.filter(filter: (index: Int, item: DataSetItem) -> Boolean): DataSet {
    val list = mutableListOf<DataSetItem>()

    forEachIndexed { index, item ->
        if (filter(index, item)) {
            list.add(item)
        }
    }

    return ListDataSet(list)
}
