package ru.pocketbyte.bcengine.core.dataset

import ru.pocketbyte.bcengine.core.dataset.item.DataSetItem
import ru.pocketbyte.bcengine.core.dataset.item.DataSetItemMerge

class DataSetMerge(
    private val primaryDataSet: DataSet,
    private val secondaryDataSet: DataSet,
    private val indexKey: String
) : DataSet {
    override val size: Int
        get() = primaryDataSet.size

    override fun get(index: Int): DataSetItem {
        val primaryItem = primaryDataSet.get(index)
        val keyValue = primaryItem.get(indexKey)
            ?: throw RuntimeException("Missing index key '$indexKey'!")
        val secondaryItem = lookupForSecondaryItem(keyValue, index)
            ?: throw RuntimeException("Missing secondary dataset item for key '$indexKey'!")
        return DataSetItemMerge(primaryItem, secondaryItem)
    }

    private fun lookupForSecondaryItem(keyValue: String, index: Int): DataSetItem? {
        val item = secondaryDataSet.get(index)
        if (item.get(indexKey) == keyValue) {
            return item
        }
        return lookupForSecondaryItem(keyValue, index, offset = 1)
    }

    private fun lookupForSecondaryItem(keyValue: String, index: Int, offset: Int): DataSetItem? {
        val nextIndex = index + offset
        if (nextIndex < secondaryDataSet.size) {
            val item = secondaryDataSet.get(nextIndex)
            if (item.get(indexKey) == keyValue) {
                return item
            }
        }

        val previousIndex = index - offset
        if (previousIndex >= 0) {
            val item = secondaryDataSet.get(previousIndex)
            if (item.get(indexKey) == keyValue) {
                return item
            }
        }

        if (nextIndex >= secondaryDataSet.size && previousIndex < 0) {
            return null
        }

        return lookupForSecondaryItem(keyValue, index, offset + 1)
    }
}
