package ru.pocketbyte.bcengine.core.dataset.factory

import ru.pocketbyte.bcengine.core.Context
import ru.pocketbyte.bcengine.core.config.ProjectConfig
import ru.pocketbyte.bcengine.core.dataset.DataSet

fun interface DataSetFactory {
    fun get(context: Context, project: ProjectConfig): DataSet
}