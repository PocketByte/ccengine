package ru.pocketbyte.bcengine.core.dataset.item

data class MapDataSetItem(
    private val map: Map<String, String>
): DataSetItem {

    constructor(vararg pairs: Pair<String, String>) : this(mapOf(*pairs))

    override fun get(key: String): String? {
        return map[key]
    }
}