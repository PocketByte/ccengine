import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("multiplatform")
    id("org.jetbrains.compose").version("1.5.10")
}

group = "ru.pocketbyte.bcengine"
version = "1.0-SNAPSHOT"

kotlin {
    jvm {
        withJava()
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(libsBce.kotlin.coroutines.core)
                implementation(libsBce.ktor.client.core)
                implementation(libsBce.ktor.client.cio)
                implementation(libsBce.kydra.log)
            }
        }
        val jvmMain by getting {
//            kotlin.srcDir("./build/generated/src/desktopMain/kotlin/")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib")
                implementation(compose.desktop.currentOs)
                implementation(libsBce.kotlin.coroutines.coreJvm)
                api(libsBce.jvm.pdfbox)
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "ru.pocketbyte.bcengine.MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "app-editor"
            packageVersion = "1.0.0"
        }
    }
}